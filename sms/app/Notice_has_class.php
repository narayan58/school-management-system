<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice_has_class extends Model {

     //notice controller // notice view
	public function getClass()
    {
        return $this->belongsTo('App\Tclass','class_id');
    }
    public function getUser()
    {
        return $this->belongsTo('App\User','created_by');
    }
    public function getNotice()
    {
        return $this->hasMany('App\Notice','id','notice_id');
    }


}
