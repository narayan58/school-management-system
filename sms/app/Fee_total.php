<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Fee_total extends Model {

	public function studentfeedetail()
    {
        return $this->hasMany('App\Fee','bill_id','bill_id');
    }

    public function studentfeename()
    {
        return $this->belongsTo('App\User','student_id');
    }

    public function studentfeecreate()
    {
        return $this->belongsTo('App\User','created_by');
    }
    public function studentfeebillSn()
    {
        return $this->belongsTo('App\Fee_bill','bill_id','bill_id');
    }
    public function getStudentInfo()
    {
        return $this->belongsTo('App\Student_has_info','student_id','user_id');
    }

}
