<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher_has_first extends Model {

	public function getTeacherFirstSubject()
    {
        return $this->belongsTo('App\Subject','subject_id');
    }

    public function getTeacherFirstCls()
    {
        return $this->belongsTo('App\Tclass','class_id');
    }
    public function getTeacherFirstSection()
    {
        return $this->belongsTo('App\Section','section_id');
    }
    public function getTeacherFirstShift()
    {
        return $this->belongsTo('App\Shift','shift_id');
    }

}
