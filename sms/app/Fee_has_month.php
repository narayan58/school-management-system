<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Fee_has_month extends Model {

	public function getMonth()
    {
        return $this->hasMany('App\Month','id','month_id');
    }

}
