<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model {

    //note controller // not view
	public function getSubject()
    {
        return $this->belongsTo('App\Subject','subject_id');
    }
    public function getClass()
    {
        return $this->belongsTo('App\Tclass','class_id');
    }
     public function getUser()
    {
        return $this->belongsTo('App\User','created_by');
    }

}
