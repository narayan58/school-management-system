<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model {

	public function subjectclass()
    {
        return $this->belongsTo('App\Tclass','class_id');
    }

}
