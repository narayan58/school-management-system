<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model {

	public function getSubjectNumber()
    {
        return $this->belongsTo('App\Subject','subject_id');
    }

    public function getStudentMonth()
    {
        return $this->belongsTo('App\Fee_has_month','id','fee_id');
    }
    public function studentfeename()
    {
        return $this->belongsTo('App\User','student_id');
    }

}
