<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Class_has_exam extends Model {

	public function getExamClass()
    {
        return $this->belongsTo('App\Tclass','class_id','id');
    }
    public function getExam()
    {
        return $this->belongsTo('App\Exam','exam_id','id');
    }
    public function getExamClassDetail()
    {
        return $this->belongsTo('App\Tclass','class_id','id');
    }

}
