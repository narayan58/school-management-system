<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherRecord extends Model {

	public function getTopic()
    {
        return $this->belongsTo('App\Topic','topic_id');
    }

    public function getSubTopic()
    {
        return $this->belongsTo('App\SubTopic','subtopic_id');
    }

    public function getSection()
    {
        return $this->belongsTo('App\Section','section_id');
    }

    public function getShift()
    {
        return $this->belongsTo('App\Shift','shift_id');
    }

    public function getPeriod()
    {
        return $this->belongsTo('App\Period','period_id');
    }
    public function getSubject()
    {
        return $this->belongsTo('App\Subject','subject_id');
    }
    public function getClass()
    {
        return $this->belongsTo('App\Tclass','class_id');
    }
    public function getTeacher()
    {
        return $this->belongsTo('App\User','teacher_id');
    }
    public function getUser()
    {
        return $this->belongsTo('App\User','created_by');
    }
    
}
