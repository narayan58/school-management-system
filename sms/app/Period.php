<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Period extends Model {

	public function getUser()
    {
        return $this->belongsTo('App\User','created_by');
    }

}
