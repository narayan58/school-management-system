<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeStructure extends Model {

	public function getClassName()
    {
        return $this->belongsTo('App\Tclass','class_id');
    }

     public function getUser()
    {
        return $this->belongsTo('App\User','created_by');
    }

}
