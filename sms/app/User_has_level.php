<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class User_has_level extends Model {

	public function getUserLevel()
    {
         return $this->belongsTo('App\User', 'user_id');
    }

}
