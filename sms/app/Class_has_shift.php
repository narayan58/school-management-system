<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Class_has_shift extends Model {

	public function getShiftClass()
    {
        return $this->belongsTo('App\Shift','shift_id');
    }

}
