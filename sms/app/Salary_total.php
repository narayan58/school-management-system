<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary_total extends Model {

	public function teacherfeedetail()
    {
        return $this->hasMany('App\Salary','bill_id','bill_id');
    }

    public function teacherfeename()
    {
        return $this->belongsTo('App\User','teacher_id');
    }

    public function teacherfeecreate()
    {
        return $this->belongsTo('App\User','created_by');
    }
    public function teacherfeebillSn()
    {
        return $this->belongsTo('App\Salary_bill','bill_id','bill_id');
    }
    public function getMonth()
    {
        return $this->belongsTo('App\Month','date');
    }
    public function getTeacherInfo()
    {
        return $this->belongsTo('App\Teacher_has_info','teacher_id','user_id');
    }

}
