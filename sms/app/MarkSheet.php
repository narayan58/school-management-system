<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MarkSheet extends Model {

	public function getStudentSubMark()
    {
        return $this->hasMany('App\ExamClass','id','marks_id');
    }

    public function getSubjectMarksheet()
    {
        return $this->belongsTo('App\Subject','subject_id');
    }

    public function getStudentMarksheet()
    {
        return $this->belongsTo('App\User','student_id');
    }
    public function getStudentExam()
    {
        return $this->belongsTo('App\Exam','exam_id');
    }
    public function getUser()
    {
        return $this->belongsTo('App\User','created_by');
    }
    public function getStudentClass()
    {
        return $this->belongsTo('App\Tclass','class_id');
    }
    public function getStudentSection()
    {
        return $this->belongsTo('App\Section','section_id');
    }
    public function getStudentShift()
    {
        return $this->belongsTo('App\Shift','shift_id');
    }
    public function getStudentGrade()
    {
        return $this->hasMany('App\Mark_has_grade','mark_sheet_id');
    }

}
