<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Mark_has_grade extends Model {

	public function getStudentMarkGrade()
    {
        return $this->belongsTo('App\Grade','grade_id');
    }

}
