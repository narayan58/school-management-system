<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Student_has_info extends Model {

	public function studentclass()
    {
        return $this->belongsTo('App\Tclass','class_id');
    }
    public function studentshift()
    {
        return $this->belongsTo('App\Shift','shift_id');
    }
    public function studentsection()
    {
        return $this->belongsTo('App\Section','section_id');
    }
    public function studentinfo()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function getUser()
    {
        return $this->belongsTo('App\User','created_by');
    }
}
