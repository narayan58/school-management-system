<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamClass extends Model {

	public function getSubjectClass()
    {
        return $this->belongsTo('App\Subject','subject_id');
    }

}
