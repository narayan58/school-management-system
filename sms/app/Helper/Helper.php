<?php 

namespace App\Helper;

class Helper{
	function slug_converter($string){
        $str = strtolower($string);
        $split = explode(' ',$str);
        $slug = implode('-',$split);
        return $slug;
    }
	public static function imageResize($file_path, $filename, $extension, $width, $height,$filecreatename){
			// Generate filename
			// Read RAW data
			$imageDetail = getimagesize($file_path);
			$thumb = imagecreatetruecolor($width, $height);
			$source = null;
			switch ($extension) {
				case 'png':
				$source = imagecreatefrompng($file_path);
					break;
				case 'PNG':
				$source = imagecreatefrompng($file_path);
					break;
				case 'jpg':
				$source = imagecreatefromjpeg($file_path);
					break;
				case 'JPG':
				$source = imagecreatefromjpeg($file_path);
					break;
				case 'jpeg':
				$source = imagecreatefromjpeg($file_path);
					break;
				case 'JPEG':
				$source = imagecreatefromjpeg($file_path);
					break;
				
				default:
					# code...
					break;
			}
			imagecopyresized($thumb, $source, 0, 0, 0, 0, $width, $height, $imageDetail[0], $imageDetail[1]);
			$thumbPath = 'image/thumb/'.$filecreatename;
			if (!file_exists($thumbPath)) {
			    mkdir($thumbPath, 0777, true);
			}
			switch ($extension) {
				case 'png':
				imagepng($thumb,$thumbPath.'/'.$filename);
					break;
				case 'PNG':
				imagepng($thumb,$thumbPath.'/'.$filename);
					break;
				case 'jpg':
				imagejpeg($thumb,$thumbPath.'/'.$filename);
					break;
				case 'JPG':
				imagejpeg($thumb,$thumbPath.'/'.$filename);
					break;
				case 'jpeg':
				imagejpeg($thumb,$thumbPath.'/'.$filename);
					break;
				case 'JPEG':
				imagejpeg($thumb,$thumbPath.'/'.$filename);
					break;
				default:
					# code...
					break;
			}
	}
}