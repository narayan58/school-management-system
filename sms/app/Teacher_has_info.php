<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher_has_info extends Model {

    public function getFirstClass()
    {
        return $this->belongsTo('App\Subject','first_subject_id');
    }

    public function getAllSubject()
    {
        return $this->belongsTo('App\Subject','subject_id');
    }
    

}
