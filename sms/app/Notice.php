<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model {
	//note controller // not view
	public function getNotice()
    {
        return $this->hasMany('App\Notice_has_class','notice_id','id');
    }
    public function getUser()
    {
        return $this->belongsTo('App\User','created_by');
    }
    public function getClass()
    {
        return $this->belongsTo('App\Tclass','class_id');
    }

	
}
