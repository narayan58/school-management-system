<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model {

	public function getUser()
    {
        return $this->belongsTo('App\User','created_by');
    }

}
