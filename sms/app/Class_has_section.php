<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Class_has_section extends Model {

	public function getSectionClass()
    {
        return $this->belongsTo('App\Section','section_id');
    }

}
