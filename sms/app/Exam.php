<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model {

	public function getExamDetail()
    {
        return $this->hasMany('App\Class_has_exam','exam_id');
    }
    public function getUser()
    {
        return $this->belongsTo('App\User','created_by');
    }
}
