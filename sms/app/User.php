<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function user()
    {
         return $this->belongsTo('App\User', 'created_by');
    }
    public function studentclass()
    {
        return $this->hasMany('App\Tclass','class_id');
    }
    public function studentshift()
    {
        return $this->hasMany('App\Shift','shift_id');
    }
    public function studentsection()
    {
        return $this->hasMany('App\Section','section_id');
    }

    public function getTeacherDetail()
    {
        return $this->belongsTo('App\Teacher_has_info','id','user_id');
    }
    // get teacher first class
    public function getTeacherFirstClass()
    {
        return $this->belongsTo('App\Teacher_has_first','id','teacher_id');
    }
    public function getTeacherInfo()
    {
        return $this->hasMany('App\Teacher_has_info','user_id','id');
    }
    public function getUserLevel()
    {
        return $this->belongsTo('App\User_has_level','id','user_id');

    }
     public function getUser()
    {
        return $this->belongsTo('App\User','created_by');
    }
    public function getStudentDetail()
    {
        return $this->belongsTo('App\Student_has_info','user_id');
    }
    public function studenthasclass()
    {
        return $this->hasOne('App\Student_has_info');
    }

}
