<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model {

	public function getStudent()
    {
        return $this->belongsTo('App\User','student_id','id');
    }

}
