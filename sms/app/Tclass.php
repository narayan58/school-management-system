<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tclass extends Model {
	public function getSectionDetail()
    {
        return $this->hasMany('App\Class_has_section','class_id');
    }
	public function getShiftDetail()
    {
        return $this->hasMany('App\Class_has_shift','class_id');
    }
    public function getUser()
    {
        return $this->belongsTo('App\User','created_by');
    }
}
