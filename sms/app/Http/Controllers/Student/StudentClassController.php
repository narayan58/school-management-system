<?php namespace App\Http\Controllers\Student;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\Tclass;
use App\Subject;
use App\Student;
use App\Exam;
use App\Class_has_exam;
use App\Class_has_section;
use App\Class_has_shift;
use App\Student_has_info;
use App\TeacherRecord;
use DB;

class StudentClassController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user_id = Auth::user()->id;
		$user_class=Student_has_info::where('user_id',$user_id)->firstOrFail();
		$real_class=$user_class->class_id;
		$section_id=$user_class->section_id;
		$shift_id=$user_class->shift_id;
		$student_class_info=TeacherRecord::where('class_id',$real_class)
							->where('shift_id',$shift_id)
							->where('section_id',$section_id)
			                ->orderBy('period_id', 'ASC')//updated with current time 
							->get();
		//for heading
		$student_details=Student_has_info::where('user_id',$user_id)->firstOrFail();
			$class_name= $student_details->studentclass->name; 
			$class_section= $student_details->studentsection->name; 
			$class_shift= $student_details->studentshift->name; 
		return view('student.student_class_info', compact(['student_class_info','exam_details','section_details','exam_shift','class_section','class_shift','class_name']));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
