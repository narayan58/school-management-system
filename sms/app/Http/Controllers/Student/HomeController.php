<?php namespace App\Http\Controllers\Student;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Validator;

use App\User;
use App\Category;
use Auth;
use DB;
use App\Helper\Helper;
use App\Student;
use App\Tclass;
use App\Student_has_info;
use App\Class_has_section;
use App\Class_has_shift;
use App\Section;
use App\Shift;

class HomeController extends Controller {



	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$students = User::where('confirmed','=', True)
					->where('is_admin', '5')
					->where('id', $user_id)
			        ->orderBy('id', 'DESC')//updated with current time 
					->get();
		return view('student.home', compact(['students']));	
		}
	}

	public function store()
	{
		$rules = array(
            'name' => 'required',
            'address' => 'required',
            'roll_no' => 'required|unique:users,email',
            'date_of_birth' => 'required',
            'telephone_no' => 'required',
            'mobile_no' => 'required',
            'image' => 'required',
            'class_id' => 'required',
            'section_id' => 'required',
            'shift_id' => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
    if (Auth::check()) {
		$user_id = Auth::user()->id;
		$students = new User;
		$image = Input::file('image');
		$destinationPath = 'image/student'; // upload path
		if (!file_exists($destinationPath)) {
			    mkdir($destinationPath, 0777, true);
			}
		$extension = $image->getClientOriginalExtension(); // getting image extension
		$fileName = md5(mt_rand()).'.'.$extension; // renameing image
		$image->move($destinationPath, $fileName);
		$file_path = $destinationPath.'/'.$fileName;
		$students->image_encrypt = $fileName;
		$students->image = $image->getClientOriginalName();
		$mid = 'student';
		// $student->mime = $image->getClientMimeType();
			$students->name = (Input::get('name'));
			$students->address = (Input::get('address'));
			$students->email = (Input::get('roll_no'));
			$students->password = bcrypt(Input::get('date_of_birth'));
			$students->gender = 'M';
			$students->dob = (Input::get('date_of_birth'));
			$students->telephone_no = (Input::get('telephone_no'));
			$students->mobile_no = (Input::get('mobile_no'));
			$students->confirmation_code = 's';
			$students->confirmed = '1';
			$students->is_admin = '5';
			$students->created_by = $user_id;
			$this->helper->imageResize($file_path, $fileName, $extension, $width=400, $height=300,$mid );
			if ($students->save()){
				$student_has_infos = new Student_has_info;
				$student_has_infos->class_id = (Input::get('class_id'));
				$student_has_infos->section_id = (Input::get('section_id'));
				$student_has_infos->shift_id = (Input::get('shift_id'));
				$student_has_infos->user_id = $students->id;
				$student_has_infos->is_active = '1';
				$student_has_infos->created_by = $user_id;
				$student_has_infos->save();
				$this->request->session()->flash('alert-success', 'Student saved successfully!');
			}
			else{
				$this->request->session()->flash('alert-warning', 'Student could not add!');
			}
			
		return back()->withInput();
		}
	}


}