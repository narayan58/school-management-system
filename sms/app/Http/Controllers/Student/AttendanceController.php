<?php namespace App\Http\Controllers\Student;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\Tclass;
use App\Subject;
use App\Student;
use App\Exam;
use App\Class_has_exam;
use App\Class_has_section;
use App\Class_has_shift;
use App\Student_has_info;
use App\Attendance;
use App\Fee_total;
use DB;

class AttendanceController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
		$user_id = Auth::user()->id;
		$students_details=Student_has_info::where('user_id',$user_id)->firstOrFail();
			$class_name= $students_details->studentclass->name; 
			$class_section= $students_details->studentsection->name; 
			$class_shift= $students_details->studentshift->name;
		$student_attendances = Attendance::where('student_id', $user_id)->get();
		return view('student.attendance', compact(['class_name','class_section','class_shift']))->with('mydata', json_decode($student_attendances, true));
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$fees_date=['created_at'=>$id];
	        try{
				 $current_fee_date = Fee_total::where($fees_date)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
			$bill_id=$current_fee_date['bill_id'];
			$message = "Copied bill";
			$student_details = Fee_total::where('bill_id',$bill_id)->get();
			$students_details=Student_has_info::where('user_id',$user_id)->firstOrFail();
				$class_name= $students_details->studentclass->name; 
				$class_section= $students_details->studentsection->name; 
				$class_shift= $students_details->studentshift->name; 
			return view('student.bill_print',compact(['message','student_details','class_name','class_section','class_shift']));
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
