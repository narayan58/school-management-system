<?php namespace App\Http\Controllers\Student;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\Tclass;
use App\Subject;
use App\Student;
use App\Exam;
use App\Class_has_exam;
use App\Class_has_section;
use App\Class_has_shift;
use App\Student_has_info;
use App\Student_login;
use DB;

class StudentExamController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user_id = Auth::user()->id;
		$user_class=Student_has_info::where('user_id',$user_id)->firstOrFail();
		$real_class=$user_class->class_id; 
		$exam_details=Class_has_exam::where('class_id',$real_class)
			->orderBy('id', 'DESC')//updated with current time 
		    ->get();
		$students_details=Student_has_info::where('user_id',$user_id)->firstOrFail();
			$class_name= $students_details->studentclass->name; 
			$class_section= $students_details->studentsection->name; 
			$class_shift= $students_details->studentshift->name; 
		return view('student.student_exam', compact(['exam_details','section_details','exam_shift','class_name','class_section','class_shift']));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
