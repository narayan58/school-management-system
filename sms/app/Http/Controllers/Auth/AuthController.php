<?php namespace App\Http\Controllers\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

use App\User;
use App\User_attempt;
use Redirect;

use Auth;
use Input;
use App\Helper\Helper;
use Mail;


class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar, Request $request)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->request = $request;

	}

		//redirect after success login
	public function redirectPath()
	{
		//overwrite from authenticateandregisteruser.php
	    if (\Auth::user()->is_admin == '1') {
	        return '/home';
	    }
	    elseif (\Auth::user()->is_admin == '6') {
	        return '/teacher';
	    }
	    elseif (\Auth::user()->is_admin == '5') {
	        return '/student';
	    }
	    elseif (\Auth::user()->is_user == True) {
	        return '/';
	    }
	    return '/';
	}
	// to get ip and os

	public function myip()
	{
	 if (isset($_SERVER)){
		if(isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
		$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		if(strpos($ip,",")){
		$exp_ip = explode(",",$ip);
		$ip = $exp_ip[0];
		}
		}else if(isset($_SERVER["HTTP_CLIENT_IP"])){
		$ip = $_SERVER["HTTP_CLIENT_IP"];
		}else{
		$ip = $_SERVER["REMOTE_ADDR"];
		}
		}else{
		if(getenv('HTTP_X_FORWARDED_FOR')){
		$ip = getenv('HTTP_X_FORWARDED_FOR');
		if(strpos($ip,",")){
		$exp_ip=explode(",",$ip);
		$ip = $exp_ip[0];
		}
		}else if(getenv('HTTP_CLIENT_IP')){
		$ip = getenv('HTTP_CLIENT_IP');
		}else {
		$ip = getenv('REMOTE_ADDR');
		}
		}
		return $ip; 
	}

	public function os_info()
	{
		$uagent = $_SERVER['HTTP_USER_AGENT'] . "<br/>";
	    // the order of this array is important
	    global $uagent;
	    $oses   = array(
	        'Win311' => 'Win16',
	        'Win95' => '(Windows 95)|(Win95)|(Windows_95)',
	        'WinME' => '(Windows 98)|(Win 9x 4.90)|(Windows ME)',
	        'Win98' => '(Windows 98)|(Win98)',
	        'Win2000' => '(Windows NT 5.0)|(Windows 2000)',
	        'WinXP' => '(Windows NT 5.1)|(Windows XP)',
	        'WinServer2003' => '(Windows NT 5.2)',
	        'WinVista' => '(Windows NT 6.0)',
	        'Windows 7' => '(Windows NT 6.1)',
	        'Windows 8' => '(Windows NT 6.2)',
	        'WinNT' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
	        'OpenBSD' => 'OpenBSD',
	        'SunOS' => 'SunOS',
	        'Ubuntu' => 'Ubuntu',
	        'Android' => 'Android',
	        'Linux' => '(Linux)|(X11)',
	        'iPhone' => 'iPhone',
	        'iPad' => 'iPad',
	        'MacOS' => '(Mac_PowerPC)|(Macintosh)',
	        'QNX' => 'QNX',
	        'BeOS' => 'BeOS',
	        'OS2' => 'OS/2',
	        'SearchBot' => '(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves/Teoma)|(ia_archiver)'
	    );
	    $uagent = strtolower($uagent ? $uagent : $_SERVER['HTTP_USER_AGENT']);
	    foreach ($oses as $os => $pattern)
	        if (preg_match('/' . $pattern . '/i', $uagent))
	            return $os;
	    return 'Unknown';
	}
	public function myMac(){
	    ob_start();
	    system('getmac');
	    $Content = ob_get_contents();
	    ob_clean();
	    return substr($Content, strpos($Content,'\\')-20, 17);
	}

	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'email' => 'required', 'password' => 'required',
		]);
			$MyipAddress = $this->myip();
			$os = $this->os_info();
			$mac = $this->myMac();
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("m/d/Y H:i:s");

		if ($this->auth->attempt(['confirmed' => True,'email'=> $request->email,'password'=>$request->password]))
		{
			$user_attempts = User_attempt::where('os', $os)
							->where('ip', $MyipAddress)
							->where('mac', $mac)
							->where('email', $request->email)
							->get();
			$user_try_attempts = User_attempt::where('os', $os)
							->where('ip', $MyipAddress)
							->where('mac', $mac)
							->where('email', $request->email)
							->where('attempt_try', '1')
							->get();
				// var_dump(count($user_attempts)); die();
			if((count($user_attempts) >= '0')){
				// var_dump("1"); die();
				if(count($user_try_attempts) >= '3'){
					// var_dump("2"); die();
					foreach ($user_try_attempts as $uattempt) {
						$getCurtime = $uattempt->created_at;
					}
					// var_dump(count($user_try_attempts)); die();
					$rext_dur=date('m/d/Y H:i:s', strtotime($getCurtime. ' + 15 minutes'));
					// var_dump($rext_dur, $current_date); die();
					if((count($user_try_attempts) >= '3') && ($rext_dur >= $current_date)){
						var_dump("Yout account suspended for 15 minutes"); die();
					}
				}
				// var_dump("success"); die();
				User_attempt::where('os', $os)
							->where('ip', $MyipAddress)
							->where('email', $request->email)
							->where('mac', $mac)
							->where('attempt_try', '1')
							->update(['attempt_try'=>'0']);
				
				$last_login_time = Auth::user()
    							->where('email', '=', $request->email)
    							->select('last_login','id','created_at')
    							->get();
				Auth::user()->last_login = $current_date;
				$user_id = Auth::user()->id = $last_login_time[0]['id'];
	    		Auth::user()->last_login_pre = $last_login_time[0]['last_login'];
	    		$created_date=Auth::user()->created_at = $last_login_time[0]['created_at'];
				$exp_date=date('m/d/Y H:i:s', strtotime($created_date. ' + 100 days'));
				// exp date year change ma kaam gareko chhaina
				// var_dump($exp_date, $current_date);

	    		if($exp_date < $current_date){
	    			// var_dump("expression"); die();
	    			$data['user_name'] = 'Illegal User';
					Mail::send(['html' =>'emails.contact_request'], ['data' =>$data], function($message)
					{
					$message->to('kapeel4@gmail.com', 'School Management System')->subject('This is an automated mail from school mgmt dharan on illegal use.');
					$message->from(('illegaluser@gmail.com'), 'Feedback mail');
					});
	    			return redirect($this->loginPath())
						->withInput($request->only('email', 'remember'))
						->withErrors([
							'email' => $this->getFailedLoginMessage(),
						]);
	    		}
	    		else{
	    			// var_dump("go"); die();
		    		Auth::user()->save();
					return redirect()->intended($this->redirectPath());
				}
			}
			elseif(count($user_try_attempts) == '1'){
				var_dump("faile");
				// 9817363825 
			}
			else{

			}
			
		}
		else{
			$user_attempts_before = new User_attempt;
			$try_email = $request->email;
			// $try_password = $request->password;
			$user_attempts_before->email = $try_email;
			$user_attempts_before->password = "currentdisable";
			$user_attempts_before->ip = $MyipAddress;
			$user_attempts_before->os = $os;
			$user_attempts_before->mac = $mac;
			$user_attempts_before->attempt_try = '1';
			$user_attempts_before->created_at = strtotime($current_date);

			$user_attempts_before->save();
			$user_attempts = User_attempt::where('os', $os)
							->where('ip', $MyipAddress)
							->where('mac', $mac)
							->where('email', $request->email)
							->get();
			$user_try_attempts = User_attempt::where('os', $os)
							->where('ip', $MyipAddress)
							->where('mac', $mac)
							->where('email', $request->email)
							->where('attempt_try', '1')
							->get();
				// var_dump(count($user_attempts)); die();
			if((count($user_attempts) >= '0')){
				// var_dump("1"); die();
				if(count($user_try_attempts) >= '3'){
					// var_dump("2"); die();
					foreach ($user_try_attempts as $uattempt) {
						$getCurtime = $uattempt->created_at;
					}
					// var_dump(count($user_try_attempts)); die();
					$rext_dur=date('m/d/Y H:i:s', strtotime($getCurtime. ' + 15 minutes'));
					// var_dump($rext_dur, $current_date); die();
					if((count($user_try_attempts) >= '3') && ($rext_dur >= $current_date)){
						var_dump("Yout account suspended for 15 minutes"); die();
					}
				}
		}
		}

		return redirect($this->loginPath())
					->withInput($request->only('email', 'remember'))
					->withErrors([
						'email' => $this->getFailedLoginMessage(),
					]);
	}

	public function getLogout()
	{
		$this->auth->logout();
		return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
	}
	public function getRegister()
	{
	    return redirect('login');
	}

}
