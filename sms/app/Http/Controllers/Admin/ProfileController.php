<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Input;
use Redirect;
use Response;
use Auth;
use App\Tclass;
use App\Section;
use App\Shift;
use App\Class_has_shift;
use App\Class_has_section;
use App\User_has_level;
use App\Helper\Helper;
use App\User;
use Hash;
use Validator;

class ProfileController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
				return view('admin.profile', compact(['user_levels']));	
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$class = Tclass::with('getSectionDetail')->find($id);
		return Response::json($class);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|same:new_password',
        );

        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$user_p_id = User::find($user_id);
			$curr_user_pass = $user_p_id['password'];
			$old_password = Input::get('old_password');
			$new_password = Input::get('new_password');
			if (Hash::check($old_password, $curr_user_pass)) {
            //Change the password
            $user_p_id->fill([
                'password' => Hash::make($new_password)
            ])->save();
 			$this->request->session()->flash('alert-success', 'Password changed successfully!');
			}
			else{
 			$this->request->session()->flash('alert-danger', 'Password couldnot successfully!');
			}
        	return back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete(){
		//
	}

	public function export()
	{
		//
	}

}
