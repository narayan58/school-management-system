<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\Tclass;
use App\Section;
use App\Shift;
use App\Exam;
use App\Class_has_exam;
use App\Class_has_section;
use App\Class_has_shift;
use App\Student_has_info;
use App\ExamClass;
use App\MarkSheet;
use App\User;
use App\User_has_level;
use App\Fee;
use App\Fee_bill;
use App\Fee_total;
use App\FeeStructure;
use App\Fee_has_month;
use App\Month;
use App\Helper\Helper;
use Validator;
use View;

class FeeController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '1') || ($user_filter_org == '6')){
	        $classes = Tclass::where('is_active','=', True)->get();
	        $fees = Fee::where('is_active','=', True)->get();
	        $months = Month::where('is_active','=', True)->get();
			return view('admin.fee', compact(['classes','user_levels','fees','months']));	
		}
		 else
		 	abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'bill_id' => 'required',
            'student_id' => 'required',
            'total_description' => 'required',
            'total_amount' => 'required',
            'total' => 'required',
            'grand_total' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$data = Input::all();
				$fees_total = new Fee_total;
				$fees_total->bill_id = $data['bill_id'];
				$fees_total->total = $data['total'];
				$dis = $data['discount'];
				if($dis == ""){
					$fees_total->discount = '0';
				}
				else{
					$fees_total->discount = $dis;
				}
				$fees_total->is_due = (isset($data['is_due'])) ? 1 : 0;
				$fees_total->grand_total = $data['grand_total'];
				$fees_total->student_id = $data['student_id'];
				$fees_total->date = '1';
				$fees_total->is_active = '1';
				$fees_total->created_by = $user_id;
				$fees_total->created_at = $current_date;

				if($fees_total->save()){
					$fee_sn = new Fee_bill;
						$fee_sn->bill_id = $data['bill_id'];
						$fee_sn->created_by = $user_id;
						if($fee_sn->save()){
					foreach($data['total_description'] as $i=>$total_description){
					//for($i= 0; $i<count($data['total_description']); $i++){
						$fee = new Fee;
						$fee->bill_id = $data['bill_id'];
						$fee->student_id = $data['student_id'];
						$fee->description = $data['total_description'][$i];
						$fee->amount = $data['total_amount'][$i];
						$fee->created_by = $user_id;
					    $fee->created_at = $current_date;

						$fee->date = '1';
						$fee->is_active = '1';
						if ($fee->save())
							{
								for($a= 0; $a<count($data['total_months_id'][$i]); $a++){
									$fee_months = new Fee_has_month;
									$fee_months->fee_id = $fee->id;
									$fee_months->month_id = $data['total_months_id'][$i][$a];
									$fee_months->created_by = $user_id;
					    			$fee_months->created_at = $current_date;
									$fee_months->save();
								}
							}
						}
					}
				$this->request->session()->flash('alert-success', 'Fee saved successfully!');
				}

				else{
				$this->request->session()->flash('alert-warning', 'Exam could not add!');
				}
			return Redirect::route('bill_print', ['id' => $data['bill_id']]);
		}
	}
	public function bill_print($bill_id){
		if (Auth::check()) 
		{
			$order_total = new Fee_total;
			$cat_id =['bill_id' => $bill_id,];
			$is_print = Fee_total::where($cat_id)->get();
			$print[0]['print_id']= $is_print[0]['is_print'];
			if($print[0]['print_id'] == '1'){
				$message = "Copied bill";
			}
			if($print[0]['print_id'] == '0'){
				$category_status = Fee_total::where($cat_id)->get();
				$current_category[0] = $category_status[0]['id'];
				$current_status = Fee_total::find($current_category[0]);
				$current_status->is_print = '1';
				$message = "";
				$current_status->save();
			}
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$student_details = Fee_total::where('bill_id',$bill_id)->get();
			return view('admin.bill_print',compact(['bill_sn','message','student_details','user_levels']));
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getSectionOnStudent($class_id){
		$sections = Class_has_section::with('getSectionClass')->where('class_id', $class_id)->get();
		return Response::json($sections);
	}

	public function getShiftOnStudent($class_id){
		$sections = Class_has_shift::with('getShiftClass')->where('class_id', $class_id)->get();
		return Response::json($sections);
	}
	public function getStudentList(){
		$class_id = Input::get('class_id');
		$shift_id = Input::get('shift_id');
		$section_id = Input::get('section_id');
		$studentlists = Student_has_info::with('studentinfo')
								->where('class_id', $class_id)
								->where('shift_id', $shift_id)
								->where('section_id', $section_id)
								->get();
		return Response::json($studentlists);
	}

	public function getStudentClassSub(){
		$student_id = Input::get('student_id');
		$exam_id = Input::get('exam_id');
		$studentsubs = Student_has_info::where('user_id', $student_id)->firstOrFail();
		$student_org_id = $studentsubs['class_id'];
		$student_ord_subs = ExamClass::with('getSubjectClass')
							->where('class_id',$student_org_id)
							->where('exam_id',$exam_id)
							->get();
		return Response::json($student_ord_subs);
	}

	public function getStudentFee(){
		$student_id = Input::get('student_id');
        $description= Input::get('description');
		$quantity_org = Input::get('quantity');
		$num_x = Input::get('num_x');

		$data = Input::all();
		// to get selected months
		// var_dump($data['months']); die();
		$count = count($data['months']);
		$months = array(); 
		for ($i=0; $i < ($count); $i++) {
			$months[$data['months'][$i]['month']]=$data['months'][$i];
			$months_val = Month::where('id', $data['months'][$i]['month'])->get();
			$months[$data['months'][$i]['month']]['name'] = $months_val[0]['name'];
		}
		$quantity = $quantity_org * $count;
		// var_dump($months_val); die();
		$student_names = User::where('id',$student_id)->get();
		$student_name = $student_names[0]['name'];
			$data = array('student_id' => $student_id,'description' => $description,'quantity' => $quantity,'student_name' => $student_name,'num_x'=>$num_x,'months' => $months);
			return View::make('admin.fee_row', $data);
	}
	public function getAllCalculation(){
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$response= array();
			$total = Input::get('total');
			if(!empty($total)){
				$total = array_sum($total);
				$response['total'] = $total;
				//$vat = (13/100) * $total;
				//$response['vat'] = $vat;
				//$service_charge = (10/100) * $total;
				//$response['service_charge'] = $service_charge;	
				// end checking service charge			

				//$grand_total = $total + $vat + $service_charge;
				// $response['grand_total'] = $grand_total;

				return View::make('admin.bill_calculation')->with('calc_data',$response);	
			}		
		}
	}
	public function viewdetail(){
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$class_list=Tclass::where('is_active','=', True)->lists('name','id');
			$section_list=Section::where('is_active','=', True)->lists('name','id');
			$shift_list=Shift::where('is_active','=', True)->lists('name','id');
	        $fee_totals = Fee_total::where('is_active','=', True)
	        				->orderBy('id', 'DESC')
	        				->paginate(20);
			return view('admin.student_fee_info', compact(['fee_totals','user_levels','class_list','section_list','shift_list']));	
		}
	}
	public function viewfeedetail($bill_id){
		if (Auth::check()) 
		{
			$org_user_id = Auth::user()->id;
			$user_levels = User::where('id', $org_user_id)->get();
			$message = "Copied bill";
			$student_details = Fee_total::where('bill_id',$bill_id)->get();
			$student_id=$student_details[0]['student_id'];
			$students_details=Student_has_info::where('user_id',$student_id)->firstOrFail();
				$class_name= $students_details->studentclass->name; 
				$class_section= $students_details->studentsection->name; 
				$class_shift= $students_details->studentshift->name;
			return view('admin.student_fee_info_detail', compact(['message','user_levels','student_details','class_name','class_section','class_shift']));	
		}
	}

	public function fee_search()
	{
		$parammeters = $_GET['parameters'];
		// var_dump($parammeters); die;
        if(is_array($parammeters))
        {
            $params=$parammeters;
        }
        else{
            $params = json_decode($parammeters);
        }

        foreach ($params as $key => $value) {
            $each_param[$key] = $value;
        }
        foreach ($each_param as $key => $value) {

            ${$key} = $value;

        }

        $class_id=$class_id;
        $section_id=$section_id;
        $shift_id=$shift_id;
        $query = Fee_total::where('is_active','=', True);
        	$query =$query->orderBy('id', 'DESC');
        				
		if($class_id!=0){
			$query=$query->whereHas('getStudentInfo', function($query) use ($class_id) {
									        $query->where('class_id',$class_id);
									  });	
		}
		if($section_id!=0){
			$query=$query->whereHas('getStudentInfo', function($query) use ($section_id) {
									        $query->where('section_id',$section_id);
									  });
		}
			
		if($shift_id!=0){

			$query=$query->whereHas('getStudentInfo', function($query) use ($shift_id) {
									        $query->where('shift_id',$shift_id);
									  });	
		}

		 $fee_totals=$query->get();
		if(is_array($parammeters))
        {
        	//$this part is not used

        }
        else{
        	return view('admin.student_fee_ajax',compact('fee_totals'));
        }
	}

	public function getClassAmount($class_id){
		$fee_structures = FeeStructure::where('class_id', $class_id)->get();
		return Response::json($fee_structures);
	}

	public function cleardue(){
		date_default_timezone_set('Asia/Kathmandu'); 
		$current_date = date("Y-m-d h:i:s");
		$id = (Input::get('id'));
		$fee_due = Fee_total::find($id);
		$fee_due->is_due = '0';
		$fee_due->due_clear_on = $current_date;
		if($fee_due->save()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Clear',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Clear Unsuccessful',
			);
		}
		return Response::json($response);
	}

}
