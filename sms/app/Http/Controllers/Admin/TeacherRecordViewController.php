<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\Helper\Helper;
use App\User;
use App\Teacher_has_info;
use App\Class_has_section;
use App\Class_has_shift;
use App\Subject;
use App\Section;
use App\Tclass;
use App\Shift;
use App\Period;
use App\Topic;
use App\SubTopic;
use App\TeacherRecord;
use App\User_has_level;
use Validator;
use DB;

class TeacherRecordViewController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper = $helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu');
			// echo $search_date=Input::get('subtopic_date'); die; 
			$current_date = date("Y-m-d");
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '4') || ($user_filter_org == '1'))
			{
				$teacher_records = TeacherRecord::where('lecture_date', $current_date)
									->orderBy('class_id','ASC')
									->get(); 
				return view('admin.teacher_record_view', compact(['user_levels','teacher_records']));
			}
				else
					abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function teacher_search()
	{
		$parammeters = $_GET['parameters'];
		// var_dump($parammeters); die;
        if(is_array($parammeters))
        {
            $params=$parammeters;
        }
        else{
            $params = json_decode($parammeters);
        }

        foreach ($params as $key => $value) {
            $each_param[$key] = $value;
        }
        foreach ($each_param as $key => $value) {

            ${$key} = $value;

        }
        $search_date=$search_date;
        $teacher_records = TeacherRecord::where('lecture_date', $search_date)
									->orderBy('class_id','ASC')
									->get();
		
		if(is_array($parammeters))
        {
        	//$this part is not used

        }
        else{
        	return view('admin.teacher_record_ajax', compact('teacher_records'));

        }
	}

}
