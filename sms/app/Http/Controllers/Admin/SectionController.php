<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Helper\Helper;
use Input;
use Redirect;
use Response;
use Auth;
use App\Section;
use App\User;
use App\User_has_level;
use Validator;

class SectionController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '2') || ($user_filter_org == '1')){
		        $sections = Section::orderBy('id', 'DESC')->get();
				return view('admin.section', compact(['sections','user_levels']));	
			}
			else
				abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$rules = array(
            'name' => 'required|unique:sections'
        );
		$validator = Validator::make(Input::all(), $rules);
						if ($validator->fails()) {
						return back()->withInput()
						->withErrors($validator)
						->withInput();
					}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$section = new Section;
			$section->name = (Input::get('name'));
			$section->slug = $this->helper->slug_converter($section->name);
			$section->is_active = '1';
			$section->created_by = $user_id;
			$section->created_at = $current_date;
			if ($section->save()){
					$this->request->session()->flash('alert-success', 'Section saved successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'Section could not add!');
			}
			return redirect('home/section');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$section = Section::find($id);
		return Response::json($section);
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'name' => 'required|unique:sections'
        );
		$validator = Validator::make(Input::all(), $rules);
						if ($validator->fails()) {
						return back()->withInput()
						->withErrors($validator)
						->withInput();
					}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$edit_id = Input::get('section_id');
			$section = Section::find($edit_id);
			$section->name = (Input::get('name'));
			$section->slug = $this->helper->slug_converter($section->name);
			$section->is_active = '1';
			$section->updated_by = $user_id;
			$section->updated_at = $current_date;
			if ($section->save()){
					$this->request->session()->flash('alert-info', 'Section updated successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'Section could not update!');
			}
			return redirect('home/section');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete(){
		$id = Input::get('id');
		$section = Section::find($id);

		if($section->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}
	public function export()
	{
		 $sections = Section::get();
		return view('admin.section_export', compact(['sections']));	
	}

}
