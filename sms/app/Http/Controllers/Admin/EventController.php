<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Input;
use Redirect;
use Response;
use Auth;
use App\Calendar;
use App\User;
use App\Helper\Helper;
use Validator;

class EventController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$calendars = Calendar::get();
			return view('admin.event',compact(['user_levels',]))->with('mydata', json_decode($calendars, true));
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$rules = array(
            'name' => 'required|unique:tclasses'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$data = Input::all();
			$class = new Tclass;
				$class->name = $data['name'];
				$class->slug = $this->helper->slug_converter($class->name);
				$class->is_active = '1';
				$class->created_by = $user_id;
				$class->created_at = $current_date;
			if ($class->save()){
				$count = count($data['shift_id']);
			for ($i=0; $i < ($count); $i++) {
					$class_has_shifts = new Class_has_shift;
					$class_has_shifts->class_id = $class->id;
					$class_has_shifts->shift_id = $data['shift_id'][$i];
					$class_has_shifts->is_active = '1';
					$class_has_shifts->created_by = $user_id;
					$class_has_shifts->created_at = $current_date;
					$class_has_shifts->save();
				}
			$count = count($data['section_id']);
			for ($i=0; $i < ($count); $i++) {
					$class_has_sections = new Class_has_section;
					$class_has_sections->class_id = $class->id;
					$class_has_sections->section_id = $data['section_id'][$i];
					$class_has_sections->is_active = '1';
					$class_has_sections->created_by = $user_id;
					$class_has_sections->created_at = $current_date;
					$class_has_sections->save();
					$this->request->session()->flash('alert-success', 'Class saved successfully!');
				}
			}
			else{
				$this->request->session()->flash('alert-warning', 'Class could not add!');
			}
			return redirect('home/class');
		}
		else{
			abort(404);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//return ('eheheehehheh');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	public function export()
	{
		$classes = Tclass::where('is_active','=', True)->get();
        $shifts = Shift::where('is_active','=', True)->get();
        $sections = Section::where('is_active','=', True)->get();
		return view('admin.class_export', compact(['classes','shifts','sections']));
	}

}
