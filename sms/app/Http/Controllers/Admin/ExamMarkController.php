<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\Tclass;
use App\Exam;
use App\Class_has_exam;
use App\Class_has_section;
use App\Class_has_shift;
use App\Student_has_info;
use App\ExamClass;
use App\MarkSheet;
use App\User;
use App\Helper\Helper;
use App\Mark_has_grade;
use App\Grade;
use Validator;
use View;
use DB;

class ExamMarkController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($exam_id_stu)
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$exam_ids=['slug'=>$exam_id_stu];
	        try{
				 $current_exam = Exam::where($exam_ids)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
			$exam_id = $current_exam['id'];
			$exam_org_name = $current_exam['name'];
			$current_exam_class = Class_has_exam::where('exam_id', $exam_id)->get();
	        $classes = Tclass::where('is_active','=', True)->get();
	        $exams = Exam::where('is_active','=', True)->get();
			return view('admin.exam_mark', compact(['current_exam_class','exam_id','user_levels','exam_id_stu']));	
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'class_id' => 'required',
            'exam_id' => 'required',
            'section_id' => 'required',
            'shift_id' => 'required',
            'student_id' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$data = Input::all();
			$exam_name = Exam::where('id',$data['exam_id'])->get();
			$exam_slug = $exam_name[0]['slug']; 
				$count = count($data['marks_id']);
			for ($i=0; $i < ($count); $i++) { 
				$marksheet = new MarkSheet;
				$marksheet->class_id = $data['class_id'];
				$marksheet->exam_id = $data['exam_id'];
				$marksheet->section_id = $data['section_id'];
				$marksheet->shift_id = $data['shift_id'];
				$marksheet->student_id = $data['student_id'];
				$marksheet->full_mark = $data['full_mark'][$i];
				$marksheet->marks_pr = $data['marks_pr'][$i];
				$marksheet->marks_th = $data['marks_th'][$i];
				$marksheet->marks_obt = $data['marks_obt'][$i];
				$marksheet->marks_id = $data['marks_id'][$i];
				$marksheet->is_active = '1';
				$marksheet->created_by = $user_id;
				$marksheet->created_at = $current_date;

				if($marksheet->save()){
					// insert to grade
					$marks = MarkSheet::where('exam_id', $data['exam_id'])
						->where('student_id', $data['student_id'])
						->get();
		foreach ($marks as $mark) {
			$mark_id = $mark->id;
			$fmark = $mark->full_mark;

			// to update
			$grade_mark = Mark_has_grade::where('mark_sheet_id', $mark_id)->get();
			if(count($grade_mark)){
				$grade_mark_id = $grade_mark[0]['id'];
				$mark_grade = Mark_has_grade::find($grade_mark_id);
				$obt_mark = $mark->marks_obt;
				if(($obt_mark == 'A') || ($obt_mark == '-')){
					$grade_obt_rev = Grade::where('lower','=',$obt_mark)
									->where('is_active', True)
									->get();
						foreach ($grade_obt_rev as $gradeobtrev) {
							$gidrev = $gradeobtrev->id;
						}
						// var_dump("Yes",$data['marks_obt'][$i],$gidrev);
					$mark_grade->grade_id = $gidrev;
				}
				elseif($fmark != '100'){
					$obt_markm = (100/$fmark)*$obt_mark;
					$grade_obt = Grade::where('lower','<=',$obt_markm)
										->where('higher','>', $obt_markm)
										->where('is_active', True)
										->get();
						foreach ($grade_obt as $gradeobt) {
							$gid = $gradeobt->id;
						}
						// var_dump("Not",$data['marks_obt'][$i],$gid);
					$mark_grade->grade_id = $gid;
				}
				elseif($fmark != '100'){
					$obt_markm = (100/$fmark)*$obt_mark;
					$grade_obt = Grade::where('lower','<=',$obt_markm)
										->where('higher','>', $obt_markm)
										->where('is_active', True)
										->get();
						foreach ($grade_obt as $gradeobt) {
							$gid = $gradeobt->id;
						}
						// var_dump("Not",$data['marks_obt'][$i],$gid);
					$mark_grade->grade_id = $gid;
				}
				else{
				$grade_obt = Grade::where('lower','<=',$obt_mark)
									->where('higher','>=', $obt_mark)
									->where('is_active', True)
									->get();
					foreach ($grade_obt as $gradeobt) {
						$gid = $gradeobt->id;
					}
					$mark_grade->grade_id = $gid;
				}
				$mark_grade->mark_sheet_id = $mark->id;
				$mark_grade->created_by = $user_id;
				$mark_grade->save();
			}
			else{
				$mark_grade = new Mark_has_grade;
				$obt_mark = $mark->marks_obt;
				if(($obt_mark == 'A') || ($obt_mark == '-')){
					$grade_obt_rev = Grade::where('lower','=',$obt_mark)
									->where('is_active', True)
									->get();
						foreach ($grade_obt_rev as $gradeobtrev) {
							$gidrev = $gradeobtrev->id;
						}
						// var_dump("Yes",$data['marks_obt'][$i],$gidrev);
					$mark_grade->grade_id = $gidrev;
				}
				elseif($fmark != '100'){
					$obt_markm = (100/$fmark)*$obt_mark;
					$grade_obt = Grade::where('lower','<=',$obt_markm)
										->where('higher','>', $obt_markm)
										->where('is_active', True)
										->get();
						foreach ($grade_obt as $gradeobt) {
							$gid = $gradeobt->id;
						}
						// var_dump("Not",$data['marks_obt'][$i],$gid);
					$mark_grade->grade_id = $gid;
				}
				elseif($fmark != '100'){
					$obt_markm = (100/$fmark)*$obt_mark;
					$grade_obt = Grade::where('lower','<=',$obt_markm)
										->where('higher','>', $obt_markm)
										->where('is_active', True)
										->get();
						foreach ($grade_obt as $gradeobt) {
							$gid = $gradeobt->id;
						}
						// var_dump("Not",$data['marks_obt'][$i],$gid);
					$mark_grade->grade_id = $gid;
				}
				else{
				$grade_obt = Grade::where('lower','<=',$obt_mark)
									->where('higher','>=', $obt_mark)
									->where('is_active', True)
									->get();
					foreach ($grade_obt as $gradeobt) {
						$gid = $gradeobt->id;
					}
					$mark_grade->grade_id = $gid;
				}
				$mark_grade->mark_sheet_id = $mark->id;
				$mark_grade->created_by = $user_id;
				$mark_grade->save();
			}
		}
		// ends here
					$this->request->session()->flash('alert-success', 'Student Marksheet saved successfully!');
				}
				else{
				$this->request->session()->flash('alert-warning', 'Student Marksheet could not add!');
				}
			}
			return back()->withInput();
		}
	}

	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'exam_id' => 'required',
            'student_id' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$data = Input::all();
			$exam_name = Exam::where('id',$data['exam_id'])->get();
			$exam_slug = $exam_name[0]['slug']; 
				$count = count($data['marks_id']);
			for ($i=0; $i < ($count); $i++) { 
				// var_dump($data['marks_obt'][$i]);
				$marksheet = MarkSheet::find($data['marks_id'][$i]);
				$marksheet->marks_obt = $data['marks_obt'][$i];
				$marksheet->marks_pr = $data['marks_pr'][$i];
				$marksheet->marks_th = $data['marks_th'][$i];
				$marksheet->is_active = '1';
				$marksheet->created_by = $user_id;
				$marksheet->created_at = $current_date;

				if($marksheet->save()){
					// insert to grade
					$marks = MarkSheet::where('exam_id', $data['exam_id'])
						->where('student_id', $data['student_id'])
						->get();
		foreach ($marks as $mark) {
			$mark_id = $mark->id;
			$fmark = $mark->full_mark;

			// to update
			$grade_mark = Mark_has_grade::where('mark_sheet_id', $mark_id)->get();
			if(count($grade_mark)){
				$grade_mark_id = $grade_mark[0]['id'];
				$mark_grade = Mark_has_grade::find($grade_mark_id);
				// var_dump($mark_grade->id); die();
				$obt_mark = $mark->marks_obt;
				if(($obt_mark == 'A') || ($obt_mark == '-')){
					$grade_obt_rev = Grade::where('lower','=',$obt_mark)
									->where('is_active', True)
									->get();
						foreach ($grade_obt_rev as $gradeobtrev) {
							$gidrev = $gradeobtrev->id;
						}
						// var_dump("Yes",$data['marks_obt'][$i],$gidrev);
					$mark_grade->grade_id = $gidrev;
				}
				elseif($fmark != '100'){
					$obt_markm = (100/$fmark)*$obt_mark;
					$grade_obt = Grade::where('lower','<=',$obt_markm)
										->where('higher','>', $obt_markm)
										->where('is_active', True)
										->get();
						foreach ($grade_obt as $gradeobt) {
							$gid = $gradeobt->id;
						}
						// var_dump("Not",$data['marks_obt'][$i],$gid);
					$mark_grade->grade_id = $gid;
				}
				else{
					$grade_obt = Grade::where('lower','<=',$obt_mark)
										->where('higher','>', $obt_mark)
										->where('is_active', True)
										->get();
						foreach ($grade_obt as $gradeobt) {
							$gid = $gradeobt->id;
						}
						// var_dump("Not",$data['marks_obt'][$i],$gid);
					$mark_grade->grade_id = $gid;
				}
				// if(($data['marks_obt'][$i] == 'A') || ($data['marks_obt'][$i]) == '-'){
				// 	// var_dump("expression",$data['marks_obt'][$i]); die();
				// }
				// else{
				// 	$mark_grade->grade_id = $gid;
				// }
				$mark_grade->mark_sheet_id = $mark->id;
				$mark_grade->created_by = $user_id;
				$mark_grade->save();
			}
			else{
				$mark_grade = new Mark_has_grade;
				$obt_mark = $mark->marks_obt;
				if(($obt_mark == 'A') || ($obt_mark == '-')){
					$grade_obt_rev = Grade::where('lower','=',$obt_mark)
									->where('is_active', True)
									->get();
						foreach ($grade_obt_rev as $gradeobtrev) {
							$gidrev = $gradeobtrev->id;
						}
						// var_dump("Yes",$data['marks_obt'][$i],$gidrev);
					$mark_grade->grade_id = $gidrev;
				}
				elseif($fmark != '100'){
					$obt_markm = (100/$fmark)*$obt_mark;
					$grade_obt = Grade::where('lower','<=',$obt_markm)
										->where('higher','>', $obt_markm)
										->where('is_active', True)
										->get();
						foreach ($grade_obt as $gradeobt) {
							$gid = $gradeobt->id;
						}
						// var_dump("Not",$data['marks_obt'][$i],$gid);
					$mark_grade->grade_id = $gid;
				}
				else{
				$grade_obt = Grade::where('lower','<=',$obt_mark)
									->where('higher','>=', $obt_mark)
									->where('is_active', True)
									->get();
					foreach ($grade_obt as $gradeobt) {
						$gid = $gradeobt->id;
					}
					$mark_grade->grade_id = $gid;
				}
				$mark_grade->mark_sheet_id = $mark->id;
				$mark_grade->created_by = $user_id;
				$mark_grade->save();
			}
		}
		// ends here
					$this->request->session()->flash('alert-success', 'Student Marksheet updated successfully!');
				}
				else{
				$this->request->session()->flash('alert-warning', 'Student Marksheet could not update!');
				}
			}
			// die();
			return back()->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getSectionOnStudent($class_id){
		$sections = Class_has_section::with('getSectionClass')->where('class_id', $class_id)->get();
		return Response::json($sections);
	}

	public function getShiftOnStudent($class_id){
		$sections = Class_has_shift::with('getShiftClass')->where('class_id', $class_id)->get();
		return Response::json($sections);
	}
	public function getStudentList($exam_id_slug){
		if (Auth::check()) 
		{
			$class_id = Input::get('class_id');
			$shift_id = Input::get('shift_id');
			$section_id = Input::get('section_id');
			$exam_name = Exam::where('slug',$exam_id_slug)->get();
			$exam_id = $exam_name[0]['id']; 
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$current_exam_class = Class_has_exam::where('exam_id', $exam_id)->get();
			$studentlists = DB::table('student_has_infos')
	            ->join('users','users.id','=','student_has_infos.user_id')
	            ->join('exam_classes','exam_classes.class_id','=','student_has_infos.class_id','left')
	            ->select('users.name as student_name', 'student_has_infos.user_id as user_id')
	            ->where('student_has_infos.class_id', $class_id)
				->where('student_has_infos.shift_id', $shift_id)
				->where('student_has_infos.section_id', $section_id)
				->where('exam_classes.exam_id', $exam_id)
	            ->orderBy('users.id')
	            ->groupBy('student_has_infos.user_id')
	            ->get();
	        $student_marksheet=Marksheet::where('class_id', $class_id)
				->where('shift_id', $shift_id)
				->where('section_id', $section_id)
				->where('exam_id', $exam_id)
				->lists('student_id');
			return view('admin.studentList', compact('studentlists','class_id','section_id','shift_id','exam_id','user_levels','current_exam_class','student_marksheet'));	
		}
	}

	public function getStudentClassSub(){
		$student_id = Input::get('student_id');
		$student_name = User::where('id',$student_id)->firstOrFail();
		$student_org_name = $student_name['name'];
		$exam_id = Input::get('exam_id');
		$exam_ids=['user_id'=>$student_id,];
		$studentsubs = Student_has_info::where('user_id',$student_id)->firstOrFail();
		$student_org_id = $studentsubs['class_id'];
		$studentMarkSheet = Marksheet::where('student_id',$student_id)
								->where('class_id', $student_org_id)
								->where('exam_id', $exam_id)
								->get();
		if(count($studentMarkSheet)){
        	$student_ord_subs = array();
        			}
        else{
			$student_ord_subs = ExamClass::where('class_id',$student_org_id)
						->where('exam_id',$exam_id)
						->get();
					}
		return View::make('admin.studentSubMark', compact(['student_ord_subs','studentMarkSheet','student_id','exam_id','student_org_name']));	
	}
	public function getStudentClassSubMarks(){
		$student_id = Input::get('student_id');
		$student_name = User::where('id',$student_id)->firstOrFail();
		$student_org_name = $student_name['name'];
		$exam_id = Input::get('exam_id');
		$exam_ids=['user_id'=>$student_id,];
		$studentsubs = Student_has_info::where('user_id',$student_id)->firstOrFail();
		$student_org_id = $studentsubs['class_id'];
		$studentMarkSheet = Marksheet::where('student_id',$student_id)
								->where('class_id', $student_org_id)
								->where('exam_id', $exam_id)
								->get();
		return View::make('admin.studentSubMarkUpdate', compact(['studentMarkSheet','student_id','exam_id','student_org_name']));	
	}

}
