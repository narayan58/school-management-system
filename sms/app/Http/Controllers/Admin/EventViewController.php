<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Helper\Helper;
use Input;
use Redirect;
use Response;
use Auth;
use App\Calendar;
use App\User;
use Validator;
use View;

class EventViewController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$calendars = Calendar::orderBy('id', 'DESC')->get();
			return view('admin.event_add', compact(['calendars','user_levels']));
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'title' => 'required',
            'start' => 'required',
            
        );
		$validator = Validator::make(Input::all(), $rules);
				if ($validator->fails()) {
				return back()->withInput()
				->withErrors($validator)
				->withInput();
			}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$calendars = new Calendar;
			$calendars->title = (Input::get('title'));
			$calendars->start = (Input::get('start'));
			$calendars->end = (Input::get('end'));
			$calendars->is_active = '1';
			$calendars->created_by = $user_id;;
			$calendars->created_at = $current_date;
			if ($calendars->save()){
					$this->request->session()->flash('alert-success', 'Calendars saved successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'Calendars could not add!');
			}
			return redirect('home/event_view');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$calender = Calendar::find($id);
		return Response::json($calender);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'title' => 'required'
            
        );
		$validator = Validator::make(Input::all(), $rules);
				if ($validator->fails()) {
				return back()->withInput()
				->withErrors($validator)
				->withInput();
			}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$edit_id = Input::get('calendar_id');
			$calendars = Calendar::find($edit_id);
			$calendars->title = (Input::get('title'));
			$calendars->start = (Input::get('start'));
			$calendars->end = (Input::get('end'));
			$calendars->created_by = '1';
			$calendars->created_at = $current_date;
			if ($calendars->save()){
					$this->request->session()->flash('alert-info', 'Calendars saved successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'Calendars could not add!');
			}
			return redirect('home/event_view');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete()
		{
			$id = Input::get('id');
			$calander = Calendar::find($id);

			if($calander->delete()){
				$response = array(
					'status' => 'success',
					'msg' => 'Successfully Deleted',
				);
			}else{
				$response = array(
					'status' => 'failure',
					'msg' => 'Deleted Unsuccessful',
				);
			}
			return Response::json($response);
		}

	public function export()
	{
		 $calendars = Calendar::orderBy('id', 'DESC')->get();
		return view('admin.eventAdd_export', compact(['calendars']));
	}

	public function event_search($id)
	{
        $calendars = Calendar::where('start', $id)
									->orderBy('id', 'DESC')
									->get();
		return View::make('admin.event_add_ajax', compact(['calendars']));
	}

}
