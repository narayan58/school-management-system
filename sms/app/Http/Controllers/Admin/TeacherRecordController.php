<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\Helper\Helper;
use App\User;
use App\Teacher_has_info;
use App\Class_has_section;
use App\Class_has_shift;
use App\Subject;
use App\Section;
use App\Tclass;
use App\Shift;
use App\Period;
use App\Topic;
use App\SubTopic;
use App\TeacherRecord;
use App\User_has_level;
use Validator;
use DB;


class TeacherRecordController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper = $helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '4') || ($user_filter_org == '1'))
			{
				$classees = Tclass::where('is_active','=', True)->get();
				$shiftes = Shift::where('is_active','=', True)->get();
				$periods = Period::where('is_active','=', True)->get();
				$teachers = User::where('is_admin','=',6)->get();
				$subjects = Subject::where('is_active','=',True)->orderBy('class_id','ASC')->get();
				return view('admin.teacher-record', compact(['teachers','subjects','sections','classees','shiftes','periods','user_levels']));
			}
			else
				abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'teacher_id' => 'required',
			'subject_id' => 'required',
			'section_id' => 'required',
			'class_id' => 'required',
			'shift_id' => 'required',
			'period_id' => 'required',
			'topic_id' => 'required',
			'subtopic_id' => 'required',
			'subtopic_date' => 'required',
			'subtopic_hour' => 'required',
		);
		$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) {
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d");
			$user_id = Auth::user()->id;
			$data = Input::all();
			$count = count($data['section_id']);
			for ($i=0; $i < ($count); $i++) { 
			$teacherrecord = new TeacherRecord;
			$teacherrecord->teacher_id = $data['teacher_id'];
			$teacherrecord->lecture_date = $data['subtopic_date'];
			$teacherrecord->subject_id = $data['subject_id'][$i];
			$teacherrecord->class_id = $data['class_id'][$i];
			$teacherrecord->section_id = $data['section_id'][$i];
			$teacherrecord->shift_id = $data['shift_id'][$i];
			$teacherrecord->period_id = $data['period_id'][$i];
			$teacherrecord->topic_id = $data['topic_id'][$i];
			$teacherrecord->subtopic_id = $data['subtopic_id'][$i];
			$teacherrecord->lecture_hr = $data['subtopic_hour'][$i];
			$teacherrecord->created_by = $user_id;
			$teacherrecord->created_at = $current_date;
			if ($teacherrecord->save()){
					$this->request->session()->flash('alert-success', 'Daily record saved successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'Daily record could not add!');
			}
		}
		return back()->withInput();
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getTeacherSubject($teacher_id){	
		$teachers = $this->teacherSubjects($teacher_id);	
		return Response::json($teachers);
	}

	public function getTeacherClass($teacher_id){
		$teacherClass = Teacher_has_info::with('getAllSubject')->where('user_id', $teacher_id)->get();
		return Response::json($teacherClass);
	}

	public function getSubjectClass($subject_id){
		$clas = Subject::where('id', $subject_id)->get();
		$class_id = $clas[0]['class_id'];
		$subjects = Tclass::where('id',$class_id)->get();
		return Response::json($subjects);
	}

	public function getSubjectTopic($subject_id){
		$clas = Subject::where('id', $subject_id)->get();
		$class_id = $clas[0]['id'];
		$subjects = Topic::where('subject_id',$class_id)->get();
		return Response::json($subjects);
	}
	public function getSubjectSubTopic($topic_id){
		$clas = Topic::where('id', $topic_id)->get();
		$class_id = $clas[0]['id'];
		$subjects = SubTopic::where('topic_id',$class_id)->get();
		return Response::json($subjects);
	}
	public function getSection($class_id){
		$sections = Class_has_section::with('getSectionClass')->where('class_id', $class_id)->get();
		return Response::json($sections);
	}
	public function getShift($class_id){
		$sections = Class_has_shift::with('getShiftClass')->where('class_id', $class_id)->get();
		return Response::json($sections);
	}

}
