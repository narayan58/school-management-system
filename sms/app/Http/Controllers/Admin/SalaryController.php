<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\Tclass;
use App\Section;
use App\Shift;
use App\Exam;
use App\Class_has_exam;
use App\Class_has_section;
use App\Class_has_shift;
use App\Student_has_info;
use App\ExamClass;
use App\MarkSheet;
use App\User;
use App\User_has_level;
use App\Salary;
use App\Salary_bill;
use App\Salary_total;
use App\FeeStructure;
use App\Fee_has_month;
use App\Month;
use App\Helper\Helper;
use Validator;
use View;

class SalaryController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '1') || ($user_filter_org == '6')){
	        $months = Month::where('is_active','=', True)->get();
	        $teachers = User::where('confirmed','=', True)->where('is_admin', '6')->get();
			return view('admin.salary', compact(['user_levels','months','teachers']));	
		}
		 else
		 	abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'bill_id' => 'required',
            'teacher_id' => 'required',
            'total_description' => 'required',
            'total_amount' => 'required',
            'month_id' => 'required',
            'total' => 'required',
            'action' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$data = Input::all();
			
				$salary_total = new Salary_total;
				$salary_total->bill_id = $data['bill_id'];
				$salary_total->grand_total = $data['total'];
				$salary_total->teacher_id = $data['teacher_id'];
				$salary_total->date = $data['month_id'];
				$salary_total->is_active = '1';
				$salary_total->created_by = $user_id;
				$salary_total->created_at = $current_date;

				if($salary_total->save()){
					$salary_sn = new Salary_bill;
						$salary_sn->bill_id = $data['bill_id'];
						$salary_sn->created_by = $user_id;
						if($salary_sn->save()){
					foreach($data['total_description'] as $i=>$total_description){
					//for($i= 0; $i<count($data['total_description']); $i++){
						$salary = new Salary;
						$salary->bill_id = $data['bill_id'];
						$salary->teacher_id = $data['teacher_id'];
						$salary->description = $data['total_description'][$i];
						$salary->amount = $data['total_amount'][$i];
						$salary->status = $data['action'][$i];
						$salary->created_by = $user_id;
					    $salary->created_at = $current_date;

						$salary->date = '1';
						$salary->is_active = '1';
						$salary->save();
						}
					}
				$this->request->session()->flash('alert-success', 'Salary saved successfully!');
				}

				else{
				$this->request->session()->flash('alert-warning', 'Salary could not add!');
				}
			return Redirect::route('salary_print', ['id' => $data['bill_id']]);
		}
	}
	public function salary_print($bill_id){
		if (Auth::check()) 
		{
			$order_total = new Salary_total;
			$cat_id =['bill_id' => $bill_id,];
			$is_print = Salary_total::where($cat_id)->get();
			$print[0]['print_id']= $is_print[0]['is_print'];
			if($print[0]['print_id'] == '1'){
				$message = "Copied bill";
			}
			if($print[0]['print_id'] == '0'){
				$category_status = Salary_total::where($cat_id)->get();
				$current_category[0] = $category_status[0]['id'];
				$current_status = Salary_total::find($current_category[0]);
				$current_status->is_print = '1';
				$message = "";
				$current_status->save();
			}
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$salary_details = Salary_total::where('bill_id',$bill_id)->get();
			return view('admin.salary_print',compact(['bill_sn','message','salary_details','user_levels']));
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	public function getTeacherSalary(){
		$teacher_id = Input::get('teacher_id');
        $description= Input::get('description');
		$quantity_val = Input::get('quantity');
		$month_id = Input::get('month_id');
		$act_id = Input::get('id');
		$num_x = Input::get('num_x');
		$data = Input::all();
		if($act_id=='add'){
			$quantity = +$quantity_val;
		}
		else{
			$quantity = -$quantity_val;
		}
		$student_names = User::where('id',$teacher_id)->get();
		$student_name = $student_names[0]['name'];
			$data = array('teacher_id' => $teacher_id,'description' => $description,'quantity' => $quantity,'student_name' => $student_name,'num_x'=>$num_x,'month_id' => $month_id, 'act_id' => $act_id);
			return View::make('admin.salary_row', $data);
	}
	public function getSalaryCalculation(){
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$response= array();
			$total = Input::get('total');
			if(!empty($total)){
				$total = array_sum($total);
				$response['total'] = $total;
				return View::make('admin.salary_calculation')->with('calc_data',$response);	
			}		
		}
	}
	public function viewdetail(){
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$teachers = User::where('confirmed','=', True)->where('is_admin', '6')->get();
			$months = Month::where('is_active', True)->get();
	        $salary_totals = Salary_total::where('is_active','=', True)
	        				->orderBy('id', 'DESC')
	        				->get();
//            dd($salary_totals);
			return view('admin.teacher_salary_info', compact(['salary_totals','user_levels','teachers','months']));	
		}
	}
    public function salary_search()
    {
        $parammeters = $_GET['parameters'];
        // var_dump($parammeters); die;
        if(is_array($parammeters))
        {
            $params=$parammeters;
        }
        else{
            $params = json_decode($parammeters);
        }

        foreach ($params as $key => $value) {
            $each_param[$key] = $value;
        }
        foreach ($each_param as $key => $value) {

            ${$key} = $value;

        }

        $teacher_id=$teacher_id;
        $month_id=$month_id;
        $query = Salary_total::where('is_active','=', True);
        $query =$query->orderBy('id', 'DESC');

        if($teacher_id!=0){
            $query=$query->whereHas('teacherfeename', function($query) use ($teacher_id) {
                $query->where('teacher_id',$teacher_id);
            });
        }
        if($month_id!=0){
            $query=$query->whereHas('getMonth', function($query) use ($month_id) {
                $query->where('date',$month_id);
            });
        }



        $salary_totals=$query->get();
        if(is_array($parammeters))
        {
            //$this part is not used

        }
        else{
            return view('admin.teachers.teacher_salary_info_ajax',compact('salary_totals'));
        }
    }

}
