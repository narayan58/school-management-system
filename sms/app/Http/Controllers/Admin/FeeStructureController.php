<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Helper\Helper;
use Input;
use Redirect;
use Response;
use Auth;
use App\FeeStructure;
use App\Tclass;
use App\User;
use App\User_has_level;
use Validator;
use DB;


class FeeStructureController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '6') || ($user_filter_org == '1')){
			$classees = Tclass::where('is_active','=', True)->get();
			$fee_structure = FeeStructure::where('is_active','=', True)
										->orderBy('id', 'DESC')//updated with current time 
			 							->get();
			return view('admin.fee_structure', compact(['fee_structure','classees','user_levels']));
		}
		 else
		 	abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'class_id' => 'required|unique:fee_structures,class_id',
            'amount' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('home/feestructure')
                        ->withErrors($validator)
                        ->withInput();
        }
        if (Auth::check()) {
	       date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
		$user_id = Auth::user()->id;
		$fee_structures = new FeeStructure;
			$fee_structures->class_id = (Input::get('class_id'));
			$fee_structures->amount = (Input::get('amount'));
			$fee_structures->is_active = '1';
			$fee_structures->created_by = $user_id;
			$fee_structures->created_at = $current_date;

		if ($fee_structures->save()){
				$this->request->session()->flash('alert-success', 'Feestructure saved successfully!');
			}
		else{
			$this->request->session()->flash('alert-warning', 'Feestructure could not add!');
		}
		return back()->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$fees = FeeStructure::find($id);
		return Response::json($fees);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'class_id' => 'required',
            'amount' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		} 
        if (Auth::check()) {
	       date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
		$user_id = Auth::user()->id;
		$edit_id = Input::get('feestructure_id');
		$fee_structures = FeeStructure::find($edit_id);
			$fee_structures->class_id = (Input::get('class_id'));
			$fee_structures->amount = (Input::get('amount'));
			$fee_structures->is_active = '1';
			$fee_structures->created_by = $user_id;
			$fee_structures->created_at = $current_date;

		if ($fee_structures->save()){
				$this->request->session()->flash('alert-info', 'Feestructure saved successfully!');
			}
		else{
			$this->request->session()->flash('alert-warning', 'Feestructure could not add!');
		}
		return back()->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete(){
		$id = Input::get('id');
		$fees = FeeStructure::find($id);

		if($fees->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}

	public function export()
	{
		$classees = Tclass::where('is_active','=', True)->get();
		 $fee_structure = FeeStructure::where('is_active','=', True)->get();
		return view('admin.feestructure_export', compact(['fee_structure','classees']));
	}

}
