<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Input;
use Redirect;
use Response;
use Auth;
use App\Tclass;
use App\Section;
use App\Shift;
use App\Student_has_info;
use App\Class_has_section;
use App\User_has_level;
use App\Attendance;
use App\Helper\Helper;
use App\User;
use Validator;
use View;

class AttendanceController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '4') || ($user_filter_org == '1')){
				$class_list=Tclass::where('is_active','=', True)->lists('name','id');
				$section_list=Section::where('is_active','=', True)->lists('name','id');
				$shift_list=Shift::where('is_active','=', True)->lists('name','id');
				return view('admin.attendance', compact(['user_levels','class_list','section_list','shift_list']));
			}
			else
				abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}
	public function getStudent(){
		$class_id = (Input::get('class_id'));
		$section_id = (Input::get('section_id'));
		$shift_id = (Input::get('shift_id'));
		$attendance_date = (Input::get('attendance_date'));
			date_default_timezone_set('Asia/Kathmandu'); 
			$currentDate = date("Y-m-d");
		$class_name = Tclass::where('id', $class_id)->get();
		$section_name = Section::where('id', $section_id)->get();
		$shift_name = Shift::where('id', $shift_id)->get();
		if(($attendance_date == "") || ($attendance_date == $currentDate)){
			$current_att_date = $currentDate;
			$btndis = "";
		}
		else{
			$current_att_date = $attendance_date;
			$btndis = "Failed";
		}
		// var_dump($class_id, $section_id, $shift_id); die();
		$class_attendance = Attendance::where('class_id', $class_id)
		 								->where('section_id',$section_id)
		 								->where('shift_id', $shift_id)
		 								->where('start', $current_att_date)
										->get();
		$message = "Already Done";
		if(count($class_attendance)){
        	$student_lists = array();
        }
        else{
			$student_lists=Student_has_info::where('is_active','=', True)
		 								->where('class_id', $class_id)
		 								->where('section_id',$section_id)
		 								->where('shift_id', $shift_id)
		 								->get();
			$class_attendance = array();
			$message = '';
		}
		 // var_dump($student_lists); die();
		return View::make('admin.attendance_ajax', compact(['student_lists','class_attendance','class_id','section_id','shift_id','message','btndis','attendance_date','class_name','section_name','shift_name']));

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'class_id' => 'required',
            'section_id' => 'required',
            'shift_id' => 'required',
            'student_id' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$current_att_date = date("Y-m-d");
			$user_id = Auth::user()->id;
			$data = Input::all();
			$count = count($data['student_id']);
			for ($i=0; $i < ($count); $i++) {
				$attend = new Attendance;
				$attend->student_id = $data['student_id'][$i];
				$attend->class_id = $data['class_id'][$i];
				$attend->section_id = $data['section_id'][$i];
				$attend->shift_id = $data['shift_id'][$i];
				$attend->title = $data['attendance'][$i];
				$attend->start = $current_att_date;
				$attend->created_by = $user_id;
				$attend->created_at = $current_date;
				$attend->save();
					$this->request->session()->flash('alert-success', 'Attendance saved successfully!');
			}
			return back()->withInput();
		}
		else{
			abort(404);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete(){
		//
	}

	public function export()
	{
		$classes = Tclass::where('is_active','=', True)->get();
        $shifts = Shift::where('is_active','=', True)->get();
        $sections = Section::where('is_active','=', True)->get();
		return view('admin.class_export', compact(['classes','shifts','sections']));
	}

}
