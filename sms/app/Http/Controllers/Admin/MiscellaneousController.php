<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Helper\Helper;
use Input;
use Redirect;
use Response;
use Auth;
use App\Miscellaneous;
use App\Tclass;
use App\User;
use App\User_has_level;
use Validator;
use DB;


class MiscellaneousController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '6') || ($user_filter_org == '1')){
			$miscellaneouses = Miscellaneous::orderBy('id', 'DESC')//updated with current time 
			 							->get();
			return view('admin.miscellaneous', compact(['miscellaneouses','user_levels']));
		}
		 else
		 	abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'detail' => 'required',
            'amount' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('home/miscellaneous')
                        ->withErrors($validator)
                        ->withInput();
        }
        if (Auth::check()) {
	       date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
		$user_id = Auth::user()->id;
		$miscellaneous = new Miscellaneous;
			$miscellaneous->name = (Input::get('detail'));
			$miscellaneous->amount = (Input::get('amount'));
			$miscellaneous->is_active = '1';
			$miscellaneous->created_by = $user_id;
			$miscellaneous->created_at = $current_date;

		if ($miscellaneous->save()){
				$this->request->session()->flash('alert-success', 'Miscellaneous saved successfully!');
			}
		else{
			$this->request->session()->flash('alert-warning', 'Miscellaneous could not add!');
		}
		return back()->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$fees = FeeStructure::find($id);
		return Response::json($fees);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function export()
	{
		$classees = Tclass::where('is_active','=', True)->get();
		 $fee_structure = FeeStructure::where('is_active','=', True)->get();
		return view('admin.feestructure_export', compact(['fee_structure','classees']));
	}

}
