<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Helper\Helper;
use Input;
use Redirect;
use Response;
use Auth;
use App\Shift;
use App\User_has_level;
use App\User;
use Validator;

class ShiftController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '2') || ($user_filter_org == '1')){
	        	$shifts = Shift::orderBy('id', 'DESC')->get();
				return view('admin.shift',compact(['shifts','user_levels']));
			}
			else
				abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'name' => 'required|unique:shifts'
            
        );
		$validator = Validator::make(Input::all(), $rules);
				if ($validator->fails()) {
				return back()->withInput()
				->withErrors($validator)
				->withInput();
			}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$shift = new Shift;
			$shift->name = (Input::get('name'));
			$shift->slug = $this->helper->slug_converter($shift->name);
			$shift->is_active = '1';
			$shift->created_by = $user_id;
			$shift->created_at = $current_date;
			if ($shift->save()){
					$this->request->session()->flash('alert-success', 'Shift saved successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'shift could not add!');
			}
			return redirect('home/shift');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$shift = Shift::find($id);
		return Response::json($shift);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'name' => 'required|unique:shifts'
            
        );
		$validator = Validator::make(Input::all(), $rules);
				if ($validator->fails()) {
				return back()->withInput()
				->withErrors($validator)
				->withInput();
			}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$edit_id = Input::get('shift_id');
			$shift = Shift::find($edit_id);
			$shift->name = (Input::get('name'));
			$shift->slug = $this->helper->slug_converter($shift->name);
			$shift->is_active = '1';
			$shift->updated_by = $user_id;
			$shift->updated_at = $current_date;
			if ($shift->save()){
					$this->request->session()->flash('alert-info', 'Shift updated successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'shift could not update!');
			}
			return redirect('home/shift');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete()
	{
		$id = Input::get('id');
		$shift = Shift::find($id);

		if($shift->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}
	
	public function export()
	{
		 $shifts = Shift::get();
		return view('admin.shift_export', compact(['shifts']));
	}
}
