<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use App\Subject;
use App\Tclass;
use App\User;
use Auth;
use Validator;
use App\Helper\Helper;
use ModelNotFoundException;

class SubjectController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($slug)
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$class_id=['slug'=>$slug];
	        try{
				 $class = Tclass::where($class_id)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
			$class_name=$class['name'];
			$class_id=$class['id'];
			$classids = ['class_id' => $class->id];
			$active = ['is_active' => True];
			$subjects = Subject::where($classids)
								->where($active)
								->orderBy('id', 'DESC')
								->get();
			return view('admin.subject', compact(['subjects','slug','class_name','class_id','user_levels']));	
		}	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'name' => 'required',
            'subject_hour' => 'required',
            'class_id' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) {
				date_default_timezone_set('Asia/Kathmandu'); 
				$current_date = date("Y-m-d h:i:s");
			    $user_id = Auth::user()->id;
				$subject = new Subject;
				$subject->name = (Input::get('name'));
				$subject->slug = $this->helper->slug_converter($subject->name).'-'.(Input::get('class_id'));
				$subject->subject_hour = (Input::get('subject_hour'));
				$subject->class_id = (Input::get('class_id'));
				$subject->is_active = '1';
				$subject->created_by = $user_id;
				$subject->created_at = $current_date;
			if ($subject->save()){
					$this->request->session()->flash('alert-success', 'Subject saved successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'Subject could not add!');
			}
			return back()->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$subject = Subject::find($id);
		return Response::json($subject);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'name' => 'required',
            'subject_hour' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) {
				date_default_timezone_set('Asia/Kathmandu'); 
				$current_date = date("Y-m-d h:i:s");
			    $user_id = Auth::user()->id;
			    $edit_id = Input::get('subject_id');
				$subject = Subject::find($edit_id);
				$subject->name = (Input::get('name'));
				$subject->slug = $this->helper->slug_converter($subject->name).'-'.(Input::get('class_id'));
				$subject->subject_hour = (Input::get('subject_hour'));
				$subject->is_active = '1';
				$subject->created_by = $user_id;
				$subject->created_at = $current_date;
			if ($subject->save()){
					$this->request->session()->flash('alert-info', 'Subject updated successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'Subject could not add!');
			}
			return back()->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete()
	{
		$id = Input::get('id');
		$subjects = Subject::find($id);

		if($subjects->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}

	public function export()
	{
		 $subjects = Subject::get();
		return view('admin.subject_export', compact(['subjects']));
	}

}
