<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Validator;

use App\User;
use App\Category;
use App\Shift;
use App\Section;
use App\Period;
use App\Tclass;
use App\Exam;
use App\FeeStructure;
use App\Notice;
use App\Calendar;
use Auth;
use App\User_has_level;
use DB;
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$shift_count = Shift::where('is_active', True)->count();
			$section_count = Section::where('is_active', True)->count();
			$period_count = Period::where('is_active', True)->count();
			$class_count = Tclass::where('is_active', True)->count();
			$student_count = User::where('is_admin', '=',5)
								->where('confirmed',True)
								->count();
			$teacher_count = User::where('is_admin', '=',6)
								->where('confirmed',True)
								->count();
			$exam_count = Exam::where('is_active', True)->count();
			$feestructure_count = FeeStructure::where('is_active', True)->count();
			$notice_count = Notice::where('is_active', True)->count();
			$event_count = Calendar::where('is_active', True)->count();
			return view('admin.home',compact(['user_levels','shift_count','section_count',
				'period_count','class_count','student_count','teacher_count','exam_count',
				'feestructure_count','notice_count','event_count']));
		}
	}

}