<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\Tclass;
use App\Notice;
use App\User;
use App\User_has_level;
use App\Notice_has_class;
use App\Helper\Helper;
use Validator;

class NoticeController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '7') || ($user_filter_org == '1')){
	        $classes = Tclass::where('is_active','=', True)->get();
	       	$notices = Notice::where('is_active','=', True)
							->where('created_by', $user_id)//user taneko through created_by
							->orderBy('id', 'DESC')//updated with current time 
							->get();
			return view('admin.notice', compact(['classes', 'notices','user_levels']));
		}
		 else
		 abort(404);
		}	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$rules = array(
            'name' => 'required',
            'title' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{ 
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$data = Input::all();
			$notice = new Notice;
				$notice->title = $data['name'];
				$notice->name = $data['title'];
				$notice->is_active = '1';
				$notice->created_by = $user_id;
				$notice->created_at = $current_date;
			if ($notice->save()){
				$count = count($data['class_id']);
			for ($i=0; $i < ($count); $i++) { 
				$notice_has_classes = new Notice_has_class;
				$notice_has_classes->notice_id = $notice->id;
				$notice_has_classes->class_id = $data['class_id'][$i];
				$notice_has_classes->is_active = '1';
				$notice_has_classes->created_by = $user_id;
				$notice_has_classes->created_at = $current_date;
				$notice_has_classes->save();
				$this->request->session()->flash('alert-success', 'Notice saved successfully!');
				}
			}
			else{
				$this->request->session()->flash('alert-warning', 'Notice could not add!');
			}
			return back()->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$notice = Notice::find($id);
		return Response::json($notice);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'name' => 'required',
            'title' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check())
		{ 
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$edit_id = Input::get('notice_id');
			$notice = Notice::find($edit_id);
				$notice->title = Input::get('name');
				$notice->name = Input::get('title');
				$notice->is_active = '1';
				$notice->updated_by = $user_id;
				$notice->updated_at = $current_date;
			if ($notice->save()){
				$notice_ids = $notice->id;
				$notice_has_class = Notice_has_class::where('notice_id',$notice_ids)->delete();
				$data = Input::all();
				$count = count($data['class_id']);
			for ($i=0; $i < ($count); $i++) { 
				$notice_has_classes = new Notice_has_class;
				$notice_has_classes->notice_id = $notice->id;
				$notice_has_classes->class_id = $data['class_id'][$i];
				$notice_has_classes->is_active = '1';
				$notice_has_classes->created_by = $user_id;
				$notice_has_classes->updated_by = $user_id;
				$notice_has_classes->updated_at = $current_date;
				$notice_has_classes->save();
				$this->request->session()->flash('alert-info', 'Notice update successfully!');
				}
			}
			else{
				$this->request->session()->flash('alert-warning', 'Notice could not update!');
			}
			return back()->withInput();
		}
	}
	//
	

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete()
	{
		$id = Input::get('id');
		$notice_has_clas = Notice_has_class::where('notice_id', $id)->delete();
		$notice = Notice::find($id);

		if($notice->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}

	public function export()
	{
		$classes = Tclass::where('is_active','=', True)->get();
	    $notices = Notice::where('is_active','=', True)->get();
			return view('admin.notice_export', compact(['classes', 'notices']));
	}

}
