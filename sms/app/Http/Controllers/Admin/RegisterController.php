<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\Helper\Helper;
use App\User;
use App\User_has_level;
use App\Subject;
use App\Tclass;
use Validator;


class RegisterController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper = $helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$rules = array(
            'name' => 'required',
            'address' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:8',
            'confirm_password' => 'required|same:password',
            'date_of_birth' => 'required',
            'telephone_no' => 'required',
            'mobile_no' => 'required',
            'image' => 'required'

        );
        
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
    if (Auth::check()) {
    	date_default_timezone_set('Asia/Kathmandu'); 
		$current_date = date("Y-m-d h:i:s");
	    $user_id = Auth::user()->id;
		$users = new User;
		$image = Input::file('image');
		$destinationPath = 'image/manager'; // upload path
		if (!file_exists($destinationPath)) {
			    mkdir($destinationPath, 0777, true);
			}
		$extension = $image->getClientOriginalExtension(); // getting image extension
		$fileName = md5(mt_rand()).'.'.$extension; // renameing image
		$image->move($destinationPath, $fileName);
		$file_path = $destinationPath.'/'.$fileName;
		$users->image_encrypt = $fileName;
		$users->image = $image->getClientOriginalName();
		$mid = 'manager';
		// $student->mime = $image->getClientMimeType();
			$users->name = (Input::get('name'));
			$users->address = (Input::get('address'));
			$users->email = (Input::get('email'));
			$users->password = bcrypt(Input::get('password'));
			$users->gender = 'M';
			$users->dob = (Input::get('date_of_birth'));
			$users->telephone_no = (Input::get('telephone_no'));
			$users->mobile_no = (Input::get('mobile_no'));
			$users->confirmation_code = 's';
			$users->confirmed = '1';
			$users->is_admin = '1';
			$users->created_by = $user_id;
			$users->created_at = $current_date;
			$this->helper->imageResize($file_path, $fileName, $extension, $width=400, $height=300,$mid );
			if ($users->save()){
			$user_has_roles = new User_has_level;
				$user_has_roles->user_id = $users->id;
				$roles_title = (Input::get('user_role'));
				if($roles_title == '1'){
					$user_has_roles->title = "Admin";
				}
				elseif ($roles_title == '2') {
					$user_has_roles->title = "Main Section";
				}
				elseif ($roles_title == '3') {
					$user_has_roles->title = "Primary Section";
				}
				elseif ($roles_title == '4') {
					$user_has_roles->title = "Daily Section";
				}
				elseif ($roles_title == '5') {
					$user_has_roles->title = "Exam Section";
				}
				elseif ($roles_title == '6') {
					$user_has_roles->title = "Fee Section";
				}
				elseif ($roles_title == '7') {
					$user_has_roles->title = "Notice Section";
				}
				else{
					$user_has_roles->title = "Event Section";
				}

				$user_has_roles->user_level = (Input::get('user_role'));
				$user_has_roles->is_active = '1';
				$user_has_roles->created_by = $user_id;
			    $user_has_roles->created_at = $current_date;
				$user_has_roles->save();
				
				$this->request->session()->flash('alert-success', 'Role created successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'Role could not add!');
			}
			
		return back()->withInput();
		}
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function export()
	{
		$teachers = User::where('is_admin','=',6)->get();
		$subjects = Subject::where('is_active','=',True)->orderBy('class_id','ASC')->get();
		$classees = Tclass::where('is_active','=',True)->orderBy('id','ASC')->get();
		return view('admin.teacher_export', compact(['teachers','subjects','classees']));
	}

}
