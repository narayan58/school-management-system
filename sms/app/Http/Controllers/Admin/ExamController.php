<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\User;
use App\Exam;
use App\MarkSheet;
use App\Class_has_exam;
use App\Student_has_info;
use App\User_has_level;
use App\Helper\Helper;
use App\Tclass;
use App\Section;
use App\Shift;
use Validator;
use View;

class ExamController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '5') || ($user_filter_org == '1'))
			{

		        $classes = Tclass::where('is_active','=', True)->get();
		        $exams = Exam::where('is_active','=', True)
		        						->orderBy('id', 'DESC')//updated with current time 
		        						->get();
				return view('admin.exam', compact(['classes','exams','user_levels']));	
			}
		else
			abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'name' => 'required|unique:exams',
            'class_id' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$data = Input::all();
			$exam = new Exam;
				$exam->name = $data['name'];
				$exam->slug = $this->helper->slug_converter($exam->name);
				$exam->is_active = '1';
				$exam->created_by = $user_id;
			if ($exam->save()){
				$count = count($data['class_id']);
			for ($i=0; $i < ($count); $i++) { 
				$class_has_exams = new Class_has_exam;
				$class_has_exams->exam_id = $exam->id;
				$class_has_exams->class_id = $data['class_id'][$i];
				$class_has_exams->is_active = '1';
				$class_has_exams->created_by = $user_id;
				$class_has_exams->created_at = $current_date;

				if($class_has_exams->save()){
					$this->request->session()->flash('alert-success', 'Exam saved successfully!');
				}
				else{
				$this->request->session()->flash('alert-warning', 'Exam could not add!');
				}
			}
		}
			return redirect('home/exam');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$exam = Exam::find($id);
		return Response::json($exam);
	
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'name' => 'required',
            'class_id' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$edit_id = Input::get('exam_id');
			$exam = Exam::find($edit_id);
				$exam->name =(Input::get('name'));
				$exam->slug = $this->helper->slug_converter($exam->name);
				$exam->is_active = '1';
				$exam->created_by = $user_id;
			if ($exam->save()){
				$exam_ids = $exam->id;
				$class_has_exams = Class_has_exam::where('exam_id',$exam_ids)->delete();
				$data = Input::all();
				$count = count($data['class_id']);
			for ($i=0; $i < ($count); $i++) { 
				$class_has_exams = new Class_has_exam;
				$class_has_exams->exam_id = $exam->id;
				$class_has_exams->class_id = $data['class_id'][$i];
				$class_has_exams->is_active = '1';
				$class_has_exams->created_by = $user_id;
				$class_has_exams->created_at = $current_date;

				if($class_has_exams->save()){
					$this->request->session()->flash('alert-info', 'Exam saved successfully!');
				}
				else{
				$this->request->session()->flash('alert-warning', 'Exam could not add!');
				}
			}
		}
			return redirect('home/exam');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete()
	{
		$id = Input::get('id');
		$exam = Exam::find($id);
		$exam_class = Class_has_exam::where('exam_id',$id)->delete();

		if($exam->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}

	public function export()
	{
		$classes = Tclass::where('is_active','=', True)->get();
        $exams = Exam::where('is_active','=', True)->get();
		return view('admin.exam_export', compact(['classes','exams']));
	}

	public function examdetail()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
	        /*$classes = Tclass::where('is_active','=', True)->get();*/
	        $class_list= Tclass::where('is_active','=', True)->lists('name','id');
			$section_list= Section::where('is_active','=', True)->lists('name','id');
			$shift_list= Shift::where('is_active','=', True)->lists('name','id');
	        $latestMarks = MarkSheet::orderBy('id', 'DESC')//updated with current time 
	        						->groupBy('student_id','exam_id')
	        						->get();
			return view('admin.student_exam_info', compact(['class_list','latestMarks','user_levels','section_list','shift_list']));	
		}
	}
	
	public function examdetailStudent($email, $exam_id){
		if (Auth::check()) 
		{
		$org_user_id = Auth::user()->id;
		$class_ids=['email'=>$email];
		 try{
				$current_class = User::where($class_ids)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
		$user_id=$current_class['id'];
		$exam_ids=['slug'=>$exam_id];
        try{
			 $current_exam = Exam::where($exam_ids)->firstOrFail();
		} catch(ModelNotFoundException $ex){
			abort(404);
		}
		$exam_id = $current_exam['id'];
		$exam_name = $current_exam['name'];
		$studentmarksheets = MarkSheet::where('student_id',$user_id)
							->where('exam_id', $exam_id)
			                ->orderBy('id', 'DESC')//updated with current time 
							->get();

		//for heading
		$student_details=Student_has_info::where('user_id',$user_id)->firstOrFail();
			$student_name = $student_details->studentinfo->name;
			$class_name= $student_details->studentclass->name; 
			$class_section= $student_details->studentsection->name; 
			$class_shift= $student_details->studentshift->name;
			$message = "Duplicate Marksheet";
			$user_levels = User::where('id', $org_user_id)->get();
			return view('admin.student_exam_detail_admin', compact(['user_levels','student_class_info','section_details',
								'exam_shift','class_section','class_shift','class_name',
								'studentmarksheets','student_name','message','exam_name']));
		}
	}
	public function gradedetailStudent($email, $exam_id){
		if (Auth::check()) 
		{
		$org_user_id = Auth::user()->id;
		$class_ids=['email'=>$email];
		 try{
				$current_class = User::where($class_ids)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
		$user_id=$current_class['id'];
		$exam_ids=['slug'=>$exam_id];
        try{
			 $current_exam = Exam::where($exam_ids)->firstOrFail();
		} catch(ModelNotFoundException $ex){
			abort(404);
		}
		$exam_id = $current_exam['id'];
		$exam_name = $current_exam['name'];
		$studentmarksheets = MarkSheet::where('student_id',$user_id)
							->where('exam_id', $exam_id)
			                ->orderBy('id', 'DESC')//updated with current time 
							->get();

		//for heading
		$student_details=Student_has_info::where('user_id',$user_id)->firstOrFail();
			$student_name = $student_details->studentinfo->name;
			$class_name= $student_details->studentclass->name; 
			$class_section= $student_details->studentsection->name; 
			$class_shift= $student_details->studentshift->name;
			$message = "Duplicate Marksheet";
			$user_levels = User::where('id', $org_user_id)->get();
			return view('admin.student_grade_detail_admin', compact(['user_levels','student_class_info','section_details',
								'exam_shift','class_section','class_shift','class_name',
								'studentmarksheets','student_name','message','exam_name']));
		}
	}

	public function exam_search()
	{
		$parammeters = $_GET['parameters'];
		// var_dump($parammeters); die;
        if(is_array($parammeters))
        {
            $params=$parammeters;
        }
        else{
            $params = json_decode($parammeters);
        }

        foreach ($params as $key => $value) {
            $each_param[$key] = $value;
        }
        foreach ($each_param as $key => $value) {
            ${$key} = $value;
        }

        $class_id=$class_id;
        $section_id=$section_id;
        $shift_id=$shift_id;
        $query1 = MarkSheet::orderBy('id', 'DESC');
        				
		if($class_id!=0){
			$queryf=$query1->whereHas('getStudentClass', function($query) use ($class_id) {
									        $query->where('class_id',$class_id);
									  });	
		}
		if($section_id!=0){
			$queryf=$query1->whereHas('getStudentSection', function($query) use ($section_id) {
									        $query->where('section_id',$section_id);
									  });
		}
			
		if($shift_id!=0){

			$queryf=$query1->whereHas('getStudentShift', function($query) use ($shift_id) {
									        $query->where('shift_id',$shift_id);
									  });	
		}

		 $latestMarks=$queryf->get();
		if(is_array($parammeters))
        {
        	//$this part is not used
        }
        else{
        	return view('admin.student_exam_ajax',compact('latestMarks'));
        }
	}

	public function getStudentMarkStudent(){
		$student_id = Input::get('student_id');
		$student_name = User::where('id',$student_id)->firstOrFail();
		$student_org_name = $student_name['name'];
		$exam_id = Input::get('exam_id');
		$exam_ids=['user_id'=>$student_id,];
		$studentsubs = Student_has_info::where('user_id',$student_id)->firstOrFail();
		$student_org_id = $studentsubs['class_id'];
		$studentMarkSheet = Marksheet::where('student_id',$student_id)
								->where('class_id', $student_org_id)
								->where('exam_id', $exam_id)
								->get();
		return View::make('admin.studentSubMarkUpdateView', compact(['studentMarkSheet','student_id','exam_id','student_org_name']));
	}

	public function unpublish(){
		$student_id = Input::get('stid');
		$exam_id = Input::get('id');
		$active = Input::get('active');
		$mark_unpublish_count = Marksheet::where('student_id', $student_id)
									->where('exam_id', $exam_id)
									->get();
		foreach ($mark_unpublish_count as $munpub) {
			$id['id'] = $munpub->id;
			$class_has_exams = Marksheet::find($id['id']);
			if($active){
				$class_has_exams->is_active = '0';
			}
			else{
				$class_has_exams->is_active = '1';
			}
			if($class_has_exams->save()){
			$response = array(
				'status' => 'success',
				'msg' => 'Status changed successful',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
			}
		}
		return Response::json($response);
	}

}
