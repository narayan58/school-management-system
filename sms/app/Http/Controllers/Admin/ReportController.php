<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Salary;
use App\Salary_total;
use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\Helper\Helper;
use App\User;
use App\Teacher_has_info;
use App\Class_has_section;
use App\Class_has_shift;
use App\Subject;
use App\Section;
use App\Tclass;
use App\Shift;
use App\Period;
use App\Topic;
use App\SubTopic;
use App\TeacherRecord;
use App\User_has_level;
use Validator;
use DB;
use App\Fee_total;
use App\Fee;
use App\Month;
use App\Fee_has_month;
use App\Miscellaneous;


class ReportController extends Controller
{
    public function __construct(Request $request, Helper $helper)
    {
        $this->request = $request;
        $this->helper = $helper;
    }

    public function teachersReport()
    {
        if (Auth::check()) {
            date_default_timezone_set('Asia/Kathmandu');
            // echo $search_date=Input::get('subtopic_date'); die;
            $current_date = date("Y-m-d");
            $user_id = Auth::user()->id;
            $user_levels = User::where('id', $user_id)->get();
            $user_levels_restriction = $user_levels[0]['id'];
            $user_filter = User_has_level::where('user_id', $user_levels_restriction)->get();
            $user_filter_org = $user_filter[0]['user_level'];
            $class_list = Tclass::where('is_active', '=', True)->lists('name', 'id');
            $teachers_list = User::where('is_admin', '6')->lists('name', 'id');
            // prevent user level on url typing
            if (($user_filter_org == '4') || ($user_filter_org == '1')) {
                $teacher_records = TeacherRecord::where('lecture_date', $current_date)
                    ->orderBy('class_id', 'ASC')
                    ->get();
                return view('report.teachers_record', compact(['user_levels', 'teacher_records', 'class_list', 'teachers_list']));
            } else
                abort(404);
        }

    }

    public function getTeachersReport()
    {
        $parammeters = $_GET['parameters'];
        // var_dump($parammeters); die;
        if (is_array($parammeters)) {
            $params = $parammeters;
        } else {
            $params = json_decode($parammeters);
        }

        foreach ($params as $key => $value) {
            $each_param[$key] = $value;
        }
        foreach ($each_param as $key => $value) {

            ${$key} = $value;

        }
        $teacher_id = $teacher_id;
        $class_id = $class_id;
        if (Auth::check()) {
            date_default_timezone_set('Asia/Kathmandu');
            // echo $search_date=Input::get('subtopic_date'); die;
            $current_date = date("Y-m-d");
            $user_id = Auth::user()->id;
            $user_levels = User::where('id', $user_id)->get();
            $user_levels_restriction = $user_levels[0]['id'];
            $user_filter = User_has_level::where('user_id', $user_levels_restriction)->get();
            $user_filter_org = $user_filter[0]['user_level'];
            if (($user_filter_org == '4') || ($user_filter_org == '1')) {
                $subject_hour=Subject::lists('subject_hour','id');
                $query = TeacherRecord::orderBy('class_id');
                $query=$query->selectRaw('*, sum(lecture_hr) as added_lecture');
                if ($class_id != 0)
                    $query = $query->where('class_id', $class_id);
                if ($teacher_id != 0)
                    $query = $query->where('teacher_id', $teacher_id);
                $teacher_records = $query->groupBy('shift_id','section_id','class_id','subtopic_id')->get();
//                return($teacher_records);
                $count_teacher_record=$query->count();
                return view('report.teachers_record_ajax', compact(['user_levels', 'teacher_records','count_teacher_record','subject_hour']));
            }
            else
                abort(404);
        }

    }
    public function teachers_export()
    {
        $parammeters = $_GET['parameters'];
        // var_dump($parammeters); die;
        if (is_array($parammeters)) {
            $params = $parammeters;
        } else {
            $params = json_decode($parammeters);
        }

        foreach ($params as $key => $value) {
            $each_param[$key] = $value;
        }
        foreach ($each_param as $key => $value) {

            ${$key} = $value;

        }
        $teacher_id = $teacher_id;
        $class_id = $class_id;
        if (Auth::check()) {
            date_default_timezone_set('Asia/Kathmandu');
            // echo $search_date=Input::get('subtopic_date'); die;
            $current_date = date("Y-m-d");
            $user_id = Auth::user()->id;
            $user_levels = User::where('id', $user_id)->get();
            $user_levels_restriction = $user_levels[0]['id'];
            $user_filter = User_has_level::where('user_id', $user_levels_restriction)->get();
            $user_filter_org = $user_filter[0]['user_level'];
            if (($user_filter_org == '4') || ($user_filter_org == '1')) {
                $subject_hour=Subject::lists('subject_hour','id');
                $query = TeacherRecord::orderBy('class_id');
                $query=$query->selectRaw('*, sum(lecture_hr) as added_lecture');
                if ($class_id != 0)
                    $query = $query->where('class_id', $class_id);
                if ($teacher_id != 0)
                    $query = $query->where('teacher_id', $teacher_id);
                $teacher_records = $query->groupBy('shift_id','section_id','class_id','subtopic_id')->get();
//                return($teacher_records);
                $count_teacher_record=$query->count();
                return view('report.teachers_record_export', compact(['user_levels', 'teacher_records','count_teacher_record','subject_hour']));
            }
            else
                abort(404);
        }

    }


    public function feeReport()
    {

        if (Auth::check())
        {
            $user_id = Auth::user()->id;
            $user_levels = User::where('id', $user_id)->get();
            $class_list=Tclass::where('is_active','=', True)->lists('name','id');
            $section_list=Section::where('is_active','=', True)->lists('name','id');
            $shift_list=Shift::where('is_active','=', True)->lists('name','id');
//            $fee_totals = Fee_total::where('is_active','=', True)
//                ->orderBy('id', 'DESC')
//                ->get();
            return view('report.student_indv_record', compact(['fee_totals','user_levels','class_list','section_list','shift_list']));
        }
    }
    public function getFeeReport()
    {
        $parammeters = $_GET['parameters'];
        // var_dump($parammeters); die;
        if (is_array($parammeters)) {
            $params = $parammeters;
        } else {
            $params = json_decode($parammeters);
        }

        foreach ($params as $key => $value) {
            $each_param[$key] = $value;
        }
        foreach ($each_param as $key => $value) {

            ${$key} = $value;

        }
        $class_id = $class_id;
        $section_id = $section_id;
        $shift_id = $shift_id;
        $student_id = $student_id;
        if (Auth::check())
        {
            $user_id = Auth::user()->id;
            $user_levels = User::where('id', $user_id)->get();
            $class_list=Tclass::where('is_active','=', True)->lists('name','id');
            $section_list=Section::where('is_active','=', True)->lists('name','id');
            $shift_list=Shift::where('is_active','=', True)->lists('name','id');
//            $fee_totals = Fee_total::where('is_active','=', True)
//                ->orderBy('id', 'DESC')
//                ->get();
            $query = Fee::orderBy('id', 'DESC');
//            $query=$query->selectRaw('*, sum(amount) as total_paid_amount');
//            if ($class_id != 0 && $class_id != '')
//                $query = $query->where('class_id', $class_id);
//
//            if ($section_id != 0 && $section_id != '')
//                $query = $query->where('section_id', $section_id);
//
//            if ($shift_id != 0 && $shift_id != '')
//                $query = $query->where('shift_id', $shift_id);

            if ($student_id != 0 && $student_id != ''){
                $query = $query->where('student_id', $student_id);
                $fee_totals=$query->groupBy('id')->get();
            }
            else{
                $fee_totals=array();

            }
            $record_count=count($fee_totals);

            return view('report.student_indv_record_ajax', compact(['fee_totals','user_levels','class_list','section_list','shift_list','record_count']));
        }
    }
    public function student_indv_export()
    {
        $parammeters = $_GET['parameters'];
        // var_dump($parammeters); die;
        if (is_array($parammeters)) {
            $params = $parammeters;
        } else {
            $params = json_decode($parammeters);
        }

        foreach ($params as $key => $value) {
            $each_param[$key] = $value;
        }
        foreach ($each_param as $key => $value) {

            ${$key} = $value;

        }
        $class_id = $class_id;
        $section_id = $section_id;
        $shift_id = $shift_id;
        $student_id = $student_id;
        if (Auth::check())
        {
            $user_id = Auth::user()->id;
            $user_levels = User::where('id', $user_id)->get();
            $class_list=Tclass::where('is_active','=', True)->lists('name','id');
            $section_list=Section::where('is_active','=', True)->lists('name','id');
            $shift_list=Shift::where('is_active','=', True)->lists('name','id');
//            $fee_totals = Fee_total::where('is_active','=', True)
//                ->orderBy('id', 'DESC')
//                ->get();
            $query = Fee::orderBy('id', 'DESC');
//            $query=$query->selectRaw('*, sum(amount) as total_paid_amount');
//            if ($class_id != 0 && $class_id != '')
//                $query = $query->where('class_id', $class_id);
//
//            if ($section_id != 0 && $section_id != '')
//                $query = $query->where('section_id', $section_id);
//
//            if ($shift_id != 0 && $shift_id != '')
//                $query = $query->where('shift_id', $shift_id);

            if ($student_id != 0 && $student_id != ''){
                $query = $query->where('student_id', $student_id);
                $fee_totals=$query->groupBy('id')->get();
            }
            else{
                $fee_totals=array();

            }

            return view('report.student_indv_record_export', compact(['fee_totals']));
        }
    }
    public function financeReport(){
        if (Auth::check())
        {
            $user_id = Auth::user()->id;
            $user_levels = User::where('id', $user_id)->get();
            $month_list = Month::lists('name','id');
//            dd($month_list);
            return view('report.finance_record', compact(['fee_totals','user_levels','month_list','section_list','shift_list']));
        }

    }
    public function getFinanceReport()
    {
        $parammeters = $_GET['parameters'];
        // var_dump($parammeters); die;
        if (is_array($parammeters)) {
            $params = $parammeters;
        } else {
            $params = json_decode($parammeters);
        }

        foreach ($params as $key => $value) {
            $each_param[$key] = $value;
        }
        foreach ($each_param as $key => $value) {

            ${$key} = $value;

        }
//        $month_id = $month_id;
        $from_date = $from_date;
        $to_date = $to_date;
        if (Auth::check())
        {
            $user_id = Auth::user()->id;
            $user_levels = User::where('id', $user_id)->get();
            $fee_totals = Fee_total::orderBy('id', 'DESC')->whereBetween('created_at', [$from_date, $to_date])->get();
//            if ($month_id != 0 && $month_id != '')
//                $fee_totals = $fee_totals->where('date', $month_id);
//             if($from_date!='' && $to_date!=''){
//                 $fee_totals->whereBetween('created_at', [$from_date, $to_date]);
//                 var_dump($fee_totals);
//             }
//             else if($from_date!='' && $to_date==''){
//                 var_dump("2");
//                 $fee_totals->where('created_at','>=',$from_date);
//             }
//             else if($from_date=='' && $to_date!=''){
// var_dump("3");
//                 $fee_totals->where('created_at','<=',$to_date);

//             }

//            if ($from_date != 0 && $from_date != '')
//                $fee_totals = $fee_totals->where('created_at', $from_date);
//
//            if ($to_date != 0 && $to_date != '') {
//                $fee_totals = $fee_totals->where('created_at', $to_date);
//            }
            // $fee_totals = $fee_totals->get();
            // var_dump($fee_totals); die();
//            return $fee_totals;
            $fee_count=count($fee_totals);

            $salary_totals = Salary_total::orderBy('id', 'DESC')->whereBetween('created_at', [$from_date, $to_date])->get();

//            if ($month_id != 0 && $month_id != '')
//                $query_salary = $query_salary->where('date', $month_id);
//
//            if ($from_date != 0 && $from_date != '')
//                $query_salary = $query_salary->where('created_at', $from_date);
//
//            if ($to_date != 0 && $to_date != '') {
//                $query_salary = $query_salary->where('created_at', $to_date);
//            }
            // if($from_date!='' && $to_date!=''){
            //     $query_salary->whereBetween('created_at', [$from_date, $to_date]);
            // }
            // else if($from_date!='' && $to_date==''){
            //     $query_salary->where('created_at','>=',$from_date);
            // }
            // else if($from_date=='' && $to_date!=''){

            //     $query_salary->where('created_at','<=',$to_date);

            // }
            // $salary_totals = $query_salary->get();
            $salary_count=count($salary_totals);


            $misc_totals= Miscellaneous::orderBy('id', 'DESC')->whereBetween('created_at', [$from_date, $to_date])->get();

            // if($from_date!='' && $to_date!=''){
            //     $query_misc->whereBetween('created_at', [$from_date, $to_date]);
            // }
            // else if($from_date!='' && $to_date==''){
            //     $query_misc->where('created_at','>=',$from_date);
            // }
            // else if($from_date=='' && $to_date!=''){

            //     $query_misc->where('created_at','<=',$to_date);

            // }
            // $misc_totals = $query_misc->get();
            $misc_count=count($misc_totals);
            return view('report.finance_record_ajax', compact(['fee_totals','salary_totals','misc_totals','user_levels','class_list','section_list','shift_list','fee_count','salary_count','misc_count']));
        }
    }
    public function finance_record_export(){
        $parammeters = $_GET['parameters'];
        // var_dump($parammeters); die;
        if (is_array($parammeters)) {
            $params = $parammeters;
        } else {
            $params = json_decode($parammeters);
        }

        foreach ($params as $key => $value) {
            $each_param[$key] = $value;
        }
        foreach ($each_param as $key => $value) {

            ${$key} = $value;

        }
//        $month_id = $month_id;
        $from_date = $from_date;
        $to_date = $to_date;
        if (Auth::check())
        {
            $user_id = Auth::user()->id;
            $user_levels = User::where('id', $user_id)->get();

            $fee_totals = Fee::orderBy('id', 'DESC');

//            if ($month_id != 0 && $month_id != '')
//                $query_fee = $query_fee->where('date', $month_id);
            if($from_date!='' && $to_date!=''){
                $query_fee->whereBetween('created_at', [$from_date, $to_date]);
            }
            else if($from_date!='' && $to_date==''){
                $query_fee->where('created_at','>=',$from_date);
            }
            else if($from_date=='' && $to_date!=''){

                $query_fee->where('created_at','<=',$to_date);

            }

//            if ($from_date != 0 && $from_date != '')
//                $query_fee = $query_fee->where('created_at', $from_date);
//
//            if ($to_date != 0 && $to_date != '') {
//                $query_fee = $query_fee->where('created_at', $to_date);
//            }
            $fee_totals = $query_fee->get();
//            return $fee_totals;
            $fee_count=count($fee_totals);

            $query_salary = Salary::orderBy('id', 'DESC');

//            if ($month_id != 0 && $month_id != '')
//                $query_salary = $query_salary->where('date', $month_id);
//
//            if ($from_date != 0 && $from_date != '')
//                $query_salary = $query_salary->where('created_at', $from_date);
//
//            if ($to_date != 0 && $to_date != '') {
//                $query_salary = $query_salary->where('created_at', $to_date);
//            }
            if($from_date!='' && $to_date!=''){
                $query_salary->whereBetween('created_at', [$from_date, $to_date]);
            }
            else if($from_date!='' && $to_date==''){
                $query_salary->where('created_at','>=',$from_date);
            }
            else if($from_date=='' && $to_date!=''){

                $query_salary->where('created_at','<=',$to_date);

            }
            $salary_totals = $query_salary->get();
            $salary_count=count($salary_totals);


            $query_misc= Miscellaneous::orderBy('id', 'DESC');

            if($from_date!='' && $to_date!=''){
                $query_misc->whereBetween('created_at', [$from_date, $to_date]);
            }
            else if($from_date!='' && $to_date==''){
                $query_misc->where('created_at','>=',$from_date);
            }
            else if($from_date=='' && $to_date!=''){

                $query_misc->where('created_at','<=',$to_date);

            }
            $misc_totals = $query_misc->get();
            $misc_count=count($misc_totals);
            return view('report.finance_record_export', compact(['fee_totals','salary_totals','misc_totals','user_levels','class_list','section_list','shift_list','fee_count','salary_count','misc_count']));
        }
    }



}
