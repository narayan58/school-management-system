<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\Helper\Helper;
use App\Student;
use App\User;
use App\User_has_level;
use App\Tclass;
use App\Student_has_info;
use App\Class_has_section;
use App\Class_has_shift;
use App\Class_has_exam;
use App\Section;
use App\Shift;
use App\Subject;
use App\Exam;
use App\MarkSheet;
use App\Fee_total;
use Validator;
use File;
use DB;

class StudentController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper = $helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '3') || ($user_filter_org == '1')){
				$sections = Section::where('is_active','=', True)->get();
				$classees = Tclass::where('is_active','=', True)->get();
				$shifts = Shift::where('is_active','=', True)->get();
				$students = Student_has_info::where('is_active','=', True)
							->whereHas('studentinfo', function($query) {
											        $query->where('is_admin', '=',5);
											  })
							->orderBy('id', 'DESC')//updated with current time
							->get();
				$class_list=Tclass::where('is_active','=', True)->lists('name','id');
                
				$section_list=Section::where('is_active','=', True)->lists('name','id');
				$shift_list=Shift::where('is_active','=', True)->lists('name','id');
				return view('admin.student', compact(['students','sections','classees','shifts','user_levels','class_list','section_list','shift_list']));
			}
			else
				abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'name' => 'required',
            'address' => 'required',
            'roll_no' => 'required|unique:users,email',
            'date_of_birth' => 'required',
            'telephone_no' => 'required',
            'mobile_no' => 'required',
            'image' => 'required',
            'class_id' => 'required',
            'section_id' => 'required',
            'shift_id' => 'required',

        );
        
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
    if (Auth::check()) {
    	date_default_timezone_set('Asia/Kathmandu'); 
		$current_date = date("Y-m-d h:i:s");
		$user_id = Auth::user()->id;
		$students = new User;
		$image = Input::file('image');
		$destinationPath = 'image/student'; // upload path
		if (!file_exists($destinationPath)) {
			    mkdir($destinationPath, 0777, true);
			}
		$extension = $image->getClientOriginalExtension(); // getting image extension
		$fileName = md5(mt_rand()).'.'.$extension; // renameing image
		$image->move($destinationPath, $fileName);
		$file_path = $destinationPath.'/'.$fileName;
		$students->image_encrypt = $fileName;
		$students->image = $image->getClientOriginalName();
		$mid = 'student';
		// $student->mime = $image->getClientMimeType();
			$students->name = (Input::get('name'));
			$students->address = (Input::get('address'));
			$students->email = (Input::get('roll_no'));
			$students->password = bcrypt(Input::get('date_of_birth'));
			$students->gender = (Input::get('gender'));
			$students->dob = (Input::get('date_of_birth'));
			$students->telephone_no = (Input::get('telephone_no'));
			$students->mobile_no = (Input::get('mobile_no'));
			$students->confirmation_code = 's';
			$students->confirmed = '1';
			$students->is_admin = '5';
			$students->created_by = $user_id;
			$students->created_at = $current_date;
			$this->helper->imageResize($file_path, $fileName, $extension, $width=400, $height=300,$mid );
			if ($students->save()){
				$student_has_infos = new Student_has_info;
				$student_has_infos->class_id = (Input::get('class_id'));
				$student_has_infos->section_id = (Input::get('section_id'));
				$student_has_infos->shift_id = (Input::get('shift_id'));
				$student_has_infos->user_id = $students->id;
				$student_has_infos->is_active = '1';
				$student_has_infos->created_by = $user_id;
			    $student_has_infos->created_at = $current_date;
				$student_has_infos->save();
				$this->request->session()->flash('alert-success', 'Student saved successfully!');
			}
			else{
				$this->request->session()->flash('alert-warning', 'Student could not add!');
			}
			
		return back()->withInput();
		}
	}

	public function getStudent($class_slug){
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			try
			{
				$classes = Tclass::where('slug',$class_slug)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
			$class_org_id = $classes['id'];
			$class_org_name = $classes['name'];
			$studentLists = Student_has_info::where('class_id', $class_org_id)->get();
			return view('admin.student_class_list', compact(['studentLists','class_org_name','user_levels']));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$student =User::find($id);
//        var_dump($student->studenthasclass); die;
        $class_id=$student->studenthasclass->class_id;
        $classees = Tclass::where('is_active','=', True)->get();
        $sections = Class_has_section::where('class_id', $class_id)->get();
        $shifts = Class_has_shift::where('class_id', $class_id)->get();
        return view('admin.students.student_edit',compact('student','classees','sections','shifts'));

//		return Response::json($student);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'name' => 'required',
            'address' => 'required',
            'roll_no' => 'required',
            'date_of_birth' => 'required',
            'telephone_no' => 'required',
            'mobile_no' => 'required',
            'old_image' => 'required',
            'class_id' => 'required',
            'section_id' => 'required',
            'shift_id' => 'required',

        );
        
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
    if (Auth::check()) {
    	$student_id = (Input::get('student_id'));
    	date_default_timezone_set('Asia/Kathmandu'); 
		$current_date = date("Y-m-d h:i:s");
		$user_id = Auth::user()->id;
		$students = User::find($student_id);
		$image = Input::file('new_image');
		if($image != ""){
		$mid = 'student';
			$destinationPath = 'image/student'; // upload path
			if (!file_exists($destinationPath)) {
				    mkdir($destinationPath, 0777, true);
				}
			$extension = $image->getClientOriginalExtension(); // getting image extension
			$fileName = md5(mt_rand()).'.'.$extension; // renameing image
			$image->move($destinationPath, $fileName);
			$file_path = $destinationPath.'/'.$fileName;
			$students->image_encrypt = $fileName;
			$students->image = $image->getClientOriginalName();
			$this->helper->imageResize($file_path, $fileName, $extension, $width=400, $height=300,$mid );
		}
		// $student->mime = $image->getClientMimeType();
			$students->name = (Input::get('name'));
			$students->address = (Input::get('address'));
			$students->email = (Input::get('roll_no'));
			$students->password = bcrypt(Input::get('date_of_birth'));
			$students->gender = (Input::get('gender'));
			$students->dob = (Input::get('date_of_birth'));
			$students->telephone_no = (Input::get('telephone_no'));
			$students->mobile_no = (Input::get('mobile_no'));
			$students->confirmation_code = 's';
			$students->confirmed = '1';
			$students->is_admin = '5';
			$students->created_by = $user_id;
			$students->created_at = $current_date;
			if ($students->save()){
				$student_has_infos = Student_has_info::where('user_id',$students->id)->get();
				$stu_info = $student_has_infos[0]['id'];
				$student_has_infos_edit = Student_has_info::find($stu_info);
				$student_has_infos_edit->class_id = (Input::get('class_id'));
				$student_has_infos_edit->section_id = (Input::get('section_id'));
				$student_has_infos_edit->shift_id = (Input::get('shift_id'));
				$student_has_infos_edit->user_id = $students->id;
				$student_has_infos_edit->is_active = '1';
				$student_has_infos_edit->created_by = $user_id;
			    $student_has_infos_edit->created_at = $current_date;
				$student_has_infos_edit->save();
				$this->request->session()->flash('alert-success', 'Student saved successfully!');
			}
			else{
				$this->request->session()->flash('alert-warning', 'Student could not add!');
			}
			
		return back()->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete(){
		$id = Input::get('id');
		$student_has_infos = Student_has_info::where('user_id',$id)->delete();
		$student = User::find($id);
		$destinationPath = 'image/student';
		$destinationPath_thumb = 'image/thumb/student';
		$file_path = $destinationPath.'/'.$student->image_encrypt;
		$file_path_thumb = $destinationPath_thumb.'/'.$student->image_encrypt;
		File::delete($file_path,$file_path_thumb);
		if($student->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}

	public function getSectionOnStudent($class_id){
		$sections = Class_has_section::with('getSectionClass')->where('class_id', $class_id)->get();
		return Response::json($sections);
	}

	public function getShiftOnStudent($class_id){
		$sections = Class_has_shift::with('getShiftClass')->where('class_id', $class_id)->get();
		return Response::json($sections);
	}
	public function getSubjectOnStudent($class_id){
		$sections = Subject::with('subjectclass')->where('class_id', $class_id)->get();
		return Response::json($sections);
	}
	public function export()
	{
		$parammeters = $_GET['parameters'];
		// var_dump($parammeters); die;
        if(is_array($parammeters))
        {
            $params=$parammeters;
        }
        else{
            $params = json_decode($parammeters);
        }

        foreach ($params as $key => $value) {
            $each_param[$key] = $value;
        }
        foreach ($each_param as $key => $value) {

            ${$key} = $value;

        }
        $class_id=$class_id;
        $section_id=$section_id;
        $shift_id=$shift_id;
        $query=Student_has_info::where('is_active','=', True);
        $query=$query->whereHas('studentinfo', function($query) {
									        $query->where('is_admin', '=',5);
									  });
		if($class_id!=0)
			$query=$query->where('class_id',$class_id);		
		if($section_id!=0)
			$query=$query->where('section_id',$section_id);	
		if($shift_id!=0)
			$query=$query->where('shift_id',$shift_id);	
		 $students=$query->get();

		return view('admin.student_export', compact(['students','sections','classees','shifts']));
	}
	
	
   public function student_record()
	{
		$parammeters = $_GET['parameters'];
		// var_dump($parammeters); die;
        if(is_array($parammeters))
        {
            $params=$parammeters;
        }
        else{
            $params = json_decode($parammeters);
        }

        foreach ($params as $key => $value) {
            $each_param[$key] = $value;
        }
        foreach ($each_param as $key => $value) {

            ${$key} = $value;

        }
        $class_id=$class_id;
        $section_id=$section_id;
        $shift_id=$shift_id;
        $query=Student_has_info::where('is_active','=', True);
        $query=$query->whereHas('studentinfo', function($query) {
									        $query->where('is_admin', '=',5);
									  });
		if($class_id!=0)
			$query=$query->where('class_id',$class_id);		
		if($section_id!=0)
			$query=$query->where('section_id',$section_id);	
		if($shift_id!=0)
			$query=$query->where('shift_id',$shift_id);	
		 $students=$query->get();
		
		if(is_array($parammeters))
        {
        	//$this part is not used

        }
        else{
        	return view('admin.students.student_record',compact('students'));

        }
	}

	public function getStudentInfo($id)
	{
		if (Auth::check()) 
		{
		$class_ids=['email'=>$id];
		 try{
				$current_class = User::where($class_ids)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
		$user_id=$current_class['id'];
		$user_class=Student_has_info::where('user_id',$user_id)->firstOrFail();
		$real_class=$user_class->class_id; 
		$exam_details=Class_has_exam::where('class_id',$real_class)
			->orderBy('id', 'DESC')//updated with current time 
		    ->get();
		$fee_details = Fee_total::where('student_id',$user_id)
			->orderBy('id', 'DESC')//updated with current time 
		    ->get();
		$students_details=Student_has_info::where('user_id',$user_id)->firstOrFail();
			$class_name= $students_details->studentclass->name; 
			$class_section= $students_details->studentsection->name; 
			$class_shift= $students_details->studentshift->name; 
			$org_user_id = Auth::user()->id;
			$user_levels = User::where('id', $org_user_id)->get();
		return view('admin.student_exam_fee_info', compact(['exam_details','fee_details','class_name','class_section','class_shift','user_levels','id']));
		}
	}

	public function getStudentExamInfo($email, $exam_id){
		if (Auth::check()) 
		{
		$org_user_id = Auth::user()->id;
		$class_ids=['email'=>$email];
		 try{
				$current_class = User::where($class_ids)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
		$user_id=$current_class['id'];
		$exam_ids=['slug'=>$exam_id];
        try{
			 $current_exam = Exam::where($exam_ids)->firstOrFail();
		} catch(ModelNotFoundException $ex){
			abort(404);
		}
		$exam_id = $current_exam['id'];
		$exam_name = $current_exam['name'];
		$studentmarksheets = MarkSheet::where('student_id',$user_id)
							->where('exam_id', $exam_id)
			                ->orderBy('id', 'DESC')//updated with current time 
							->get();

		//for heading
		$student_details=Student_has_info::where('user_id',$user_id)->firstOrFail();
			$student_name = $student_details->studentinfo->name;
			$class_name= $student_details->studentclass->name; 
			$class_section= $student_details->studentsection->name; 
			$class_shift= $student_details->studentshift->name;
			$message = "Duplicate Marksheet";
		$user_levels = User::where('id', $org_user_id)->get();
			return view('admin.student_exam_detail', compact(['exam_name','student_name','message','user_levels','student_class_info','section_details','exam_shift','class_section','class_shift','class_name','studentmarksheets']));
		}
	}
	public function getStudentGradeInfo($email, $exam_id){
		if (Auth::check()) 
		{
		$org_user_id = Auth::user()->id;
		$class_ids=['email'=>$email];
		 try{
				$current_class = User::where($class_ids)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
		$user_id=$current_class['id'];
		$exam_ids=['slug'=>$exam_id];
        try{
			 $current_exam = Exam::where($exam_ids)->firstOrFail();
		} catch(ModelNotFoundException $ex){
			abort(404);
		}
		$exam_id = $current_exam['id'];
		$exam_name = $current_exam['name'];
		$studentmarksheets = MarkSheet::where('student_id',$user_id)
							->where('exam_id', $exam_id)
			                ->orderBy('id', 'DESC')//updated with current time 
							->get();

		//for heading
		$student_details=Student_has_info::where('user_id',$user_id)->firstOrFail();
			$student_name = $student_details->studentinfo->name;
			$class_name= $student_details->studentclass->name; 
			$class_section= $student_details->studentsection->name; 
			$class_shift= $student_details->studentshift->name;
			$message = "Duplicate Marksheet";
		$user_levels = User::where('id', $org_user_id)->get();
			return view('admin.student_exam_detail_grade', compact(['exam_name','student_name','message','user_levels','student_class_info','section_details','exam_shift','class_section','class_shift','class_name','studentmarksheets']));
		}
	}
	public function getStudentFeeInfo($email, $bill_id){
		if (Auth::check()) 
		{
			$org_user_id = Auth::user()->id;
			$class_ids=['email'=>$email];
		 try{
				$current_class = User::where($class_ids)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
		$user_id=$current_class['id'];
			$message = "Copied bill";
			$student_details = Fee_total::where('bill_id',$bill_id)->get();
			$students_details=Student_has_info::where('user_id',$user_id)->firstOrFail();
				$class_name= $students_details->studentclass->name; 
				$class_section= $students_details->studentsection->name; 
				$class_shift= $students_details->studentshift->name;
			$user_levels = User::where('id', $org_user_id)->get(); 
			return view('admin.student_fee_detail',compact(['user_levels','message','student_details','class_name','class_section','class_shift']));
		}
	}



}
