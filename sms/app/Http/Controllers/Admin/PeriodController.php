<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\Helper\Helper;
use App\Period;
use App\User_has_level;
use App\User;
use Validator;
use DB;

class PeriodController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper = $helper;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '2') || ($user_filter_org == '1')){
			$periods = Period::where('is_active','=', True)
								->orderBy('id', 'DESC')//updated with current time
								->get();
			return view('admin.period', compact(['periods','user_levels']));
		}
		else
			abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'name' => 'required|unique:periods'
            
        );
		$validator = Validator::make(Input::all(), $rules);
				if ($validator->fails()) {
				return back()->withInput()
				->withErrors($validator)
				->withInput();
			}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$period = new Period;
			$period->name = (Input::get('name'));
			$period->slug = $this->helper->slug_converter($period->name);
			$period->is_active = '1';
			$period->created_by = $user_id;
			$period->created_at = $current_date;
			if ($period->save()){
					$this->request->session()->flash('alert-success', 'Period saved successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'Period could not add!');
			}
			return redirect('home/period');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		$period = Period::find($id);
		return Response::json($period);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'name' => 'required|unique:periods'
            
        );
		$validator = Validator::make(Input::all(), $rules);
				if ($validator->fails()) {
				return back()->withInput()
				->withErrors($validator)
				->withInput();
			}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$edit_id = Input::get('period_id');
			$period = Period::find($edit_id);
			$period->name = (Input::get('name'));
			$period->slug = $this->helper->slug_converter($period->name);
			$period->is_active = '1';
			$period->updated_by = $user_id;
			$period->updated_at = $current_date;
			if ($period->save()){
					$this->request->session()->flash('alert-info', 'Period updated successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'Period could not update!');
			}
			return redirect('home/period');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete(){
		$id = Input::get('id');
		$period = Period::find($id);

		if($period->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}
	public function export()
	{
		$periods = Period::where('is_active','=', True)->get();
		return view('admin.period_export', compact(['periods']));
	}

}
