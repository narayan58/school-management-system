<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\Helper\Helper;
use App\User;
use App\User_has_level;
use App\Teacher_has_info;
use App\Teacher_has_first;
use App\Subject;
use App\Tclass;
use Validator;
use File;
use DB;
use App\Class_has_section;
use App\Class_has_shift;


class TeacherController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper = $helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '3') || ($user_filter_org == '1')){
			$teachers = User::where('is_admin','=',6)
								->orderBy('id', 'DESC')//updated with current time
								->get();
			$subject_all_lists = Subject::where('is_active','=',True)->orderBy('class_id','ASC')->get();
			$classees = Tclass::where('is_active','=',True)->orderBy('id','ASC')->get();
			return view('admin.teacher', compact(['teachers','subject_all_lists','classees','user_levels']));
		}
		else
				abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'name' => 'required',
            'address' => 'required',
            'email' => 'required|unique:users,email',
            'date_of_birth' => 'required',
            'telephone_no' => 'required',
            'mobile_no' => 'required',
            'image' => 'required'

        );
        
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
    if (Auth::check()) {
    	date_default_timezone_set('Asia/Kathmandu'); 
		$current_date = date("Y-m-d h:i:s");
	    $user_id = Auth::user()->id;
		$teachers = new User;
		$image = Input::file('image');
		$destinationPath = 'image/teacher'; // upload path
		if (!file_exists($destinationPath)) {
			    mkdir($destinationPath, 0777, true);
			}
		$extension = $image->getClientOriginalExtension(); // getting image extension
		$fileName = md5(mt_rand()).'.'.$extension; // renameing image
		$image->move($destinationPath, $fileName);
		$file_path = $destinationPath.'/'.$fileName;
		$teachers->image_encrypt = $fileName;
		$teachers->image = $image->getClientOriginalName();
		$mid = 'teacher';
		// $student->mime = $image->getClientMimeType();
			$teachers->name = (Input::get('name'));
			$teachers->address = (Input::get('address'));
			$teachers->email = (Input::get('email'));
			$teachers->password = bcrypt(Input::get('date_of_birth'));
			$teachers->gender = (Input::get('gender'));
			$teachers->dob = (Input::get('date_of_birth'));
			$teachers->telephone_no = (Input::get('telephone_no'));
			$teachers->mobile_no = (Input::get('mobile_no'));
			$teachers->confirmation_code = 's';
			$teachers->confirmed = '1';
			$teachers->is_admin = '6';
			$teachers->created_by = $user_id;
			$teachers->created_at = $current_date;
			$this->helper->imageResize($file_path, $fileName, $extension, $width=400, $height=300,$mid );
			if ($teachers->save()){
				if((Input::get('subject_id'))&&(Input::get('class_id'))&&(Input::get('section_id'))&&(Input::get('shift_id'))){
				$teacher_first_class = new Teacher_has_first;
					$teacher_first_class->teacher_id = $teachers->id;
					$teacher_first_class->subject_id = (Input::get('subject_id'));
					$teacher_first_class->class_id = (Input::get('class_id'));
					$teacher_first_class->section_id = (Input::get('section_id'));
					$teacher_first_class->shift_id = (Input::get('shift_id'));
					$teacher_first_class->is_active = '1';
					$teacher_first_class->created_by = $user_id;
				    $teacher_first_class->created_at = $current_date;
					$teacher_first_class->save();
				}
					$data = Input::all();
				// to save on teacher subjects
				$count = count($data['subject_teacher_id']);
			for ($i=0; $i < ($count); $i++) { 
				$teacher_has_info = new Teacher_has_info;
				$teacher_has_info->user_id = $teachers->id;
				$teacher_has_info->subject_id = $data['subject_teacher_id'][$i];
				// subject id
				$teacher_has_info->is_active = '1';
				$teacher_has_info->created_by = $user_id;
			    $teacher_has_info->created_at = $current_date;
				$teacher_has_info->save();
				$this->request->session()->flash('alert-success', 'Teacher saved successfully!');
				}
			}
			else{
				$this->request->session()->flash('alert-warning', 'Teacher could not add!');
			}
			
		return back()->withInput();
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$teacher = User::find($id);
        $subject_all_lists = Subject::where('is_active','=',True)->orderBy('class_id','ASC')->get();
//        $classees = Tclass::where('is_active','=',True)->orderBy('id','ASC')->get();
        $class_id=$teacher->getTeacherFirstClass->class_id;
        $classees = Tclass::where('is_active','=', True)->get();
        $sections = Class_has_section::where('class_id', $class_id)->get();
        $shifts = Class_has_shift::where('class_id', $class_id)->get();
        $subjects = Subject::with('subjectclass')->where('class_id', $class_id)->get();
//        dd($subjects);
//        dd($teacher->getTeacherInfo->toArray());


        return view('admin.teachers.teacher_edit',compact('teacher','subject_all_lists','classees','sections','shifts','subjects'));


//        return Response::json($teacher);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'date_of_birth' => 'required',
            'telephone_no' => 'required',
            'mobile_no' => 'required',
            'image' => 'required'

        );
        
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
    if (Auth::check()) {
    	$teacher_id = (Input::get('teacher_id'));
    	date_default_timezone_set('Asia/Kathmandu'); 
		$current_date = date("Y-m-d h:i:s");
	    $user_id = Auth::user()->id;
		$teachers = User::find($teacher_id);
		$image = Input::file('new_image');
		if($image != ""){
		$mid = 'teacher';
			$destinationPath = 'image/teacher'; // upload path
			if (!file_exists($destinationPath)) {
				    mkdir($destinationPath, 0777, true);
				}
			$extension = $image->getClientOriginalExtension(); // getting image extension
			$fileName = md5(mt_rand()).'.'.$extension; // renameing image
			$image->move($destinationPath, $fileName);
			$file_path = $destinationPath.'/'.$fileName;
			$teachers->image_encrypt = $fileName;
			$teachers->image = $image->getClientOriginalName();
			$this->helper->imageResize($file_path, $fileName, $extension, $width=400, $height=300,$mid );
		}
		
		// $student->mime = $image->getClientMimeType();
			$teachers->name = (Input::get('name'));
			$teachers->address = (Input::get('address'));
			$teachers->email = (Input::get('email'));
			$teachers->password = bcrypt(Input::get('date_of_birth'));
			$teachers->gender = (Input::get('gender'));
			$teachers->dob = (Input::get('date_of_birth'));
			$teachers->telephone_no = (Input::get('telephone_no'));
			$teachers->mobile_no = (Input::get('mobile_no'));
			$teachers->confirmation_code = 's';
			$teachers->confirmed = '1';
			$teachers->is_admin = '6';
			$teachers->created_by = $user_id;
			$teachers->created_at = $current_date;
			
			if ($teachers->save()){
				if((Input::get('subject_id'))&&(Input::get('class_id'))&&(Input::get('section_id'))&&(Input::get('shift_id'))){
					$teacher_info = Teacher_has_first::where('teacher_id',$teachers->id)->get();
					$stu_info = $teacher_info[0]['id'];
					$teacher_first_class = Teacher_has_first::find($stu_info);
					$teacher_first_class->teacher_id = $teachers->id;
					$teacher_first_class->subject_id = (Input::get('subject_id'));
					$teacher_first_class->class_id = (Input::get('class_id'));
					$teacher_first_class->section_id = (Input::get('section_id'));
					$teacher_first_class->shift_id = (Input::get('shift_id'));
					$teacher_first_class->is_active = '1';
					$teacher_first_class->created_by = $user_id;
				    $teacher_first_class->created_at = $current_date;
					$teacher_first_class->save();
				}
					$data = Input::all();
				// to save on teacher subjects
				$count = count($data['subject_teacher_id']);
			for ($i=0; $i < ($count); $i++) { 
				$teacher_first = Teacher_has_info::where('user_id',$teachers->id)->get();
				$tea_first = $teacher_first[0]['id'];
				$teacher_has_info = Teacher_has_info::find($tea_first);
				$teacher_has_info->user_id = $teachers->id;
				$teacher_has_info->subject_id = $data['subject_teacher_id'][$i];
				// subject id
				$teacher_has_info->is_active = '1';
				$teacher_has_info->created_by = $user_id;
			    $teacher_has_info->created_at = $current_date;
				$teacher_has_info->save();
				$this->request->session()->flash('alert-success', 'Teacher saved successfully!');
				}
			}
			else{
				$this->request->session()->flash('alert-warning', 'Teacher could not add!');
			}
			
		return back()->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete(){
		$id = Input::get('id');
		$teacher_first_class = Teacher_has_first::where('teacher_id',$id)->delete();
		$teacher_has_info = Teacher_has_info::where('user_id',$id)->delete();
		$teacher = User::find($id);

		$destinationPath = 'image/teacher';
		$destinationPath_thumb = 'image/thumb/teacher';
		$file_path = $destinationPath.'/'.$teacher->image_encrypt;
		$file_path_thumb = $destinationPath_thumb.'/'.$teacher->image_encrypt;
		File::delete($file_path,$file_path_thumb);

		if($teacher->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}

	public function export()
	{
		$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$teachers = User::where('is_admin','=',6)
								->orderBy('id', 'DESC')//updated with current time
								->get();
			$subjects = Subject::where('is_active','=',True)->orderBy('class_id','ASC')->get();
			$classees = Tclass::where('is_active','=',True)->orderBy('id','ASC')->get();
			return view('admin.teacher_export', compact(['teachers','subjects','classees','user_levels']));

			// return view('admin.teacher_export', compact(['teachers','subjects','classees','user_levels']));
	}

}
