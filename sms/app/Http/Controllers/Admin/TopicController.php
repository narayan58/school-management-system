<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\Subject;
use App\Topic;
use App\Tclass;
use App\User;
use Validator;
use App\Helper\Helper;
use ModelNotFoundException;

class TopicController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($class, $slug)
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$subject_id=['slug'=>$slug];
	        try{
				 $subject = Subject::where($subject_id)->firstOrFail();
				 
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
			$topic_name=$subject['name'];
			$topic_id=$subject['id'];
			$subjectids = ['subject_id' => $subject->id];
			$active = ['is_active' => True];
			$topics = Topic::where($subjectids)
								->where($active)
								->get();
			return view('admin.topic', compact(['topics','class','slug','topic_name','topic_id','user_levels']));	
		}	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'name' => 'required',
            'topic_hour' => 'required',
            'subject_id' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) {

			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$topic = new Topic;
			$topic->name = (Input::get('name'));
			$topic->slug = $this->helper->slug_converter($topic->name).'-'.(Input::get('subject_id'));
			$topic->topic_hour = (Input::get('topic_hour'));
			$topic->subject_id = (Input::get('subject_id'));
			$topic->is_active = '1';
			$topic->created_by = $user_id;
			$topic->created_at = $current_date;
		if ($topic->save()){
				$this->request->session()->flash('alert-success', 'Topic saved successfully!');
			}
		else{
			$this->request->session()->flash('alert-warning', 'Topic could not add!');
		}
		return back()->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id ,$top_id)
	{
		$topic = Topic::find($top_id);
		return Response::json($topic);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'name' => 'required',
            'topic_hour' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) {

			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$edit_id = Input::get('topic_id');
			$topic = Topic::find($edit_id);
			$topic->name = (Input::get('name'));
			$topic->slug = $this->helper->slug_converter($topic->name).'-'.(Input::get('subject_id'));
			$topic->topic_hour = (Input::get('topic_hour'));
			$topic->is_active = '1';
			$topic->created_by = $user_id;
			$topic->created_at = $current_date;
		if ($topic->save()){
				$this->request->session()->flash('alert-info', 'Topic saved successfully!');
			}
		else{
			$this->request->session()->flash('alert-warning', 'Topic could not add!');
		}
		return back()->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete(){
		$id = Input::get('id');
		$topic = Topic::find($id);

		if($topic->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}

}
