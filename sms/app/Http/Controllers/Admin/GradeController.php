<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Help;
use App\User;
use App\Grade;
use App\MarkSheet;
use App\Mark_has_grade;
use App\Student_has_info;
use App\User_has_level;
use Auth;
use Input;
use Redirect;
use Response;
use View;
use Validator;

class GradeController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '5') || ($user_filter_org == '1')){
				$grades = Grade::get();
				return view('admin.grade', compact('grades','user_levels'));
			}
		}
		else
			abort(404);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$rules = array(
            'name' => 'required|unique:grades',
            'lower' => 'required',
            'higher' => 'required'
            
        );
		$validator = Validator::make(Input::all(), $rules);
				if ($validator->fails()) {
				return back()->withInput()
				->withErrors($validator)
				->withInput();
			}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$grade = new Grade;
			$grade->name = (Input::get('name'));
			$grade->lower = (Input::get('lower'));
			$grade->higher = (Input::get('higher'));
			$grade->is_active = '1';
			$grade->created_by = $user_id;
			$grade->created_at = $current_date;
			if ($grade->save()){
					$this->request->session()->flash('alert-success', 'Grade saved successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'Grade could not add!');
			}
			return redirect('home/grade');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function getStudentClassGrade()
	{
		$student_id = Input::get('student_id');
		$student_name = User::where('id',$student_id)->firstOrFail();
		$student_org_name = $student_name['name'];
		$exam_id = Input::get('exam_id');
		$exam_ids=['user_id'=>$student_id,];
		$studentsubs = Student_has_info::where('user_id',$student_id)->firstOrFail();
		$student_org_id = $studentsubs['class_id'];
		$studentMarkSheet = Marksheet::where('student_id',$student_id)
								->where('class_id', $student_org_id)
								->where('exam_id', $exam_id)
								->get();
		return View::make('admin.studentGrade', compact(['studentMarkSheet','student_id','exam_id','student_org_name']));	
	}

	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$grade = Grade::find($id);
		return Response::json($grade);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$edit_id = Input::get('grade_id');
			$grade = Grade::find($edit_id);
			$grade->name = (Input::get('name'));
			$grade->lower = (Input::get('lower'));
			$grade->higher = (Input::get('higher'));
			$grade->is_active = '1';
			$grade->updated_by = $user_id;
			$grade->updated_at = $current_date;
			if ($grade->save()){
					$this->request->session()->flash('alert-info', 'grade updated successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'grade could not update!');
			}
			return redirect('home/grade');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete()
	{
		$id = Input::get('id');
		$grade = Grade::find($id);

		if($grade->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}


	public function export()
	{
		$grades = Grade::get();
		return view('admin.grade_export', compact('grades'));
	}

}
