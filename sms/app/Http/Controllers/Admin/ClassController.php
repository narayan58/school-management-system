<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Input;
use Redirect;
use Response;
use Auth;
use App\Tclass;
use App\Section;
use App\Shift;
use App\Class_has_shift;
use App\Class_has_section;
use App\User_has_level;
use App\Helper\Helper;
use App\User;
use Validator;

class ClassController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$user_levels_restriction = $user_levels[0]['id'];
			$user_filter = User_has_level::where('user_id',$user_levels_restriction)->get();
			$user_filter_org = $user_filter[0]['user_level'];
			// prevent user level on url typing
			if(($user_filter_org == '2') || ($user_filter_org == '1')){
        	$classes = Tclass::where('is_active','=', True)
        						->orderBy('id', 'DESC')//updated with current time 
        						->get();
	        $shifts = Shift::where('is_active','=', True)->get();
	        $sections = Section::where('is_active','=', True)
	        						->get();
			return view('admin.class', compact(['classes','shifts','sections','user_levels']));	
			} 
			else
				abort(404);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$rules = array(
            'name' => 'required|unique:tclasses'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$data = Input::all();
			$class = new Tclass;
				$class->name = $data['name'];
				$class->slug = $this->helper->slug_converter($class->name);
				$class->is_active = '1';
				$class->created_by = $user_id;
				$class->created_at = $current_date;
			if ($class->save()){
				$count = count($data['shift_id']);
			for ($i=0; $i < ($count); $i++) {

					$class_has_shifts = new Class_has_shift;
					$class_has_shifts->class_id = $class->id;
					$class_has_shifts->shift_id = $data['shift_id'][$i];
					$class_has_shifts->is_active = '1';
					$class_has_shifts->created_by = $user_id;
					$class_has_shifts->created_at = $current_date;
					$class_has_shifts->save();
				}
			$count = count($data['section_id']);
			for ($i=0; $i < ($count); $i++) {
					$class_has_sections = new Class_has_section;
					$class_has_sections->class_id = $class->id;
					$class_has_sections->section_id = $data['section_id'][$i];
					$class_has_sections->is_active = '1';
					$class_has_sections->created_by = $user_id;
					$class_has_sections->created_at = $current_date;
					$class_has_sections->save();
					$this->request->session()->flash('alert-success', 'Class saved successfully!');
				}
			}
			else{
				$this->request->session()->flash('alert-warning', 'Class could not add!');
			}
			return redirect('home/class');
		}
		else{
			abort(404);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$class = Tclass::find($id);
//        var_dump($class->getSectionDetail->toArray()); die;
        $shifts = Shift::where('is_active','=', True)->lists('name','id');
        $sections = Section::where('is_active','=', True)
            ->lists('name','id');
        return view('admin.classes.class_edit',compact('class','shifts','sections'));
//		return Response::json($class);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
//        var_dump($_POST); die;
		$rules = array(
            'name' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$edit_id = Input::get('class_id');
			$class = Tclass::find($edit_id);

				$class->name = Input::get('name');
				$class->slug = $this->helper->slug_converter($class->name);
				$class->is_active = '1';
				$class->created_by = $user_id;
				$class->created_at = $current_date;
			if ($class->save()){
				$class_ids = $class->id;
				$class_has_shifts = Class_has_shift::where('class_id',$class_ids)->delete();
				$class_has_sections = Class_has_section::where('class_id',$class_ids)->delete();
				$data = Input::all();
				$count = count($data['shift_id']);
			for ($i=0; $i < ($count); $i++) {
					$class_shift = new Class_has_shift;
					$class_shift->class_id = $class->id;
					$class_shift->shift_id = $data['shift_id'][$i];
					$class_shift->is_active = '1';
					$class_shift->created_by = $user_id;
					$class_shift->created_at = $current_date;
					$class_shift->save();
				}
			$count = count($data['section_id']);
			for ($i=0; $i < ($count); $i++) {
					$class_section = new Class_has_section;
					$class_section->class_id = $class->id;
					$class_section->section_id = $data['section_id'][$i];
					$class_section->is_active = '1';
					$class_section->created_by = $user_id;
					$class_section->created_at = $current_date;
					$class_section->save();
					$this->request->session()->flash('alert-info', 'Class saved successfully!');
				}
			}
			else{
				$this->request->session()->flash('alert-warning', 'Class could not update!');
			}
			return redirect('home/class');
		}
		else{
			abort(404);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete(){
		$id = Input::get('id');
		$class_has_shifts = Class_has_shift::where('class_id',$id)->delete();
		$class_has_sections = Class_has_section::where('class_id',$id)->delete();
		$tclass = Tclass::find($id);
		if($tclass->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}

	public function export()
	{
		$classes = Tclass::where('is_active','=', True)->get();
        $shifts = Shift::where('is_active','=', True)->get();
        $sections = Section::where('is_active','=', True)->get();
		return view('admin.class_export', compact(['classes','shifts','sections']));
	}

}
