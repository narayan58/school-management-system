<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Auth;
use App\Subject;
use App\Topic;
use App\Tclass;
use App\User;
use Validator;
use App\SubTopic;
use App\Helper\Helper;
use ModelNotFoundException;

class SubTopicController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($class, $slug, $topic)
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$topic_id=['slug'=>$topic];
	        try{
				 $topics = Topic::where($topic_id)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
			$subtopic_name=$topics['name'];
			$subtopic_id=$topics['id'];
			$topicids = ['topic_id' => $subtopic_id];
			$active = ['is_active' => True];
			$subtopics = SubTopic::where($topicids)
								->where($active)
								->orderBy('id', 'DESC')
								->get();
			return view('admin.sub_topic', compact(['subtopics','class','slug','subtopic_name','subtopic_id','user_levels']));	
		}	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'name' => 'required',
            'subtopic_hour' => 'required',
            'topic_id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('home/sub_topic')
                        ->withErrors($validator)
                        ->withInput();
        }
        if (Auth::check()) {
        	date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$subtopic = new SubTopic;
			$subtopic->name = (Input::get('name'));
			$subtopic->slug = $this->helper->slug_converter($subtopic->name).'-'.(Input::get('topic_id'));
			$subtopic->subtopic_hour = (Input::get('subtopic_hour'));
			$subtopic->topic_id = (Input::get('topic_id'));
			$subtopic->is_active = '1';
			$subtopic->created_by = $user_id;
			$subtopic->created_at = $current_date;
		if ($subtopic->save()){
				$this->request->session()->flash('alert-success', 'Sub topic saved successfully!');
			}
		else{
			$this->request->session()->flash('alert-warning', 'Sub topic could not add!');
		}
		return back()->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id, $subtop_id, $sid)
	{
		$sub_topic = SubTopic::find($sid);
		return Response::json($sub_topic);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'name' => 'required',
            'subtopic_hour' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
        if (Auth::check()) {
        	date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$edit_id = Input::get('subtopic_id');
			$subtopic = SubTopic::find($edit_id);
			$subtopic->name = (Input::get('name'));
			$subtopic->slug = $this->helper->slug_converter($subtopic->name).'-'.(Input::get('topic_id'));
			$subtopic->subtopic_hour = (Input::get('subtopic_hour'));
			$subtopic->is_active = '1';
			$subtopic->created_by = $user_id;
			$subtopic->created_at = $current_date;
		if ($subtopic->save()){
				$this->request->session()->flash('alert-info', 'Sub topic saved successfully!');
			}
		else{
			$this->request->session()->flash('alert-warning', 'Sub topic could not add!');
		}
		return back()->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	
	public function delete(){
		$id = Input::get('id');
		$topic = SubTopic::find($id);

		if($topic->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}

	public function export()
	{
		$subtopic_name=$topics['name'];
		$subtopic_id=$topics['id'];
		$topicids = ['topic_id' => $subtopic_id];
		$active = ['is_active' => True];
		$subtopics = SubTopic::where($topicids)
								->where($active)
								->orderBy('id', 'DESC')
								->get();
		return view('admin.sub_topic_export', compact(['subtopics','class','slug','subtopic_name','subtopic_id','user_levels']));
	}
}
