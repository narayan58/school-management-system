<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use ModelNotFoundException;
use Input;
use Redirect;
use Response;
use Auth;
use App\Tclass;
use App\Section;
use App\Shift;
use App\Subject;
use App\Exam;
use App\ExamClass;
use App\User;
use App\Class_has_shift;
use App\Class_has_section;
use App\Helper\Helper;
use Validator;

class ExamClassController extends Controller {

	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($exam_id, $class_id)
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user_levels = User::where('id', $user_id)->get();
			$exam_ids=['slug'=>$exam_id];
			$class_ids=['slug'=>$class_id];
	        try{
				 $current_class = Tclass::where($class_ids)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
			try{
				 $current_exam = Exam::where($exam_ids)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
			$class_name=$current_class['id'];
			$class_org_name=$current_class['name'];
			//to get current exam
			$class_org_exam=$current_exam['name'];
			$class_org_exam_id=$current_exam['id'];
			//$class_id = ['id'=>$current_class];
	        $marksSubexams = ExamClass::where('is_active','=', True)
	        			->where('class_id',$class_name)
	        			->where('exam_id',$class_org_exam_id)
	        			->get();
	        if(count($marksSubexams)){
	        	$subjects = array();
	        			}
	        else{
	        	$subjects = Subject::where('is_active','=', True)
	        			->where('class_id',$class_name)
	        			->get();
	        }
	        $shifts = Shift::where('is_active','=', True)->get();
	        $sections = Section::where('is_active','=', True)->get();
			return view('admin.examclass', compact(['subjects','marksSubexams','class_org_name','class_org_exam','class_org_exam_id','class_name','shifts','sections','user_levels']));	
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'class_id' => 'required',
            'exam_id' => 'required',
            'subject_id' => 'required',
            'fullmarks' => 'required',
            'passmarks' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$data = Input::all();
			$count = count($data['subject_id']);
			for ($i=0; $i < ($count); $i++) {
					$examclass = new ExamClass;
					$examclass->class_id = $data['class_id'];
					$examclass->exam_id = $data['exam_id'];
					$examclass->subject_id = $data['subject_id'][$i];
					$examclass->full_mark = $data['fullmarks'][$i];
					$examclass->pass_mark = $data['passmarks'][$i];
					$examclass->is_active = '1';
					$examclass->created_by = $user_id;
					$examclass->created_at = $current_date;
					
				if($examclass->save()){
					$this->request->session()->flash('alert-success', 'Subject Marks saved successfully!');
				}
				else{
				$this->request->session()->flash('alert-warning', 'Subject Marks could not add!');
			}
		}
			return redirect('home/exam');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return ('eheheehehheh');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'class_id' => 'required',
            'exam_id' => 'required',
            'subject_id' => 'required',
            'fullmarks' => 'required',
            'passmarks' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$data = Input::all();
			$count = count($data['subject_id']);
			for ($i=0; $i < ($count); $i++) {
					$examclass = ExamClass::find($data['marks_id'][$i]);
					$examclass->full_mark = $data['fullmarks'][$i];
					$examclass->pass_mark = $data['passmarks'][$i];
					$examclass->is_active = '1';
					$examclass->created_by = $user_id;
					$examclass->updated_by = $user_id;
					$examclass->updated_at = $current_date;
					
				if($examclass->save()){
					$this->request->session()->flash('alert-success', 'Subject Marks updated successfully!');
				}
				else{
				$this->request->session()->flash('alert-warning', 'Subject Marks could not update!');
			}
		}
			return redirect('home/exam');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
