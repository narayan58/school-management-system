<?php namespace App\Http\Controllers\Teacher;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Helper\Helper;
use Input;
use Redirect;
use Response;
use Auth;
use App\TeacherRecord;
use App\User;
use App\Tclass;
use App\Note;
use App\Subject;
use Validator;

class NoteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct(Request $request, Helper $helper)
	{
		$this->request = $request;
		$this->helper =$helper;
	}
	
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$teacherClass = User::where('id', $user_id)->get();
			$notes = Note::where('is_active','=', True)
							->where('created_by', $user_id)//user taneko through created_by
							->orderBy('id', 'DESC')//updated with current time 
							->get();
			return view('teacher.note_view', compact(['teacherClass','notes']));
		}
	}
	

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'subject_id' => 'required',
            'title' => 'required',
            'file' => 'required|mimes:pdf'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$subject_id = (Input::get('subject_id'));
			$title = (Input::get('title'));

			$subject_note_id = Subject::where('id', $subject_id)->get();
			$class_org_note_id = $subject_note_id[0]['class_id'];
			$notes = new Note;
			
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d");//current date and time
			$file = Input::file('file');
			$destinationPath = 'file/teacher'; // upload path
			if (!file_exists($destinationPath)) {
				    mkdir($destinationPath, 0777, true);
				}
			$extension = $file->getClientOriginalExtension(); // getting file extension
			$fileName = md5(mt_rand()).'.'.$extension; // renameing file
			$file->move($destinationPath, $fileName);
			$file_path = $destinationPath.'/'.$fileName;
			$notes->file_encrypt = $fileName;
			$notes->file = $file->getClientOriginalName();
			$notes->is_active = '1';

			$notes->title = $title;
			$notes->class_id = $class_org_note_id;
			$notes->subject_id = $subject_id;
			$notes->created_by = $user_id;
			$notes->created_at = $current_date;
		if ($notes->save()){
				$this->request->session()->flash('alert-success', 'Notes saved successfully!');
			}
		else{
			$this->request->session()->flash('alert-warning', 'Notes could not add!');
		}
		return back()->withInput();


		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$note = Note::find($id);
		return Response::json($note);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$rules = array(
            'title' => 'required',
            'file' => 'required|mimes:pdf'
        );
        $validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) {
			return back()->withInput()
			->withErrors($validator)
			->withInput();
		}
		if (Auth::check()) 
		{
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
			$user_id = Auth::user()->id;
			$title = (Input::get('title'));
			$edit_id = Input::get('note_id');
			$notes = Note::find($edit_id);
			
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d");//current date and time
			$file = Input::file('file');
			$destinationPath = 'file/teacher'; // upload path
			if (!file_exists($destinationPath)) {
				    mkdir($destinationPath, 0777, true);
				}
			$extension = $file->getClientOriginalExtension(); // getting file extension
			$fileName = md5(mt_rand()).'.'.$extension; // renameing file
			$file->move($destinationPath, $fileName);
			$file_path = $destinationPath.'/'.$fileName;
			$notes->file_encrypt = $fileName;
			$notes->file = $file->getClientOriginalName();
			$notes->is_active = '1';

			$notes->title = $title;
			$notes->class_id = $class_org_note_id;
			$notes->subject_id = $subject_id;
			$notes->created_by = $user_id;
			$notes->created_at = $current_date;
		if ($notes->save()){
				$this->request->session()->flash('alert-success', 'Notes saved successfully!');
			}
		else{
			$this->request->session()->flash('alert-warning', 'Notes could not add!');
		}
		return back()->withInput();


		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete()
	{
		$id = Input::get('id');
		$notes = Note::find($id);

		if($notes->delete()){
			$response = array(
				'status' => 'success',
				'msg' => 'Successfully Deleted',
			);
		}else{
			$response = array(
				'status' => 'failure',
				'msg' => 'Deleted Unsuccessful',
			);
		}
		return Response::json($response);
	}

	/*public function note_search()
	{
		$id=$_GET['search_date'];
        $notes = Note::where('updated_at', $id)
									->orderBy('id', 'DESC')
									->get();
									
		return view('teacher.note_add_ajax', compact(['notes']));
	}
*/
	public function note_search()
	{
		if (Auth::check()) 
		{
			$parammeters = $_GET['parameters'];
			// var_dump($parammeters); die;
	        if(is_array($parammeters))
	        {
	            $params=$parammeters;
	        }
	        else{
	            $params = json_decode($parammeters);
	        }

	        foreach ($params as $key => $value) {
	            $each_param[$key] = $value;
	        }
	        foreach ($each_param as $key => $value) {
	            ${$key} = $value;
	        }

	        $class_id=$class_id;
	        $query = Note::where('is_active','=', True)
	        		->where('created_by', Auth::user()->id);
	        	$query =$query->orderBy('id', 'DESC');
	        				
			if($class_id!=0){
				$query=$query->whereHas('getClass', function($query) use ($class_id) {
										        $query->where('subject_id',$class_id);
										  });	
			}
			
			 $teacher_notes=$query->get();
			if(is_array($parammeters))
	        {
	        	//$this part is not used
	        }
	        else{
	        	return view('teacher.note_add_ajax',compact('teacher_notes'));
	        }
		}
	}
}
