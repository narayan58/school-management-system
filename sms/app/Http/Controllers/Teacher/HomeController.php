<?php namespace App\Http\Controllers\Teacher;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;
use Validator;

use App\User;
use App\Tclass;
use App\Subject;
use App\Teacher_has_info;
use Auth;
use DB;
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$teacherClass = User::where('id', $user_id)->get();
			$teacher_dets = User::where('confirmed','=', True)
					->where('is_admin', '6')
					->where('id', $user_id)
					->orderBy('id', 'DESC')//updated with current time 
					->get();
			return view('teacher.home', compact(['teacher_dets','teacherClass']));
		}
	}

}