<?php namespace App\Http\Controllers\Teacher;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;
use App\Teacher_has_info;
use App\Teacher_has_first;
use App\Student_has_info;
use App\Tclass;
use App\Section;
use App\Shift;
use App\Subject;
use App\TeacherRecord;
use App\User;
use Validator;

class TeacherController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($subject_slug)
	{
		$user_id = Auth::user()->id;
			$teacherClass = User::where('id', $user_id)->get();
			$teacherID = $teacherClass[0]['id'];

			try{
				$subjects = Subject::where('slug',$subject_slug)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}

			$subject_org_id = $subjects['id'];
			$subject_org_class_id = $subjects['class_id'];

			$teacher_sub_id = Teacher_has_info::where('subject_id', $subject_org_id)->get();
			 $teacher_org_sub_id = $teacher_sub_id[0]['user_id'];
			 $teacher_sub_id_rev = Teacher_has_info::where('user_id', $user_id)->get();
			 $teacher_org_sub_id_rev = $teacher_sub_id_rev[0]['subject_id'];
			 foreach ($teacher_sub_id_rev as $key => $tea_sub) {
			 	$sub = $tea_sub->subject_id;
			 }
			$teacher_sub_id = Teacher_has_info::where('subject_id', $sub)->get();
			$teacher_org_sub_id = $teacher_sub_id[0]['user_id'];


			//if($user_id == $teacher_org_sub_id){
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d");
			$teachers_record = TeacherRecord::where('class_id',$subject_org_class_id)
								->where('subject_id',$subject_org_id)
								->where('teacher_id',$user_id)
								->orderBy('id', 'DESC')//updated with current time 
								// ->where('lecture_date',$current_date)
								->get();
			$section_list=Section::where('is_active','=', True)->lists('name','id');
			$shift_list=Shift::where('is_active','=', True)->lists('name','id');
				return view('teacher.teacher', compact(['teacherClass','teachers_record','subject_org_id','class_list','section_list','shift_list']));
			// }
			// else{
			// 	abort(404);
			// 	}
				return view('teacher.teacher', compact(['teacherClass','teachers_record','subject_org_id']));
			
	}

	public function getClassTeacher($class_id){
		$user_id = Auth::user()->id;
			$teacherClass = User::where('id', $user_id)->get();
			$teacherID = $teacherClass[0]['id'];

			try{
				$classes = Tclass::where('slug',$class_id)->firstOrFail();
			} catch(ModelNotFoundException $ex){
				abort(404);
			}
			$class_org_id = $classes['id'];

			$teacher_and_class_id = Teacher_has_first::where('teacher_id', $user_id)
														->where('class_id',$class_org_id)
														->get();
			$teacherID = $teacher_and_class_id[0]['teacher_id'];
			if($user_id == $teacherID){
			$sectionID = $teacher_and_class_id[0]['section_id'];
			$shiftID = $teacher_and_class_id[0]['shift_id'];
			$studentLists = Student_has_info::where('class_id', $class_org_id)
											->where('section_id', $sectionID)
											->where('shift_id', $shiftID)
											->get();
				return view('teacher.teacherStuList', compact(['teacherClass','studentLists']));
			}
			else{
				abort(404);
				}

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
            'name' => 'required|unique:sections'
        );
		$validator = Validator::make(Input::all(), $rules);
						if ($validator->fails()) {
						return back()->withInput()
						->withErrors($validator)
						->withInput();
					}
		if (Auth::check()) {
			date_default_timezone_set('Asia/Kathmandu'); 
			$current_date = date("Y-m-d h:i:s");
		    $user_id = Auth::user()->id;
			$note = new Subject;
				$note->name = (Input::get('name'));
				$note->class_id = (Input::get('class_id'));
				$note->is_active = '1';
				$note->created_by = $user_id;
				$note->created_at = $current_date;
			if ($note->save()){
					$this->request->session()->flash('alert-success', 'note saved successfully!');
				}
			else{
				$this->request->session()->flash('alert-warning', 'note could not add!');
			}
			return back()->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function teacher_search($subject_slugs)
	{
		// die($subject_slugs);
		$parammeters = $_GET['parameters'];
		// var_dump($parammeters); die;
        if(is_array($parammeters))
        {
            $params=$parammeters;
        }
        else{
            $params = json_decode($parammeters);
        }

        foreach ($params as $key => $value) {
            $each_param[$key] = $value;
        }
        foreach ($each_param as $key => $value) {
            ${$key} = $value;
        }
        $section_id=$section_id;
        $shift_id=$shift_id;

        $user_id = Auth::user()->id;
		$subjects = Subject::where('slug',$subject_slugs)->firstOrFail();
		$subject_org_id = $subjects['id'];
		$subject_org_class_id = $subjects['class_id'];
        $query = TeacherRecord::where('class_id',$subject_org_class_id)
								->where('subject_id',$subject_org_id)
        						->where('teacher_id',$user_id)
        						->orderBy('id', 'DESC');
		
		if($section_id!=0){
			$query=$query->whereHas('getSection', function($query) use ($section_id) {
									        $query->where('section_id',$section_id);
									  });
		}
			
		if($shift_id!=0){

			$query=$query->whereHas('getShift', function($query) use ($shift_id) {
									        $query->where('shift_id',$shift_id);
									  });	
		}

		 $teachers_record=$query->get();
		if(is_array($parammeters))
        {
        	//$this part is not used
        }
        else{
        	return view('teacher.teacher_add_ajax',compact('teachers_record'));
        }
	}
}
