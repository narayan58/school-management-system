<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

// admin middleware
Route::group(['middleware'=>['admin']],function(){
	Route::get('home', ['as' => 'home','uses' => 'Admin\HomeController@index']);
	Route::get('home/change_password', ['as' => 'cpassword','uses' => 'Admin\ProfileController@index']);
	Route::post('home/change_password/update', ['as' => 'cpassword','uses' => 'Admin\ProfileController@update']);
	Route::post('home/create_role', ['as' => 'crole','uses' => 'Admin\RegisterController@index']);
	Route::get('home/class/{slug}', ['as' => 'classslug','uses' => 'Admin\SubjectController@index']);

	// to get student of respective class
	Route::get('home/class/student/{class_id}', ['as' => 'getStudent','uses' => 'Admin\StudentController@getStudent']);

	Route::get('home/class/{class}/{sub}', ['as' => 'topicslug','uses' => 'Admin\TopicController@index']);
	Route::get('home/class/{class}/{sub}/{topic}', ['as' => 'subtopicslug','uses' => 'Admin\SubTopicController@index']);
	Route::post('home/student/store', ['as' => 'studentstore','uses' => 'Admin\StudentController@store']);
	Route::get('home/student/{email}', ['as' => 'studentinfo','uses' => 'Admin\StudentController@getStudentInfo']);
	Route::get('home/student/{email}/exam/mark/{exam}', ['as' => 'studentinfoexammark','uses' => 'Admin\StudentController@getStudentExamInfo']);
	Route::get('home/student/{email}/exam/grade/{exam}', ['as' => 'studentinfoexamgrade','uses' => 'Admin\StudentController@getStudentGradeInfo']);
	Route::get('home/student/{email}/fee/{fee}', ['as' => 'studentinfofee','uses' => 'Admin\StudentController@getStudentFeeInfo']);
	Route::post('home/teacher/store', ['as' => 'teacherstore','uses' => 'Admin\TeacherController@store']);

	//for admin->class
	Route::resource('home/class','Admin\ClassController');
	Route::post('home/class/delete', ['as' => 'dcategory','uses' => 'Admin\ClassController@delete']);
	Route::get('home/tclass/edit/{id}', ['as' => 'eclass','uses' => 'Admin\ClassController@edit']);
	Route::post('home/class/update', ['as' => 'uclass','uses' => 'Admin\ClassController@update']);
	Route::resource('home/class_export','Admin\ClassController@export');

	//for admin->student
	Route::resource('home/student','Admin\StudentController');
	Route::resource('home/student_export','Admin\StudentController@export');
	Route::get('home/student_record','Admin\StudentController@student_record');
	Route::post('home/student/delete', ['as' => 'dstudent','uses' => 'Admin\StudentController@delete']);
	Route::get('home/student/edit/{id}', ['as' => 'estudent','uses' => 'Admin\StudentController@edit']);
	Route::post('home/student/update', ['as' => 'ustudent','uses' => 'Admin\StudentController@update']);

	//for shift
	Route::resource('home/shift','Admin\ShiftController');
	Route::post('home/shift/delete', ['as' => 'dshift','uses' => 'Admin\ShiftController@delete']);
	Route::get('home/shift/edit/{id}', ['as' => 'eshift','uses' => 'Admin\ShiftController@edit']);
	Route::post('home/shift/update', ['as' => 'ushift','uses' => 'Admin\ShiftController@update']);
	Route::get('home/shift_export','Admin\ShiftController@export');

	//for section
	Route::resource('home/section','Admin\SectionController');
	Route::post('home/sec/delete', ['as' => 'dcategory','uses' => 'Admin\SectionController@delete']);
	Route::get('home/section/edit/{id}', ['as' => 'esection','uses' => 'Admin\SectionController@edit']);
	Route::post('home/section/update', ['as' => 'usection','uses' => 'Admin\SectionController@update']);
	Route::get('home/section_export','Admin\SectionController@export');

	//for admin->subject
	Route::resource('home/subject','Admin\SubjectController');
	Route::post('home/class/subject/delete', ['as' => 'dsubject','uses' => 'Admin\SubjectController@delete']);
	Route::get('home/class/subject/esub/edit/{id}', ['as' => 'esubject','uses' => 'Admin\SubjectController@edit']);
	Route::post('home/subject/update', ['as' => 'usubject','uses' => 'Admin\SubjectController@update']);
	Route::get('home/subject_export','Admin\SubjectController@export');

	// for teacher
	Route::resource('home/teacher','Admin\TeacherController');
	Route::get('home/teacher_export','Admin\TeacherController@export');
	Route::post('home/teacher/delete', ['as' => 'dteacher','uses' => 'Admin\TeacherController@delete']);
	Route::get('home/teacher/edit/{id}', ['as' => 'eteacher','uses' => 'Admin\TeacherController@edit']);
	Route::post('home/teacher/update', ['as' => 'uteacher','uses' => 'Admin\TeacherController@update']);

	// for period
	Route::resource('home/period','Admin\PeriodController');
	Route::post('home/period/delete', ['as' => 'dcategory','uses' => 'Admin\PeriodController@delete']);
	Route::get('home/period_export','Admin\PeriodController@export');
	Route::get('home/period/edit/{id}', ['as' => 'eperiod','uses' => 'Admin\PeriodController@edit']);
	Route::post('home/period/update', ['as' => 'uperiod','uses' => 'Admin\PeriodController@update']);

	// for topic
	Route::resource('home/topic','Admin\TopicController');
	Route::post('home/class/{id}/topic/delete', ['as' => 'dtopic','uses' => 'Admin\TopicController@delete']);
	Route::get('home/class/{cid}/topic/edit/{id}', ['as' => 'etopic','uses' => 'Admin\TopicController@edit']);
	Route::post('home/topic/update', ['as' => 'uteacher','uses' => 'Admin\TopicController@update']);


	//for admin->subtopic
	Route::resource('home/sub_topic','Admin\SubTopicController');
	Route::post('home/class/{id}/{class_id}/subtopic/delete', ['as' => 'dsubject','uses' => 'Admin\SubTopicController@delete']);
	Route::get('home/class/{cid}/{class_id}/subtopic/edit/{id}', ['as' => 'esubtopic','uses' => 'Admin\SubTopicController@edit']);
	Route::post('home/subtopic/update', ['as' => 'usub_topic','uses' => 'Admin\SubTopicController@update']);
	Route::get('home/sub_topic_export','Admin\SubTopicController@export');

	//Teacher record
	Route::get('home/daily-teacher-record', ['as' => 'teacherstore','uses' => 'Admin\TeacherRecordController@index']);
	Route::get('home/getSubject/{teacher_id}', ['as' => 'gsubjectrecord', 'uses' => 'Admin\TeacherRecordController@getTeacherClass']);
	Route::get('home/getClass/{subject_id}', ['as' => 'gsubjectrecord', 'uses' => 'Admin\TeacherRecordController@getSubjectClass']);
	Route::get('home/getTopic/{subject_id}', ['as' => 'gsubjectrecord', 'uses' => 'Admin\TeacherRecordController@getSubjectTopic']);
	Route::get('home/getSubTopic/{topic_id}', ['as' => 'gsubjectrecord', 'uses' => 'Admin\TeacherRecordController@getSubjectSubTopic']);
	Route::get('home/getSection/{class_id}', ['as' => 'gsectionrecord', 'uses' => 'Admin\TeacherRecordController@getSection']);
	Route::get('home/getShift/{class_id}', ['as' => 'gsectionrecord', 'uses' => 'Admin\TeacherRecordController@getShift']);
	Route::post('home/daily-teacher-record/store', ['as' => 'teacherstore','uses' => 'Admin\TeacherRecordController@store']);
	Route::get('home/daily_teacher_record/view', ['as' => 'teacherstore','uses' => 'Admin\TeacherRecordViewController@index']);
	Route::get('home/daily_teacher_record/teacher_search', ['as' => 'teachersearch','uses' => 'Admin\TeacherRecordViewController@teacher_search']);
	
	// route to get shift and section on student
	Route::get('home/getSectionOnStudent/{class_id}', ['as' => 'gsectionrecord', 'uses' => 'Admin\StudentController@getSectionOnStudent']);
	Route::get('home/getShiftOnStudent/{class_id}', ['as' => 'gsectionrecord', 'uses' => 'Admin\StudentController@getShiftOnStudent']);
	Route::get('home/getSubjectOnStudent/{class_id}', ['as' => 'gsubjectrecord', 'uses' => 'Admin\StudentController@getSubjectOnStudent']);

	//Exam control
	Route::get('home/exam', ['as' => 'examindex','uses' => 'Admin\ExamController@index']);
	Route::post('home/exam/delete', ['as' => 'dexam','uses' => 'Admin\ExamController@delete']);
	Route::get('home/exam/edit/{id}', ['as' => 'eexam','uses' => 'Admin\ExamController@edit']);
	Route::get('home/exam_export', ['as' => 'examexport','uses' => 'Admin\ExamController@export']);
	Route::post('home/exam/store', ['as' => 'examstore','uses' => 'Admin\ExamController@store']);
	Route::post('home/exam/update', ['as' => 'examstore','uses' => 'Admin\ExamController@update']);
	Route::get('home/exam/exam_search', ['as' => 'examsearch','uses' => 'Admin\ExamController@exam_search']);

	Route::get('home/exam-detail/view', ['as' => 'examdetailview','uses' => 'Admin\ExamController@examdetail']);
	Route::get('home/exam-detail/view/mark/{exam}/{roll}', ['as' => 'examdetailview','uses' => 'Admin\ExamController@examdetailStudent']);
	Route::get('home/exam-detail/view/grade/{exam}/{roll}', ['as' => 'examdetailview','uses' => 'Admin\ExamController@gradedetailStudent']);
	Route::post('home/exam-detail/marksheet/unpublish', ['as' => 'examdetailview','uses' => 'Admin\ExamController@unpublish']);
	Route::post('home/exam-detail/getStudentMarkStudent', ['as' => 'examdetailview','uses' => 'Admin\ExamController@getStudentMarkStudent']);

	//Exam and class subjects
	Route::get('home/exam/{exam_id}/class/{class_id}', ['as' => 'examclassindex','uses' => 'Admin\ExamClassController@index']);
	Route::post('home/exam/class/store', ['as' => 'examclassindex','uses' => 'Admin\ExamClassController@store']);
	Route::post('home/exam/class/update', ['as' => 'examclassindex','uses' => 'Admin\ExamClassController@update']);

	// get all value after exam click
	Route::get('home/exam/{exam_id}', ['as' => 'exammarkindex','uses' => 'Admin\ExamMarkController@index']);
	Route::get('home/exam/getSectionOnStudent/{class_id}', ['as' => 'gsectionrecord', 'uses' => 'Admin\ExamMarkController@getSectionOnStudent']);
	Route::get('home/exam/getShiftOnStudent/{class_id}', ['as' => 'gsectionrecord', 'uses' => 'Admin\ExamMarkController@getShiftOnStudent']);
	Route::get('home/exam/getStudentList/{exam_id}', ['as' => 'gstudentlistexam', 'uses' => 'Admin\ExamMarkController@getStudentList']);

	Route::post('home/exam/getStudentClassSub', ['as' => 'gstudentlistmark', 'uses' => 'Admin\ExamMarkController@getStudentClassSub']);
	Route::post('home/exam/getStudentClassSubMarks', ['as' => 'gstudentlistmarkupdate', 'uses' => 'Admin\ExamMarkController@getStudentClassSubMarks']);

	// student grade
	Route::post('home/exam/getStudentClassGrade', ['as' => 'gstudentlistgrade', 'uses' => 'Admin\GradeController@getStudentClassGrade']);

	// student marksheet
	Route::get('home/exam/marksheet/store', ['as' => 'gstudentmark', 'uses' => 'Admin\ExamMarkController@store']);
	Route::get('home/exam/marksheet/update', ['as' => 'ustudentmark', 'uses' => 'Admin\ExamMarkController@update']);

	// student fee
	Route::get('home/fee', ['as' => 'feeindex','uses' => 'Admin\FeeController@index']);
	Route::resource('home/feestructure','Admin\FeeStructureController');
	Route::post('home/fees/delete', ['as' => 'dfees','uses' => 'Admin\FeeStructureController@delete']);
	Route::get('home/fees/edit/{id}', ['as' => 'efees','uses' => 'Admin\FeeStructureController@edit']);
	Route::post('home/fees/update', ['as' => 'feestore','uses' => 'Admin\FeeStructureController@update']);
	Route::get('home/feestructure_export','Admin\FeeStructureController@export');
	
	Route::post('home/getStudents', ['as' => 'gstudentlist', 'uses' => 'Admin\FeeController@getStudentList']);
	Route::post('home/getStudentFee', ['as' => 'gstudentlist', 'uses' => 'Admin\FeeController@getStudentFee']);
	Route::post('home/getCalculation', ['as' => 'gstudentCalculation', 'uses' => 'Admin\FeeController@getAllCalculation']);
	Route::post('home/fee/save', ['as' => 'feestore', 'uses' => 'Admin\FeeController@store']);
	Route::get('home/fee/{id}', ['as' => 'bill_print', 'uses' => 'Admin\FeeController@bill_print']);
	Route::get('home/fee-detail/view', ['as' => 'feeview', 'uses' => 'Admin\FeeController@viewdetail']);
	Route::get('home/fee-detail/fee_search', ['as' => 'feesearch', 'uses' => 'Admin\FeeController@fee_search']);
	Route::get('home/fee-detail/view/{fee_id}', ['as' => 'feedetailview', 'uses' => 'Admin\FeeController@viewfeedetail']);
	Route::post('home/fee-detail/view/confirm', ['as' => 'feedueclear', 'uses' => 'Admin\FeeController@cleardue']);

	// notice section
	Route::get('home/notice', ['as' => 'notice', 'uses' => 'Admin\NoticeController@index']);
	Route::post('home/notice/delete', ['as' => 'dnotice','uses' => 'Admin\NoticeController@delete']);
	Route::get('home/notice/edit/{id}', ['as' => 'enotice','uses' => 'Admin\NoticeController@edit']);
	Route::post('home/notice/update', ['as' => 'unotice','uses' => 'Admin\NoticeController@update']);
	Route::post('home/notice/store', ['as' => 'notice', 'uses' => 'Admin\NoticeController@store']);
	Route::get('home/notice_export','Admin\NoticeController@export');


	// event calendar
	Route::get('home/event', ['as' => 'notice', 'uses' => 'Admin\EventController@index']);
	Route::get('home/calendar/edit/{id}', ['as' => 'ecalender','uses' => 'Admin\EventViewController@edit']);
	Route::post('home/calendar/update', ['as' => 'ucalender','uses' => 'Admin\EventViewController@update']);
	Route::get('home/eventAdd_export','Admin\EventViewController@export'); //event add 
	Route::post('home/calendar/delete', ['as' => 'dcategory','uses' => 'Admin\EventViewController@delete']);
	Route::resource('home/event_view','Admin\EventViewController'); //event add
	Route::get('home/event_view/event_search/{id}','Admin\EventViewController@event_search'); //event add

	// view user role
	Route::get('home/role', ['as' => 'role', 'uses' => 'Admin\RoleController@index']);
	Route::post('home/role/delete', ['as' => 'drole','uses' => 'Admin\RoleController@delete']);
	Route::get('home/role_export','Admin\RoleController@export');


	Route::get('home/help','Admin\HelpController@index');
	Route::get('home/fee_structure/{class_id}','Admin\FeeController@getClassAmount');

	// get student Attendance
	Route::get('home/daily-student-attendance', ['as' => 'iattendance', 'uses' => 'Admin\AttendanceController@index']);
	Route::post('home/daily-student-attendance/search', ['as' => 'searchattendance', 'uses' => 'Admin\AttendanceController@getStudent']);
	Route::post('home/daily-student-attendance/store', ['as' => 'storeattendance', 'uses' => 'Admin\AttendanceController@store']);

	// report routes by harry
	Route::get('home/report/teachers','Admin\ReportController@teachersReport');
	Route::get('home/report/fee','Admin\ReportController@feeReport');
    Route::get('home/report/getTeachers','Admin\ReportController@getTeachersReport');
	Route::get('home/report/getFee','Admin\ReportController@getFeeReport');
	Route::get('home/report/teacher_export','Admin\ReportController@teachers_export');
	Route::get('home/report/student_indv_export','Admin\ReportController@student_indv_export');
    Route::get('home/report/finance','Admin\ReportController@financeReport');
    Route::get('home/report/getFinance','Admin\ReportController@getFinanceReport');
    Route::get('home/report/finance_record_export','Admin\ReportController@finance_record_export');



    // grade
	Route::get('home/grade', ['as' => 'grade', 'uses' => 'Admin\GradeController@index']);
	Route::post('home/grade/gstore', ['as' => 'gradestore', 'uses' => 'Admin\GradeController@create']);
	Route::get('home/grade/edit/{id}', ['as' => 'egrade','uses' => 'Admin\GradeController@edit']);
	Route::post('home/grade/update', ['as' => 'ugrade','uses' => 'Admin\GradeController@update']);
	Route::post('home/grade/delete', ['as' => 'dcategory','uses' => 'Admin\GradeController@delete']);
	Route::get('home/grade_export','Admin\GradeController@export');

	// salary
	Route::get('home/salary', ['as' => 'salaryindex','uses' => 'Admin\SalaryController@index']);
	Route::post('home/getTeacherSalary', ['as' => 'getTeacherSalary', 'uses' => 'Admin\SalaryController@getTeacherSalary']);
	Route::post('home/getSalaryCalculation', ['as' => 'getSalaryCalculation', 'uses' => 'Admin\SalaryController@getSalaryCalculation']);
	Route::post('home/salary/save', ['as' => 'feestore', 'uses' => 'Admin\SalaryController@store']);
	Route::get('home/salary/{id}', ['as' => 'salary_print', 'uses' => 'Admin\SalaryController@salary_print']);
	Route::get('home/salary-detail/view', ['as' => 'salaryview', 'uses' => 'Admin\SalaryController@viewdetail']);
	Route::get('home/salary-detail/view/{fee_id}', ['as' => 'salarydetailview', 'uses' => 'Admin\SalaryController@salary_print']);
    Route::get('home/salary-detail/salary_search', ['as' => 'salarysearch', 'uses' => 'Admin\SalaryController@salary_search']);


    // miscellaneouses
	Route::get('home/miscellaneous', ['as' => 'miscellaneousindex','uses' => 'Admin\MiscellaneousController@index']);
	Route::post('home/miscellaneous/store', ['as' => 'miscellaneousindex','uses' => 'Admin\MiscellaneousController@store']);

});

// teacher middleware
Route::group(['middleware'=>['teacher']],function(){
	Route::get('teacher/change_password', ['as' => 'cpassword','uses' => 'Teacher\ProfileController@index']);
	Route::post('teacher/change_password/update', ['as' => 'cpassword','uses' => 'Teacher\ProfileController@update']);
	Route::resource('teacher/note','Teacher\NoteController'); //teacher notes
	Route::post('teacher/note/delete', ['as' => 'dnote','uses' => 'Teacher\NoteController@delete']);
	Route::get('teacher/note/edit/{id}', ['as' => 'enote','uses' => 'Teacher\NoteController@edit']);
	Route::post('teacher/note/update', ['as' => 'unote','uses' => 'Teacher\NoteController@update']);
	Route::get('teacher/teacher_note/note_search','Teacher\NoteController@note_search'); //event add


	Route::get('teacher', ['as' => 'teacher','uses' => 'Teacher\HomeController@index']);
	Route::get('teacher/teacher_search/{subject_slugs}', ['as' => 'teacher','uses' => 'Teacher\TeacherController@teacher_search']);
	Route::get('teacher/{subject_slug}', ['as' => 'teacherview','uses' => 'Teacher\TeacherController@index']);
	Route::get('teacher/class-teacher/{class_id}', ['as' => 'teacherview','uses' => 'Teacher\TeacherController@getClassTeacher']);
	Route::get('teacher/cal/event', ['as' => 'notice', 'uses' => 'Teacher\EventController@index']);
});

// student middleware
Route::group(['middleware'=>['student']],function(){
	Route::get('student/change_password', ['as' => 'cpassword','uses' => 'Student\ProfileController@index']);
	Route::post('student/change_password/update', ['as' => 'cpassword','uses' => 'Student\ProfileController@update']);
	Route::get('student', ['as' => 'student','uses' => 'Student\HomeController@index']);
	Route::get('student/exam', ['as' => 'studentview','uses' => 'Student\StudentExamController@index']);
	Route::get('student/todays_class', ['as' => 'todays_classview','uses' => 'Student\StudentClassController@index']);
	// mark
	Route::get('student/exam/{exam_id}', ['as' => 'studentviewmark','uses' => 'Student\StudentMarkSheetController@index']);
	//grade
	Route::get('student/grade/{exam_id}', ['as' => 'studentviewgrade','uses' => 'Student\StudentGradeController@index']);
	// get student fee
	Route::get('student/fee', ['as' => 'studentfee','uses' => 'Student\FeeController@index']);
	Route::get('student/fee/{date}', ['as' => 'studentfeedate','uses' => 'Student\FeeController@show']);

	//notes student
	Route::get('student/notes-view', ['as' => 'studentnote','uses' => 'Student\NoteController@index']);

	//notice
	Route::get('student/notice-view', ['as' => 'studentnotice','uses' => 'Student\NoticeController@index']);
	//event
	Route::get('student/event', ['as' => 'notice', 'uses' => 'Student\EventController@index']);
	// attendance
	Route::get('student/attendance', ['as' => 'iattendance', 'uses' => 'Student\AttendanceController@index']);

});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

	get('/register', array('as' => 'register', 'uses' => 'Auth\AuthController@getRegister'));
	post('/register', array('as' => 'register', 'uses' => 'Auth\AuthController@postRegister'));
	get('/login', array('as' => 'login', 'uses' => 'Auth\AuthController@getLogin'));
	get('/logout', array('as' => 'logout', 'uses' => 'Auth\AuthController@getLogout'));
	post('/logout', array('as' => 'logout', 'uses' => 'Auth\AuthController@getLogout'));
//	post('/login', array('as' => 'login', 'uses' => 'Auth\AuthController@postLogin'));

//route to send verification code
Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'Confirm\RegistrationController@confirm'
]);