<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticeHasClassesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notice_has_classes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('notice_id')->unsigned(); 
    		$table->foreign('notice_id')->references('id')->on('notices');
    		$table->integer('class_id')->unsigned(); 
    		$table->foreign('class_id')->references('id')->on('tclasses');
			$table->integer('is_active');
		    $table->integer('created_by')->unsigned(); 
    		$table->foreign('created_by')->references('id')->on('users');
    		$table->integer('updated_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notice_has_classes');
	}

}
