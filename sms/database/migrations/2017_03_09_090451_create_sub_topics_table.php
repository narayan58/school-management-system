<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubTopicsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sub_topics', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');			
			$table->string('slug');
			$table->string('subtopic_hour');
			$table->integer('topic_id')->unsigned(); 
    		$table->foreign('topic_id')->references('id')->on('topics'); 
			$table->integer('is_active');
			$table->integer('created_by')->unsigned(); 
    		$table->foreign('created_by')->references('id')->on('users'); 
			$table->integer('updated_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sub_topics');
	}

}
