<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassHasSectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('class_has_sections', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('class_id')->unsigned(); 
    		$table->foreign('class_id')->references('id')->on('tclasses'); 
			$table->integer('section_id')->unsigned(); 
    		$table->foreign('section_id')->references('id')->on('sections'); 
			$table->integer('is_active');
			$table->integer('created_by')->unsigned(); 
    		$table->foreign('created_by')->references('id')->on('users'); 
			$table->integer('updated_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('class_has_sections');
	}

}
