<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->integer('is_active');
			$table->integer('class_id')->unsigned(); 
    		$table->foreign('class_id')->references('id')->on('tclasses');
    		$table->integer('subject_id')->unsigned(); 
    		$table->foreign('subject_id')->references('id')->on('subjects');
    		$table->string('file');
			$table->string('file_encrypt');
		    $table->integer('created_by')->unsigned(); 
    		$table->foreign('created_by')->references('id')->on('users');
			$table->integer('updated_by');
			$table->timestamps();
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notes');
	}

}
