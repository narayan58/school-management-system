<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attendances', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('student_id')->unsigned(); 
    		$table->foreign('student_id')->references('id')->on('users');
    		$table->integer('class_id')->unsigned(); 
    		$table->foreign('class_id')->references('id')->on('tclasses');
    		$table->integer('shift_id')->unsigned(); 
    		$table->foreign('shift_id')->references('id')->on('shifts');
    		$table->integer('section_id')->unsigned(); 
    		$table->foreign('section_id')->references('id')->on('sections');
    		$table->string('title');
    		$table->integer('created_by')->unsigned(); 
    		$table->foreign('created_by')->references('id')->on('users');
    		$table->integer('updated_by');
    		$table->string('start');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attendances');
	}

}
