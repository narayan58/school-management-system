<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherHasInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teacher_has_infos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned(); 
    		$table->foreign('user_id')->references('id')->on('users'); 
			$table->integer('subject_id')->unsigned(); 
    		$table->foreign('subject_id')->references('id')->on('subjects');
			$table->integer('is_active');
			$table->integer('created_by')->unsigned(); 
    		$table->foreign('created_by')->references('id')->on('users'); 
			$table->integer('updated_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('teacher_has_infos');
	}

}
