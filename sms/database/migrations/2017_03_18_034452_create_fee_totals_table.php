<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeTotalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fee_totals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('bill_id');
			$table->integer('student_id')->unsigned(); 
    		$table->foreign('student_id')->references('id')->on('users');
			$table->string('total');
			$table->string('discount');
			$table->string('grand_total');
			$table->string('date');
			$table->integer('is_active');
			$table->integer('is_print');
			$table->integer('is_due');
			$table->string('due_clear_on');
			$table->integer('created_by')->unsigned(); 
    		$table->foreign('created_by')->references('id')->on('users'); 
			$table->integer('updated_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fee_totals');
	}

}
