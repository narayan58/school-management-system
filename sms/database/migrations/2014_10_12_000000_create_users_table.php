<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('address');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('gender');
			$table->string('dob');
			$table->string('telephone_no');
			$table->string('mobile_no');
			$table->string('image');
			$table->string('image_encrypt');
			$table->string('confirmation_code');
			$table->integer('confirmed');

			$table->string('last_login');
			$table->string('last_login_pre');
			
			$table->integer('is_admin')->default(0); //manages manager

			$table->rememberToken();
			
			$table->integer('created_by');
			$table->integer('updated_by')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
