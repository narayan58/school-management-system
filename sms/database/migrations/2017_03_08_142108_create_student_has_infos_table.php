<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentHasInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('student_has_infos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned(); 
    		$table->foreign('user_id')->references('id')->on('users'); 
			$table->integer('class_id')->unsigned(); 
    		$table->foreign('class_id')->references('id')->on('tclasses');
			$table->integer('section_id')->unsigned(); 
    		$table->foreign('section_id')->references('id')->on('sections'); 
			$table->integer('shift_id')->unsigned(); 
    		$table->foreign('shift_id')->references('id')->on('shifts'); 
			$table->integer('is_active');
			$table->integer('created_by')->unsigned(); 
    		$table->foreign('created_by')->references('id')->on('users'); 
			$table->integer('updated_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('student_has_infos');
	}

}
