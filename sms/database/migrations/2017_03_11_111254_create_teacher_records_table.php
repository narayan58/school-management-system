<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teacher_records', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('teacher_id')->unsigned(); 
    		$table->foreign('teacher_id')->references('id')->on('users');
			$table->integer('section_id')->unsigned(); 
    		$table->foreign('section_id')->references('id')->on('sections'); 
			$table->integer('shift_id')->unsigned(); 
    		$table->foreign('shift_id')->references('id')->on('shifts');
			$table->integer('period_id')->unsigned(); 
    		$table->foreign('period_id')->references('id')->on('periods');
			$table->integer('subject_id')->unsigned(); 
    		$table->foreign('subject_id')->references('id')->on('subjects');
			$table->integer('class_id')->unsigned(); 
    		$table->foreign('class_id')->references('id')->on('tclasses');
			$table->integer('topic_id')->unsigned(); 
    		$table->foreign('topic_id')->references('id')->on('topics');
			$table->integer('subtopic_id')->unsigned(); 
    		$table->foreign('subtopic_id')->references('id')->on('sub_topics');
			$table->string('lecture_hr');
			$table->string('lecture_date');
			$table->integer('created_by')->unsigned(); 
    		$table->foreign('created_by')->references('id')->on('users'); 
			$table->integer('updated_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('teacher_records');
	}

}
