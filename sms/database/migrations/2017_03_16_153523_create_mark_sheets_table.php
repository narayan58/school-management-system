<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarkSheetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mark_sheets', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('student_id')->unsigned(); 
    		$table->foreign('student_id')->references('id')->on('users');
			$table->integer('exam_id')->unsigned(); 
    		$table->foreign('exam_id')->references('id')->on('exams');
			$table->integer('class_id')->unsigned(); 
    		$table->foreign('class_id')->references('id')->on('tclasses');
			$table->integer('section_id')->unsigned(); 
    		$table->foreign('section_id')->references('id')->on('sections');
			$table->integer('shift_id')->unsigned(); 
    		$table->foreign('shift_id')->references('id')->on('shifts');
			$table->integer('marks_id')->unsigned(); 
    		$table->foreign('marks_id')->references('id')->on('exam_classes');

			$table->string('full_mark');
			$table->string('marks_pr');
			$table->string('marks_th');
			
			$table->string('marks_obt');
			$table->integer('is_active');
			$table->integer('created_by')->unsigned(); 
    		$table->foreign('created_by')->references('id')->on('users'); 
			$table->integer('updated_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mark_sheets');
	}

}
