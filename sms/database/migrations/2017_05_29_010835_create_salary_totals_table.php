<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryTotalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('salary_totals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('bill_id');
			$table->integer('teacher_id')->unsigned(); 
    		$table->foreign('teacher_id')->references('id')->on('users');
			$table->string('grand_total');
			$table->string('date');
			$table->integer('is_active');
			$table->integer('is_print');
			$table->integer('created_by')->unsigned(); 
    		$table->foreign('created_by')->references('id')->on('users'); 
			$table->integer('updated_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('salary_totals');
	}

}
