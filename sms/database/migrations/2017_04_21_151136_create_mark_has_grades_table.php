<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarkHasGradesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mark_has_grades', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('mark_sheet_id')->unsigned(); 
    		$table->foreign('mark_sheet_id')->references('id')->on('mark_sheets');
    		$table->integer('grade_id')->unsigned(); 
    		$table->foreign('grade_id')->references('id')->on('grades');
    		$table->integer('created_by')->unsigned(); 
    		$table->foreign('created_by')->references('id')->on('users');
    		$table->integer('updated_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mark_has_grades');
	}

}
