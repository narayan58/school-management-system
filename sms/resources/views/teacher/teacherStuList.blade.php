@extends('teacher.app')

@section('content')
@include('teacher.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
	
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
	<div class="heading"><h2>Class Summary</h2></div>
<div class="cl-md-8">
    <table class="table table-bordered">
    <thead> 
      <tr>
          <th>SN</th>
          <th>Student Name</th>
          <th>Roll No</th>
          <th>Rate</th>
      </tr>
    </thead>
    <tbody>
        @foreach($studentLists as $index=>$record)
        
          <tr>
             <td>{{$index+1}}</td>
             <td>{{$record->studentinfo->name}}</td>
             <td>{{$record->studentinfo->email}}</td>
             <td>Star</td>
         </tr>
        @endforeach
    </tbody>
  </table>
 </div>
@endsection