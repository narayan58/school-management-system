@extends('teacher.app')

@section('content')
@include('teacher.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
	<div class="heading"><h2>Teacher Summary</h2></div>
<div class="row">
		@foreach($teacher_dets as $teacher)
		<div class="col-md-3">
      		<a href="{{URL::to('/')}}/image/teacher/{{$teacher->image_encrypt}}" data-lightbox="{{$teacher->image}}" data-title="{{$teacher->image}}"><img src="{{URL::to('/')}}/image/thumb/teacher/{{$teacher->image_encrypt}}" alt="{{$teacher->image}}" class="img-responsive"></a>
	 	</div>
	 	<div class="col-md-9">
			<p>Fullname: {{$teacher->name}}</p>
			<p>Address: {{$teacher->address}}</p>
			<p>Roll no: {{$teacher->email}}</p>
			<p>Date of birth: {{$teacher->dob}}</p>
			<p>Gender: {{$teacher->gender}}</p>
			<p>Qualification: {{$teacher->telephone_no}}</p>
			<p>Mobile No: {{$teacher->mobile_no}}</p>
	 	</div>
	 	@endforeach
	</div>
@endsection
