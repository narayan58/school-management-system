@extends('teacher.app')

@section('content')
@include('teacher.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
	<div class="heading"><h2>Note Summary</h2></div>
  <div class="form-holder">
  <form class="general-form" id="fee_valid">
  <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
  <div class="row">
  <div class="form-group col-md-4">
    <label for="category">Class</label>
      <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="class_id" name="class_id" selected="">
        <option value="0">--Please Select--</option>
        @foreach($teacherClass as $teacher)
        @foreach($teacher->getTeacherInfo()->get() as $subjects)
                <option value="{{$subjects->getAllSubject->id}}">{{$subjects->getAllSubject->name}}({{$subjects->getAllSubject->subjectclass->name}})</option>
              @endforeach
        @endforeach
      </select>
  </div>
  <div class="text-center">
  <button type="button" class="btn btn-default btn-sm" id="search">
    Search<i class="fa fa-paper-plane-o"></i>
  </button>
</div>
</div>
</form>
</div>

  <div class="col-md-12">
    <div class="row">
    <table class="table table-bordered" id="replaceTable">
    <thead> 
      <tr>
          <th>SN</th>
          <th>Class</th>
          <th>Subject</th>
          <th>Title</th>
          <th>Notes File</th>
          <th>Updated Date</th>
          <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($notes as $index=>$note)
          <tr>
             <td>{{$index+1}}</td>
             <td>{{$note->getClass->name}}</td>
             <td>{{$note->getSubject->name}}</td>
             <td>{{$note->title}}</td>
             <td><a href="{{URL::to('/')}}/file/teacher/{{$note->file_encrypt}}" target="new">{{$note->file}}</a></td>
             <td>{{$note->updated_at}}</td>
             <td>
                <a class="action-btn edit-note" id="note" data-target="#myNote" data-toggle="modal" href=""><i class="fa fa-pencil-square-o" id="{{$note->id}}"></i></a>
                <a class="action-btn bg-green delete-note" id="note" href="javascript:void(0)"><i class="fa fa-times" id="{{$note->id}}"></i></a>
             </td>
         </tr>
        @endforeach
    </tbody>
  </table>
  </div>
  </div>

  <div id="myNote" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
    <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
   <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form edit-form" id="note_valid" action="{{ URL::to('/') }}/teacher/note/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="subject_id" class="id" value="">
        <h2>Note</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label  for="name" class="col-md-4 control-label">Title:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm title" id="title" name="title" placeholder="Please enter title" required>
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="file" class="col-md-4 control-label">File:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm file" id="old_file" name="file">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="file" class="col-md-4 control-label">File:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="file" class="form-control input-sm file" id="file" name="file">
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Update Note<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
  </div>
  </div>
  </div>
 </div>

    <script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
    <script src="{{ URL::to('/') }}/js/datepicker.js"></script>
    <!-- edit/delete script -->
  <script type="text/javascript">
      $('.delete-note').click(function(event){
    var id = $(event.target).attr("id");
    t=$(event.target).parent().attr("id"),
    token=$(".token").val();

    alertify.confirm("Are you sure you want to delete?",function(){
      url= t+"/delete",
      $.ajax({
        type:"POST",
        dataType:"JSON",
        url:url,
        data:{
        _token:token,
          id : id
        },
        success:function(e){
          alertify.alert(e.msg,function(){
            location.reload()
          })
        },
        error: function (e) {
          alertify.alert('Sorry! this data is used some where');
        }
      });
    });
  });
</script>
<script type="text/javascript">
  $('.edit-note').click(function(event){
    var id=$(event.target).attr("id"),
    t=$(event.target).parent().attr("id");

    url=t+"/edit/"+id,
    $.ajax({
      type:"GET",
      dataType:"JSON",
      url:url,
      success:function(response){
        $.each( response, function( key, value ) {
          $('.edit-form .'+key).val(value);
        });
      }
    });
  });
</script>
<!-- for search -->
<script type="text/javascript">
    document.addEventListener('keypress', function(event) {
                 if (event.keyCode == 13) {
                     event.preventDefault();
                     $("#search").click();
                 }
             });
        $('#search').click(function () {
        $('#loader').show();
    var searchParams = {};
    var base_url = '<?php echo url();?>/teacher/teacher_note/note_search';
    searchParams['class_id']=$('#class_id').val();
    var token = $('.token').val();
    $.ajax({
        type: 'get',
        data: 'parameters= ' + JSON.stringify(searchParams) + '&_token=' + token,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
                    $('#loader').hide('slow');
        }
    });
    return false;
});
</script>
@endsection
