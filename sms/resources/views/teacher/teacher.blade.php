@extends('teacher.app')

@section('content')
@include('teacher.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
  <div class="admin-top">
  	<div class="flash-message">
  		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
  		    @if(Session::has('alert-' . $msg))
  		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
  		  @endif
  		@endforeach
  	</div>
  		@if (count($errors) > 0)
  	<div class="alert alert-danger">
  		<strong>Whoops!</strong> There were some problems with your input.<br><br>
  		<ul>
  			@foreach ($errors->all() as $error)
  				<li>{{ $error }}</li>
  			@endforeach
  		</ul>
  	</div>
  		@endif
	</div>

<div class="heading"><h2>Teacher Summary</h2></div>
<div class="form-holder">
    <form class="general-form" id="category_valid">
    <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
    <div class="row">
    <div class="form-group col-md-3">
      <label for="category">Section</label>
      <div class="">
        <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="section_id" name="section_id" selected="">
        <option value="0">--Please Select--</option>
            @foreach($section_list as $index=>$class)
            <option value="{{$index}}">{{$class}}</option>
            @endforeach
        </select>
      </div>
    </div>
    <div class="form-group col-md-3">
      <label for="category">Shift</label>
      <div class="">
        <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="shift_id" name="shift_id" selected="">
        <option value="0">--Please Select--</option>
            @foreach($shift_list as $index=>$class)
            <option value="{{$index}}">{{$class}}</option>
            @endforeach
        </select>
      </div>
    </div>
    <div class="text-center">
    <button type="button" class="btn btn-default btn-sm" id="search">
      Search<i class="fa fa-paper-plane-o"></i>
    </button>
  </div>
  </div>
  </form>
</div>
<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myNote">
      Add Note
    </button>
            <!-- Modal -->
  <div id="myNote" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
    <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-shift" id="shift_valid" action="{{ URL::to('/') }}/teacher/note"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="subject_id" value="{{$subject_org_id}}">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <h2>Note</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label  for="name" class="col-md-4 control-label">Title:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="title" name="title" placeholder="Please enter title" required>
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="file" class="col-md-4 control-label">File:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="file" class="form-control input-sm" id="file" name="file">
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Add Note<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
  </div>
  </div>
  </div>
  <div class="row">
  <div id="printDiv">
  <div class="col-md-12">
      <table class="table table-bordered" id="replaceTable">
      <thead> 
        <tr>
            <th>SN</th>
            <th>Section</th>
            <th>Shift</th>
            <th>Period</th>
            <th>Topic</th>
            <th>Subtopic</th>
        </tr>
      </thead>
      <tbody>
          @foreach($teachers_record as $index=>$record)
        <tr>
           <td>{{$index+1}}</td>
           <td>{{$record->getSection->name}}</td>
           <td>{{$record->getShift->name}}</td>
           <td>{{$record->getPeriod->name}}</td>
           <td>{{$record->getTopic->name}}</td>
           <td>{{$record->getSubTopic->name}}</td>
       </tr>
          @endforeach
      </tbody>
      </table>
   </div>
   </div>
   </div>
 </div>
<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="{{ URL::to('/') }}/js/datepicker.js"></script>
 <script type="text/javascript">
    document.addEventListener('keypress', function(event) {
                 if (event.keyCode == 13)//for disabling the click enter
                  {
                     event.preventDefault();
                     $("#search").click();
                 }
             });
        $('#search').click(function () {
        $('#loader').show();

    var searchParams = {};
    var base_url = '<?php echo url();?>/teacher/teacher_search/<?php echo Request::segment(2)?>';
    searchParams['section_id']=$('#section_id').val();
    searchParams['shift_id']=$('#shift_id').val();
    var token = $('.token').val();
    $.ajax({
        type: 'get',
        data: 'parameters= ' + JSON.stringify(searchParams) + '&_token=' + token,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
                    $('#loader').hide('slow');

        }
    });
    return false;
});

</script>
@endsection