<table class="table table-bordered">
    <thead> 
      <tr>
          <th>SN</th>
          <th>Class</th>
          <th>Subject</th>
          <th>Title</th>
          <th>Notes File</th>
          <th>Updated Date</th>
          <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($teacher_notes as $index=>$note)
          <tr>
             <td>{{$index+1}}</td>
             <td>{{$note->getClass->name}}</td>
             <td>{{$note->getSubject->name}}</td>
             <td>{{$note->title}}</td>
             <td><a href="{{URL::to('/')}}/file/teacher/{{$note->file_encrypt}}" target="new">{{$note->file}}</a></td>
             <td>{{$note->updated_at}}</td>
             <td>
                <a class="action-btn edit-note" id="note" data-target="#myNote" data-toggle="modal" href=""><i class="fa fa-pencil-square-o" id="{{$note->id}}"></i></a>
                <a class="action-btn bg-green delete-note" id="note" href="javascript:void(0)"><i class="fa fa-times" id="{{$note->id}}"></i></a>
             </td>
         </tr>
        @endforeach
    </tbody>
  </table>