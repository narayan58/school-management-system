<table class="table table-bordered">
      <thead> 
        <tr>
            <th>SN</th>
            <th>Section</th>
            <th>Shift</th>
            <th>Period</th>
            <th>Topic</th>
            <th>Subtopic</th>
        </tr>
      </thead>
      <tbody>
          @foreach($teachers_record as $index=>$record)
        <tr>
           <td>{{$index+1}}</td>
           <td>{{$record->getSection->name}}</td>
           <td>{{$record->getShift->name}}</td>
           <td>{{$record->getPeriod->name}}</td>
           <td>{{$record->getTopic->name}}</td>
           <td>{{$record->getSubTopic->name}}</td>
       </tr>
          @endforeach
      </tbody>
</table>