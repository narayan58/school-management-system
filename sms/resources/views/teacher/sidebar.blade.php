<div class="sidebar">
	<div id="AdminMenu" class="active-menu">
		<a href="javascript:void(0)" class="sidemenu-open"><i class="fa fa-bars"></i></a>
		<div class="list-group panel">
			<!-- master data-->
			<a href="#master" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Class</span> <i class="fa fa-user"></i></a>
			<div class="collapse" id="master">
				@foreach($teacherClass as $teacher)
				@foreach($teacher->getTeacherInfo()->get() as $subjects)
				<a href="{{ URL::to('/') }}/teacher/{{$subjects->getAllSubject->slug}}" class="list-group-item"><span class="text">{{$subjects->getAllSubject->name}}({{$subjects->getAllSubject->subjectclass->name}})</span><i class="fa fa-list"></i></a>
          		@endforeach
				@endforeach
			</div>
				@if($teacher->getTeacherFirstClass)
			<a href="#teacher" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Class Teacher</span> <i class="fa fa-user"></i></a>
			<div class="collapse" id="teacher">
				@foreach($teacherClass as $teacher)
				<a href="{{ URL::to('/') }}/teacher/class-teacher/{{$teacher->getTeacherFirstClass->getTeacherFirstCls->slug}}" class="list-group-item"><span class="text">{{$teacher->getTeacherFirstClass->getTeacherFirstSubject->name}}({{$teacher->getTeacherFirstClass->getTeacherFirstCls->name}})<!--, {{$teacher->getTeacherFirstClass->getTeacherFirstSection->name}}, {{$teacher->getTeacherFirstClass->getTeacherFirstShift->name}}--></span><i class="fa fa-list"></i></a>
				@endforeach
			</div>
				@endif
			
			<a href="#notes" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Note Section</span> <i class="fa fa-user"></i></a>
			<div class="collapse" id="notes">
				<a href="{{ URL::to('/') }}/teacher/note" class="list-group-item"><span class="text">View Notes</span><i class="fa fa-list"></i></a>
			</div>
			<a href="#event" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Calendar</span> <i class="fa fa-calendar"></i></a>
			<div class="collapse" id="event">
				<a href="{{ URL::to('/') }}/teacher/cal/event" class="list-group-item"><span class="text">View Calendar</span><i class="fa fa-calendar"></i></a>
			</div>
		</div>
	</div>
</div>
