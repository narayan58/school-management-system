<div class="sidebar">
	<div id="AdminMenu" class="active-menu">
		<a href="javascript:void(0)" class="sidemenu-open"><i class="fa fa-bars"></i></a>
		<div class="list-group panel">
			<!-- master data-->
			<a href="#master" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Main</span> <i class="fa fa-user"></i></a>
			<div class="collapse" id="master">
				<a href="{{ URL::to('/') }}/student/exam" class="list-group-item"><span class="text">Exam</span><i class="fa fa-list"></i></a>
				<a href="{{ URL::to('/') }}/student/fee" class="list-group-item"><span class="text">Fee</span><i class="fa fa-list"></i></a>
				<a href="{{ URL::to('/') }}/student/todays_class" class="list-group-item"><span class="text">Today's Class</span><i class="fa fa-list"></i></a>
			</div>

			<a href="#notes" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Note</span> <i class="fa fa-user"></i></a>
			<div class="collapse" id="notes">
				<a href="{{ URL::to('/') }}/student/notes-view" class="list-group-item"><span class="text">View Note</span><i class="fa fa-list"></i></a>
			</div>

			<a href="#notices" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Notice</span> <i class="fa fa-user"></i></a>
			<div class="collapse" id="notices">
				<a href="{{ URL::to('/') }}/student/notice-view" class="list-group-item"><span class="text">View Notice</span><i class="fa fa-list"></i></a>
			</div>
			<a href="#events" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Calendar</span> <i class="fa fa-calendar"></i></a>
			<div class="collapse" id="events">
				<a href="{{ URL::to('/') }}/student/event" class="list-group-item"><span class="text">View Calendar</span><i class="fa fa-calendar"></i></a>
			</div>
			<a href="#attendance" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Attendance</span> <i class="fa fa-calendar"></i></a>
			<div class="collapse" id="attendance">
				<a href="{{ URL::to('/') }}/student/attendance" class="list-group-item"><span class="text">View Attendance</span><i class="fa fa-calendar"></i></a>
			</div>
		</div>
	</div>
</div>

