<header id="header">
<div class="header-holder">
	<div class="container">
		<div class="row text-right">
			<div class="col-md-9 col-md-offset-3">
				<ul class="header-list list-inline">
					<li><i class="fa fa-user text-center"></i>Student Panel</li>
					<li><i class="fa fa-phone"></i>9851196943 | 9849220335</li>
				</ul>
			</div>
		</div>
	</div>
</div>
	<nav id="mainNav" class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav">
						<span class="sr-only">Toggle navigation</span>
						<i class="fa fa-bars"></i>
					</button>
					<a class="navbar-brand page-scroll" href="{{ url('/student') }}"><img class="img-responsive" src="{{ URL::to('/') }}/images/logo.png" alt="company name" style="height: 85px;"></a>
			</div>
		<div class="collapse navbar-collapse" id="nav">
			

			<ul class="nav navbar-nav navbar-right">
				@if (Auth::guest())
					<li><a href="{{ url('/login') }}">Login</a></li>
					<li><a href="{{ url('/register') }}">Register</a></li>
				@else
				<li><a href="">Last Login:{{ Auth::user()->last_login_pre }}</a></li>
				<!-- <li><a href="{{ URL::to('/') }}/home/menu">Menu</a></li> -->
				<li><a href="{{ url('/student') }}">Home</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i>{{ Auth::user()->name }} <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ url('/logout') }}">Logout</a></li>
						<li><a href="{{URL::to('/')}}/student/change_password">Change Password</a></li>
					</ul>
				</li>
				@endif
			</ul>
		</div>
	</div>
</nav>
</header>