@extends('student.app')

@section('content')
@include('student.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
	<div class="heading"><h2>Student Summary</h2></div>
	<div class="row">
		@foreach($students as $student)
		<div class="col-md-3">
      		<a href="{{URL::to('/')}}/image/student/{{$student->image_encrypt}}" data-lightbox="{{$student->image}}" data-title="{{$student->image}}"><img src="{{URL::to('/')}}/image/thumb/student/{{$student->image_encrypt}}" alt="{{$student->image}}" class="img-responsive"></a>
	 	</div>
	 	<div class="col-md-9">
			<p>Fullname: {{$student->name}}</p>
			<p>Parents Name: {{$student->mobile_no}}</p>
			<p>Address: {{$student->address}}</p>
			<p>Roll no: {{$student->email}}</p>
			<p>Date of birth: {{$student->dob}}</p>
			<p>Gender: {{$student->gender}}</p>
			<p>Telephone No: {{$student->telephone_no}}</p>
	 	</div>
	 	@endforeach
	</div>
@endsection
