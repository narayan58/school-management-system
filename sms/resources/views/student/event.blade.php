@extends('student.app')

@section('content')
@include('student.sidebar')
<div class="admin-content">
	<div class="heading"><h2>Event Summary</h2></div>
    <div class="row">
      <div class="col-md-8">
    <div id='calendar'></div>
      </div>
    </div>
</div>
@endsection
<script src="{{ url('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#calendar').fullCalendar({
      eventLimit: true,
      events: [
      @for ($i = 0; $i < count($mydata); $i++)
        {
          title: '{{ $mydata[$i]['title'] }}',
          start: '{{ $mydata[$i]['start'] }}'
          @if($mydata[$i]['end'])
          ,end : '{{$mydata[$i]['end']}}'
          @endif
        },
        @endfor
      ]
    });
    
  });

</script>
