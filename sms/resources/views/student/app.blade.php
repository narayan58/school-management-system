<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Welcome Student | School Management System</title>
	<link rel="shortcut icon" href="{{ URL::to('/') }}/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="{{ URL::to('/') }}/images/favicon.ico" type="image/x-icon">
	<!-- <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,700italic,400italic%7cOswald:400,700' rel='stylesheet' type='text/css'> -->
	<link rel="stylesheet" href="{{ URL::to('/') }}/css/bootstrap.css">
	<link rel="stylesheet" href="{{ URL::to('/') }}/css/select2.min.css">
	<link rel="stylesheet" href="{{ URL::to('/') }}/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ URL::to('/') }}/css/all.css">
	<link rel="stylesheet" href="{{ URL::to('/') }}/css/alertify.min.css">
	<link rel="stylesheet" href="{{ URL::to('/') }}/css/default.min.css">
	<link rel="stylesheet" href="{{ URL::to('/')}}/css/datatables.css">
	<link rel="stylesheet" href="{{ URL::to('/')}}/css/lightbox.min.css">
	<link rel="stylesheet" href="{{ URL::to('/')}}/css/fullcalendar.min.css">
	
</head>
<body>
	<div id="wrapper">
		<!-- <div id="preloader" class="cssload-container">
			<div class="cssload-whirlpool"></div>
		</div> -->
		<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
		@include('student.header')
		<main id="main" class="admin-main" role="main">
			<div class="clearfix">
				@yield('content')
			</div>
		</main>
		@include('student.footer')
	</div>
	<!-- Scripts -->

	<script src="{{ url('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
	<script src="{{ url('/') }}/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/select2.full.min.js" type="text/javascript"></script>
	<script src="{{ url('/') }}/js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="{{ url('/') }}/js/alertify.min.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/datatables.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/validator.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/lightbox.min.js" type="text/javascript"></script>
  <script src="{{ URL::to('/') }}/js/moment.min.js" type="text/javascript"></script>
  <script src="{{ URL::to('/') }}/js/fullcalendar.min.js" type="text/javascript"></script>
	<script>
	    lightbox.option({
	      'resizeDuration': 200,
	      'wrapAround': true,
	      'fitImagesInViewport': true,
	      'disableScrolling':true
	    })
	</script>
	<script>
	function PrintDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
	</script>

</body>
</html>
