@extends('student.app')
@section('content')
@include('student.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
	<div class="heading"><h2>Class:{{$class_name}} &nbsp; Section:{{$class_section}}  &nbsp; Shift:{{$class_shift}}</h2></div>
	<div class="row">
	<div class="col-md-8">
    @foreach($notices as $index=>$notice)
    <table class="table table-bordered">
    <thead> 
      <tr>
          <th>SN</th>
           @foreach($notice->getNotice()->get() as $notices)
          <th>{{$notices->name}} : {{$notice->created_at}} Published By: {{$notice->getUser->name}}</th>
          	@endforeach
      </tr>
    </thead>
    <tbody>
          <tr>
             <td>{{$index+1}}</td>
             <td>
            @foreach($notice->getNotice()->get() as $noticees)
                  {!! $noticees->title !!}
          	@endforeach
             </td>
       		</tr>
    </tbody>
 	</table>
    @endforeach
 	</div>
	</div>
</div>
@endsection
