@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '8'))
  <div class="heading"><h2>Event Summary</h2></div>
  <div class="row">
  <form class="general-form" action="{{ URL::to('/') }}/home/event_view/event_search"  method="GET">
  <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
  <div class="form-group col-sm-4">
    <div class="required">
      <div class="input-group date" data-provide="datepicker">
          <input data-type='to_date' type="text" id="search_date" name="start" class="form-control input-sm to-date datepicker searchKeys required" value="<?= (isset($search_date))?$search_date:''?>"  placeholder="Date">
            <div class="input-group-addon">
               <span class="fa fa-calendar"></span>
            </div>
      </div>
    </div>
  </div>
  <div class="row form-group ">
    <div class="col-md-4">
          <button type="button" class="btn btn-default btn-sm" id='search'>
            Search<i class="fa fa-paper-plane-o"></i>
          </button>
    </div>
  </div>
  </form>
  </div>
  <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myEvent">
      Add Event
  </button>
  <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/eventAdd_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button>
  <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
  
  <div id="printDiv">
    <div class="col-md-12">
    <div class="row" >
    <table class="table table-bordered" id="replaceTable">
      <thead> 
        <tr>
            <th>SN</th>
            <th>Title</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Created By</th>
            <th>Created On</th>
            <th class="hidden-print">Action</th>
        </tr>
      </thead>
      <tbody>
          @foreach($calendars as $index=>$calendar)
        <tr>
         <td>{{$index+1}}</td>
         <td>{{$calendar->title}}</td>
         <td>{{$calendar->start}}</td>
         <td>{{$calendar->end}}</td>
         <td>{{$calendar->getUser->name}}</td>
         <td>{{$calendar->created_at}}</td>
         <td class="hidden-print">
          <a class="action-btn edit" id="calendar" data-target="#edit_calendar" data-toggle="modal" href=""><i class="fa fa-pencil-square-o" id="{{$calendar->id}}"></i></a>
          <a class="action-btn bg-green delete" id="calendar" href="javascript:void(0)"><i class="fa fa-times" id="{{$calendar->id}}"></i></a>
         </td>
        </tr>
          @endforeach
      </tbody>
    </table>
    </div>
    </div>
  </div>
            <!-- Modal -->
  <div id="myEvent" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
   <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-event" action="{{ URL::to('/') }}/home/event_view"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <h2>Event</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label  for="name" class="col-md-4 control-label">Title:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="title" name="title" placeholder="Please enter section name">
            </div>
            </div>
          </div>
        </div>
         <div class="row">
            <label for="date"class="col-md-4 control-label">Start Date:</label>
             <div class="form-group col-sm-8">
              <div class="required">
                <div class="input-group date" data-provide="datepicker">
                    <input data-type='to_date' type="text" id="start" name="start" class="form-control input-sm to-date datepicker required" placeholder="Please Select Start Date">
                      <div class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                      </div>
                </div>
              </div>
             </div>
         </div>
         <div class="row">
            <label for="date"class="control-label col-md-4">End Date:</label>
            <div class="form-group col-sm-8">
                <div class="input-group date" data-provide="datepicker">
                    <input data-type='to_date' type="text" id="end" name="end" class="form-control input-sm to-date datepicker"  placeholder="Please Select End Date">
                      <div class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                      </div>
                </div>
            </div>
         </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Add Event<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
  </div>
  </div>
  </div>

<div id="edit_calendar" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
   <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form edit-form" action="{{ URL::to('/') }}/home/calendar/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="calendar_id" class="id">
        <h2>Event</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label  for="name" class="col-md-4 control-label">Title:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm title" id="title" name="title" placeholder="Please enter section name">
            </div>
            </div>
          </div>
        </div>
         <div class="row">
            <label for="date"class="col-md-4 control-label">Start Date:</label>
             <div class="form-group col-sm-8">
              <div class="required">
                <div class="input-group date" data-provide="datepicker">
                    <input data-type='to_date' type="text" id="start" name="start" class="form-control input-sm to-date datepicker required start" placeholder="Date">
                      <div class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                      </div>
                </div>
              </div>
             </div>
         </div>
         <div class="row">
            <label for="date"class="control-label col-md-4">End Date:</label>
            <div class="form-group col-sm-8">
              <div class="required">
                <div class="input-group date" data-provide="datepicker">
                    <input data-type='to_date' type="text" id="end" name="end" class="form-control input-sm to-date datepicker end" placeholder="Date">
                      <div class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                      </div>
                </div>
              </div>
            </div>
         </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Update Event<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
  </div>
  </div>
  </div>
     @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
  </div>


<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="{{ URL::to('/') }}/js/datepicker.js"></script>
<script type="text/javascript">
    // filter for teacher to get shift
    $('#search').click(function(){
        var search_date = $('#search_date').val(),
            token =$('.token').val();
        $.ajax({
            type:"GET",
            dataType:"html",
            url: "event_view/event_search/"+search_date,
            data: {
              _token: token
            },
            success: function(response) {
              if(response){
                $('#replaceTable').html(response);
                $('#loader').hide('slow');
              }
            },
            error: function (response) {
            alertify.alert('Sorry! this data couldnot display');
          }
        });
    });
</script>
@endsection
