<button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
    <div id="printDiv">
<table class="table table-bordered">
      <thead> 
        <tr>
            <th>SN</th>
            <th>Teacher Name</th>
            <th>Shift</th>
            <th>Section</th>
            <th>Period</th>
            <th>Subject</th>
            <th>Class</th>
            <th>Topic</th>
            <th>Subtopic</th>
            <th>Date</th>
            <th>Created By</th>
            <th>Created On</th>
            <th class="hidden-print">Action</th>
        </tr>
      </thead>
      <tbody>
          @foreach($teacher_records as $index=>$teacher_record)
        <tr>
         <td>{{$index+1}}</td>
         <td>{{$teacher_record->getTeacher->name}}</td>
         <td>{{$teacher_record->getShift->name}}</td>
         <td>{{$teacher_record->getSection->name}}</td>
         <td>{{$teacher_record->getPeriod->name}}</td>
         <td>{{$teacher_record->getSubject->name}}</td>
         <td>{{$teacher_record->getClass->name}}</td>
         <td>{{$teacher_record->getTopic->name}}</td>
         <td>{{$teacher_record->getSubTopic->name}}</td>
         <td>{{$teacher_record->lecture_date}}</td>
         <td>{{$teacher_record->getUser->name}}</td>
         <td>{{$teacher_record->created_at}}</td>
         <td class="hidden-print">
          <a class="action-btn bg-green delete" id="teacher_record" href="javascript:void(0)"><i class="fa fa-times" id="{{$teacher_record->id}}"></i></a>
         </td>
        </tr>
          @endforeach
      </tbody>
    </table>
</div>