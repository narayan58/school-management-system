@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '6'))
  <div class="heading"><h2>Fee Structure Summary</h2></div>
  <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myShift">
      Add Fee Structure
  </button>
  <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/feestructure_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button></span>
  <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>

  <div id="printDiv">
  <div class="col-md-8">
    <div class="row">
    <table class="table table-bordered">
      <thead> 
        <tr>
            <th>SN</th>
            <th>Class</th>
            <th>Monthly Amount</th>
            <th>Created By</th>
            <th>Created On</th>
            <th class="hidden-print">Action</th>
        </tr>
      </thead>
      <tbody>
          @foreach($fee_structure as $index=>$fees)
      <tr>
       <td>{{$index+1}}</td>
       <td>{{$fees->getClassName->name}}</td>
       <td>{{$fees->amount}}</td>
       <td>{{$fees->getUser->name}}</td>
       <td>{{$fees->created_at}}</td>
       <td class="hidden-print">
          <a class="action-btn edit" id="fees" data-target="#edit_fees" data-toggle="modal" href=""><i class="fa fa-pencil-square-o" id="{{$fees->id}}"></i></a>
          <a class="action-btn bg-green delete" id="fees" href="javascript:void(0)"><i class="fa fa-times" id="{{$fees->id}}"></i></a>
       </td>
      </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  </div>
  </div>

            <!-- Modal -->
  <div id="myShift" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
    <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
   <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-feestructure_valid" id="feestructure_valid" action="{{ URL::to('/') }}/home/feestructure"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <h2>Fee Structure</h2>
        <p class="add-info">* Denotes required Field</p>
     <div class="row">
          <label for="name" class="col-md-4 control-label">Class:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control class-name" id="class_id" name="class_id">
                <option value="">Please select class</option>
                  @foreach($classees as $fee_structures)
                  <option value="{{$fee_structures->id}}">{{$fee_structures->name}}</option>
                  @endforeach
                </select>
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-md-4 control-label">Amount:</label>
          <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm" id="amount" name="amount" placeholder="Please enter class name">
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Add Fee Structure<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
  </div>
  </div>
  </div>


<div id="edit_fees" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
    <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
   <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form edit-form" id="feestructure_valid" action="{{ URL::to('/') }}/home/fees/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="feestructure_id" class="id">

        <h2>Fee Structure</h2>
        <p class="add-info">* Denotes required Field</p>
     <div class="row">
          <label for="name" class="col-md-4 control-label">Class:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control class-name class_id" id="class_id" name="class_id">
                <option value="">Please select class</option>
                  @foreach($classees as $fee_structures)
                  <option value="{{$fee_structures->id}}">{{$fee_structures->name}}</option>
                  @endforeach
                </select>
            </div>
            </div>
          </div>
        </div>
        <div class="row">

          <label class="col-md-4 control-label">Amount:</label>
          <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm amount" id="amount" name="amount" placeholder="Please enter class name">
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Update Fee Structure<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
  </div>
  </div>
  </div>
     @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
  </div>


@endsection
