<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
//header("Content-type: application/octet-stream;charset=utf-8");
header("Content-Type: text/html; charset=UTF-8");
header("Content-Disposition: attachment; filename=SectionRecord.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table class="table table-bordered" border="1">
    <thead> 
      <tr>
          <th>SN</th>
          <th>Section</th>
      </tr>
    </thead>
    <tbody>
        @foreach($sections as $index=>$sec)
          <tr>
             <td>{{$index+1}}</td>
             <td>{{$sec->name}}</td>
         </tr>
        @endforeach
    </tbody>
  </table>