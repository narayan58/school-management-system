@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '3'))
	<div class="heading"><h2>Student Attendance</h2></div>
            <!-- Button trigger modal -->
  
<div class="form-holder">
<form class="general-form">
  <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
  <div class="row">
  <div class="form-group col-md-2">
    <label for="category">Class</label>
    <div class="">
      <select data-type="integer" type="text" class="form-control input-sm class-name" id="class_id" name="class_id" selected="">
      <option value="0">--Please Select--</option>
          @foreach($class_list as $index=>$class)
          <option value="{{$index}}">{{$class}}</option>

          @endforeach
      </select>
    </div>
  </div>
  <div class="form-group col-md-2">
    <label for="category">Section</label>
    <div class="">
      <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="section_id_org" name="section_id" selected="">
      <option value="0">--Please Select--</option>
          
      </select>
    </div>
  </div>
  <div class="form-group col-md-2">
    <label for="category">Shift</label>
    <div class="">
      <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="shift_id_org" name="shift_id" selected="">
      <option value="0">--Please Select--</option>
      </select>
    </div>
  </div>
  <div class="form-group col-md-2">
    <label for="category">Date</label>
    <div class="input-group date" data-provide="datepicker" data-date-end-date="0d">
          <input data-type='to_date' type="text" id="attendance_date" name="attendance_date" class="form-control input-sm to-date datepicker required" value="<?= (isset($from_date))?$from_date:''?>"  placeholder="Date">
            <div class="input-group-addon">
               <span class="fa fa-calendar"></span>
            </div>
      </div>
  </div>
  <div class="text-center">
  <button type="button" class="btn btn-default btn-sm" id="search">
    Search<i class="fa fa-paper-plane-o"></i>
  </button>
</div>
</div>
</form>
</div>
    
    <div class="col-md-12">
    <div class="row" id="replaceTable">
    
    </div>
    </div>
   @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="{{ URL::to('/') }}/js/datepicker.js"></script> 
<script type="text/javascript">
    // filter for teacher to get section
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getSectionOnStudent/"+teacher_id,
            success:function(e){
                $('#section_id_org').html('');
                $('#section_id_org').append('<option value="">Please Select Section</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#section_id_org').append('<option value='+val.get_section_class.id+'>'+val.get_section_class.name+'</option>');
                    }
                });
            }
        })
    });
    </script>
    <script type="text/javascript">
    // filter for teacher to get shift
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getShiftOnStudent/"+teacher_id,
            success:function(e){
                $('#shift_id_org').html('');
                $('#shift_id_org').append('<option value="">Please Select Shift</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#shift_id_org').append('<option value='+val.get_shift_class.id+'>'+val.get_shift_class.name+'</option>');
                    }
                });
            }
        })
    });
    </script>
    <script type="text/javascript">
    // filter for teacher to get shift
    $('#search').click(function(){
        var class_id = $('#class_id').val(),
            section_id = $('#section_id_org').val(),
            shift_id = $('#shift_id_org').val(),
            attendance_date = $('#attendance_date').val(),
            token =$('.token').val();
        $.ajax({
            type:"POST",
            dataType:"html",
            url: "daily-student-attendance/search",
            data: {
              _token: token,
              class_id:class_id,
              section_id:section_id,
              shift_id:shift_id,
              attendance_date:attendance_date
            },
            success: function(response) {
              if(response){
                $('#replaceTable').html(response);
                $('#loader').hide('slow');
              }
            },
            error: function (response) {
            alertify.alert('Sorry! this data couldnot display');
          }
        });
    });
</script>
@endsection
