@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '5'))
	<div class="heading"><h2>Class {{$class_org_name}} Subjects on {{$class_org_exam}}</h2></div>
    <div class="col-md-8 form-box">
      @if(count($marksSubexams))
          <table class="table table-bordered">
    <thead> 
        <tr>
            <th>SN</th>
            <th>Subject</th>
            <th>Full Marks</th>
            <th>Pass Marks</th>
        </tr>
    </thead>
    <tbody>
      <form class="general-form" id="exam_class_valid" action="{{ URL::to('/') }}/home/exam/class/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="exam_id" class="exam_id" value="{{$class_org_exam_id}}">
        <input type="hidden" name="class_id" class="class_id" value="{{$class_name}}">
        @foreach($marksSubexams as $index=>$marksSubexam)
        <tr>
           <td><input type="hidden" name="marks_id[]" value="{{$marksSubexam->id}}">{{$index+1}}</td>
           <input type="hidden" name="subject_id[]" value="{{$marksSubexam->getSubjectClass->id}}">
           <td>{{$marksSubexam->getSubjectClass->name}}</td>
           <td><input type="text" class="form-control input-sm" id="fullmarks" name="fullmarks[]" value="{{$marksSubexam->full_mark}}" placeholder="Please enter full marks" required></td>
           <td><input type="text" class="form-control input-sm" id="passmarks" name="passmarks[]" value="{{$marksSubexam->pass_mark}}" placeholder="Please enter pass marks" required></td>
           </td>
        </tr>
        @endforeach
        <tr>
          <td colspan="4">
        <div class="text-right">
          <button type="submit" class="btn btn-default btn-sm">
            Update Exam Subject Marks<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </td>
        </tr>
      </form>
    </tbody>
    </table>
      @else
        @if(count($subjects))
    <table class="table table-bordered">
    <thead> 
        <tr>
            <th>SN</th>
            <th>Subject</th>
            <th>Full Marks</th>
            <th>Pass Marks</th>
        </tr>
    </thead>
    <tbody>
      <form class="general-form exam_class_valid" id="exam_class_valid" action="{{ URL::to('/') }}/home/exam/class/store"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="exam_id" class="exam_id" value="{{$class_org_exam_id}}">
        <input type="hidden" name="class_id" class="class_id" value="{{$class_name}}">
        @foreach($subjects as $index=>$subject)
        <tr>
           <td>{{$index+1}}</td>
           <input type="hidden" name="subject_id[]" value="{{$subject->id}}">
           <td>{{$subject->name}}</td>
           <td><input type="text" class="form-control input-sm" id="fullmarks" name="fullmarks[]" placeholder="Please enter full marks"></td>
           <td><input type="text" class="form-control input-sm" id="passmarks" name="passmarks[]" placeholder="Please enter pass marks"></td>
           </td>
        </tr>
        @endforeach
        <tr>
          <td colspan="4">
          <div class="text-right">
            <button type="submit" class="btn btn-default btn-sm">
              Add Exam Subject Marks<i class="fa fa-paper-plane-o"></i>
            </button>
          </div>
          </td
        </tr>
      </form>
    </tbody>
    </table>
        @else
        <tr>Please enter subjects</tr>
        @endif
    @endif
    </div>
    @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
@endsection
