  <?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
//header("Content-type: application/octet-stream;charset=utf-8");
header("Content-Type: text/html; charset=UTF-8");
header("Content-Disposition: attachment; filename=StudentRecord.xls");
header("Pragma: no-cache");
header("Expires: 0");
?><table class="table table-bordered" border="1">
    <thead> 
      <tr>
         <th>SN</th>
          <th>Full Name</th>
          <th>Address</th>
          <th>Roll no</th>
          <th>Date of Birth</th>
          <th>Gender</th>
          <th>Telephone No</th>
          <th>Mobile No</th>
          <th>Class</th>
          <th>Section</th>
          <th>Shift</th>
      </tr>
    </thead>
    <tbody>
        @foreach($students as $index=>$student)
      <tr>
       <td>{{$index+1}}</td>
       <td>{{$student->studentinfo->name}}</td>
       <td>{{$student->studentinfo->address}}</td>
       <td>{{$student->studentinfo->email}}</td>
       <td>{{$student->studentinfo->dob}}</td>
       <td>{{$student->studentinfo->gender}}</td>
       <td>{{$student->studentinfo->telephone_no}}</td>
       <td>{{$student->studentinfo->mobile_no}}</td>
       <td>{{$student->studentclass->name}}</td>
       <td>{{$student->studentsection->name}}</td>
       <td>{{$student->studentshift->name}}</td>
      </tr>
        @endforeach
    </tbody>
  </table>