@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
    <div class="admin-top">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
        @endforeach
    </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    <div class="heading"><h2>Student Mark Summary</h2></div>
<table class="table table-bordered">
<thead> 
    <tr>
        <th>SN</th>
        <th>Student Name</th>
        <th>Action</th>
        <th>Grade View</th>
    </tr>
</thead>
<tbody>
    @foreach($studentlists as $index=>$studentlist)
        <tr>
            <td>{{$index+1}}</td>
            <td>{{$studentlist->student_name}}</td>
@if(in_array($studentlist->user_id,$student_marksheet))
            <td>
    <button type="button" class="btn btn-default btn-sm update_marksheet" id="{{$studentlist->user_id}}" data-toggle="modal" data-target="#update_marksheet">
      View | Edit
    </button>
            </td>
            <td>
    <button type="button" class="btn btn-default btn-sm grade_mark" id="{{$studentlist->user_id}}" data-toggle="modal" data-target="#update_grade">
      Grade
    </button>
            </td>
@else
            <td>
    <button type="button" class="btn btn-default btn-sm marksheet" id="{{$studentlist->user_id}}" data-toggle="modal" data-target="#edit_category">
      Add Marksheet
    </button>
            </td>
            <td>Grade not availiable</td>
@endif
            
        </tr>
    @endforeach
</tbody>
</table>
    <div id="edit_category" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-class" id="studentsub" action="{{ URL::to('/') }}/home/exam/marksheet/store"  method="GET" enctype="multipart/form-data">
            <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
            <input type="hidden" name="class_id" class="class_id" value="{{$class_id}}">
            <input type="hidden" name="section_id" class="section_id" value="{{$section_id}}">
            <input type="hidden" name="shift_id" class="shift_id" value="{{$shift_id}}">
            <input type="hidden" name="exam_id" class="exam_id" value="{{$exam_id}}">
       <div class="append_mark">
            <!-- append list here -->
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>
    <!-- updated marksheet -->
    <div id="update_marksheet" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-class" id="studentsub" action="{{ URL::to('/') }}/home/exam/marksheet/update"  method="GET" enctype="multipart/form-data">
            <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
            <input type="hidden" name="class_id" class="class_id" value="{{$class_id}}">
            <input type="hidden" name="section_id" class="section_id" value="{{$section_id}}">
            <input type="hidden" name="shift_id" class="shift_id" value="{{$shift_id}}">
            <input type="hidden" name="exam_id" class="exam_id" value="{{$exam_id}}">
       <div class="append_mark_update">
            <!-- append list here -->
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>
    <!-- grade view -->
    <div id="update_grade" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-class" id="studentsub" action="{{ URL::to('/') }}/home/exam/marksheet/store"  method="GET" enctype="multipart/form-data">
            <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
            <input type="hidden" name="class_id" class="class_id" value="{{$class_id}}">
            <input type="hidden" name="section_id" class="section_id" value="{{$section_id}}">
            <input type="hidden" name="shift_id" class="shift_id" value="{{$shift_id}}">
            <input type="hidden" name="exam_id" class="exam_id" value="{{$exam_id}}">
       <div class="append_grade">
            <!-- append list here -->
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>

    </div>
<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    // filter for teacher to get section
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getSectionOnStudent/"+teacher_id,
            success:function(e){
                $('#section_id').html('');
                $('#section_id').append('<option value="">Please Select Section</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#section_id').append('<option value='+val.get_section_class.id+'>'+val.get_section_class.name+'</option>');
                    }
                });
            }
        })
    });
</script>
<script type="text/javascript">
    // filter for teacher to get shift
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getShiftOnStudent/"+teacher_id,
            success:function(e){
                $('#shift_id').html('');
                $('#shift_id').append('<option value="">Please Select Shift</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#shift_id').append('<option value='+val.get_shift_class.id+'>'+val.get_shift_class.name+'</option>');
                    }
                });
            }
        })
    });
</script>
<script type="text/javascript">
    // filter get student list
    $('.studentlist').click(function(event){
        var class_id = $('.class-name').val(),
            shift_id = $('.shift-name').val(),
            section_id = $('.section-name').val(),
            exam_id = $('.exam_id').val(),
            token =$('.token').val();
        $.ajax({
            type:"POST",
            dataType:"html",
            url: "getStudentList",
            data: {
                _token: token,
                class_id: class_id,
                shift_id: shift_id,
                section_id:section_id,
                exam_id:exam_id,
            },
            success: function(response){
                    $('.append_items').html(response);
            },
            error: function (e) {
                alert('Sorry! we cannot load data this time');
                return false;
            }
        })
    });
</script>
<script type="text/javascript">
    // filter get student class respective subjects
    $('.marksheet').click(function(event){
        var student_id = $(event.target).attr('id'),
            exam_id = $('.exam_id').val(),
            token =$('.token').val();
        $.ajax({
            type:"POST",
            dataType:"html",
            //141
            url: "{{URL::route('gstudentlistmark')}}",
            data: {
                _token: token,
                student_id: student_id,
                exam_id: exam_id,
            },
            success:function(response){
               $('.append_mark').html(response);
            }
        })
    });
    </script>
    <script type="text/javascript">
    // filter get student class respective subjects
    $('.update_marksheet').click(function(event){
        var student_id = $(event.target).attr('id'),
            exam_id = $('.exam_id').val(),
            token =$('.token').val();
        $.ajax({
            type:"POST",
            dataType:"html",
            //142
            url: "{{URL::route('gstudentlistmarkupdate')}}",
            data: {
                _token: token,
                student_id: student_id,
                exam_id: exam_id,
            },
            success:function(response){
               $('.append_mark_update').html(response);
            }
        })
    });
    </script>
    <script type="text/javascript">
    // filter get student grade respective subjects
    $('.grade_mark').click(function(event){
        var student_id = $(event.target).attr('id'),
            exam_id = $('.exam_id').val(),
            token =$('.token').val();
        $.ajax({
            type:"POST",
            dataType:"html",
            //142
            url: "{{URL::route('gstudentlistgrade')}}",
            data: {
                _token: token,
                student_id: student_id,
                exam_id: exam_id,
            },
            success:function(response){
               $('.append_grade').html(response);
            }
        })
    });
    </script>
    


    
@endsection

