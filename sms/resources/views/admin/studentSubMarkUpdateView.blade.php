<div class="heading"><h2>{{$student_org_name}}</h2></div>
<div class="row">
<div class="col-md-12">
<table class="table table-bordered">
<thead> 
    <tr>
        <th>SN</th>
        <th>Subject</th>
        <th>Full Mark</th>
        <th>Pass Mark</th>
        <th>Practical</th>
        <th>Theory</th>
        <th>Obtained Mark</th>
    </tr>
</thead>
<tbody>
    <input type="hidden" name="student_id" value="{{$student_id}}">
    <input type="hidden" name="exam_id" value="{{$exam_id}}">
    @foreach($studentMarkSheet as $index=>$mark)
    <tr>
        <td><input type="hidden" name="marks_id[]" value="{{$mark->id}}">{{$index+1}}</td>
        @foreach($mark->getStudentSubMark()->get() as $sub_mark)
        <td>{{$sub_mark->getSubjectClass->name}}</td>
        <td><input type="hidden" class="form-control input-sm" id="full_mark" name="full_mark[]" value="{{$sub_mark->full_mark}}">{{$sub_mark->full_mark}}</td>
        <td>{{$sub_mark->pass_mark}}</td>
        @endforeach
        <td><input type="text" class="form-control input-sm" id="marks_pr" name="marks_pr[]" value="{{$mark->marks_pr}}"></td>
        <td><input type="text" class="form-control input-sm" id="marks_th" name="marks_th[]" value="{{$mark->marks_th}}"></td>
        <td><input type="text" class="form-control input-sm" name="marks_obt[]" value="{{$mark->marks_obt}}"></td>
    </tr>
    @endforeach
</tbody>
</table>
</div>
</div>
<div class="text-center">
  <button type="submit" class="btn btn-default btn-sm">
    Update Marksheet<i class="fa fa-paper-plane-o"></i>
  </button>
</div>
