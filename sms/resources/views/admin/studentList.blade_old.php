<div class="admin-top">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
        @endforeach
    </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
<table class="table table-bordered">
<thead> 
    <tr>
        <th>SN</th>
        <th>Student Name</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    @foreach($studentlists as $index=>$studentlist)
        <tr>
            <td>{{$index+1}}</td>
            <td>{{$studentlist->student_name}}</td>
            <td>
                @if($studentlist->mark_stu!=null && $exam_id==$studentlist->stu_exam)
    <button type="button" class="btn btn-default btn-sm marksheet" id="{{$studentlist->user_id}}" data-toggle="modal" data-target="#edit_category">
      Marksheet Added
    </button>
                @else
    <button type="button" class="btn btn-default btn-sm marksheet" id="{{$studentlist->user_id}}" data-toggle="modal" data-target="#edit_category">
      Add Marksheet
    </button>
        @endif
            </td>
        </tr>
    @endforeach
</tbody>
</table>
    <div id="edit_category" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-class" id="studentsub" action="{{ URL::to('/') }}/home/exam/marksheet/store"  method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
            <input type="hidden" name="class_id" class="class_id" value="{{$class_id}}">
            <input type="hidden" name="section_id" class="section_id" value="{{$section_id}}">
            <input type="hidden" name="shift_id" class="shift_id" value="{{$shift_id}}">
       <div class="append_mark">
            <!-- append list here -->
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>
<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    // filter for teacher to get section
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getSectionOnStudent/"+teacher_id,
            success:function(e){
                $('#section_id').html('');
                $('#section_id').append('<option value="">Please Select Section</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#section_id').append('<option value='+val.get_section_class.id+'>'+val.get_section_class.name+'</option>');
                    }
                });
            }
        })
    });
</script>
<script type="text/javascript">
    // filter for teacher to get shift
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getShiftOnStudent/"+teacher_id,
            success:function(e){
                $('#shift_id').html('');
                $('#shift_id').append('<option value="">Please Select Shift</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#shift_id').append('<option value='+val.get_shift_class.id+'>'+val.get_shift_class.name+'</option>');
                    }
                });
            }
        })
    });
</script>
<script type="text/javascript">
    // filter get student list
    $('.studentlist').click(function(event){
        var class_id = $('.class-name').val(),
            shift_id = $('.shift-name').val(),
            section_id = $('.section-name').val(),
            exam_id = $('.exam_id').val(),
            token =$('.token').val();
        $.ajax({
            type:"POST",
            dataType:"html",
            url: "getStudentList",
            data: {
                _token: token,
                class_id: class_id,
                shift_id: shift_id,
                section_id:section_id,
                exam_id:exam_id,
            },
            success: function(response){
                    $('.append_items').html(response);
            },
            error: function (e) {
                alert('Sorry! we cannot load data this time');
                return false;
            }
        })
    });
</script>
<script type="text/javascript">
    // filter get student class respective subjects
    $('.marksheet').click(function(event){
        var student_id = $(event.target).attr('id'),
            exam_id = $('.exam_id').val(),
            token =$('.token').val();
        $.ajax({
            type:"POST",
            dataType:"html",
            url: "getStudentClassSub",
            data: {
                _token: token,
                student_id: student_id,
                exam_id: exam_id,
            },
            success:function(response){
               $('.append_mark').html(response);
            }
        })
    });
    </script>

