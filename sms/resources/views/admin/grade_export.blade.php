<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
//header("Content-type: application/octet-stream;charset=utf-8");
header("Content-Type: text/html; charset=UTF-8");
header("Content-Disposition: attachment; filename=GradeRecord.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table class="table table-bordered" border="1">
    <thead> 
        <tr>
            <th>SN</th>
            <th>Grade</th>
            <th>From</th>
            <th>To</th>
    </thead>
    <tbody>
        @foreach($grades as $index=>$grade)
        <tr>
           <td>{{$index+1}}</td>
           <td>{{$grade->name}}</a></td>
           <td>{{$grade->lower}}</td>
           <td>{{$grade->higher}}</td>
        </tr>
        @endforeach
    </tbody>
  </table>