@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>@foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '5'))
	<div class="heading"><h2>Exam Summary</h2></div>
	<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myExam">
      Add Exam
    </button>
     <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/exam_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button>
     <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
    <div id="printDiv">
  <div class="col-md-12">
    <div class="row">
    <table class="table table-bordered">
    <thead> 
        <tr>
            <th>SN</th>
            <th>Exam</th>
            <th>Class</th>
            <th>Created By</th>
            <th>Created On</th>
            <th class="hidden-print">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($exams as $index=>$exam)
        <tr>
           <td>{{$index+1}}</td>
           <td><a href="{{URL::to('/')}}/home/exam/{{$exam->slug}}">{{$exam->name}}</a></td>
           <td>
            @foreach($exam->getExamDetail()->get() as $examp)
            <button type="button" class="btn btn-info btn-sm">
              <a href="{{URL::to('/')}}/home/exam/{{$exam->slug}}/class/{{$examp->getExamClassDetail->slug}}">{{$examp->getExamClassDetail->name}}</a>
            </button>
            @endforeach
           </td>
           <td>{{$exam->getUser->name}}</td>
           <td>{{$exam->created_at}}</td>
           <td class="hidden-print">
          <a class="action-btn edit" id="exam" data-target="#edit_exam" data-toggle="modal" href=""><i class="fa fa-pencil-square-o" id="{{$exam->id}}"></i></a>
          <a class="action-btn bg-green delete" id="exam" href="javascript:void(0)"><i class="fa fa-times" id="{{$exam->id}}"></i></a>
         </td>
        </tr>
        @endforeach
    </tbody>
    </table>
    </div>
    </div>
    </div>

    <div id="myExam" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-class" id="class_valid" action="{{ URL::to('/') }}/home/exam/store"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <h2>Exam</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label class="col-md-4 control-label">Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm" id="exam" name="name" placeholder="Please enter exam name" >
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="image" class="col-md-4 control-label">Class:</label>
            <div class="col-md-8">
              @foreach($classes as $class)
                <div class="col-sm-6">
                  <div class="checkbox">
                      <input type="checkbox" id="{{$class->id}}" name="class_id[]" value="{{$class->id}}">
                      {{$class->name}}
                  </div>
                </div>
                @endforeach
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Add Exam<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>

<div id="edit_exam" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form edit-form" id="exam_valid" action="{{ URL::to('/') }}/home/exam/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="exam_id" class="id">

        <h2>Exam</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label class="col-md-4 control-label">Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm name" id="exam" name="name" placeholder="Please enter exam name" >
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="image" class="col-md-4 control-label">Class:</label>
            <div class="col-md-8">
              @foreach($classes as $class)
                <div class="col-sm-6">
                  <div class="checkbox">
                      <input type="checkbox" id="{{$class->id}}" name="class_id[]" value="{{$class->id}}">
                      {{$class->name}}
                  </div>
                </div>
                @endforeach
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Update Exam<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>
     @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>

@endsection
