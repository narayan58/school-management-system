@extends('admin.app')
@section('content')
@include('admin.sidebar')
<div class="admin-content">
  <div class="admin-top">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
  </div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '2'))
  <div class="heading"><h2>{{$subtopic_name}} Topic Summary</h2></div>
  <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#mysubTopic">
      Add Sub Topic on {{$subtopic_name}}
    </button> 
  <div class="col-md-12">
    <div class="row">
    <table class="table table-bordered">
    <thead> 
        <tr>
         <th>SN</th>
          <th>Sub Topic Name</th>
          <th>Sub Topic hour</th>
          <th class="hidden-print">Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach($subtopics as $index=>$subtopic)
        <tr>
         <td>{{$index+1}}</td>
         <td>{{$subtopic->name}}</td>
         <td>{{$subtopic->subtopic_hour}}</td>
         <td class="hidden-print">
          <a class="action-btn edit" id="subtopic" data-target="#edit_subtopic" data-toggle="modal" href=""><i class="fa fa-pencil-square-o" id="{{$subtopic->id}}"></i></a>
            <a class="action-btn bg-green delete" id="subtopic" href="javascript:void(0)"><i class="fa fa-times" id="{{$subtopic->id}}"></i></a>
            </tr>
      </tr>
        @endforeach
    </tbody>
    </table>
      </div>
    </div>
    </div>
    <div id="mysubTopic" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-category" id="subtopic_valid" action="{{URL::route('home.sub_topic.store')}}"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="topic_id" value="{{$subtopic_id}}">
        <h2>Add Sub Topic on {{$subtopic_name}}</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label for="subtopic_name" class="col-md-4 control-label">Sub Topic Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm" id="subtopic_name" name="name" placeholder="Please enter sub topic name">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="subtopic_hour" class="col-md-4 control-label">Sub Topic Hour:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm" id="subtopic_hour" name="subtopic_hour" placeholder="Please enter sub topic hour ">
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Add Sub Topic<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>

    <div id="edit_subtopic" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form edit-form" id="subtopic_valid" action="{{ URL::to('/') }}/home/subtopic/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="subtopic_id" class="id">
        

        <h2>Update Sub Topic on {{$subtopic_name}}</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label for="subtopic_name" class="col-md-4 control-label">Sub Topic Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm name" id="subtopic_name" name="name" placeholder="Please enter sub topic name">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="subtopic_hour" class="col-md-4 control-label">Sub Topic Hour:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm subtopic_hour" id="subtopic_hour" name="subtopic_hour" placeholder="Please enter sub topic hour ">
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Update Sub Topic<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>

   @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
  </div>

  
@endsection