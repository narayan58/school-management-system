<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<input type="text" class="form-control input-sm item" id="teacher_id" name="total_description[{{$num_x}}]" placeholder="Item" readonly="readonly" value="{{$description}}">
			<input type="hidden" name="teacher_id" class="teacher_id" value="{{$teacher_id}}">
			<input type="hidden" name="month_id" class="month_id" value="{{$month_id}}">
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<input type="text" class="form-control input-sm action" id="action_id_{{$num_x}}" name="action[{{$num_x}}]" placeholder="Action" readonly="readonly" value="{{$act_id}}">
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<input type="text" class="form-control input-sm total" id="total_id_{{$num_x}}" name="total_amount[{{$num_x}}]" placeholder="Total" readonly="readonly" value="{{$quantity}}">
		</div>
	</div>
	<div class="col-md-1">
		<div class="form-group" id="delete_row">

		</div>
	</div>
</div>

