<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
//header("Content-type: application/octet-stream;charset=utf-8");
header("Content-Type: text/html; charset=UTF-8");
header("Content-Disposition: attachment; filename=ExamRecord.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<table class="table table-bordered" border="1">
    <thead>
        <tr>
            <th>SN</th>
            <th>Exam</th>
            <th>Class</th>
        </tr>
    </thead>
    <tbody>
        @foreach($exams as $index=>$exam)
        <tr>
           <td>{{$index+1}}</td>
           <td><a href="{{URL::to('/')}}/home/exam/{{$exam->slug}}">{{$exam->name}}</a></td>
           <td>
            @foreach($exam->getExamDetail()->get() as $examp)
            <button type="button" class="btn btn-info btn-sm">
              <a href="{{URL::to('/')}}/home/exam/{{$exam->slug}}/class/{{$examp->getExamClassDetail->slug}}">{{$examp->getExamClassDetail->name}}</a>
            </button>
            @endforeach
           </td>
        </tr>
        @endforeach
    </tbody>
</table>