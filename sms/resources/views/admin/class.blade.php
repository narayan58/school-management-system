@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '2'))
	<div class="heading"><h2>Class Summary</h2></div>
	  <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myClass">
      Add Class
    </button>
    <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/class_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button></span>
    <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
    <div id="printDiv">
  	<div class="col-md-12">
    <div class="row">
    <table class="table table-bordered">
    <thead> 
        <tr>
            <th>SN</th>
            <th>Class</th>
            <th>Section</th>
            <th>Shift</th>
            <th class="hidden-print">Student List</th>
            <th>Created By</th>
            <th>Created On</th>
            <th class="hidden-print">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($classes as $index=>$class)
        <tr>
           <td>{{$index+1}}</td>
           <td><a href="{{URL::to('/')}}/home/class/{{$class->slug}}">{{$class->name}}</a></td>
           <td>  
            @foreach($class->getSectionDetail()->get() as $section)
              <button type="button" class="btn btn-info btn-sm">
              {{$section->getSectionClass->name}}
              </button>
            @endforeach
            </td>
            <td>  
            @foreach($class->getShiftDetail()->get() as $shift)
              <button type="button" class="btn btn-info btn-sm">
              {{$shift->getShiftClass->name}}
              </button>
            @endforeach
            </td>
            <td class="hidden-print">
              <a href="{{URL::to('/')}}/home/class/student/{{$class->slug}}">
              <button type="button" class="btn btn-warning btn-sm">
              View Student on {{$class->name}}
              </button>
              </a>
            </td>
            <td>{{$class->getUser->name}}</td>
            <td>{{$class->created_at}}</td>
            <td class="hidden-print">
              <a class="action-btn " id="" data-target="#edit_class" data-toggle="modal" href="{{URL::to('/').'/home/tclass/edit/'.$class->id}}"><i class="fa fa-pencil-square-o" id="{{$class->id}}"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
    </table>
    </div>
    </div>
    </div>

    <div id="myClass" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-class" id="class_valid" action="{{ URL::to('/') }}/home/class"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <h2>Class</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label class="col-md-4 control-label">Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm" id="name" name="name" placeholder="Please enter class name" >
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="image" class="col-md-4 control-label">Shift:</label>
            <div class="col-md-8">
                @foreach($shifts as $shift)
                <div class="col-sm-6">
                  <div class="checkbox">
                      <input type="checkbox" id="shift.{{$shift->id}}" name="shift_id[]" value="{{$shift->id}}">
                      <label class="control-label" for="shift.{{$shift->id}}">{{$shift->name}}</label> 
                  </div>
                </div>
                @endforeach
          </div>
        </div>
        <div class="row">
          <label for="image" class="col-md-4 control-label">Section:</label>
            <div class="col-md-8">
              @foreach($sections as $section)
                <div class="col-sm-6">
                  <div class="checkbox">
                      <input type="checkbox" id="section.{{$section->id}}" name="section_id[]" value="{{$section->id}}">
                      <label class="control-label" for="section.{{$section->id}}">{{$section->name}}</label> 
                  </div>
                </div>
                @endforeach
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Add Class<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>



<div id="edit_class" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">



    </div>
    </div>
    </div>
    @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>

    <script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $('.editclas').click(function(event){
    var id=$(event.target).attr("id"),
    t=$(event.target).parent().attr("id");

    url=t+"/edit/"+id,
    $.ajax({
      type:"GET",
      dataType:"JSON",
      url:url,
      success:function(response){
        $.each( response, function( key, value ) {
          $('.edit-form .'+key).val(value);
        });
      }
    });
  });
  </script>

@endsection
