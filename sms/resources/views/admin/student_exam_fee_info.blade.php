@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '2'))
  <div class="heading"><h2>Class:{{$class_name}} &nbsp; Section:{{$class_section}}  &nbsp; Shift:{{$class_shift}}</h2></div>
  <div class="row">
  <div class="col-md-6">
    <table class="table table-bordered">
    <thead> 
      <tr>
          <th>SN</th>
          <th>Exam</th>
          <th>Mark</th>
          <th>Grade</th>
      </tr>
    </thead>
    <tbody>
    @foreach($exam_details as $index=>$exam)
          <tr>
             <td>{{$index+1}}</td>
             <td>{{$exam->getExam->name}}</td>
             <td><a href="{{URL::to('/')}}/home/student/{{$id}}/exam/mark/{{$exam->getExam->slug}}">Mark</a></td>
             <td><a href="{{URL::to('/')}}/home/student/{{$id}}/exam/grade/{{$exam->getExam->slug}}">Grade</a></td>
          </tr>
    @endforeach
    </tbody>
  </table>
  </div>
  <div class="col-md-6">
    <table class="table table-bordered">
    <thead> 
      <tr>
          <th>SN</th>
          <th>Fee</th>
          <th>Status</th>
          <th>Submited On</th>
      </tr>
    </thead>
    <tbody>
    @foreach($fee_details as $index=>$fee)
          <tr>
             <td>{{$index+1}}</td>
             <td><a href="{{URL::to('/')}}/home/student/{{$id}}/fee/{{$fee->bill_id}}">{{$fee->bill_id}}</a></td>
             @if($fee->is_due == '1')
             <td  style="color:red;">Due Bill</td>
             @else
             <td>Paid Bill</td>
             @endif
             <td>{{$fee->created_at}}</td>
          </tr>
    @endforeach
    </tbody>
  </table>
  </div>
  </div>
    @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
@endsection
