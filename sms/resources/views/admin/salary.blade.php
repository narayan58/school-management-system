@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div id="mainMan">
<div class="admin-content">
	<div class="admin-top">
		<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			    @if(Session::has('alert-' . $msg))
			<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  	@endif
			@endforeach
		</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
	@foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '6'))
		<div class="form-holder">
		<div class="row">
			<div class="col-sm-6">
				<div class="general-form form-box padding-sm ">
					<div class="heading bg-blue">
						<h2 class="text-center">Salary Sheet</h2>
					</div>
					<input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
					<div class="row">
						<label class="col-md-3 control-label">Teacher Name:</label>
						<div class="col-md-4">
							<div class="form-group">
								<div class="form-group">
								<div class="required">
									<select class="form-control input-sm" id="teacher_id" name="teacher_id">
					                	<option value="">Select Teacher</option>
					                	@foreach($teachers as $teacher)
					                	<option value="{{$teacher->id}}">{{$teacher->name}}</option>
					                	@endforeach
					            	</select>
								</div>
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<div class="form-group">
								<div class="required">
									<select class="form-control input-sm" id="month_id" name="month_id">
					                	<option value="">Select Month</option>
					                	@foreach($months as $month)
					                	<option value="{{$month->id}}">{{$month->name}}</option>
					                	@endforeach
					            	</select>
								</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<label class="col-md-1 control-label">Des:</label>
						<div class="col-md-4">
							<div class="form-group">
								<div class="form-group">
								<div class="required">
									<input type="text" class="form-control input-sm description" id="description" name="name" placeholder="Enter Description">
								</div>
								</div>
							</div>
						</div>
						<label class="col-md-1 control-label">A:</label>
						<div class="col-md-3">
							<div class="form-group">
								<div class="form-group">
								<div class="required">
									<input type="text" class="form-control input-sm quantity" id="quantity" name="name" placeholder="Enter amount">
								</div>
								</div>
							</div>
						</div>
						<div class="col-md-1">
							<button  class="add_items btn btn-default btn-sm" id="add">Add</button>
						</div>
						<div class="col-md-1">
							<button  class="add_items btn btn-default btn-sm" id="ded">Ded</button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="print-section general-form form-box padding-sm item_order_form" id="bill">
					<div class="heading bg-blue">
						<h2 class="text-center">Salary</h2>
					</div>

					<form action="{{ URL::to('/') }}/home/salary/save" id="bill_form" method="POST">
					<div class="row">
							<div class="col-md-2">
								<label>Bill ID: </label>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<input type="text" class="form-control input-sm" id="bill_id" name="bill_id"  readonly="readonly" value="<?php echo strtotime(date(("Y-m-d H:i:s")));?>" required>
								</div>
							</div>
						</div>
						<input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
						<div class="row">
							<div class="col-md-6">
								<label>Item</label>
							</div>
							<div class="col-md-2">
								<label>Action</label>
							</div>
							<div class="col-md-3">
								<label>Total</label>
							</div>
						</div>
						<div class="append_items">
							<!-- append list here -->
						</div>
						<div class="row">
							<div class="col-md-12 text-right container-pd">
								<a href="javascript:void(0);" id="calculate-item" class="calculate btn btn-default btn-sm">Calculate</a>
							</div>							
						</div>
						<div class="append_calculations">
							<!-- append calculations here -->
						</div>
					</form>
				</div>				
			</div>	
		</div>
	</div>
	 @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
</div>
<script src="{{ url('/') }}/js/jquery-2.1.3.min.js"></script>
<script type="text/javascript">
	$('.quantity').keypress(function(event){
        if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
            event.preventDefault();
        }});
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('.crossclose').click(function(e){
		$(e.target).closest('.row').remove();
	});

});
</script>
<script type="text/javascript">
	$(document).ready(function() {
	  $(".rest_category").select2();
	});

	var x = 0;
	$('.add_items').click(function(event){
		var teacher_id = $('#teacher_id').val(),
			description = $('#description').val(),
			quantity = $('#quantity').val(),
			month_id = $('#month_id').val(),
			id = $(event.target).attr('id'),
			token =$('.token').val();

			if(teacher_id == "" || description == "" || quantity == "" || month_id == ""){
				alertify.alert ("Please some field are missing, please check again");
				return false;
			}
			
		var grand_total = 0;
		$.ajax({
			type:"POST",
			dataType:"html",
			url: "getTeacherSalary",
			data: {
				_token: token,
				teacher_id: teacher_id,
				description: description,
				quantity: quantity,
				month_id: month_id,
				id: id,
				num_x : x+1
			},
			success: function(response){
				x++;
				$('.append_items').append(response);
				$('#delete_row').attr('id','delete_row'+x);
				$('#delete_row'+x).append('<a href="javascript:void(0);"><span class="fa fa-times crossclose remove-field-'+x+'" aria-hidden="true"></span></a>');

				$(".remove-field-"+x).click(function(e){
					e.preventDefault();
					$(e.target).closest('.row').remove();
				});
        		$(document).on("keyup",'#total_id_'+x,function(event) {
                var xn = event.currentTarget.id.replace(/[^\d\.]*/g,'');
                		var quantity_id = $('#total_id_'+xn).val();
                		if(quantity_id == '0'){
                			alertify.alert ("Please give some rate, please check again");
                			$('.save_button').addClass('disabled');
                		}
                		else{
                			$('#total_id_'+xn).val(quantity_id);
                			$('.save_button').removeClass('disabled').attr('disabled',false);
                		}
                	});

			},
			error: function (e) {
				alertify.alert('Some error occured, please check again');
				return false;
			}
		})
	});
</script>
<script type="text/javascript">
$('#calculate-item').click(function(event){
		var total_length = $('.total').length,
			token =$('.token').val();
		var total = [];
		$.each($('.total'),function(val) {
			var tot = $('.total')[val].value;
			total.push(tot);
		});
		$.ajax({
			type:"POST",
			dataType:"html",
			url: "getSalaryCalculation",
			data: {
				_token: token,
				total: total,
			},
			success: function(response){
				if(response){
					$('.append_calculations').html(response);
				}
			},
			error: function (e) {
				alert('Sorry! we cannot load data this time');
				return false;
			}
		});
	});
</script>
<script type="text/javascript">
	$(document).on("keyup",".discount",function() {
		var discount = $('.discount').val();
		var amount = $('.total_sum').val();
		$('.grand_total').val(amount-discount);
		if(amount == '0'){
			$('.save_button').addClass('disabled');
		}
		else{
			$('.save_button').removeClass('disabled').attr('disabled',false);
		}
	});
</script>
<script src="{{ url('/') }}/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
$("#bill_form").validate({
    rules: {
    	total: {
                required: true
                },
        grand_total:{
        	required:true
        }
            },
    messages: {
        total: {
            required: "Please enter a section name"
        	}
        },
    submitHandler: function(form) {
    if($(form).find('#submit').prop('disabled')) return;
    $(form).find('#submit').prop('disabled', true);
    form.submit();
    }
    });
 </script>
@endsection