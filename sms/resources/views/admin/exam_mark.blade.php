@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
    @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '5'))
	<div class="heading"><h2>Marks Summary</h2></div>
    <form class="general-form" id="exam_class_valid" action="{{ URL::to('/') }}/home/exam/getStudentList/{{$exam_id_stu}}"  method="GET" enctype="multipart/form-data">
        <div class="col-xs-12 general-form form-box padding-sm">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" class="exam_id" name="exam_id" value="{{$exam_id}}">
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label for="class" class="col-md-1 control-label">Class:</label>
            <div class="col-md-2">
            <div class="form-group">
            <div class="required">
              <select class="form-control input-sm class-name searchKeys" id="class-name" name="class_id">
                <option value="">Please select class</option>
                @foreach($current_exam_class as $currexam)
              <option value="{{$currexam->getExamClass->id}}">{{$currexam->getExamClass->name}}</option>
              @endforeach
              </select>
            </div>
            </div>
          </div>
          <label for="section_id" class="col-md-1 control-label">Section:</label>
            <div class="col-md-2">
            <div class="form-group">
            <div class="required">
              <select class="form-control input-sm section-name searchKeys" id="section_id" name="section_id">
                <option value="">Please select section</option>
              </select>
            </div>
            </div>
          </div>
          <label for="shift_id" class="col-md-1 control-label">Shift:</label>
            <div class="col-md-2">
            <div class="form-group">
            <div class="required">
              <select class="form-control input-sm shift-name searchKeys" id="shift_id" name="shift_id">
                <option value="">Please select shift</option>
              </select>
            </div>
            </div>
          </div>
          <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm" id="search">
            Search Student<i class="fa fa-paper-plane-o"></i>
          </button>
            </div>
        </div>
        </div>
        </form>
        <div class="append_items">
            <div class="row" id="replaceTable">

            </div>
            <!-- append list here -->
        </div>
         @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    // filter for teacher to get section
    $('.class-name').change(function(event){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getSectionOnStudent/"+teacher_id,
            success:function(e){
                $('#section_id').html('');
                $('#section_id').append('<option value="">Please Select Section</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#section_id').append('<option value='+val.get_section_class.id+'>'+val.get_section_class.name+'</option>');
                    }
                });
            }
        })
    });
</script>
<script type="text/javascript">
    // filter for teacher to get shift
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getShiftOnStudent/"+teacher_id,
            success:function(e){
                $('#shift_id').html('');
                $('#shift_id').append('<option value="">Please Select Shift</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#shift_id').append('<option value='+val.get_shift_class.id+'>'+val.get_shift_class.name+'</option>');
                    }
                });
            }
        })
    });
</script>
<script type="text/javascript">
    // filter get student list
    $('.studentlist').click(function(event){
        var class_id = $('.class-name').val(),
            shift_id = $('.shift-name').val(),
            section_id = $('.section-name').val(),
            exam_id = $('.exam_id').val(),
            token =$('.token').val();
        $.ajax({
            type:"POST",
            dataType:"html",
            url: "getStudentList",
            data: {
                _token: token,
                class_id: class_id,
                shift_id: shift_id,
                section_id:section_id,
                exam_id:exam_id,
            },
            success: function(response){
                    $('.append_items').html(response);
            },
            error: function (e) {
                alert('Sorry! we cannot load data this time');
                return false;
            }
        })
    });
</script>

    
@endsection
