@extends('admin.app')
@section('content')
@include('admin.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
  <div class="admin-top">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
  </div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '6'))
  <div class="heading"><h2>Latest Salary</h2></div>

<div class="form-holder">
  <form class="general-form" id="exam_valid">
    <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
    <div class="row">
    <div class="form-group col-md-3">
      <label for="category">Teacher Name</label>
      <div class="">
        <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="teacher_id" name="teacher_id" >
          <option value="0">--Please Select--</option>
          @foreach($teachers as $teacher)   
          <option value="{{$teacher->id}}">{{$teacher->name}}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="form-group col-md-3">
      <label for="category">Month</label>
      <div class="">
        <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="month_id" name="month_id" >
          <option value="0">--Please Select--</option>
          @foreach($months as $month)   
          <option value="{{$month->id}}">{{$month->name}}</option>
          @endforeach   
        </select>
      </div>
    </div>
    <div class="text-center">
    <button type="button" class="btn btn-default btn-sm" id="search">
      Search<i class="fa fa-paper-plane-o"></i>
    </button>
  </div>
  </div>
  </form>
</div>

<div class="row" id="replaceTable">
  <div class="col-md-8" >
    <table class="table table-bordered">
    <thead>
      <tr>
          <th>SN</th>
          <th>System ID</th>
          <th>Bill SN</th>
          <th>Teacher</th>
          <th>Month</th>
          <th>Bill On</th>
      </tr>
    </thead>
    <tbody>
    @foreach($salary_totals as $index=>$salary)
          <tr>
             <td>{{$index+1}}</td>
             <td><a href="{{URL::to('/')}}/home/salary-detail/view/{{$salary->bill_id}}">{{$salary->bill_id}}</a></td>
             <td>{{$salary->teacherfeebillSn->id}}</td>
             <td>{{$salary->teacherfeename->name}}</td>
             <td>{{$salary->getMonth->name}}</td>
             <td>{{$salary->created_at}}</td>
          </tr>
    @endforeach
    </tbody>
  </table>
  {{--{!! str_replace('/?', '?', $salary_totals->render()) !!}--}}
  </div>
</div>
    @else
  <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    document.addEventListener('keypress', function(event) {
                 if (event.keyCode == 13) {
                     event.preventDefault();
                     $("#search").click();
                 }
             });
        $('#search').click(function () {
        $('#loader').show();

    var searchParams = {};
    var base_url = '<?php echo url();?>/home/salary-detail/salary_search';
    searchParams['teacher_id']=$('#teacher_id').val();
    searchParams['month_id']=$('#month_id').val();
    var token = $('.token').val();
    $.ajax({
        type: 'get',
        data: 'parameters= ' + JSON.stringify(searchParams) + '&_token=' + token,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
                    $('#loader').hide('slow');
        }
    });
    return false;
});
</script>
@endsection
