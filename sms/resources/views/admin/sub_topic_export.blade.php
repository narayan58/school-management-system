<table class="table table-bordered">
    <thead> 
      <tr>
         <th>SN</th>
          <th>Sub Topic Name</th>
          <th>Sub Topic hour</th>
      </tr>
    </thead>
    <tbody>
        @foreach($subtopics as $index=>$subtopic)
      <tr>
         <td>{{$index+1}}</td>
         <td>{{$subtopic->name}}</td>
         <td>{{$subtopic->subtopic_hour}}</td>
      </tr>
        @endforeach
    </tbody>
  </table>