@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
	<div class="heading"><h2>Admin Summary</h2></div>
	@foreach($user_levels as $userlevel)
	@if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '2'))
	<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-3">
	    <div class="panel panel-primary">
	        <div class="panel-heading">
	            <div class="row">
	                <div class="col-xs-3">
	                    <i class="glyphicon glyphicon-tasks gi-5x"></i>
	                </div>
	                <div class="col-xs-9 text-right">
	                    <div class="huge">{{$shift_count}}</div>
	                    <div>Shift</div>
	                </div>
	            </div>
	        </div>
	        <a href="{{ URL::to('/') }}/home/shift">
	            <div class="panel-footer">
	                <span class="pull-left">View Details</span>
	                <span class="pull-right"><i class="glyphicon glyphicon-circle-arrow-right"></i></span>
	                <div class="clearfix"></div>
	            </div>
	        </a>
	    </div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-3">
	    <div class="panel panel-primary">
	        <div class="panel-heading">
	            <div class="row">
	                <div class="col-xs-3">
	                    <i class="glyphicon glyphicon-tasks gi-5x"></i>
	                </div>
	                <div class="col-xs-9 text-right">
	                    <div class="huge">{{$section_count}}</div>
	                    <div>Section</div>
	                </div>
	            </div>
	        </div>
	        <a href="{{ URL::to('/') }}/home/section">
	            <div class="panel-footer">
	                <span class="pull-left">View Details</span>
	                <span class="pull-right"><i class="glyphicon glyphicon-circle-arrow-right"></i></span>
	                <div class="clearfix"></div>
	            </div>
	        </a>
	    </div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-3">
	    <div class="panel panel-primary">
	        <div class="panel-heading">
	            <div class="row">
	                <div class="col-xs-3">
	                    <i class="glyphicon glyphicon-tasks gi-5x"></i>
	                </div>
	                <div class="col-xs-9 text-right">
	                    <div class="huge">{{$period_count}}</div>
	                    <div>Period</div>
	                </div>
	            </div>
	        </div>
	        <a href="{{ URL::to('/') }}/home/period">
	            <div class="panel-footer">
	                <span class="pull-left">View Details</span>
	                <span class="pull-right"><i class="glyphicon glyphicon-circle-arrow-right"></i></span>
	                <div class="clearfix"></div>
	            </div>
	        </a>
	    </div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-3">
	    <div class="panel panel-primary">
	        <div class="panel-heading">
	            <div class="row">
	                <div class="col-xs-3">
	                    <i class="glyphicon glyphicon-tasks gi-5x"></i>
	                </div>
	                <div class="col-xs-9 text-right">
	                    <div class="huge">{{$class_count}}</div>
	                    <div>Class</div>
	                </div>
	            </div>
	        </div>
	        <a href="{{ URL::to('/') }}/home/class">
	            <div class="panel-footer">
	                <span class="pull-left">View Details</span>
	                <span class="pull-right"><i class="glyphicon glyphicon-circle-arrow-right"></i></span>
	                <div class="clearfix"></div>
	            </div>
	        </a>
	    </div>
	</div>
	</div>
	@endif
	<div class="row">
		@if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '3'))
	<div class="col-lg-3 col-md-3 col-sm-3">
	    <div class="panel panel-info">
	        <div class="panel-heading">
	            <div class="row">
	                <div class="col-xs-3">
	                    <i class="glyphicon glyphicon-tasks gi-5x"></i>
	                </div>
	                <div class="col-xs-9 text-right">
	                    <div class="huge">{{$student_count}}</div>
	                    <div>Student</div>
	                </div>
	            </div>
	        </div>
	        <a href="{{ URL::to('/') }}/home/student">
	            <div class="panel-footer">
	                <span class="pull-left">View Details</span>
	                <span class="pull-right"><i class="glyphicon glyphicon-circle-arrow-right"></i></span>
	                <div class="clearfix"></div>
	            </div>
	        </a>
	    </div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-3">
	    <div class="panel panel-info">
	        <div class="panel-heading">
	            <div class="row">
	                <div class="col-xs-3">
	                    <i class="glyphicon glyphicon-tasks gi-5x"></i>
	                </div>
	                <div class="col-xs-9 text-right">
	                    <div class="huge">{{$teacher_count}}</div>
	                    <div>Teacher</div>
	                </div>
	            </div>
	        </div>
	        <a href="{{ URL::to('/') }}/home/teacher">
	            <div class="panel-footer">
	                <span class="pull-left">View Details</span>
	                <span class="pull-right"><i class="glyphicon glyphicon-circle-arrow-right"></i></span>
	                <div class="clearfix"></div>
	            </div>
	        </a>
	    </div>
	</div>
	@endif
	@if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '5'))
	<div class="col-lg-3 col-md-3 col-sm-3">
	    <div class="panel panel-info">
	        <div class="panel-heading">
	            <div class="row">
	                <div class="col-xs-3">
	                    <i class="glyphicon glyphicon-tasks gi-5x"></i>
	                </div>
	                <div class="col-xs-9 text-right">
	                    <div class="huge">{{$exam_count}}</div>
	                    <div>Exam</div>
	                </div>
	            </div>
	        </div>
	        <a href="{{ URL::to('/') }}/home/exam">
	            <div class="panel-footer">
	                <span class="pull-left">View Details</span>
	                <span class="pull-right"><i class="glyphicon glyphicon-circle-arrow-right"></i></span>
	                <div class="clearfix"></div>
	            </div>
	        </a>
	    </div>
	</div>
	@endif
	@if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '6'))
	<div class="col-lg-3 col-md-3 col-sm-3">
	    <div class="panel panel-info">
	        <div class="panel-heading">
	            <div class="row">
	                <div class="col-xs-3">
	                    <i class="glyphicon glyphicon-tasks gi-5x"></i>
	                </div>
	                <div class="col-xs-9 text-right">
	                    <div class="huge">{{$feestructure_count}}</div>
	                    <div>Fee Structure</div>
	                </div>
	            </div>
	        </div>
	        <a href="{{ URL::to('/') }}/home/feestructure">
	            <div class="panel-footer">
	                <span class="pull-left">View Details</span>
	                <span class="pull-right"><i class="glyphicon glyphicon-circle-arrow-right"></i></span>
	                <div class="clearfix"></div>
	            </div>
	        </a>
	    </div>
	</div>
	@endif
	</div>

	<div class="row">
	@if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '7'))
	<div class="col-lg-3 col-md-3 col-sm-3">
	    <div class="panel panel-primary">
	        <div class="panel-heading">
	            <div class="row">
	                <div class="col-xs-3">
	                    <i class="glyphicon glyphicon-tasks gi-5x"></i>
	                </div>
	                <div class="col-xs-9 text-right">
	                    <div class="huge">{{$notice_count}}</div>
	                    <div>Notice</div>
	                </div>
	            </div>
	        </div>
	        <a href="{{ URL::to('/') }}/home/notice">
	            <div class="panel-footer">
	                <span class="pull-left">View Details</span>
	                <span class="pull-right"><i class="glyphicon glyphicon-circle-arrow-right"></i></span>
	                <div class="clearfix"></div>
	            </div>
	        </a>
	    </div>
	</div>
	@endif
	@if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '8'))
	<div class="col-lg-3 col-md-3 col-sm-3">
	    <div class="panel panel-primary">
	        <div class="panel-heading">
	            <div class="row">
	                <div class="col-xs-3">
	                    <i class="glyphicon glyphicon-tasks gi-5x"></i>
	                </div>
	                <div class="col-xs-9 text-right">
	                    <div class="huge">{{$event_count}}</div>
	                    <div>Event</div>
	                </div>
	            </div>
	        </div>
	        <a href="{{ URL::to('/') }}/home/event_view">
	            <div class="panel-footer">
	                <span class="pull-left">View Details</span>
	                <span class="pull-right"><i class="glyphicon glyphicon-circle-arrow-right"></i></span>
	                <div class="clearfix"></div>
	            </div>
	        </a>
	    </div>
	</div>
	@endif
	</div>
	@endforeach

</div>
@endsection
