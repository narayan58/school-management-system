<div class="row">
	<div class="col-md-6">
		
	</div>
	<div class="col-md-3">
		<label>Total: </label>
	</div>
	<div class="col-md-3">
		<input type="text"  class="form-control input-sm total_sum" id="total" name="total" readonly="readonly" placeholder="Total" value="{{$calc_data['total']}}">
	</div>
</div>
<div class="row">
	<div class="text-right save_button container-ptd">
		<button type="submit" id="submit" class="btn btn-default btn-sm save_button">Save Data</button>
	</div>
</div>
