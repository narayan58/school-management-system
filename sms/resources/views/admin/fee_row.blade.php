<div class="row">
	<div class="col-md-8">
		<div class="form-group">
			<input type="text" class="form-control input-sm item" id="student_id" name="total_description[{{$num_x}}]" placeholder="Item" readonly="readonly" value="{{$description}}">
			<input type="hidden" name="student_id" class="student_id" value="{{$student_id}}">
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<input type="text" class="form-control input-sm total" id="total_id_{{$num_x}}" name="total_amount[{{$num_x}}]" placeholder="Total" value="{{$quantity}}">
		</div>
	</div>
	<div class="col-md-1">
		<div class="form-group" id="delete_row">

		</div>
	</div>
			@foreach($months as $month)
	<div class="col-md-2">
		<label id="month_{{$num_x}}" value="{{$month['name']}}">{{$month['name']}}</label>
		<input type="hidden" class="form-control input-sm" name="total_months_id[{{$num_x}}][]" placeholder="Months" value="{{$month['month']}}" readonly>
	</div>
			@endforeach
</div>

