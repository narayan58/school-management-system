<table class="table table-bordered">
  <thead> 
    <tr>
        <th>SN</th>
        <th>Title</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Created By</th>
        <th>Created On</th>
        <th class="hidden-print">Action</th>
    </tr>
  </thead>
  <tbody>
      @foreach($calendars as $index=>$calendar)
    <tr>
     <td>{{$index+1}}</td>
     <td>{{$calendar->title}}</td>
     <td>{{$calendar->start}}</td>
     <td>{{$calendar->end}}</td>
     <td>{{$calendar->getUser->name}}</td>
     <td>{{$calendar->created_at}}</td>
     <td class="hidden-print">
      <a class="action-btn edit" id="calendar" data-target="#edit_calendar" data-toggle="modal" href=""><i class="fa fa-pencil-square-o" id="{{$calendar->id}}"></i></a>
      <a class="action-btn bg-green delete" id="calendar" href="javascript:void(0)"><i class="fa fa-times" id="{{$calendar->id}}"></i></a>
     </td>
    </tr>
      @endforeach
  </tbody>
</table>