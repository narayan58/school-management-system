@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '3'))
	<div class="heading"><h2>Student Summary</h2></div>
            <!-- Button trigger modal -->
  
<div class="form-holder">
<form class="general-form" id="category_valid">
  <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
  <div class="row">
  <div class="form-group col-md-3">
    <label for="category">Class</label>
    <div class="">
      <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="class_id" name="class_id" selected="">
      <option value="0">--Please Select--</option>
          @foreach($class_list as $index=>$class)
          <option value="{{$index}}">{{$class}}</option>

          @endforeach
      </select>
    </div>
  </div>
  <div class="form-group col-md-3">
    <label for="category">Section</label>
    <div class="">
      <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="section_id" name="section_id" selected="">
      <option value="0">--Please Select--</option>
          @foreach($section_list as $index=>$class)
          <option value="{{$index}}">{{$class}}</option>

          @endforeach
      </select>
    </div>
  </div>
  <div class="form-group col-md-3">
    <label for="category">Shift</label>
    <div class="">
      <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="shift_id" name="shift_id" selected="">
      <option value="0">--Please Select--</option>
          @foreach($shift_list as $index=>$class)
          <option value="{{$index}}">{{$class}}</option>

          @endforeach
      </select>
    </div>
  </div>
  <div class="text-center">
  <button type="button" class="btn btn-default btn-sm" id="search">
    Search<i class="fa fa-paper-plane-o"></i>
  </button>
</div>
</div>
</form>
</div>
    <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myStudent">
    Add Student
    </button>
    <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/student_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button>
    <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>

    <div id="printDiv">
    <div class="col-md-12">
    <div class="row" id="replaceTable">
    <table class="table table-bordered">
    <thead> 
      <tr>
         <th>SN</th>
          <th>Name</th>
          <th>Parents</th>
          <th>Address</th>
          <th>Roll no</th>
          <th>DOB</th>
          <th>Gender</th>
          <th>Contact No</th>
          <th>Class</th>
          <th>Section</th>
          <th>Shift</th>
          <th class="hidden-print">Preview</th>
          <th class="hidden-print">Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($students as $index=>$student)
      <tr>
       <td>{{$index+1}}</td>
       <td><a href="{{URL::to('/')}}/home/student/{{$student->studentinfo->email}}">{{$student->studentinfo->name}}</a>
       </td>
       <td>{{$student->studentinfo->mobile_no}}</td>
       <td>{{$student->studentinfo->address}}</td>
       <td>{{$student->studentinfo->email}}</td>
       <td>{{$student->studentinfo->dob}}</td>
       <td>{{$student->studentinfo->gender}}</td>
       <td>{{$student->studentinfo->telephone_no}}</td>
       <td>{{$student->studentclass->name}}</td>
       <td>{{$student->studentsection->name}}</td>
       <td>{{$student->studentshift->name}}</td>
       <td class="hidden-print"><a href="{{URL::to('/')}}/image/student/{{$student->studentinfo->image_encrypt}}" data-lightbox="{{$student->studentinfo->image}}" data-title="{{$student->studentinfo->image}}"><img src="{{URL::to('/')}}/image/thumb/student/{{$student->studentinfo->image_encrypt}}" style="width:50px; height:50px "  alt="{{$student->image}}" class="img-responsive"></a></td>
       <td class="hidden-print">
          <a class="action-btn " id="student" data-target="#edit_student" data-toggle="modal" href="{{URL::to('/').'/home/student/edit/'.$student->studentinfo->id}}"><i class="fa fa-pencil-square-o" ></i></a>
         </td>
      </tr>
        @endforeach
    </tbody>
  </table>
  </div>
    </div>
  </div>
    <div id="myStudent" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-student" id="student_valid" action="{{ url('/') }}/home/student/store"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <h2>Add Student</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label for="firstname" class="col-md-4 control-label">Full Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="firstname" name="name" placeholder="Please enter full name">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="mobile_no" class="col-md-4 control-label">Parents Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="mobile_no" name="mobile_no" placeholder="Please enter parents name">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="address" class="col-md-4 control-label">Address:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="address" name="address" placeholder="Please enter address">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="roll_no" class="col-md-4 control-label">Roll no:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="roll_no" name="roll_no" placeholder="Please enter roll number">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="date_of_birth" class="col-md-4 control-label">Date of Birth:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="date_of_birth" name="date_of_birth" placeholder="Please enter date of birth">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="gender" class="col-md-4 control-label">Gender:</label>
            <div class="col-md-8">
            <div class="form-group">
              <label class="radio-inline">
                  <input type="radio" name="gender" id="gender" value="male" checked> Male
                </label>
                <label class="radio-inline">
                  <input type="radio" name="gender" id="gender" value="female"> Female
                </label>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="telephone_no" class="col-md-4 control-label">Telephone no:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="telephone_no" name="telephone_no" placeholder="Please enter telephone number">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="image" class="col-md-4 control-label">Image:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="file" class="form-control input-sm" id="image" name="image">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="class_id" class="col-md-4 control-label">Class:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control class-name" id="class_id" name="class_id">
                <option value="">Please select class</option>
                  @foreach($classees as $class)
                  <option value="{{$class->id}}">{{$class->name}}</option>
                  @endforeach
                </select>
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="section_id_org" class="col-md-4 control-label">Section:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control" id="section_id_org" name="section_id">
                <option value="">Please select section</option>
              </select>
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="shift_id_org" class="col-md-4 control-label">Shift:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control" id="shift_id_org" name="shift_id">
                <option value="">Please select shift</option>
              </select>
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Add Student<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
      </div>
    </div>
    </div>

     <div id="edit_student" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">

      </div>
    </div>
    </div>
   @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    // filter for teacher to get section
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getSectionOnStudent/"+teacher_id,
            success:function(e){
                $('#section_id_org').html('');
                $('#section_id_org').append('<option value="">Please Select Section</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#section_id_org').append('<option value='+val.get_section_class.id+'>'+val.get_section_class.name+'</option>');
                    }
                });
            }
        })
    });
    </script>
    <script type="text/javascript">
    // filter for teacher to get shift
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getShiftOnStudent/"+teacher_id,
            success:function(e){
                $('#shift_id_org').html('');
                $('#shift_id_org').append('<option value="">Please Select Shift</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#shift_id_org').append('<option value='+val.get_shift_class.id+'>'+val.get_shift_class.name+'</option>');
                    }
                });
            }
        })
    });
    </script>
    <script type="text/javascript">
    document.addEventListener('keypress', function(event) {
                 if (event.keyCode == 13) {
                     event.preventDefault();
                     $("#search").click();
                 }
             });
        $('#search').click(function () {
        $('#loader').show();

    var searchParams = {};
    var base_url = '<?php echo url();?>/home/student_record';
    searchParams['class_id']=$('#class_id').val();
    searchParams['section_id']=$('#section_id').val();
    searchParams['shift_id']=$('#shift_id').val();
    var token = $('.token').val();
    $.ajax({
        type: 'get',
        data: 'parameters= ' + JSON.stringify(searchParams) + '&_token=' + token,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
                    $('#loader').hide('slow');
        }
    });
    return false;
});

</script>
<script type="text/javascript">
      $('.editstu').click(function(event){
    var id=$(event.target).attr("id"),
    t=$(event.target).parent().attr("id");

    url=t+"/edit/"+id,
    $.ajax({
      type:"GET",
      dataType:"JSON",
      url:url,
      success:function(response){
        $.each( response, function( key, value ) {
          $('.add-student .'+key).val(value);
        });
      }
    });
  });
</script>
<script>
              $('#myStudent').on('hide.bs.modal', function () {
                $('#myStudent').removeData('bs.modal')
              })
              </script>
@endsection
