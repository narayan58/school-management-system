@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>@foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '5'))
	<div class="heading"><h2>Exam Summary</h2></div>
<div class="form-holder">
  <form class="general-form" id="fee_valid">
  <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
  <div class="row">
  <div class="form-group col-md-3">
    <label for="category">Class</label>
    <div class="">
      <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="class_id" name="class_id" selected="">
        <option value="0">--Please Select--</option>
                @foreach($class_list as $index=>$class)
                <option value="{{$index}}">{{$class}}</option>
                @endforeach
      </select>
    </div>
  </div>
  <div class="form-group col-md-3">
    <label for="category">Section</label>
    <div class="">
      <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="section_id" name="section_id" selected="">
        <option value="0">--Please Select--</option>
                @foreach($section_list as $index=>$class)
                <option value="{{$index}}">{{$class}}</option>
                @endforeach
      </select>
    </div>
  </div>
  <div class="form-group col-md-3">
    <label for="category">Shift</label>
    <div class="">
      <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="shift_id" name="shift_id" selected="">
        <option value="0">--Please Select--</option>
                @foreach($shift_list as $index=>$class)
                <option value="{{$index}}">{{$class}}</option>
                @endforeach
      </select>
    </div>
  </div>
  <div class="text-center">
  <button type="button" class="btn btn-default btn-sm" id="search">
    Search<i class="fa fa-paper-plane-o"></i>
  </button>
</div>
</div>
</form>
</div>

  <div class="col-md-12">
    <div id="printDiv">
    <div class="row" id="replaceTable">
    <table class="table table-bordered">
    <thead> 
        <tr>
            <th>SN</th>
            <th>Exam</th>
            <th>Student</th>
            <th>Mark</th>
            <th>Grade</th>
            <th>Class</th>
            <th>Shift</th>
            <th>Section</th>
            <th>Created By</th>
            <th>Created On</th>
            <th>View</th>
            <th>Grade</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($latestMarks as $index=>$exam)
        <tr>
           <td>{{$index+1}}</td>
           <td>{{$exam->getStudentExam->name}}</td>
           <td>{{$exam->getStudentMarksheet->name}}</td>
           <td><a href="{{URL::to('/')}}/home/exam-detail/view/mark/{{$exam->getStudentMarksheet->email}}/{{$exam->getStudentExam->slug}}">Mark</a></td>
           <td><a href="{{URL::to('/')}}/home/exam-detail/view/grade/{{$exam->getStudentMarksheet->email}}/{{$exam->getStudentExam->slug}}">Grade</a></td>
           <td>{{$exam->getStudentClass->name}}</td>
           <td>{{$exam->getStudentSection->name}}</td>
           <td>{{$exam->getStudentShift->name}}</td>
           <td>{{$exam->getUser->name}}</td>
           <td>{{$exam->created_at}}</td>
           <td>
            <button type="button" class="btn btn-default btn-sm update_marksheet" id="{{$exam->getStudentMarksheet->id}}" ids="{{$exam->getStudentExam->id}}" data-toggle="modal" data-target="#update_marksheet">
            Mark
            </button>
          </td>
          <td>
            <button type="button" class="btn btn-default btn-sm grade_mark" id="{{$exam->getStudentMarksheet->id}}" ids="{{$exam->getStudentExam->id}}" data-toggle="modal" data-target="#update_grade">
            Grade
            </button>
          </td>
            @if($exam->is_active)
          <td>
            <a href="#" id="{{$exam->getStudentMarksheet->id}}" ids="{{$exam->getStudentExam->id}}" active="{{$exam->is_active}}" class="unpublish">Unpublish</a>
          </td>
          @else
          <td>
            <a href="#" id="{{$exam->getStudentMarksheet->id}}" ids="{{$exam->getStudentExam->id}}" active="{{$exam->is_active}}" class="unpublish">publish</a>
          </td>
          @endif
        </tr>
        @endforeach
    </tbody>
    </table>
        <!-- updated marksheet -->
    <div id="update_marksheet" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-class" id="studentsub" action="{{ URL::to('/') }}/home/exam/marksheet/update"  method="GET" enctype="multipart/form-data">
            <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
       <div class="append_mark_update_view">
            <!-- append list here -->
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>
        <!-- grade view -->
    <div id="update_grade" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-class" id="studentsub" action="{{ URL::to('/') }}/home/exam/marksheet/store"  method="GET" enctype="multipart/form-data">
            <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
       <div class="append_grade">
            <!-- append list here -->
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
  </div>
     @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    document.addEventListener('keypress', function(event) {
                 if (event.keyCode == 13) {
                     event.preventDefault();
                     $("#search").click();
                 }
             });
        $('#search').click(function () {
          debugger;
        $('#loader').show();
    var searchParams = {};
    var base_url = '<?php echo url();?>/home/exam/exam_search';
    searchParams['class_id']=$('#class_id').val();
    searchParams['section_id']=$('#section_id').val();
    searchParams['shift_id']=$('#shift_id').val();
    var token = $('.token').val();
    $.ajax({
        type: 'get',
        data: 'parameters= ' + JSON.stringify(searchParams) + '&_token=' + token,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
                    $('#loader').hide('slow');
        }
    });
    return false;
});
</script>
<script type="text/javascript">
    // filter get student class respective subjects
    $('.update_marksheet').click(function(event){
        var student_id = $(event.target).attr('id'),
            exam_id = $(event.target).attr('ids'),
            token =$('.token').val();
        $.ajax({
            type:"POST",
            dataType:"html",
            url: "getStudentMarkStudent",
            data: {
                _token: token,
                student_id: student_id,
                exam_id: exam_id,
            },
            success:function(response){
               $('.append_mark_update_view').html(response);
            }
        })
    });
</script>
<script type="text/javascript">
    // filter get student grade respective subjects
    $('.grade_mark').click(function(event){
        var student_id = $(event.target).attr('id'),
            exam_id = $(event.target).attr('ids'),
            token =$('.token').val();
        $.ajax({
            type:"POST",
            dataType:"html",
            //142
            url: "{{URL::route('gstudentlistgrade')}}",
            data: {
                _token: token,
                student_id: student_id,
                exam_id: exam_id,
            },
            success:function(response){
               $('.append_grade').html(response);
            }
        })
    });
    </script>
<script type="text/javascript">
$('.unpublish').click(function(event){
    var stid = $(event.target).attr("id"),
      active = $(event.target).attr("active"),
      id = $(event.target).attr("ids");
      token=$(".token").val();

    alertify.confirm("Are you sure you want to unpublish?",function(){
      $.ajax({
        type:"POST",
        dataType:"JSON",
        url:"marksheet/unpublish",
        data:{
        _token:token,
          stid : stid,
          id : id,
          active : active
        },
        success:function(e){
          alertify.alert(e.msg,function(){
            location.reload()
          })
        },
        error: function (e) {
          alertify.alert('Sorry! this data is used some where');
        }
      });
    });
  });
</script>
@endsection
