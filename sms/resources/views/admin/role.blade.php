@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1'))
	<div class="heading"><h2>Role Summary</h2></div>
  <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/role_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button>
  <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
    <div id="printDiv">
    <div class="col-md-12">
    <div class="row">
    <table class="table table-bordered">
    <thead> 
      <tr>
          <th>SN</th>
          <th>Full Name</th>
          <th>Address</th>
          <th>Email</th>
          <th>DOB</th>
          <th>Gender</th>
          <th>T No</th>
          <th>M No</th>
          <th>User Level</th>
          <th class="hidden-print">Preview</th>
          <th>Created By</th>
          <th>Created On</th>
          <th class="hidden-print">Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($roles as $index=>$role)
        <tr>
       <td>{{$index+1}}</td>
       <td>{{$role->name}}</td>
       <td>{{$role->address}}</td>
       <td>{{$role->email}}</td>
       <td>{{$role->dob}}</td>
       <td>{{$role->gender}}</td>
       <td>{{$role->telephone_no}}</td>
       <td>{{$role->mobile_no}}</td>
       <td>{{$role->getUserLevel->title}}</td>
       <td class="hidden-print"><a href="{{URL::to('/')}}/image/manager/{{$role->image_encrypt}}" data-lightbox="{{$role->image}}" data-title="{{$role->image}}"><img src="{{URL::to('/')}}/image/thumb/manager/{{$role->image_encrypt}}" style="width:50px; height:50px "  alt="{{$role->image}}" class="img-responsive"></a></td>
       <td>{{$role->getUser->name}}</td>
       <td>{{$role->created_at}}</td>
       <td class="hidden-print">
          <a class="action-btn bg-green delete" id="role" href="javascript:void(0)"><i class="fa fa-times" id="{{$role->id}}"></i></a>
       </td>     
        </tr>
        @endforeach
    </tbody>
  </table>
  </div>
  </div>
  </div>
   @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
  </div>
  @endsection