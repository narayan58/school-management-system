    <button type="button" class="close" data-dismiss="modal">&times;</button>
  <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form edit-form" id="teacher_valid" action="{{URL::to('/') }}/home/teacher/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="teacher_id" class="id" value="{{$teacher->id}}">
<?php

        foreach($teacher->getTeacherInfo as $index=>$class_slected){
        $subject_list[$index]=$class_slected['subject_id'];
        }

        ?>
        <h2> Update Teacher</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label for="firstname" class="col-md-4 control-label">Full Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm name" id="firstname" name="name" placeholder="Please enter full name" value="{{$teacher->name}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="address" class="col-md-4 control-label">Address:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm address" id="address" name="address" placeholder="Please enter address" value="{{$teacher->address}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="email" class="col-md-4 control-label">Email:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="email" class="form-control input-sm email" id="email" name="email" placeholder="Please enter email" value="{{$teacher->email}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="date_of_birth" class="col-md-4 control-label">Date of Birth:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm dob" id="date_of_birth" name="date_of_birth" placeholder="Please enter date of birth" value="{{$teacher->dob}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="gender" class="col-md-4 control-label">Gender:</label>
            <div class="col-md-8">
            <div class="form-group">
              <label class="radio-inline">
                  <input type="radio" name="gender" id="gender" value="male" checked> Male
                </label>
                <label class="radio-inline">
                  <input type="radio" name="gender" id="gender" value="female"> Female
                </label>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="telephone_no" class="col-md-4 control-label">Telephone no:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm telephone_no" id="telephone_no" name="telephone_no" placeholder="Please enter telephone number" value="{{$teacher->telephone_no}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="mobile_no" class="col-md-4 control-label">Mobile no:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm mobile_no" id="mobile_no" name="mobile_no" placeholder="Please enter mobile number" value="{{$teacher->mobile_no}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="old_image" class="col-md-4 control-label">Old Image:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="old_image" name="image" value="{{$teacher->image}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="image" class="col-md-4 control-label">Image:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="file" class="form-control input-sm" id="image" name="new_image">
            </div>
            </div>
          </div>
        </div>


        <fieldset>
        <div class="row">
          <label for="class_id" class="col-md-4 control-label">Class & Subjects:</label>
            <div class="col-md-8">
            @foreach($subject_all_lists as $subject_all_list)
                <div class="col-sm-6">
                  <div class="checkbox">
                      <input type="checkbox" id="teachere.{{$subject_all_list->id}}" name="subject_teacher_id[]" value="{{$subject_all_list->id}}" <?php echo (in_array($subject_all_list->id,$subject_list)?'checked':'');?>>
                      <label class="control-label" for="teachere.{{$subject_all_list->id}}">{{$subject_all_list->subjectclass->name}}({{$subject_all_list->name}})</label> 
                  </div>
                </div>
                @endforeach
          </div>
        </div>
        </br>
        <div class="col-md-6">
        <div class="row">
          <label for="name" class="col-md-4 control-label">Class:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control class-name" id="class_id" name="class_id">
                <option value="">Please select class</option>
                  @foreach($classees as $class)
                  <option value="{{$class->id}}" <?php echo($class->id==$teacher->getTeacherFirstClass->class_id)?'selected':''?>>{{$class->name}}</option>
                  @endforeach
                </select>
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="name" class="col-md-4 control-label">Section:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control" id="section_id" name="section_id">
              @foreach($sections as $section)
                <option value="{{$section->getSectionClass->id}}" <?php echo($section->getSectionClass->id==$teacher->getTeacherFirstClass->section_id)?'selected':''?>>{{$section->getSectionClass->name}}</option>
              @endforeach
              </select>
            </div>
            </div>
          </div>
        </div>
        </div>
        <div class="col-md-6">
        <div class="row">
          <label for="name" class="col-md-4 control-label">Shift:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control" id="shift_id" name="shift_id">
              @foreach($shifts as $shift)
                <option value="{{$shift->getShiftClass->id}}" <?php echo($shift->getShiftClass->id==$teacher->getTeacherFirstClass->shift_id)?'selected':''?>>{{$shift->getShiftClass->name}}</option>
              @endforeach
              </select>
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="subject_id" class="col-md-4 control-label">Subject:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control" id="subject_id" name="subject_id">
              @foreach($subjects as $subject)
                                <option value="{{$subject->id}}" <?php echo($subject->id==$teacher->getTeacherFirstClass->subject_id)?'selected':''?>>{{$subject->name}}</option>
                @endforeach
              </select>
            </div>
            </div>
          </div>
        </div>
        </div>
        </fieldset>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Add Teacher<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>