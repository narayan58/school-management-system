
  <div class="col-md-8" >
    <table class="table table-bordered">
    <thead>
      <tr>
          <th>SN</th>
          <th>System ID</th>
          <th>Bill SN</th>
          <th>Teacher</th>
          <th>Month</th>
          <th>Bill On</th>
      </tr>
    </thead>
    <tbody>
    @foreach($salary_totals as $index=>$salary)
          <tr>
             <td>{{$index+1}}</td>
             <td><a href="{{URL::to('/')}}/home/salary-detail/view/{{$salary->bill_id}}">{{$salary->bill_id}}</a></td>
             <td>{{$salary->teacherfeebillSn->id}}</td>
             <td>{{$salary->teacherfeename->name}}</td>
             <td>{{$salary->getMonth->name}}</td>
             <td>{{$salary->created_at}}</td>
          </tr>
    @endforeach
    </tbody>
  </table>
  </div>


