@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '2'))
	<div class="heading"><h2>{{$topic_name}} Topic Summary</h2></div>
	<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myTopic">
      Add Topic on {{$topic_name}}
  </button>
  <div class="col-md-12">
    <div class="row">
    <table class="table table-bordered">
      <thead> 
        <tr>
            <th>SN</th>
            <th>Topic Name</th>
            <th>Topic hour</th>
            <th class="hidden-print">Action</th>
        </tr>
      </thead>
      <tbody>
          @foreach($topics as $index=>$topic)
          <tr>
             <td>{{$index+1}}</td>
             <td><a href="{{URL::to('/')}}/home/class/{{$class}}/{{$slug}}/{{$topic->slug}}">{{$topic->name}}</a></td>
             <td>{{$topic->topic_hour}}</td>
             <td class="hidden-print">
            <a class="action-btn edit" id="topic" data-target="#edit_topic" data-toggle="modal" href=""><i class="fa fa-pencil-square-o" id="{{$topic->id}}"></i></a>
            <a class="action-btn bg-green delete" id="topic" href="javascript:void(0)"><i class="fa fa-times" id="{{$topic->id}}"></i></a>
            </td>
          </tr>
          @endforeach
      </tbody>
    </table>
  </div>
  </div> 

    <div id="myTopic" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-topic" id="topic_valid" action="{{URL::route('home.topic.store')}}"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="subject_id" value="{{$topic_id}}">
        <h2>Add Topic on {{$topic_name}}</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label for="topic_name" class="col-md-4 control-label">Topic Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm" id="topic_name" name="name" placeholder="Please enter topic name">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="topic_hour" class="col-md-4 control-label">Topic Hour:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm" id="topic_hour" name="topic_hour" placeholder="Please enter topic hour ">
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Add topic<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>


<div id="edit_topic" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form edit-form" id="topic_valid" action="{{ URL::to('/') }}/home/topic/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="subject_id" value="{{$topic_id}}">
        <input type="hidden" name="topic_id" class="id">

        <h2>Update Topic on {{$topic_name}}</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label for="topic_name" class="col-md-4 control-label">Topic Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm name" id="topic_name" name="name" placeholder="Please enter topic name">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="topic_hour" class="col-md-4 control-label">Topic Hour:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm topic_hour" id="topic_hour" name="topic_hour" placeholder="Please enter topic hour ">
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Update topic<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>
   @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
  </div>


@endsection
