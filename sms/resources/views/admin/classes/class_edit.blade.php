<button type="button" class="close" data-dismiss="modal">&times;</button>
<div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form edit-form" id="class_valid" action="{{ URL::to('/') }}/home/class/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
         <input type="hidden" name="class_id" class="id" value="{{$class->id}}">
        <h2>Class</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label class="col-md-4 control-label">Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="text" class="form-control input-sm name" id="name" name="name" value="{{$class->name}}" placeholder="Please enter class name" >
            </div>
            </div>
          </div>
        </div>
        <?php

        foreach($class->getSectionDetail as $index=>$section_slected){
        $section_list[$index]=$section_slected['section_id'];
        }
        foreach($class->getShiftDetail as $index=>$shift_slected){
                $shift_list[$index]=$shift_slected['shift_id'];
                }

        ?>
        <div class="row">
          <label for="image" class="col-md-4 control-label">Shift:</label>
            <div class="col-md-8">
                @foreach($shifts as $id=>$name)
                <div class="col-sm-6">
                  <div class="checkbox">

                      <input type="checkbox" id="shiftedit.{{$id}}" name="shift_id[]" value="{{$id}}" <?php echo (in_array($id,$shift_list)?'checked':'');?>>
                                            <label class="control-label" for="shiftedit.{{$id}}">{{$name}}</label>
                  </div>
                </div>
                @endforeach
          </div>
        </div>
        <div class="row">
          <label for="image" class="col-md-4 control-label">Section:</label>
            <div class="col-md-8">
              @foreach($sections as $id=>$name)
                <div class="col-sm-6">
                  <div class="checkbox">
                      <input type="checkbox" id="sectionedit.{{$id}}" name="section_id[]" value="{{$id}}" <?php echo (in_array($id,$section_list)?'checked':'');?>>
                     <label class="control-label" for="sectionedit.{{$id}}">{{$name}}</label>
                  </div>
                </div>
                @endforeach
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Update Class<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        </div>
        <script>
          $('#edit_class').on('hide.bs.modal', function () {
            $('#edit_class').removeData('bs.modal')
          })
          </script>