@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '2'))
	<div class="heading"><h2>Student Summary on class {{$class_org_name}}</h2></div>
            <!-- Button trigger modal -->
  <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/student_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button>
  <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
    <div id="printDiv">
  <div class="col-md-12">
    <div class="row">
		<table class="table table-bordered">
    <thead> 
      <tr>
         <th>SN</th>
          <th>Full Name</th>
          <th>Address</th>
          <th>Roll no</th>
          <th>Date of Birth</th>
          <th>Gender</th>
          <th>Telephone No</th>
          <th>Mobile No</th>
          <th>Section</th>
          <th>Shift</th>
          <th class="hidden-print">Preview</th>
      </tr>
    </thead>
    <tbody>
        @foreach($studentLists as $index=>$student)
      <tr>
       <td>{{$index+1}}</td>
       <td>{{$student->studentinfo->name}}</td>
       <td>{{$student->studentinfo->address}}</td>
       <td>{{$student->studentinfo->email}}</td>
       <td>{{$student->studentinfo->dob}}</td>
       <td>{{$student->studentinfo->gender}}</td>
       <td>{{$student->studentinfo->telephone_no}}</td>
       <td>{{$student->studentinfo->mobile_no}}</td>
       <td>{{$student->studentsection->name}}</td>
       <td>{{$student->studentshift->name}}</td>
       <td  class="hidden-print"><a href="{{URL::to('/')}}/image/student/{{$student->studentinfo->image_encrypt}}" data-lightbox="{{$student->studentinfo->image}}" data-title="{{$student->studentinfo->image}}"><img src="{{URL::to('/')}}/image/thumb/student/{{$student->studentinfo->image_encrypt}}" style="width:50px; height:50px "  alt="{{$student->image}}" class="img-responsive"></a></td>
      </tr>
        @endforeach
    </tbody>
  </table>
  </div>
	</div>
  </div>
   @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    // filter for teacher to get section
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getSectionOnStudent/"+teacher_id,
            success:function(e){
                $('#section_id').html('');
                $('#section_id').append('<option value="">Please Select Section</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#section_id').append('<option value='+val.get_section_class.id+'>'+val.get_section_class.name+'</option>');
                    }
                });
            }
        })
    });
    </script>
    <script type="text/javascript">
    // filter for teacher to get shift
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getShiftOnStudent/"+teacher_id,
            success:function(e){
                $('#shift_id').html('');
                $('#shift_id').append('<option value="">Please Select Shift</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#shift_id').append('<option value='+val.get_shift_class.id+'>'+val.get_shift_class.name+'</option>');
                    }
                });
            }
        })
    });
    </script>
@endsection
