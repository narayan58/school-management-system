<form class="general-form" method="POST" action="{{URL::to('/')}}/home/daily-student-attendance/store">
<input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
<div id="printDiv">
  Date:{{$attendance_date}} | Class: {{$class_name['0']['name']}} | Section: {{$section_name['0']['name']}} | Shift: {{$shift_name['0']['name']}} 
<table class="table table-bordered">
  <thead> 
    <tr>
        <th>SN</th>
        <th>Name</th>
        <th>Attendance</th>
        <th>Remark</th>
    </tr>
  </thead>
  <tbody>
        @if(count($class_attendance))
          @foreach($class_attendance as $index=>$student_list)
          <tr>
           <td>{{$index+1}}</td>
           <td>{{$student_list->getStudent->name}}</td>
           <input type="hidden" name="student_id[]" value="{{$student_list->getStudent->id}}">
           <input type="hidden" name="class_id[]" value="{{$class_id}}">
           <input type="hidden" name="shift_id[]" value="{{$shift_id}}">
           <input type="hidden" name="section_id[]" value="{{$section_id}}">
           <td>
              {{$student_list->title}}
            </td>
            <td>{{$message}}</td>
          </tr>
          @endforeach
        @endif
        
        @if(count($student_lists))
        @foreach($student_lists as $index=>$student_list)
    <tr>
     <td>{{$index+1}}</td>
     <td>{{$student_list->studentinfo->name}}</td>
     <input type="hidden" name="student_id[]" value="{{$student_list->studentinfo->id}}">
     <input type="hidden" name="class_id[]" value="{{$class_id}}">
     <input type="hidden" name="shift_id[]" value="{{$shift_id}}">
     <input type="hidden" name="section_id[]" value="{{$section_id}}">
     <td>
        <input type="radio" id="present.{{$student_list->studentinfo->id}}" name="attendance[].{{$student_list->studentinfo->id}}" value="Present"  checked>
          <label class="control-label" for="present.{{$student_list->studentinfo->id}}">Present</label> | 
        <input type="radio" id="absent.{{$student_list->studentinfo->id}}" name="attendance[].{{$student_list->studentinfo->id}}" value="Absent">
          <label class="control-label" for="absent.{{$student_list->studentinfo->id}}">Absent</label>
      </td>
      <td>{{$message}}</td>
    </tr>
      @endforeach
  </tbody>
</table>
</div>
@if(!$btndis)
  <div class="text-center">
    <button type="submit" class="btn btn-default btn-sm">
      Make Attendance<i class="fa fa-paper-plane-o"></i>
    </button>
  </div>
  @else
<button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
@endif
        @endif

      
</form>