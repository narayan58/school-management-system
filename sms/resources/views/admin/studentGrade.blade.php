<div class="heading"><h2>{{$student_org_name}}</h2></div>
<div class="row">
<div class="col-md-12">
<table class="table table-bordered">
<thead> 
    <tr>
        <th>SN</th>
        <th>Subject</th>
        <th>Grade</th>
    </tr>
</thead>
<tbody>
    @foreach($studentMarkSheet as $index=>$mark)
    <tr>
        <td><input type="hidden" name="marks_id[]" value="{{$mark->id}}">{{$index+1}}</td>
        @foreach($mark->getStudentSubMark()->get() as $sub_mark)
        <td>{{$sub_mark->getSubjectClass->name}}</td>
        @foreach($mark->getStudentGrade()->get() as $sub_grade)
        <td>{{$sub_grade->getStudentMarkGrade->name}}</td>
        @endforeach
        @endforeach
    </tr>
    @endforeach
    
</tbody>
</table>
</div>
</div>