@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '2'))
	<div class="heading"><h2>Help Summary</h2></div>
    <div class="row">
    <div class="col-md-8">
      <table class="table table-bordered">
        <thead> 
          <tr>
              <th>Help</th>
          </tr>
        </thead>
        <tbody>
          @foreach($helps as $index=>$help)
            <tr>
               <td>{!! $help->name !!}</td>
            </tr>
          @endforeach 
        </tbody>
      </table>
    </div>
    </div>
    @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
@endsection
