<header id="header">
<div class="header-holder">
	<div class="container">
		<div class="row text-right">
			<div class="col-md-9 col-md-offset-3">
				<ul class="header-list list-inline">
					<li><i class="fa fa-user text-center"></i>Admin Panel</li>
					<li><i class="fa fa-envelope"></i><a href="mailto:mailattechware@mail.com"> mailattechware@mail.com</a></li>
					<li><i class="fa fa-phone"></i>9851196943 | 9849220335</li>
					<li><a href="{{URL::to('/')}}/home/help" title="Help"><i class="fa fa-question"></i></a></li>
					<li><a href="https://www.youtube.com/watch?v=wd9S6pWNo4s" target="new" title="You Tube"><i class="fa fa-youtube"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
	<nav id="mainNav" class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav">
						<span class="sr-only">Toggle navigation</span>
						<i class="fa fa-bars"></i>
					</button>
					<a class="navbar-brand page-scroll" href="{{URL::to('/')}}/home"><img class="img-responsive" src="{{ URL::to('/') }}/images/logo.png" alt="company name" style="height: 85px;"></a>
			</div>
		<div class="collapse navbar-collapse" id="nav">
			<ul class="nav navbar-nav">
				<!-- <li><a href="{{ url('/') }}">Home</a></li> -->
			</ul>

			<ul class="nav navbar-nav navbar-right">
				@if (Auth::guest())
					<li><a href="{{ url('/login') }}">Login</a></li>
					<li><a href="{{ url('/register') }}">Register</a></li>
				@else
				<li><a href="">Last Login:{{ Auth::user()->last_login_pre }}</a></li>
				<!-- <li><a href="{{ URL::to('/') }}/home/menu">Menu</a></li> -->
				<li><a href="{{ url('/home') }}">Home</a></li>
				@foreach($user_levels as $userlevel)
  							@if(($userlevel->getUserLevel->user_level == '1'))
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i>Report <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ URL::to('/') }}/home/report/teachers">Teachers Report</a></li>
						<li><a href="{{ URL::to('/') }}/home/report/fee">Fee Report</a></li>
						<li><a href="{{ URL::to('/') }}/home/report/finance">FInance Report</a></li>
					</ul>
				</li>
							@endif
				@endforeach

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i>{{ Auth::user()->name }} <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						@foreach($user_levels as $userlevel)
  							@if(($userlevel->getUserLevel->user_level == '1'))
						<li><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">Create Role</a></li>
							@endif
						<li><a href="{{URL::to('/')}}/home/change_password">Change Password</a></li>
						<li><a href="{{ url('/logout') }}">Logout</a></li>
						@endforeach
					</ul>
				</li>
				@endif
			</ul>
		</div>
	</div>
</nav>
<!-- Modal -->
@foreach($user_levels as $userlevel)
  	@if(($userlevel->getUserLevel->user_level == '1'))
<div class="modal fade admin-model" id="myModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body">
    <div class="form-holder">
	<div class="row">
		<div class="col-xs-12 form-box">
			<form class="general-form validation" role="form" id="member_form" enctype="multipart/form-data" method="POST" action="{{URL::to('/')}}/home/create_role">
				<input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
				<h2><span class="number">1</span> USER ROLE FORM</h2>
				<p class="add-info">* Denotes required Field</p>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="full-name" class="control-label">Full Name:</label>
							<div class="required">
								<input type="text" class="form-control input-sm" id="full-name" name="name" value="{{ old('name') }}" placeholder="Enter Full Name">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="address" class="control-label">Address:</label>
							<div class="required">
								<input type="text" class="form-control input-sm" id="address"  name="address" value="{{ old('address') }}"  placeholder="Enter Address">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="email" class="control-label">Email:</label>
							<div class="required">
								<input type="email" class="form-control input-sm" id="email" name="email" value="{{ old('email') }}" placeholder="Enter valid email">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="password" class="control-label">Password:</label>
							<div class="required">
								<input type="password" class="form-control input-sm pass" id="password" name="password"  placeholder="Enter Password">
							</div>

						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="confirm_password" class="control-label">Confirm Password:</label>
							<div class="required">
								<input type="password" class="form-control input-sm pass" id="confirm_password" name="confirm_password"  placeholder="Repeat Password">
							</div>

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<strong class="title col-md-3">Gender:</strong>
							<div class="col-md-3">
								<div class="radio">
									<label><input type="radio" name="gender" value="M" checked >Male</label>
								</div>
							</div>
							<div class="col-md-3">
								<div class="radio">
									<label><input type="radio" name="gender" value="F">Female</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="dob" class="control-label">Date of Birth:</label>
							<div class="required">
								<input type="text" class="form-control input-sm" id="dob" name="date_of_birth" value="{{ old('date_of_birth') }}"  placeholder="Enter Date of birth">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="telephone" class="control-label">Telephone no:</label>
							<div class="required">
								<input type="text" class="form-control input-sm" id="telephone" name="telephone_no" value="{{ old('telephone_no') }}"  placeholder="Enter Telephone Number">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="mobile-no" class="control-label">Mobile no:</label>
							<div class="required">
								<input type="text" class="form-control input-sm" id="mobile-no" name="mobile_no" value="{{ old('mobile_no') }}"  placeholder="Enter Mobile Number">
							</div>

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="image" class="control-label">Image:</label>
							<div class="required">
								<input type="file" class="form-control input-sm" id="image"  name="image">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
						<label for="user_role" class="control-label">Role:</label>
						<div class="required">
							<select class="form-control input-sm" id="user_role" name="user_role">
								<option value="">Select Role</option>
								<option value="1">Admin</option>
								<option value="2">Main Section</option>
								<option value="3">Primary Section</option>
								<option value="4">Daily Section</option>
								<option value="5">Exam Section</option>
								<option value="6">Fee Section</option>
								<option value="7">Notice Section</option>
								<option value="8">Event Section</option>
							</select>
						</div>
						</div>
					</div>
				</div>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-default btn-sm">
						Register<i class="fa fa-paper-plane-o"></i>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
	@endif
@endforeach
</header>