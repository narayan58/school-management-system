@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div id="mainMan">
<div class="admin-content">
	<div class="admin-top">
		<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			    @if(Session::has('alert-' . $msg))
			<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  	@endif
			@endforeach
		</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
		@foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '6'))
		<div class="heading"><h2>Class:{{$class_name}} &nbsp; Section:{{$class_section}}  &nbsp; Shift:{{$class_shift}}</h2></div>
	            <div id="printDiv">

		<div class="row">
			<div class="col-sm-8">
				<div class="print-section general-form form-box padding-sm" id="bill">
					<div class="heading bg-blue">
						<h2 class="text-center">Bill</h2>
					</div>
					<div class="col-md-12">
    				<div class="row">
					<table class="table table-default">
						<tbody>
							@foreach($student_details as $studentdetail)
							<tr>
							<tr>
								<td>Student Name: {{$studentdetail->studentfeename->name}}</td>
								<td>Received by: {{$studentdetail->studentfeecreate->name}}</td>
								<td>
								Status: @if($studentdetail->is_due == '1')
									<b style="color:red;">Due Bill</b>
									<a class="action-btn bg-green confirm" id="grade" href="javascript:void(0)"><i class="fa fa-times" id="{{$studentdetail->id}}"></i></a>
									@else
									<b>Paid Bill</b>
									@endif
								</td>
							</tr>
							<div class="watermark">{{$message}}</div>
							<tr>
								<td>Date:{{$studentdetail->created_at}}</td>
								<td>System Code: {{$studentdetail->bill_id}}</td>
								<td>Bill ID:{{$studentdetail->studentfeebillSn->id}} </td>
							</tr>
							<tr></tr>
							</tr>
							<tr class="success">
								<th>SN</th>
								<th>Item(s), Months</th>
								<th>Amount</th>
							</tr>
							@foreach($studentdetail->studentfeedetail()->get() as $index=>$student)
							<tr  class="active">
								<td>{{$index+1}}</td>
								<td>
									{{$student->description}} (@foreach($student->getStudentMonth()->get() as $fee_stu)
									@foreach($fee_stu->getMonth()->get() as $fee_month)
									{{$fee_month->name}},
									@endforeach
									@endforeach)
								</td>
								<td>{{number_format($student->amount, 2) }}</td>
							</tr>
							@endforeach
							<tr class="text-right">
								<td></td>
								<td></td>
								<td>Total: {{number_format($studentdetail->total, 2) }}</td>
							</tr>
							<tr class="text-right">
								<td></td>
								<td></td>
								<td>Discount: {{number_format($studentdetail->discount, 2) }}</td>
							</tr>
							<tr class="text-right">
								<td></td>
								<td></td>
								<td>Grand Total: {{number_format($studentdetail->grand_total, 2) }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				</div>				
	        <button type="button" class="btn btn-default btn-sm hidden-print" onclick="PrintDiv('printDiv')" style=" margin-bottom: 2px;" >PRINT<i class="fa fa-print"></i></button>
			</div>	
		</div>
		</div>
		@else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
</div>
<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
    <script type="text/javascript">
   $(document).on('click', '.confirm', function(){
        var id = $(event.target).attr("id");
        token=$(".token").val();

        alertify.confirm("Are you sure you want to clear this bill?",function(){
            url= "{{URL::route('feedueclear')}}",
            $.ajax({
                type:"POST",
                dataType:"JSON",
                url:url,
                data:{
                _token:token,
                    id : id
                },
                success:function(e){
                    alertify.alert(e.msg,function(){
                        location.reload()
                    })
                },
                error: function (e) {
                    alertify.alert('Sorry! this please try again');
                }
            });
        });
    });
  </script>
@endsection