@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
  <div class="admin-top">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
  </div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '3'))
  <div class="heading"><h2>Teacher Summary</h2></div>
  <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myTeacher">
      Add Teacher
    </button>
     <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/teacher_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button>
        <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
    <div id="myTeacher" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
    <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-teacher" id="teacher_valid" action="{{URL::to('/') }}/home/teacher/store"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <h2>Teacher</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label for="firstname" class="col-md-4 control-label">Full Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="firstname" name="name" placeholder="Please enter full name">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="address" class="col-md-4 control-label">Address:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="address" name="address" placeholder="Please enter address">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="email" class="col-md-4 control-label">Email:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="email" class="form-control input-sm" id="email" name="email" placeholder="Please enter email">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="date_of_birth" class="col-md-4 control-label">Date of Birth:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="date_of_birth" name="date_of_birth" placeholder="Please enter date of birth">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="gender" class="col-md-4 control-label">Gender:</label>
            <div class="col-md-8">
            <div class="form-group">
              <label class="radio-inline">
                  <input type="radio" name="gender" id="gender" value="Male" checked> Male
                </label>
                <label class="radio-inline">
                  <input type="radio" name="gender" id="gender" value="Female"> Female
                </label>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="telephone_no" class="col-md-4 control-label">Qualification:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="telephone_no" name="telephone_no" placeholder="Please enter teacher qualification">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="mobile_no" class="col-md-4 control-label">Mobile no:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="mobile_no" name="mobile_no" placeholder="Please enter mobile number">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="old_image" class="col-md-4 control-label">Image:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm image" id="old_image" name="image">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="image" class="col-md-4 control-label">Image:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="file" class="form-control input-sm" id="image" name="new_image">
            </div>
            </div>
          </div>
        </div>

        <fieldset>
        <div class="row">
          <label class="col-md-4 control-label">Class & Subjects:</label>
            <div class="col-md-8">
              @foreach($subject_all_lists as $subject_all_list)
                <div class="col-sm-6">
                  <div class="checkbox">
                      <input type="checkbox" id="teacher.{{$subject_all_list->id}}" name="subject_teacher_id[]" value="{{$subject_all_list->id}}">
                      <label class="control-label" for="teacher.{{$subject_all_list->id}}">{{$subject_all_list->subjectclass->name}}({{$subject_all_list->name}})</label> 
                  </div>
                </div>
                @endforeach
          </div>
        </div>
        </br>
        <div class="col-md-6">
        <div class="row">
          <label for="class_id" class="col-md-4 control-label">Class:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control class-name" id="class_id" name="class_id">
                <option value="">Please select class</option>
                  @foreach($classees as $class)
                  <option value="{{$class->id}}">{{$class->name}}</option>
                  @endforeach
                </select>
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="section_id" class="col-md-4 control-label">Section:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control" id="section_id" name="section_id">
                <option value="">Select section</option>
              </select>
            </div>
            </div>
          </div>
        </div>
        </div>
        <div class="col-md-6">
        <div class="row">
          <label for="shift_id" class="col-md-4 control-label">Shift:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control" id="shift_id" name="shift_id">
                <option value="">Please select shift</option>
              </select>
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="subject_id" class="col-md-4 control-label">Subject:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control" id="subject_id" name="subject_id">
                <option value="">Please select subject</option>
              </select>
            </div>
            </div>
          </div>
        </div>
        </div>
        </fieldset>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Add Teacher<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
  </div>
    </div>
  </div>

    <div id="printDiv">
    <div class="col-md-12">
    <div class="row">
    <table class="table table-bordered">
    <thead> 
      <tr>
          <th>SN</th>
          <th>Name</th>
          <th>Address</th>
          <th>Email</th>
          <th>DOB</th>
          <th>Gender</th>
          <th>Qualification</th>
          <th>Contact No</th>
          <th>First Class</th>
          <th>Other Class</th>
          <th class="hidden-print">Preview</th>
          <th class="hidden-print">Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($teachers as $index=>$teacher)
        <tr>
       <td>{{$index+1}}</td>
       <td>{{$teacher->name}}</td>
       <td>{{$teacher->address}}</td>
       <td>{{$teacher->email}}</td>
       <td>{{$teacher->dob}}</td>
       <td>{{$teacher->gender}}</td>
       <td>{{$teacher->telephone_no}}</td>
       <td>{{$teacher->mobile_no}}</td>
       @if(count($teacher->getTeacherFirstClass))
       <td>{{$teacher->getTeacherFirstClass->getTeacherFirstSubject->name}}({{$teacher->getTeacherFirstClass->getTeacherFirstCls->name}}), {{$teacher->getTeacherFirstClass->getTeacherFirstSection->name}}, {{$teacher->getTeacherFirstClass->getTeacherFirstShift->name}}</td>
       @else
       <td>-</td>
       @endif
       <td>
          @foreach($teacher->getTeacherInfo()->get() as $subject_lists)
                  {{$subject_lists->getAllSubject->name}}({{$subject_lists->getAllSubject->subjectclass->name}}),
          @endforeach
       </td>
       <td class="hidden-print"><a href="{{URL::to('/')}}/image/teacher/{{$teacher->image_encrypt}}" data-lightbox="{{$teacher->image}}" data-title="{{$teacher->image}}"><img src="{{URL::to('/')}}/image/thumb/teacher/{{$teacher->image_encrypt}}" style="width:50px; height:50px "  alt="{{$teacher->image}}" class="img-responsive"></a></td>
        <td class="hidden-print">
          <a class="action-btn " id="teacher" data-target="#edit_teacher" data-toggle="modal" href="{{URL::to('/').'/home/teacher/edit/'.$teacher->id}}"><i class="fa fa-pencil-square-o" id="{{$teacher->id}}"></i></a>
         </td>      
         </tr>
        @endforeach
    </tbody>
  </table>
  </div>
  </div>
  </div>
  <div id="edit_teacher" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
    <div class="modal-content">

  </div>
    </div>
  </div>
   @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
  </div>
   

  <script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    // filter for teacher to get section
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getSectionOnStudent/"+teacher_id,
            success:function(e){
                $('#section_id').html('');
                $('#section_id').append('<option value="">Please Select Section</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#section_id').append('<option value='+val.get_section_class.id+'>'+val.get_section_class.name+'</option>');
                    }
                });
            }
        })
    });
</script>
<script type="text/javascript">
    // filter for teacher to get shift
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getShiftOnStudent/"+teacher_id,
            success:function(e){
                $('#shift_id').html('');
                $('#shift_id').append('<option value="">Please Select Shift</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#shift_id').append('<option value='+val.get_shift_class.id+'>'+val.get_shift_class.name+'</option>');
                    }
                });
            }
        })
    });
</script>
<script type="text/javascript">
    // filter for teacher to get shift
    $('.class-name').change(function(){
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getSubjectOnStudent/"+teacher_id,
            success:function(e){
                $('#subject_id').html('');
                $('#subject_id').append('<option value="">Please Select Subject</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#subject_id').append('<option value='+val.id+'>'+val.name+'</option>');
                    }
                });
            }
        })
    });
</script>
<script>
              $('#myTeacher').on('hide.bs.modal', function () {
                $('#myTeacher').removeData('bs.modal')
              })
              </script>
  @endsection