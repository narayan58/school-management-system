<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
//header("Content-type: application/octet-stream;charset=utf-8");
header("Content-Type: text/html; charset=UTF-8");
header("Content-Disposition: attachment; filename=RoleRecord.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table class="table table-bordered" border="1">
    <thead> 
      <tr>
          <th>SN</th>
          <th>Full Name</th>
          <th>Address</th>
          <th>Email</th>
          <th>DOB</th>
          <th>Gender</th>
          <th>T No</th>
          <th>M No</th>
          <th>User Level</th>
    </thead>
    <tbody>
        @foreach($roles as $index=>$role)
        <tr>
       <td>{{$index+1}}</td>
       <td>{{$role->name}}</td>
       <td>{{$role->address}}</td>
       <td>{{$role->email}}</td>
       <td>{{$role->dob}}</td>
       <td>{{$role->gender}}</td>
       <td>{{$role->telephone_no}}</td>
       <td>{{$role->mobile_no}}</td>
       <td>{{$role->getUserLevel->title}}</td>
        </tr>
        @endforeach
    </tbody>
  </table>