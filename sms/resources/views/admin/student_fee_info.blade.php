@extends('admin.app')
@section('content')
@include('admin.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
  <div class="admin-top">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
  </div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '6'))
  <div class="heading"><h2>Latest Fee</h2></div>

<div class="form-holder">
  <form class="general-form" id="exam_valid">
    <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
    <div class="row">
    <div class="form-group col-md-3">
      <label for="category">Class</label>
      <div class="">
        <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="class_id" name="class_id" selected="">
          <option value="0">--Please Select--</option>
                  @foreach($class_list as $index=>$class)
                  <option value="{{$index}}">{{$class}}</option>

                  @endforeach
        </select>
      </div>
    </div>
    <div class="form-group col-md-3">
      <label for="category">Section</label>
      <div class="">
        <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="section_id" name="section_id" selected="">
          <option value="0">--Please Select--</option>
                  @foreach($section_list as $index=>$class)
                  <option value="{{$index}}">{{$class}}</option>
                  @endforeach
        </select>
      </div>
    </div>
    <div class="form-group col-md-3">
      <label for="category">Shift</label>
      <div class="">
        <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="shift_id" name="shift_id" selected="">
          <option value="0">--Please Select--</option>
                  @foreach($shift_list as $index=>$class)
                  <option value="{{$index}}">{{$class}}</option>
                  @endforeach
        </select>
      </div>
    </div>
    <div class="text-center">
    <button type="button" class="btn btn-default btn-sm" id="search">
      Search<i class="fa fa-paper-plane-o"></i>
    </button>
  </div>
  </div>
  </form>
</div>

<div class="row" id="replaceTable">
  <div class="col-md-8" >
    <table class="table table-bordered">
    <thead>
      <tr>
          <th>SN</th>
          <th>System ID</th>
          <th>Bill SN</th>
          <th>Student</th>
          <th>Class</th>
          <th>Section</th>
          <th>Shift</th>
          <th>Bill On</th>
          <th>Status</th>
      </tr>
    </thead>
    <tbody>
    @foreach($fee_totals as $index=>$fee)
          <tr>
             <td>{{$index+1}}</td>
             <td><a href="{{URL::to('/')}}/home/fee-detail/view/{{$fee->bill_id}}">{{$fee->bill_id}}</a></td>
             <td>{{$fee->studentfeebillSn->id}}</td>
             <td>{{$fee->studentfeename->name}}</td>
             <td>{{$fee->getStudentInfo->studentclass->name}}</td>
             <td>{{$fee->getStudentInfo->studentsection->name}}</td>
             <td>{{$fee->getStudentInfo->studentshift->name}}</td>
             <td>{{$fee->created_at}}</td>
             @if($fee->is_due == '1')
             <td  style="color:red;">Due Bill</td>
             @else
             <td>Paid Bill</td>
             @endif
          </tr>
    @endforeach
    </tbody>
  </table>
  {!! str_replace('/?', '?', $fee_totals->render()) !!}
  </div>
</div>
    @else
  <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    document.addEventListener('keypress', function(event) {
                 if (event.keyCode == 13) {
                     event.preventDefault();
                     $("#search").click();
                 }
             });
        $('#search').click(function () {
        $('#loader').show();

    var searchParams = {};
    var base_url = '<?php echo url();?>/home/fee-detail/fee_search';
    searchParams['class_id']=$('#class_id').val();
    searchParams['section_id']=$('#section_id').val();
    searchParams['shift_id']=$('#shift_id').val();

    var token = $('.token').val();
    $.ajax({
        type: 'get',
        data: 'parameters= ' + JSON.stringify(searchParams) + '&_token=' + token,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
                    $('#loader').hide('slow');
        }
    });
    return false;
});
</script>
@endsection
