<div class="col-md-8" >
  <table class="table table-bordered">
      <thead> 
        <tr>
          <th>SN</th>
          <th>System Id</th>
          <th>Bill Id</th>
          <th>Student</th>
          <th>Class</th>
          <th>Section</th>
          <th>Shift</th>
          <th>Billed On</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
      @foreach($fee_totals as $index=>$fee)
            <tr>
             <td>{{$index+1}}</td>
             <td><a href="{{URL::to('/')}}/home/fee-detail/view/{{$fee->bill_id}}">{{$fee->bill_id}}</a></td>
             <td>{{$fee->studentfeebillSn->id}}</td>
             <td>{{$fee->studentfeename->name}}</td>
             <td>{{$fee->getStudentInfo->studentclass->name}}</td>
             <td>{{$fee->getStudentInfo->studentsection->name}}</td>
             <td>{{$fee->getStudentInfo->studentshift->name}}</td>
             <td>{{$fee->created_at}}</td>
             @if($fee->is_due == '1')
             <td  style="color:red;">Due Bill</td>
             @else
             <td>Paid Bill</td>
             @endif
          </tr>
      @endforeach
      </tbody>
  </table>
</div>