@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '7'))
	<div class="heading"><h2>Notice Summary</h2></div>
	<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myNotice">
      Add Notice
  </button>
  <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/notice_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button></span>
  <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
  
  <div class="col-md-12">
    <div id="printDiv">
    <div class="row">
    <table class="table table-bordered">
    <thead> 
        <tr>
            <th>SN</th>
            <th>Title</th>
            <th>Notice</th>
            <th>Class</th>
            <th>By</th>
            <th>On</th>
            <th class="hidden-print">Action</th>
        </tr>
    </thead>
    <tbody>
          @foreach($notices as $index=>$notice)
            <tr>
               <td>{{$index+1}}</td>
               <td>{{$notice->name}}</td>
               <td>{!! $notice->title !!}</td>
               <td>
                @foreach($notice->getNotice()->get() as $notices)
                 <button type="button" class="btn btn-info btn-sm">
                {{$notices->getClass->name}}
                </button>
                @endforeach
                </td>
               <td>{{$notice->getUser->name}}</td>
               <td>{{$notice->created_at}}</td>
              <td class="hidden-print">
                <a class="action-btn edit-notice" id="notice" data-target="#edit_notice" data-toggle="modal" href=""><i class="fa fa-pencil-square-o" id="{{$notice->id}}"></i></a>
                <a class="action-btn bg-green delete" id="notice" href="javascript:void(0)"><i class="fa fa-times" id="{{$notice->id}}"></i></a>
             </td>            
            </tr>
          @endforeach 
    </tbody>
    </table>
    </div>
    </div>
    </div>

    <div id="myNotice" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-class" id="class_valid" action="{{ URL::to('/') }}/home/notice/store"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <h2>Notice</h2>
        <p class="add-info">* Denotes required Field</p>
         <div class="row">
          <label for="title" class="col-md-4 control-label">Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="title" name="title" placeholder="Please enter title name">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="form-group">
            <div class="required">
              <textarea id="editor1" name="name" aria-required="true" rows="10" cols="80" style="visibility: hidden; display: none;">
              </textarea>
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="class" class="col-md-4 control-label">Notice for:</label>
            <div class="col-md-8">
                @foreach($classes as $class)
                <div class="col-sm-6">
                  <div class="checkbox">
                      <input type="checkbox" id="class.{{$class->id}}" name="class_id[]" value="{{$class->id}}">
                      <label class="control-label" for="class.{{$class->id}}">{{$class->name}}</label> 
                  </div>
                </div>
                @endforeach
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Add Notice<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>

    <div id="edit_notice" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form edit-form" id="class_valid" action="{{ URL::to('/') }}/home/notice/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="notice_id" class="id">

        <h2>Notic Update</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label for="title" class="col-md-4 control-label">Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm name" id="title" name="title" placeholder="Please enter title name">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="form-group">
            <div class="required">
            <textarea id="description" class="title" name="name" aria-required="true" rows="10" cols="80" style="visibility: hidden; display: none;">
              </textarea>
            </div>
          </div>
        </div>
        </div>
        <div class="row">
          <label for="class" class="col-md-4 control-label">Notice for:</label>
            <div class="col-md-8">
                @foreach($classes as $class)
                <div class="col-sm-6">
                  <div class="checkbox">
                      <input type="checkbox" id="classedit.{{$class->id}}" name="class_id[]" value="{{$class->id}}">
                      <label class="control-label" for="classedit.{{$class->id}}">{{$class->name}}</label> 
                  </div>
                </div>
                @endforeach
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Update Notice<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>
     @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>


    <script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script type="text/javascript">
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
        CKEDITOR.replace('description');
      });
</script>
<script type="text/javascript">
    $('.edit-notice').click(function(event){
    var id=$(event.target).attr("id"),
    t=$(event.target).parent().attr("id");

    url=t+"/edit/"+id,
    $.ajax({
      type:"GET",
      dataType:"JSON",
      url:url,
      success:function(response){
       CKEDITOR.instances['description'].destroy();
          $.each( response, function( key, value ) {
            $('.edit-form .'+key).val(value);

          });
          CKEDITOR.replace('description');
      }
    });
  });
  </script>
@endsection
