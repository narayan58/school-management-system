<div class="heading"><h2>{{$student_org_name}}</h2></div>
@if(count($studentMarkSheet))
<div class="text-center">
  <button class="btn btn-default btn-sm">
    Marksheet Added Already<i class="fa fa-paper-plane-o"></i>
  </button>
</div>
@else
<div class="row">
<div class="col-md-12">
<table class="table table-bordered">
<thead> 
    <tr>
        <th>SN</th>
        <th>Subject</th>
        <th>Full Mark</th>
        <th>Pass Mark</th>
        <th>Practical Mark</th>
        <th>Theory Mark</th>
        <th>Obtained Mark</th>
    </tr>
</thead>
<tbody>
    <input type="hidden" name="student_id" value="{{$student_id}}">
    <input type="hidden" name="exam_id" value="{{$exam_id}}">
    @foreach($student_ord_subs as $index=>$sub)
    <tr>
        <td><input type="hidden" name="marks_id[]" value="{{$sub->id}}">{{$index+1}}</td>
        <td>{{$sub->getSubjectClass->name}}</td>
        <td><input type="hidden" class="form-control input-sm" id="full_mark" name="full_mark[]" value="{{$sub->full_mark}}">{{$sub->full_mark}}</td>
        <td>{{$sub->pass_mark}}</td>
        <td><input type="text" class="form-control input-sm" id="marks_pr" name="marks_pr[]"></td>
        <td><input type="text" class="form-control input-sm" id="marks_th" name="marks_th[]"></td>
        <td><input type="text" class="form-control input-sm" id="marks_obt" name="marks_obt[]"></td>
    </tr>
    @endforeach
   
</tbody>
</table>
</div>
</div>
<div class="text-center">
  <button type="submit" class="btn btn-default btn-sm">
    Add Marksheet<i class="fa fa-paper-plane-o"></i>
  </button>
</div>
@endif

<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    // filter for teacher to get section
    $('#marks_obt').keyup(function(){
        var marks_pr = $('#marks_pr').val(),
            marks_th = $('#marks_th').val();
            totmark = parseInt(marks_pr) + parseInt(marks_th);
        $('#marks_obt').val(totmark);
       
    });
</script>