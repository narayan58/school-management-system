<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
//header("Content-type: application/octet-stream;charset=utf-8");
header("Content-Type: text/html; charset=UTF-8");
header("Content-Disposition: attachment; filename=ClassRecord.xls");
header("Pragma: no-cache");
header("Expires: 0");
?><table class="table table-bordered" border="1">
    <thead> 
        <tr>
            <th>SN</th>
            <th>Class</th>
            <th>Section</th>
            <th>Shift</th>
        </tr>
    </thead>
    <tbody>
        @foreach($classes as $index=>$class)
        <tr>
           <td>{{$index+1}}</td>
           <td><a href="{{URL::to('/')}}/home/class/{{$class->slug}}">{{$class->name}}</a></td>
           <td>  
            @foreach($class->getSectionDetail()->get() as $key=>$section)
              @if($key =='0') 
              {{$section->getSectionClass->name}}
              @else
              {{', '.$section->getSectionClass->name}}
              @endif
            @endforeach
            </td>
            <td>  
            @foreach($class->getShiftDetail()->get() as $key=>$shift)
             @if($key =='0') 
              {{$shift->getShiftClass->name}}
              @else
              {{', '.$shift->getShiftClass->name}}
              @endif
            @endforeach
            </td>
        </tr>
        @endforeach
    </tbody>
    </table>