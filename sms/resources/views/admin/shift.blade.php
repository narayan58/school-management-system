@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '2'))
    <div class="heading"><h2>Shift Summary</h2></div>
    <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myShift">
      Add Shift
    </button>
    <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/shift_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button>
    <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
    <div id="printDiv">
    <div class="col-md-12">
    <div class="row">
    <table class="table table-bordered">
      <thead> 
        <tr>
            <th>SN</th>
            <th>Shift</th>
            <th>Created By</th>
            <th>Created On</th>
            <th class="hidden-print">Action</th>
        </tr>
      </thead>
      <tbody>
          @foreach($shifts as $index=>$shift)
        <tr>
         <td>{{$index+1}}</td>
         <td>{{$shift->name}}</td>
         <td>{{$shift->getUser->name}}</td>
         <td>{{$shift->created_at}}</td>
         <td class="hidden-print">
          <a class="action-btn edit" id="shift" data-target="#edit_shift" data-toggle="modal" href=""><i class="fa fa-pencil-square-o" id="{{$shift->id}}"></i></a>
          <a class="action-btn bg-green delete" id="shift" href="javascript:void(0)"><i class="fa fa-times" id="{{$shift->id}}"></i></a>
         </td>
        </tr>
          @endforeach
      </tbody>
    </table>
      </div>
      </div>
    </div>
            <!-- Modal -->
  <div id="myShift" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
   <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-shift" id="shift_valid" action="{{ URL::to('/') }}/home/shift"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <h2>Shift</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label  for="name" class="col-md-4 control-label">Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="name" name="name" placeholder="Please enter section name">
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" id="submit" class="btn btn-default btn-sm">
            Add Shift<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
  </div>
  </div>
  </div>

<div id="edit_shift" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
   <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form edit-form" id="edit_shift_valid" action="{{ URL::to('/') }}/home/shift/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="shift_id" class="id">
        <h2>Shift</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label  for="name" class="col-md-4 control-label">Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm name" id="name" name="name" placeholder="Please enter section name">
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Update Shift<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
  </div>
  </div>
  </div>
    @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
  </div>
<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $("#edit_shift_valid").validate({
        rules:{
            name:{
                required:true,
                minlength:2
            }
        },
        messages:{
            name:{
                required:"Please enter a shift name",
                minlength:"Your topic name must consists of at least 2 characters"
            }
        }
    });
</script>
@endsection
