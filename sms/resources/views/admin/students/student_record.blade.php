<table class="table table-bordered">
    <thead> 
      <tr>
         <th>SN</th>
          <th>Full Name</th>
          <th>Address</th>
          <th>Roll no</th>
          <th>Date of Birth</th>
          <th>Gender</th>
          <th>Telephone No</th>
          <th>Mobile No</th>
          <th>Class</th>
          <th>Section</th>
          <th>Shift</th>
          <th>Preview</th>
          <th class="hidden-print">Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($students as $index=>$student)
      <tr>
       <td>{{$index+1}}</td>
       <td><a href="{{URL::to('/')}}/home/student/{{$student->studentinfo->email}}">{{$student->studentinfo->name}}</a></td>
       <td>{{$student->studentinfo->address}}</td>
       <td>{{$student->studentinfo->email}}</td>
       <td>{{$student->studentinfo->dob}}</td>
       <td>{{$student->studentinfo->gender}}</td>
       <td>{{$student->studentinfo->telephone_no}}</td>
       <td>{{$student->studentinfo->mobile_no}}</td>
       <td>{{$student->studentclass->name}}</td>
       <td>{{$student->studentsection->name}}</td>
       <td>{{$student->studentshift->name}}</td>
       <td><a href="{{URL::to('/')}}/image/student/{{$student->studentinfo->image_encrypt}}" data-lightbox="{{$student->studentinfo->image}}" data-title="{{$student->studentinfo->image}}"><img src="{{URL::to('/')}}/image/thumb/student/{{$student->studentinfo->image_encrypt}}" style="width:50px; height:50px "  alt="{{$student->image}}" class="img-responsive"></a></td>
       <td class="hidden-print">
          <a class="action-btn edit" id="student" data-target="#edit_student" data-toggle="modal" href=""><i class="fa fa-pencil-square-o" id="{{$student->id}}"></i></a>
          <a class="action-btn bg-green delete" id="student" href="javascript:void(0)"><i class="fa fa-times" id="{{$student->id}}"></i></a>
         </td>
      </tr>
        @endforeach
    </tbody>
  </table>