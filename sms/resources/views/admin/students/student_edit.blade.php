<button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-student" id="student_valid" action="{{ url('/') }}/home/student/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="student_id" class="id" value="{{$student->id}}">
        <h2>Update Student</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label for="firstname" class="col-md-4 control-label">Full Name::</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm name" id="firstname" name="name" placeholder="Please enter full name" value="{{$student->name}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="mobile_no" class="col-md-4 control-label">Parents:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm mobile_no" id="mobile_no" name="mobile_no" placeholder="Please enter parent name" value="{{$student->mobile_no}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="address" class="col-md-4 control-label">Address:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm address" id="address" name="address" placeholder="Please enter address" value="{{$student->address}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="roll_no" class="col-md-4 control-label">Roll no:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm email" id="roll_no" name="roll_no" placeholder="Please enter roll number" value="{{$student->email}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="date_of_birth" class="col-md-4 control-label">Date of Birth:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm dob" id="date_of_birth" name="date_of_birth" placeholder="Please enter date of birth" value="{{$student->dob}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="gender" class="col-md-4 control-label">Gender:</label>
            <div class="col-md-8">
            <div class="form-group">
              <label class="radio-inline">
                  <input type="radio" name="gender" id="gender" value="male" checked> Male
                </label>
                <label class="radio-inline">
                  <input type="radio" name="gender" id="gender" value="female"> Female
                </label>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="name" class="col-md-4 control-label">Telephone no:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm telephone_no" id="telephone_no" name="telephone_no" placeholder="Please enter telephone number" value="{{$student->telephone_no}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="image" class="col-md-4 control-label">Old Image:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm image" id="image" name="old_image" value="{{$student->image}}">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="image" class="col-md-4 control-label">Image:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="file" class="form-control input-sm" id="image" name="new_image">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="name" class="col-md-4 control-label">Class:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control class-name-edit" id="class_id" name="class_id" >
                <option value="">Please select class</option>
                  @foreach($classees as $class)
                  <option value="{{$class->id}}" <?php echo($class->id==$student->studenthasclass->class_id)?'selected':''?>>{{$class->name}}</option>
                  @endforeach
                </select>
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="name" class="col-md-4 control-label">Section:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control" id="section_id_edit" name="section_id">
                <option value="">Please select section</option>
                @foreach($sections as $section)
                  <option value="{{$section->getSectionClass->id}}" <?php echo($section->getSectionClass->id==$student->studenthasclass->section_id)?'selected':''?>>{{$section->getSectionClass->name}}</option>
                  @endforeach
              </select>
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label for="name" class="col-md-4 control-label">Shift:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <select class="form-control" id="shift_id_edit" name="shift_id">
                <option value="">Please select shift</option>
                @foreach($shifts as $shift)
                  <option value="{{$shift->getShiftClass->id}}" <?php echo($shift->getShiftClass->id==$student->studenthasclass->shift_id)?'selected':''?>>{{$shift->getShiftClass->name}}</option>
                  @endforeach
              </select>
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Update Student<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    <script>
              $('#edit_student').on('hide.bs.modal', function () {
                $('#edit_student').removeData('bs.modal')
              })
              </script>
              {{--<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>--}}

              <script type="text/javascript">
                  // filter for teacher to get section
                  $('.class-name-edit').change(function(){
                      var teacher_id = $('.class-name-edit').val();
                      $.ajax({
                          type:"GET",
                          dataType:"JSON",
                          url: "getSectionOnStudent/"+teacher_id,
                          success:function(e){
                              $('#section_id_edit').html('');
                              $('#section_id_edit').append('<option value="">Please Select Section</option>');
                              $.each( e, function( i, val ) {
                                  if(val != ""){
                                      $('#section_id_edit').append('<option value='+val.get_section_class.id+'>'+val.get_section_class.name+'</option>');
                                  }
                              });
                          }
                      })
                        $.ajax({
                            type:"GET",
                            dataType:"JSON",
                            url: "getShiftOnStudent/"+teacher_id,
                            success:function(e){
                                $('#shift_id_edit').html('');
                                $('#shift_id_edit').append('<option value="">Please Select Shift</option>');
                                $.each( e, function( i, val ) {
                                    if(val != ""){
                                        $('#shift_id_edit').append('<option value='+val.get_shift_class.id+'>'+val.get_shift_class.name+'</option>');
                                    }
                                });
                            }
                        })
                  });
                  </script>
