<div class="sidebar">
	<div id="AdminMenu" class="active-menu">
		<a href="javascript:void(0)" class="sidemenu-open"><i class="fa fa-bars"></i></a>
		<div class="list-group panel">
			<!-- master data-->
			@foreach($user_levels as $userlevel)
			@if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '2'))
			<a href="#master" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Main Entry</span> <i class="fa fa-user"></i></a>
			<div class="collapse" id="master">
				<a href="{{ URL::to('/') }}/home/shift" class="list-group-item"><span class="text">Shift</span><i class="fa fa-list"></i></a>
				<a href="{{ URL::to('/') }}/home/section" class="list-group-item"><span class="text">Section</span><i class="fa fa-list"></i></a>
				<a href="{{ URL::to('/') }}/home/period" class="list-group-item"><span class="text">Period</span><i class="fa fa-list"></i></a>
				<a href="{{ URL::to('/') }}/home/class" class="list-group-item"><span class="text">Class</span><i class="fa fa-list"></i></a>
			</div>
			@endif

			@if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '3'))
			<a href="#entry" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Primary Entry</span> <i class="fa fa-address-card-o" aria-hidden="true"></i></a>
			<div class="collapse" id="entry">
				<a href="{{ URL::to('/') }}/home/student" class="list-group-item"><span class="text">Student</span><i class="fa fa-user-plus"></i></a>
				<a href="{{ URL::to('/') }}/home/teacher" class="list-group-item"><span class="text">Teacher</span><i class="fa fa-user-plus"></i></a>
			</div>
			@endif

			@if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '4'))
			<a href="#teacher_record" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Daily Record</span> <i class="fa fa-user"></i></a>
			<div class="collapse" id="teacher_record">
				<a href="{{ URL::to('/') }}/home/daily-teacher-record" class="list-group-item"><span class="text">Teacher</span><i class="fa fa-list"></i></a>
				<a href="{{ URL::to('/') }}/home/daily_teacher_record/view" class="list-group-item"><span class="text">View</span><i class="fa fa-eye"></i></a>
				<a href="{{ URL::to('/') }}/home/daily-student-attendance" class="list-group-item"><span class="text">Attendance</span><i class="fa fa-list"></i></a>
			</div>
			@endif

			@if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '5'))
			<a href="#exam" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Exam Section</span> <i class="fa fa-user"></i></a>
			<div class="collapse" id="exam">
				<a href="{{ URL::to('/') }}/home/grade" class="list-group-item"><span class="text">Grade</span><i class="fa fa-list"></i></a>
				<a href="{{ URL::to('/') }}/home/exam" class="list-group-item"><span class="text">Exam</span><i class="fa fa-list"></i></a>
				<a href="{{ URL::to('/') }}/home/exam-detail/view" class="list-group-item"><span class="text">View</span><i class="fa fa-eye"></i></a>
			</div>
			@endif

			@if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '6'))
			<a href="#fee" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Account Section</span> <i class="fa fa-money"></i></a>
			<div class="collapse" id="fee">
				<a href="{{ URL::to('/') }}/home/feestructure" class="list-group-item"><span class="text">Fee Structure</span><i class="fa fa-money"></i></a>
				<a href="{{ URL::to('/') }}/home/fee" class="list-group-item"><span class="text">Fee</span><i class="fa fa-money"></i></a>
				<a href="{{ URL::to('/') }}/home/fee-detail/view" class="list-group-item"><span class="text">Fee View</span><i class="fa fa-eye"></i></a>
				<a href="{{ URL::to('/') }}/home/salary" class="list-group-item"><span class="text">Salary</span><i class="fa fa-money"></i></a>
				<a href="{{ URL::to('/') }}/home/salary-detail/view" class="list-group-item"><span class="text">Salary View</span><i class="fa fa-money"></i></a>
				<a href="{{ URL::to('/') }}/home/miscellaneous" class="list-group-item"><span class="text">Miscellaneous</span><i class="fa fa-money"></i></a>
			</div>
			@endif

			@if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '7'))
			<a href="#notices" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Notice Section</span> <i class="fa fa-sticky-note"></i></a>
			<div class="collapse" id="notices">
				<a href="{{ URL::to('/') }}/home/notice" class="list-group-item"><span class="text">Notice</span><i class="fa fa-sticky-note-o"></i></a>
			</div>
			@endif

			@if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '8'))
			<a href="#event" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Event</span> <i class="fa fa-calendar-plus-o"></i></a>
			<div class="collapse" id="event">
				<a href="{{ URL::to('/') }}/home/event_view" class="list-group-item"><span class="text">Add Event</span><i class="fa fa-calendar-plus-o"></i></a>
			</div>
			@endif

			<a href="#cal" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Calendar</span> <i class="fa fa-calendar-o" aria-hidden="true"></i></a>
			<div class="collapse" id="cal">
				<a href="{{ URL::to('/') }}/home/event" class="list-group-item"><span class="text">View Calendar</span><i class="fa fa-calendar-o"></i></a>
			</div>

			@if(($userlevel->getUserLevel->user_level == '1'))
			<a href="#role" class="list-group-item list-group-item-info active" data-toggle="collapse" data-parent="#AdminMenu"><span class="text">Role</span> <i class="fa fa-user"></i></a>
			<div class="collapse" id="role">
				<a href="{{ URL::to('/') }}/home/role" class="list-group-item"><span class="text">View Role</span><i class="fa fa-list"></i></a>
			</div>
			@endif

			@endforeach
		</div>
	</div>
</div>
