<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
//header("Content-type: application/octet-stream;charset=utf-8");
header("Content-Type: text/html; charset=UTF-8");
header("Content-Disposition: attachment; filename=TeacherRecord.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table class="table table-bordered" border="1">
    <thead> 
      <tr>
          <th>SN</th>
          <th>Full Name</th>
          <th>Address</th>
          <th>Email</th>
          <th>DOB</th>
          <th>Gender</th>
          <th>T No</th>
          <th>M No</th>
          <th>First Class</th>
          <th>Other Class</th>
          <th>Created By</th>
          <th>Created On</th>
      </tr>
    </thead>
    <tbody>
        @foreach($teachers as $index=>$teacher)
        <tr>
       <td>{{$index+1}}</td>
       <td>{{$teacher->name}}</td>
       <td>{{$teacher->address}}</td>
       <td>{{$teacher->email}}</td>
       <td>{{$teacher->dob}}</td>
       <td>{{$teacher->gender}}</td>
       <td>{{$teacher->telephone_no}}</td>
       <td>{{$teacher->mobile_no}}</td>
       <td>{{$teacher->getTeacherFirstClass->getTeacherFirstSubject->name}}({{$teacher->getTeacherFirstClass->getTeacherFirstCls->name}}), {{$teacher->getTeacherFirstClass->getTeacherFirstSection->name}}, {{$teacher->getTeacherFirstClass->getTeacherFirstShift->name}}</td>
       <td>
          @foreach($teacher->getTeacherInfo()->get() as $subjects)
                  {{$subjects->getAllSubject->name}}({{$subjects->getAllSubject->subjectclass->name}}),
          @endforeach
       </td>
       <td>{{$teacher->getUser->name}}</td>
       <td>{{$teacher->created_at}}</td>
              
         </tr>
        @endforeach
    </tbody>
  </table>