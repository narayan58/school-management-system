@extends('admin.app')
@section('content')
@include('admin.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '4'))
  <div class="heading"><h2>Daily Teacher Record Summary</h2></div>
  <form class="general-form" id="class_valid" action="{{ URL::to('/') }}/home/daily_teacher_record/view"  method="GET">
  <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">

  <div class="form-group col-sm-4">
    <div class="required">
      <div class="input-group date" data-provide="datepicker" data-date-end-date="0d">
          <input data-type='to_date' type="text" id="search_date" name="subtopic_date" class="form-control input-sm to-date datepicker searchKeys required" value="<?= (isset($from_date))?$from_date:''?>"  placeholder="Date">
            <div class="input-group-addon">
               <span class="fa fa-calendar"></span>
            </div>
      </div>
    </div>
  </div>
  <div class="row form-group ">
    <div class="col-md-4">
          <button type="button" class="btn btn-default btn-sm" id='search'>
            Search<i class="fa fa-paper-plane-o"></i>
          </button>
    </div>
  </div>
  </form>
  <div class="col-md-12">
    <div id="printDiv">
    <div class="row" id="replaceTable">
    <table class="table table-bordered">
      <thead> 
        <tr>
            <th>SN</th>
            <th>Teacher Name</th>
            <th>Shift</th>
            <th>Section</th>
            <th>Period</th>
            <th>Subject</th>
            <th>Class</th>
            <th>Topic</th>
            <th>Subtopic</th>
            <th>Date</th>
            <th>Created By</th>
            <th>Created On</th>
            <th class="hidden-print">Action</th>
        </tr>
      </thead>
      <tbody>
          @foreach($teacher_records as $index=>$teacher_record)
        <tr>
         <td>{{$index+1}}</td>
         <td>{{$teacher_record->getTeacher->name}}</td>
         <td>{{$teacher_record->getShift->name}}</td>
         <td>{{$teacher_record->getSection->name}}</td>
         <td>{{$teacher_record->getPeriod->name}}</td>
         <td>{{$teacher_record->getSubject->name}}</td>
         <td>{{$teacher_record->getClass->name}}</td>
         <td>{{$teacher_record->getTopic->name}}</td>
         <td>{{$teacher_record->getSubTopic->name}}</td>
         <td>{{$teacher_record->lecture_date}}</td>
         <td>{{$teacher_record->getUser->name}}</td>
         <td>{{$teacher_record->created_at}}</td>
         <td class="hidden-print">
          <a class="action-btn bg-green delete" id="teacher_record" href="javascript:void(0)"><i class="fa fa-times" id="{{$teacher_record->id}}"></i></a>
         </td>
        </tr>
          @endforeach
      </tbody>
    </table>
    </div>
    </div>
    @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
  </div>
</div>

<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="{{ URL::to('/') }}/js/datepicker.js"></script> 
<script type="text/javascript">
    document.addEventListener('keypress', function(event) {
                 if (event.keyCode == 13) {
                     event.preventDefault();
                     $("#search").click();
                 }
             });
        $('#search').click(function () {
        $('#loader').show();

    var searchParams = {};
    var base_url = '<?php echo url();?>/home/daily_teacher_record/teacher_search';
    searchParams['search_date']=$('#search_date').val();
    var token = $('.token').val();
    $.ajax({
        type: 'get',
        data: 'parameters= ' + JSON.stringify(searchParams) + '&_token=' + token,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
                    $('#loader').hide('slow');

        }
    });
    return false;
});
</script>   
@endsection
