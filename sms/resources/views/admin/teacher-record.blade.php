@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
    @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '4'))
	<div class="heading"><h2>Teacher Daily Record</h2></div>
    <div id="wrapper"> 
    <form class="general-form teacher-record" id="daily_record_add" action="{{ URL::to('/') }}/home/daily-teacher-record/store" method="POST">
    <input type="hidden" name="_token" class="daily_record token" value="{{ csrf_token() }}">
    <p class="add-info">* Denotes required Field</p> 
            <div class="row">
                <div class="col-md-3">
                    <label for="search-byteacher" class="control-label">Select Teacher</label>
                    <div class="required">
                    <select class="form-control input-sm daily_record_teacher" id="drop-teacher" name="teacher_id">
                        <option value="">Please Select</option>
                        @foreach($teachers as $teacher)
                        <option value="{{$teacher->id}}">{{$teacher->name}}</option>
                        @endforeach
                    </select>
                    </div>
                </div>
                <div class="form-group col-sm-3">
                    <label for="date"class="control-label">Date</label>
                    <div class="required">
                    <div class="input-group date" data-provide="datepicker" data-date-end-date="0d">
                        <input data-type='to_date' type="text" id="from_date" name="subtopic_date" class="form-control input-sm to-date datepicker searchKeys required" value="<?= (isset($from_date))?$from_date:''?>"  placeholder="Date">
                        <div class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="panel entry-panel">
                <div class="panel-body">
                    <div id="entry-table">
                        <div class="row main-entry">
                            <div class="form-group col-sm-1">
                                <label for="period"class="control-label">Period</label>
                                <div class="required">
                                <select  class="form-control input-sm" id="cboPeriod0" name="period_id[0]" required >
                                    <option value="">Please Select</option>
                                    @foreach($periods as $period)
                                    <option value="{{$period->id}}">{{$period->name}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="form-group col-sm-2">
                                <label for="section"class="control-label">Subject</label>
                                <div class="required">
                                <select  class="form-control input-sm center daily_record_subject" id="subject-name0" name="subject_id[0]" required >
                                    <option value="">Please Select</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group col-sm-2">
                                <label for="section"class="control-label">Class</label>
                                <div class="required">
                                <select  class="form-control input-sm center drop-class-name" id="class-name0" name="class_id[0]" required >
                                    <option value="">Please Select</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group col-sm-1">
                                <label for="section"class="control-label">Section</label>
                                <div class="required">
                                <select  class="form-control input-sm center drop-section-name" id="drop-section-name0" name="section_id[0]" required >
                                    <option value="">Please Select</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group col-sm-1">
                                <label for="shift"class="control-label">Shift</label>
                                <div class="required">
                                <select  class="form-control input-sm drop-shift-name" name="shift_id[0]" id="cboShift0" required>
                                    <option value="">Please Select</option>
                                </select> 
                                </div>
                            </div>
                            <div class="form-group col-sm-2">
                                <label for="topic-name"class="control-label">Topic</label>
                                <div class="required">
                                <select class="form-control input-sm daily_record_topic" id="topic-name0" name="topic_id[0]" required >
                                    <option value="">Please Select</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group col-sm-2">
                                <label for="subtopic-name"class="control-label">Sub Topic</label>
                                <div class="required">
                                <select class="form-control input-sm daily_record_subtopic"  id="subtopic-name0" name="subtopic_id[0]" required >
                                <option value="">Please Select</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group col-sm-1">
                                <div>
                                    <input type="hidden" title="Enter valid hour" value="1" class="form-control input-sm" readonly id="spent-hour0" name="subtopic_hour[0]" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button id="add-more-entry" class="btn btn-default btn-sm button pull-right"><i class="fa fa-paper-plane-o"></i>  Add More</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group last-row row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-default btn-sm button pull-right">Save</button>
                </div>
            </div>
    </form>         
    </div>
     @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
    <script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
    <script src="{{ URL::to('/') }}/js/datepicker.js"></script>
    <?php 
    $period_option='';
    ?>
    @foreach($periods as $period)
    <?php
        $period_option.="<option value={$period->id}>{$period->period_name}</option>";
    ?>
    @endforeach
    <?php 
    $shift_option='';
    ?>
    @foreach($shiftes as $shift)
    <?php
        $shift_option.="<option value={$shift->id}>{$shift->shift_name}</option>";
    ?>
    @endforeach
    <script type="text/javascript">
    $(document).ready(function() {
        var max_fields      = 12; 
        var wrapper         = $("#entry-table"); 
        var add_button      = $("#add-more-entry"); 
            var x = 0;
            add_button.click(function(e){ 
                e.preventDefault();
                if(x < max_fields){
                     x++;
                     var period_option='<?php echo $period_option; ?>';
                     var shift_option='<?php echo $shift_option; ?>';
                     var $cloned = $(".main-entry:first").clone();
                     $cloned.find("#drop-section-name0").attr("name","section_id[" + x+"]");
                     $cloned.find("#drop-section-name0").attr("id","drop-section-name" + x);
                     $cloned.find("#cboShift0").attr("name","shift_id[" + x+"]");
                     $cloned.find("#cboShift0").attr("id","cboShift" + x);
                     $cloned.find("#cboPeriod0").attr("name","period_id[" + x+"]");
                     $cloned.find("#cboPeriod0").attr("id","cboPeriod" + x);
                     $cloned.find(".drop-class-name").attr("name","class_id[" + x+"]");
                     $cloned.find(".drop-class-name").attr("id","class-name" + x);
                     $cloned.find(".daily_record_subject").attr("name","subject_id[" + x+"]");
                     $cloned.find(".daily_record_subject").attr("id","subject-name" + x);
                     $cloned.find(".daily_record_topic").attr("name","topic_id[" + x+"]");
                     $cloned.find(".daily_record_topic").attr("id","topic-name" + x);
                     $cloned.find(".daily_record_subtopic").attr("name","subtopic_id[" + x+"]");
                     $cloned.find(".daily_record_subtopic").attr("id","subtopic-name" + x);
                     $cloned.find("#spent-hour0").attr("name","subtopic_hour[" + x+"]");
                     $cloned.find("#spent-hour0").attr("id","spent-hour" + x);
                     $cloned.append('<a href="#" class="remove_field"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>');
                     wrapper.append($cloned);

                     $('#subject-name'+x).change(function(event){
                        var str = $(event.target).attr('id');
                        if(str){
                            x= str.split('subject-name');
                            x= x[1];
                        }
                        var sub_id = $('#subject-name'+x).val();
                        $.ajax({
                            type:"GET",
                            dataType:"JSON",
                            url: "getTopic/"+sub_id,
                            success:function(e){
                                $('#topic-name'+x).html('');
                                $('#topic-name'+x).append('<option>Please Select</option>');
                                $.each( e, function( i, val ) {
                                    $('#topic-name'+x).append('<option value='+val.id+'>'+val.name+'</option>');
                                });
                            }
                        })
                    });

                     $('#subject-name'+x).change(function(event){
                        var str = $(event.target).attr('id');
                        if(str){
                            x= str.split('subject-name');
                            x= x[1];
                        }
                        var sub_id = $('#subject-name'+x).val();
                        $.ajax({
                            type:"GET",
                            dataType:"JSON",
                            url: "getClass/"+sub_id,
                            success:function(e){
                                $('#class-name'+x).html('');
                                $('#class-name'+x).append('<option>Please Select</option>');
                                $.each( e, function( i, val ) {
                                    $('#class-name'+x).append('<option value='+val.id+'>'+val.name+'</option>');
                                });
                            }
                        })
                    });


                     $('#topic-name'+x).change(function(event){
                        var str = $(event.target).attr('id');
                        if(str){
                            x= str.split('topic-name');
                            x= x[1];
                        }
                        var topic_id = $('#topic-name'+x).val();
                        $.ajax({
                            type:"GET",
                            dataType:"JSON",
                            url: "getSubTopic/"+topic_id,
                            success:function(e){
                                $('#subtopic-name'+x).html('');
                                $('#subtopic-name'+x).append('<option>Please Select</option>');
                                $.each( e, function( i, val ) {
                                    $('#subtopic-name'+x).append('<option value='+val.id+'>'+val.name+'</option>');
                                });
                            }
                        })
                    });
                }
                 else
                alert("only max 12 entries");
            });
            wrapper.on("click",".remove_field", function(e){
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
    </script>
    <script type="text/javascript">
    // filter for teacher to get subject
    $('#drop-teacher').change(function(event){
        var teacher_id = $('.daily_record_teacher').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getSubject/"+teacher_id,
            success:function(e){
                debugger;
                $('.daily_record_subject').html('');
                $('.daily_record_subject').append('<option>Please Select</option>');
                $.each(e, function( i, val) {
                    if(val != ""){
                        $('.daily_record_subject').append('<option value='+val.get_all_subject.id+'>'+val.get_all_subject.name+'</option>');
                    }
                });
            }
        })
    });
    </script>
    <script type="text/javascript">
    // filter for teacher to get subject
    $('.daily_record_subject').change(function(event){
        var teacher_id = $('.daily_record_subject').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getClass/"+teacher_id,
            success:function(e){
                $('#class-name0').html('');
                $('#class-name0').append('<option>Please Select</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#class-name0').append('<option value='+val.id+'>'+val.name+'</option>');
                    }
                });
            }
        })
    });
    </script>
    <script type="text/javascript">
    // filter for teacher to get section
    $('.drop-class-name').change(function(event){
        var teacher_id = $('.drop-class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getSection/"+teacher_id,
            success:function(e){
                $('#drop-section-name0').html('');
                $('#drop-section-name0').append('<option>Please Select</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#drop-section-name0').append('<option value='+val.get_section_class.id+'>'+val.get_section_class.name+'</option>');
                    }
                });
            }
        })
    });
    </script>
    <script type="text/javascript">
    // filter for teacher to get shift
    $('.drop-class-name').change(function(event){
        var teacher_id = $('.drop-class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getShift/"+teacher_id,
            success:function(e){
                $('#cboShift0').html('');
                $('#cboShift0').append('<option>Please Select</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#cboShift0').append('<option value='+val.get_shift_class.id+'>'+val.get_shift_class.name+'</option>');
                    }
                });
            }
        })
    });
    </script>
    <script type="text/javascript">
    // filter for teacher to get topic
    $('.daily_record_subject').change(function(event){
        var str = $(event.target).attr('id');
        if(str != 'subject-name'){
            x= str.split('subject-name');
            x= x[1];
            var topic_id = $('#subject-name'+x).val();
        }
        else {
            var topic_id = $('#subject-name').val();
            x = '';
        }
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getTopic/"+topic_id,
            success:function(e){
                if(x!=""){
                    $('#topic-name'+x).html('');
                    $('#topic-name'+x).append('<option>Please Select</option>');
                    $.each( e, function( i, val ) {
                        $('#topic-name'+x).append('<option value='+val.id+'>'+val.name+'</option>');
                    });                     
                }
                else{
                    $('#topic-name').html('');
                    $('#topic-name').append('<option>Please Select</option>');
                    $.each( e, function( i, val ) {
                        $('#topic-name').append('<option value='+val.id+'>'+val.name+'</option>');
                    });
                }
            }
        })
    });
    </script>
    <script type="text/javascript">
    // filter for topic to get subtopic
    $('.daily_record_topic').change(function(event){
        var str = $(event.target).attr('id');
        if(str != 'topic-name'){
            x= str.split('topic-name');
            x= x[1];
            var topic_id = $('#topic-name'+x).val();
        }
        else {
            var topic_id = $('#topic-name').val();
            x = '';
        }
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: "getSubTopic/"+topic_id,
            success:function(e){
                if(x!=""){
                    $('#subtopic-name'+x).html('');
                    $('#subtopic-name'+x).append('<option>Please Select</option>');
                    $.each( e, function( i, val ) {
                        $('#subtopic-name'+x).append('<option value='+val.id+'>'+val.name+'</option>');
                    });                     
                }
                else{
                    $('#subtopic-name').html('');
                    $('#subtopic-name').append('<option>Please Select</option>');
                    $.each( e, function( i, val ) {
                        $('#subtopic-name').append('<option value='+val.id+'>'+val.name+'</option>');
                    });
                }
            }
        })
    });
    </script>
@endsection
