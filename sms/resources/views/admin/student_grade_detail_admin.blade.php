@extends('admin.app')
@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
	<div class="heading"><h2>Marksheet Summary</h2></div>
	<div class="row" >
	<div class="col-md-8">
	     <div id="printDiv">
    <p>Student Name: <b>{{$student_name}}</b><span class="pull-right"> Class:{{$class_name}}</span></p>
     <p>Section:{{$class_section}} <span class="pull-right">Shift:{{$class_shift}}</span></p>
     <p><span class="pull-center">Exam: {{$exam_name}}</span></p>
    <table class="table table-bordered" >
    <thead> 
      <tr>
          <th>SN</th>
          <th>Subject</th>
          <th>Grade</th>
      </tr>
    </thead>
    <div class="watermark">{{$message}}</div>
    @foreach($studentmarksheets as $index=>$studentmarksheet)
          <tr>
             <td>{{$index+1}}</td>
             	@foreach($studentmarksheet->getStudentSubMark()->get() as $studentmark)
             <td>
              		{{$studentmark->getSubjectClass->name}}
             </td>
            	@endforeach
              @foreach($studentmarksheet->getStudentGrade()->get() as $sub_grade)
              <td>{{$sub_grade->getStudentMarkGrade->name}}</td>
              @endforeach

            </tr>

    @endforeach
 	</table>
 	</div>
  <div class="row">
    <div class="col-md-12 text-right container-pd">
      <button onclick="PrintDiv('printDiv')" id="print" class="btn btn-default btn-sm">Print</button>
    </div>              
  </div>
 	</div>
	</div>
</div>
@endsection
