@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '2'))
	<div class="heading"><h2>Class {{$class_name}} Subject Summary</h2></div>
      <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#mySubject">
      Add Subject on {{$class_name}}
      </button>
      <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/subject_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button>
      <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
      <div class="row">
      <div class="col-md-12">
      <div id="printDiv">
      <table class="table table-bordered">
      <thead> 
            <tr>
                <th>SN</th>
                <th>Subject</th>
                <th>Subject hour</th>
                <th class="hidden-print">Action</th>
           </tr>
      </thead>
      <tbody>
          @foreach($subjects as $index=>$subject)
          <tr>
             <td>{{$index+1}}</td>
             <td><a href="{{URL::to('/')}}/home/class/{{$slug}}/{{$subject->slug}}">{{$subject->name}}</a></td>
             <td>{{$subject->subject_hour}}</td>
             <td class="hidden-print">
              <a class="action-btn edit" id="subject/esub" data-target="#edit_subject" data-toggle="modal" href=""><i class="fa fa-pencil-square-o" id="{{$subject->id}}"></i></a>
              <a class="action-btn bg-green delete" id="subject" href="javascript:void(0)"><i class="fa fa-times" id="{{$subject->id}}"></i></a>
             </td>
          </tr>
          @endforeach
      </tbody>
      </table>
      </div>
      </div>
  </div>

    
    <div id="mySubject" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form add-subject" id="subject_valid" action="{{ URL::route('home.subject.store') }}"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="class_id" value="{{$class_id}}">
        <h2>Add Subject on {{$class_name}}</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label  for="name" class="col-md-4 control-label">Subject Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="name" name="name" placeholder="Please enter subject name">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label  for="subject_hour" class="col-md-4 control-label">Subject hour:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm" id="subject_hour" name="subject_hour" placeholder="Please enter subject hour">
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Add Subject<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>


   <div id="edit_subject" class="modal fade admin-model" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-body">
    <div class="form-holder">
      <div class="row">
        <div class="col-xs-12 form-box">
        <form class="general-form edit-form" id="subject_valid" action="{{ URL::to('/') }}/home/subject/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <input type="hidden" name="class_id" value="{{$class_id}}">
        <input type="hidden" name="subject_id" class="id">
        <h2>Update Subject on {{$class_name}}</h2>
        <p class="add-info">* Denotes required Field</p>
        <div class="row">
          <label  for="name" class="col-md-4 control-label">Subject Name:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm name" id="name" name="name" placeholder="Please enter subject name">
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label  for="subject_hour" class="col-md-4 control-label">Subject hour:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
              <input type="text" class="form-control input-sm subject_hour" id="subject_hour" name="subject_hour" placeholder="Please enter subject hour">
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Update Subject<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>

    @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>


@endsection
