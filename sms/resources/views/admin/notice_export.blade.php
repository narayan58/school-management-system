<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
 
//header("Content-type: application/octet-stream;charset=utf-8");
header("Content-Type: text/html; charset=UTF-8");
header("Content-Disposition: attachment; filename=NoticeRecord.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<table class="table table-bordered" border="1">
    <thead> 
        <tr>
            <th>SN</th>
            <th>Title</th>
            <th>Notice</th>
            <th>Class</th>
        </tr>
    </thead>
    <tbody>
          @foreach($notices as $index=>$notice)
            <tr>
               <td>{{$index+1}}</td>
               <td>{{$notice->name}}</td>
               <td>{!! $notice->title !!}</td>
               <td>
                @foreach($notice->getNotice()->get() as $notices)
                 <button type="button" class="btn btn-info btn-sm">
                {{$notices->getClass->name}}
              </button>
                @endforeach
                </td>
            </tr>
          @endforeach 
    </tbody>
    </table>