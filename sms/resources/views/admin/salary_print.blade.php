@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div id="mainMan">
<div class="admin-content">
	<div class="admin-top">
		<div class="flash-message">
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			    @if(Session::has('alert-' . $msg))
			<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			  	@endif
			@endforeach
		</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
		@foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '6'))
		<div class="heading"><h2>Salary Sheet</h2></div>
	            <div id="printDiv">

		<div class="row">
			<div class="col-sm-8">
				<div class="print-section general-form form-box padding-sm" id="bill">
					<div class="heading bg-blue">
						<h2 class="text-center">Salary Sheet</h2>
					</div>
					<div class="col-md-12">
    				<div class="row">
					<table class="table table-default">
						<tbody>
							@foreach($salary_details as $salarydetail)
							<tr>
								<tr>
									<td>Teacher Name: {{$salarydetail->teacherfeename->name}}</td>
									<td>Received by: {{$salarydetail->teacherfeecreate->name}}</td>
									<td></td>
									<td>Salary month : {{$salarydetail->getMonth->name}}</td>
								</tr>
								<div class="watermark">{{$message}}</div>
								<tr>
									<td>Date:{{$salarydetail->created_at}}</td>
									<td>System Code: {{$salarydetail->bill_id}}</td>
									<td></td>
									<td>Bill ID:{{$salarydetail->teacherfeebillSn->id}} </td>
								</tr>
								<tr></tr>
							</tr>
							<tr class="info">
								<th>Earnings</th>
								<th></th>
								<th></th>
								<th>Deduction</th>
							</tr>
							<tr class="success">
								<th>Item(s)</th>
								<th>Amount</th>
								<th>Item(s)</th>
								<th>Amount</th>
							</tr>
							@foreach($salarydetail->teacherfeedetail()->get() as $index=>$student)
								@if($student->status == 'add')
							<tr  class="active">
								<td>{{$student->description}}</td>
								<td>{{number_format($student->amount, 2) }}</td>
								@else
								<td>{{$student->description}}</td>
								<td>{{number_format($student->amount, 2) }}</td>
							</tr>
								@endif
							@endforeach
							
							<tr class="text-right">
								<td></td>
								<td></td>
								<td></td>
								<td>Grand Total: {{number_format($salarydetail->grand_total, 2) }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				</div>				
	        <button type="button" class="btn btn-default btn-sm hidden-print" onclick="PrintDiv('printDiv')" style=" margin-bottom: 2px;" >PRINT<i class="fa fa-print"></i></button>
			</div>	
		</div>
		</div>
		@else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
</div>
@endsection