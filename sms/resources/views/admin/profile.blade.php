@extends('admin.app')

@section('content')
@include('admin.sidebar')
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '2'))
	<div class="heading"><h2>Password</h2></div>
  <div class="form-holder">
      <div class="row">
        <div class="col-sm-6">
          <div class="heading bg-blue">
            <h2 class="text-center">Change Password</h2>
          </div>
        <form class="general-form form-box padding-sm" id="class_valid" action="{{ URL::to('/') }}/home/change_password/update"  method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
        <div class="row">
          <label class="col-md-4 control-label">Old Password:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="password" class="form-control input-sm" id="old_password" name="old_password" placeholder="Please enter old Password" >
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-md-4 control-label">New Password:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="password" class="form-control input-sm" id="new_password" name="new_password" placeholder="Please enter new Password" >
            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <label class="col-md-4 control-label">Confirm Password:</label>
            <div class="col-md-8">
            <div class="form-group">
            <div class="required">
            <input type="password" class="form-control input-sm" id="confirm_password" name="confirm_password" placeholder="Please enter confirm Password" >
            </div>
            </div>
          </div>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-default btn-sm">
            Change Password<i class="fa fa-paper-plane-o"></i>
          </button>
        </div>
        </form>
        </div>
      </div>
      </div>
    @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>

    <script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $('.editclas').click(function(event){
    var id=$(event.target).attr("id"),
    t=$(event.target).parent().attr("id");

    url=t+"/edit/"+id,
    $.ajax({
      type:"GET",
      dataType:"JSON",
      url:url,
      success:function(response){
        $.each( response, function( key, value ) {
          $('.edit-form .'+key).val(value);
        });
      }
    });
  });
  </script>
@endsection
