<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Dolphin Purwanchal Academy $ Sagarmatha College</title>
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,700italic,400italic%7cOswald:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{{URL::to('/')}}/css/bootstrap.css">
	<link rel="stylesheet" href="{{ URL::to('/') }}/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{URL::to('/')}}/css/all.css">
</head>
<body>
	<div id="wrapper">
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.6";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
<!-- 		<div id="preloader" class="cssload-container">
			<div class="cssload-whirlpool"></div>
		</div> -->
		<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
		<header id="header">
			<div class="header-holder">
				<div class="container">
					<div class="row text-right">
						<div class="col-md-9 col-md-offset-3">
							<ul class="header-list list-inline">
								<li><i class="fa fa-envelope"></i><a href="mailto:dolphineducationdharan@mail.com"> dolphineducationdharan@mail.com</a></li>
								<li><i class="fa fa-map-marker"></i>Dharan-3, Sadan Road</li>
								<li>
									<button class="btn-default btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Signin <i class="fa fa-sign-in"></i></button>
									<div class="dropdown-menu login-holder">
										<form role="form" class="login-form" action="{{ url('/auth/login') }}" method="POST">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<div class="form-group">
												<strong class="title test-center">User Login</strong>
												<label for="email">Email address:</label>
												<input type="text" class="form-control input-sm" name="email" placeholder="Please provide a email" id="email">
											</div>
											<div class="form-group">
												<label for="pwd">Password:</label>
												<input type="password" class="form-control input-sm" name="password" placeholder="Please provide a password" id="pwd">
											</div>
											<div class="checkbox">
												<label><input type="checkbox" name="remember"> Remember me</label>
											</div>
											<button type="submit" class="btn btn-default btn-sm">Submit</button>
										</form>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<nav id="mainNav" class="navbar navbar-default ">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand page-scroll" href="{{URL::to('/')}}"><img class="img-responsive" src="images/logo.png" alt="company name" style="height: 85px;"></a>
					</div>
					<div class="collapse navbar-collapse" id="nav">
						<ul class="nav navbar-nav navbar-right">
							<li class="active"><a class="page-scroll" href="#header">Home</a></li>
							<li><a class="page-scroll" href="#about">About</a></li>
							<li><a class="page-scroll" href="#features">Features</a></li>
							<li><a class="page-scroll" href="#testimonial">Testimonial</a></li>
							<li><a class="page-scroll" href="#contact">Contact</a></li>
							
						</ul>
					</div>
				</div>
			</nav>
		</header>
		<main id="main" role="main">
			<div class="banner">
				<ul class="rslides">
					<li>
						<div class="bg-stretch">
							<img src="images/slider/slide1.jpg" alt="image description">
							<div class="caption">
								<h1>Welcome To Dolphin Purwanchal Dharan.</h1>
								<p>Quality education for all</p>
							</div>
						</div>
					</li>
					<li>
						<div class="bg-stretch">
							<img src="images/slider/slide2.jpg" alt="image description">
							<div class="caption">
								<h1>Welcome To Dolphin Purwanchal Dharan.</h1>
								<p>Quality education for all</p>
							</div>
						</div>
					</li>
					<li>
						<div class="bg-stretch">
							<img src="images/slider/slide3.jpg" alt="image description">
							<div class="caption">
								<h1>Welcome To Dolphin Purwanchal Dharan.</h1>
								<p>Quality education for all</p>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<section id="about" class="section">
				<div class="container text-center">
					<div class="row">
						<div class="col-md-12">
							<h2>Some word from Principal</h2>
							<img class="img-responsive align-center" src="images/details/principal.jpg" alt="image description">
							<p>शिक्षालाई पैसामा किनबेच नगरी गुणस्तारिया शिक्षा प्रबाह बनाउनु पर्दछ / समय अनुसार शिक्षालाई बिद्धार्थी समक्ष पुराउन सक्नु नै अहिलेको आवसक्ता हो /
								Theoritical भन्दा Practical प्रकियाबाट शिक्षालाई परिवर्तन गर्नु नै आजको आवसक्ता हो र त्येही आवसक्तालाई यस डल्फिन पूर्वान्चल / सगरमाथा कलेजले 
								पूरा गर्दै आएको सर्बबिदितै छ / त्ससैले यस संस्थालाई सेवा गर्ने अवसर दिनु हुने हार्दिक अपिल गर्दछु /</p>
						</div>
						<!-- <div class="col-md-6">
							<h2>Few words from Chairman</h2>
							<img class="img-responsive align-center" src="images/details/img1.jpg" alt="image description">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis iusto ex veritatis laboriosam distinctio eos vel dicta pariatur rerum mollitia a, nisi repellendus sit fugit, voluptatibus et, labore, amet! Aliquam!</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, deleniti, eum. Maiores explicabo praesentium itaque modi tempore totam autem reprehenderit esse quod ullam illo dignissimos magni aliquid velit, optio nisi.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, omnis, nihil? Pariatur mollitia cumque minima cupiditate, ex voluptates nemo illo doloribus odit, minus numquam. Dolore velit veritatis consequatur laudantium numquam?</p>
						</div> -->
					</div>
				</div>
			</section>
			<section id="features" class="section">
				<div class="container text-center">
					<h2>Extra Activity Glance</h2>
					<div class="row">
						<div class="col-md-3">
							<div class="thumb-block">
								<img class="img-responsive" src="images/details/pic1.jpg" alt="image description">
								<div class="caption">
									<span class="name">Boys Basketball</span>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="thumb-block">
								<img class="img-responsive" src="images/details/pic2.jpg" alt="image description">
								<div class="caption">
									<span class="name">Boys Volleyball</span>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="thumb-block">
								<img class="img-responsive" src="images/details/pic3.jpg" alt="image description">
								<div class="caption">
									<span class="name">Girls Football</span>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="thumb-block">
								<img class="img-responsive" src="images/details/pic4.jpg" alt="image description">
								<div class="caption">
									<span class="name">Sports Week</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section id="testimonial" class="testimonial-block text-center section" style="background-image:url(images/slider/slide1.jpg)">
				<div class="container">
					<h2>Few words From our students.</h2>
					<div id="carousel-testimonial" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner testimonial">
							<div class="item active">
								<div class="unit sameheight">
									<span class="avatar">
										<img class="img-responsive img-circle" src="images/avatar/stu1.jpg" alt="image description">
									</span>
									<div class="text-wrap">
										<blockquote>
											<q>An amazing learning experience.An amazing learning experience.An amazing learning experience.An amazing learning experience.An amazing learning experience.</q>
											<cite>Himani Rai, 2072 Batch</cite>
										</blockquote>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="unit sameheight">
									<span class="avatar">
										<img class="img-responsive img-circle" src="images/avatar/stu2.jpg" alt="image description">
									</span>
									<div class="text-wrap">
										<blockquote>
											<q>An amazing learning experience.An amazing learning experience.An amazing learning experience.An amazing learning experience.An amazing learning experience.</q>
											<cite>Binita Rai, Dolphin Star, 2072 Batch</cite>
										</blockquote>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="unit sameheight">
									<span class="avatar">
										<img class="img-responsive img-circle" src="images/avatar/stu3.jpg" alt="image description">
									</span>
									<div class="text-wrap">
										<blockquote>
											<q>An amazing learning experience.An amazing learning experience.An amazing learning experience.An amazing learning experience.An amazing learning experience.</q>
											<cite>Sabina Rai, 2072 Batch</cite>
										</blockquote>
									</div>
								</div>
							</div>

						</div>
						<a class="left control" href="#carousel-testimonial" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a class="right control" href="#carousel-testimonial" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
				</div>
			</section>
			<section id="contact" class="section">
				<div class="container">
					<h2 class="text-center">Contact Us</h2>
					<div class="row">
						<div class="col-md-8">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3560.8973333145595!2d87.28670191430089!3d26.81139747090041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ef419beda152db%3A0x8268ad93205e4472!2sSadan+Road%2C+Dharan+56700!5e0!3m2!1sen!2snp!4v1497404834153" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
						<div class="col-md-4">
							<p>Feel free To contact us.<br>If you have any queries please feel free to reach us.</p>
							<address class="address contact-details">
								<strong class="title">Come Visit</strong>
								<p><i class="fa fa-pencil"></i>Dharan-3<span>Sadan Road</span> <span>Sunsari</span><span>Australia</span></p>
								<p><i class="fa fa-phone"></i>Phone: 025-533974, 9842041330, 9814330088 </p>
								<p><i class="fa fa-phone"></i>Mobile: 9842522057, 9817327190 </p>
							</address>
						</div>
					</div>
				</div>
			</section>
		</main>
		<div id="footer">
			<div class="footer-frame">
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							
							<h3>About us</h3>
							<ul class="list-unstyled">
								<li><a class="page-scroll" href="#header">Home</a></li>
								<li><a class="page-scroll" href="#about">About</a></li>
								<li><a class="page-scroll" href="#features">Features</a></li>
								<li><a class="page-scroll" href="#testimonial">Testimonial</a></li>
								<li><a class="page-scroll" href="#contact">Contact</a></li>
							</ul>
						</div>
						<div class="col-sm-4">
							<h3>Connect With Us</h3>
							<ul class="social-networks list-inline">
								<li><a href="https://www.facebook.com/Higher-Dolphin-Purwanchal-Academy-1848327915389509/" class="facebook"><i class="fa fa-2x fa-fw fa-facebook"></i></a></li>
							</ul>
						</div>
						<div class="col-sm-4">
							<h3>Like us on facebook.</h3>
							<div class="fb-page" data-href="https://www.facebook.com/Higher-Dolphin-Purwanchal-Academy-1848327915389509/" data-width="226px" data-small-header="true" data-adapt-container-width="false" data-hide-cover="true" data-show-facepile="false" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Higher-Dolphin-Purwanchal-Academy-1848327915389509/"><a href="https://www.facebook.com/Higher-Dolphin-Purwanchal-Academy-1848327915389509/">goal</a></blockquote></div></div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="footer-holder pull-left">
				<div class="container">
					<p>&copy; 2017. All right reserved. </p>
				</div>
			</div>
			<div class="footer-holder pull-right">
					<p>Developed By: <a href="http://www.techware.com.np" target="new">Techware Pvt Ltd</a></p>
			</div>
		</div>
	</div>
	<script src="{{URL::to('/')}}/js/jquery-1.11.1.min.js"></script>
	<script src="{{URL::to('/')}}/js/bootstrap.min.js"></script>
	<script src="{{URL::to('/')}}/js/responsiveslides.min.js"></script>
	<script src="{{URL::to('/')}}/js/owl.carousel.min.js"></script>
	<script src="{{URL::to('/')}}/js/jquery.main.js"></script>
</body>
</html>