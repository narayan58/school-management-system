@if($count_teacher_record>0)
    <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/report/teacher_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button>
    <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
<div id="printDiv">
<table class="table table-bordered">
      <thead>
        <tr>
            <th>SN</th>
            <th>Teacher Name</th>
            <th>Shift</th>
            <th>Section</th>
            <th>Period</th>
            <th>Subject</th>
            <th>Class</th>
            <th>Topic</th>
            <th>Subtopic</th>
            <th>Lecture Hour</th>
        </tr>
      </thead>
      <tbody>
      <?php $sn=1;
             $previous_class_id='';
             $previous_subject_id='';
             $total_lecture=0;
         ?>
          @foreach($teacher_records as $index=>$teacher_record)
          <?php
            $class_id=$teacher_record->class_id;
            ?>
            @if($class_id!=$previous_class_id && $previous_class_id!='')
            <tr class="text-right">
            <td colspan="9">
               <label> Total Lecture</label>
                </td>
                <td>
                <label>{{$total_lecture}} Hour</label>
                </td>
            </tr>
            <tr class="text-right">
                <td colspan="9">
                    <label> Assigned Lecture</label>
                </td>
                <td>
                    <label>{{$subject_hour[$previous_subject_id]}} Hour</label>
                </td>
            </tr>
            <?php
            $total_lecture=0;
            ?>
            @endif
        <tr>
         <td>{{$index+1}}</td>
         <td>{{$teacher_record->getTeacher->name}}</td>
         <td>{{$teacher_record->getShift->name}}</td>
         <td>{{$teacher_record->getSection->name}}</td>
         <td>{{$teacher_record->getPeriod->name}}</td>
         <td>{{$teacher_record->getSubject->name}}</td>
         <td>{{$teacher_record->getClass->name}}</td>
         <td>{{$teacher_record->getTopic->name}}</td>
         <td>{{$teacher_record->getSubTopic->name}}</td>
         <td>{{$teacher_record->added_lecture}}</td>
         <?php $total_lecture+=$teacher_record->added_lecture;?>

        </tr>
        <?php
            $previous_class_id=$teacher_record->class_id;
            $previous_subject_id=$teacher_record->subject_id;
            ?>
          @endforeach
          <tr class="text-right bold">
            <td colspan="9">
              <label>Total Lecture</label>
              </td>
              <td>
                <label>{{$total_lecture}} Hour</label>
              </td>
          </tr>
            <tr class="text-right">
                <td colspan="9">
                    <label> Assigned Lecture</label>
                </td>
                <td>
                    <label>{{$subject_hour[$previous_subject_id]}} Hour</label>
                </td>
            </tr>
      </tbody>
    </table>
    </div>
    @else
    <table>
    <div class="alert alert-danger">No result found!!!</div>

    </table>
    @endif