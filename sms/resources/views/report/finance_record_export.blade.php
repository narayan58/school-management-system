<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.

//header("Content-type: application/octet-stream;charset=utf-8");
header("Content-Type: text/html; charset=UTF-8");
header("Content-Disposition: attachment; filename=Finance_report.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table class="table table-bordered" border="1">
    <thead>
      <tr>
          <th>SN</th>
          <th>System ID</th>
          <th>Person</th>
          {{--<th>Topic</th>--}}
          <th>Amount</th>
          <th>Submited On</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $total_paid_amount=0;
    ?>
    <tr>
    <td colspan="2"><strong>Fee Record</strong></td>
    </tr>
    @foreach($fee_totals as $index=>$fee)
    <?php
    $total_paid_amount+=$fee->amount;
    ?>
          <tr>
             <td>{{$index+1}}</td>
             <td>{{$fee->bill_id}}</td>
             <td>{{$fee->studentfeename->name}}</td>
            {{--<td>{{$fee->description}}</td>--}}
            <td>{{$fee->amount}}</td>
             <td>{{$fee->created_at}}</td>
          </tr>
    @endforeach
    <tr>
        <td colspan="3">
        Total

        </td>
        <td>
        <strong>Rs. {{$total_paid_amount}}</strong>
        </td>
    </tr>
    <?php
        $total_paid_salary=0;
        ?>
    <tr>
        <td colspan="2"><strong>Salary Record</strong></td>
    </tr>
     @foreach($salary_totals as $index=>$salary)
        <?php
        $total_paid_salary+=$salary->amount;
        ?>
              <tr>
                 <td>{{$index+1}}</td>
                 <td>{{$salary->bill_id}}</td>
                 <td>{{$salary->studentfeename->name}}</td>
                {{--<td>{{$salary->description}}</td>--}}
                <td>{{$salary->amount}}</td>
                 <td>{{$salary->created_at}}</td>
              </tr>
        @endforeach
        <tr>
                <td colspan="3">
                Total

                </td>
                <td>
                <strong>Rs. {{$total_paid_salary}}</strong>
                </td>
            </tr>
                <?php
                    $total_miscellaneous=0;
                    ?>
            <tr>
                    <td colspan="2"><strong>Miscellaneous Record</strong></td>
                </tr>
                    <thead>
                      <tr>
                          <th>SN</th>
                          <th>Topic</th>
                          <th>Amount</th>
                          <th>Submited On</th>
                      </tr>
                    </thead>
                 @foreach($misc_totals as $index=>$misc)
                    <?php
                    $total_miscellaneous+=$misc->amount;
                    ?>
                          <tr>
                             <td>{{$index+1}}</td>
                             <td>{{$misc->name}}</td>
                            <td>{{$misc->amount}}</td>
                             <td>{{$misc->created_at}}</td>
                          </tr>
                    @endforeach
                    <tr>
                            <td colspan="2">
                            Total

                            </td>
                            <td>
                            <strong>Rs. {{$total_miscellaneous}}</strong>
                            </td>
                        </tr>
            <tr>
            <td colspan="4">
            <strong>Finance Summary</strong>
            </td>
            </tr>
            <tr>
                            <td colspan="3">
                            Total Income

                            </td>
                            <td>
                            <strong>Rs. {{$total_paid_amount}}</strong>
                            </td>
                        </tr>
                        <tr>
                                        <td colspan="3">
                                        Total Expenditure

                                        </td>
                                        <td>
                                        <strong>Rs. {{$total_paid_salary+$total_miscellaneous}}</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                                    <td colspan="3">
                                                    Available Balance

                                                    </td>
                                                    <td>
                                                    <strong>Rs. {{$total_paid_amount-$total_paid_salary-$total_miscellaneous}}</strong>
                                                    </td>
                                                </tr>
    </tbody>
  </table>
