<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.

//header("Content-type: application/octet-stream;charset=utf-8");
header("Content-Type: text/html; charset=UTF-8");
header("Content-Disposition: attachment; filename=StudentFee.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
    <table class="table table-bordered" border="1">
    <thead>
      <tr>
          <th>SN</th>
          <th>System ID</th>
          <th>Student</th>
          <th>Topic</th>
          <th>Amount</th>
          <th>Submited On</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $total_paid_amount=0;
    ?>
    @foreach($fee_totals as $index=>$fee)
    <?php
    $total_paid_amount+=$fee->amount;
    ?>
          <tr>
             <td>{{$index+1}}</td>
             <td>{{$fee->bill_id}}</td>
             <td>{{$fee->studentfeename->name}}</td>
            <td>{{$fee->description}}</td>
            <td>{{$fee->amount}}</td>
             <td>{{$fee->created_at}}</td>
          </tr>
    @endforeach
    <tr>
        <td colspan="4">
        Total

        </td>
        <td>
        <strong>Rs. {{$total_paid_amount}}</strong>
        </td>
    </tr>
    </tbody>
  </table>
