@extends('admin.app')
@section('content')
@include('admin.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
  <div class="admin-top">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
  </div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '6'))
  <div class="heading"><h2>Latest Fee</h2></div>

<div class="form-holder">
  <form class="general-form" id="exam_valid">
    <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <div class="required">
                    <select class="form-control input-sm class-name searchKeys" id="class_id" name="class_id">
                            <option value="">Select Class</option>
                            @foreach($class_list as $index=>$class)
                                <option value="{{$index}}">{{$class}}</option>
                            @endforeach

                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <div class="required">
                    <select class="form-control input-sm section-name searchKeys" id="section_id" name="section_id">
                        <option value="">Select Section</option>
                     </select>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <div class="required">
                    <select class="form-control input-sm shift-name searchKeys" id="shift_id" name="shift_id">
                        <option value="">Select Shift</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="required">
                    <select class="form-control input-sm student-name searchKeys" id="student_id" name="student_id">
                        <option value="">Select Student</option>
                    </select>
                </div>
            </div>
        </div>
    <div class="text-center">
    <button type="button" class="btn btn-default btn-sm" id="search">
      Search<i class="fa fa-paper-plane-o"></i>
    </button>
  </div>
  </div>
  </form>
</div>
<div class="col-md-12">
<div class="row" id="replaceTable">


</div>
    @else
  <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
    document.addEventListener('keypress', function(event) {
                 if (event.keyCode == 13) {
                     event.preventDefault();
                     $("#search").click();
                 }
             });
        $('#search').click(function () {
        $('#loader').show();

    var searchParams = {};
    var base_url = '<?php echo url();?>/home/report/getFee';
    searchParams['class_id']=$('#class_id').val();
    searchParams['section_id']=$('#section_id').val();
    searchParams['shift_id']=$('#shift_id').val();
    searchParams['student_id']=$('#student_id').val();

    var token = $('.token').val();
    $.ajax({
        type: 'get',
        data: 'parameters= ' + JSON.stringify(searchParams) + '&_token=' + token,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
                    $('#loader').hide('slow');
        }
    });
    return false;
});
</script>
<script src="{{ url('/') }}/js/jquery-2.1.3.min.js"></script>

<script type="text/javascript">
    // filter for teacher to get section
    $('.class-name').change(function(){
    $('#loader').show();
    var base_url = '<?php echo url();?>/home/';
        var teacher_id = $('.class-name').val();
        $.ajax({
            type:"GET",
            dataType:"JSON",
            url: base_url+"getSectionOnStudent/"+teacher_id,
            success:function(e){
                $('#section_id').html('');
                $('#section_id').append('<option value=""> Select Section</option>');
                $.each( e, function( i, val ) {
                    if(val != ""){
                        $('#section_id').append('<option value='+val.get_section_class.id+'>'+val.get_section_class.name+'</option>');
                    }
                });
                $('#loader').hide();
            }
        })
    });
    </script>
     <script type="text/javascript">
        // filter for teacher to get shift
        $('.class-name').change(function(){
         var base_url = '<?php echo url();?>/home/';
            var teacher_id = $('.class-name').val();
            $.ajax({
                type:"GET",
                dataType:"JSON",
                url: base_url+"getShiftOnStudent/"+teacher_id,
                success:function(e){
                    $('#shift_id').html('');
                    $('#shift_id').append('<option value=""> Select Shift</option>');
                    $('#student_id').html('');
                    $('#student_id').append('<option value=""> Select Student</option>');
                    $.each( e, function( i, val ) {
                        if(val != ""){
                            $('#shift_id').append('<option value='+val.get_shift_class.id+'>'+val.get_shift_class.name+'</option>');
                        }
                    });
                }
            })
        });
        </script>
        <script type="text/javascript">
        // filter get student list
        $('.shift-name').change(function(event){
        $('#loader').show();
         var base_url = '<?php echo url();?>/home/';
            var class_id = $('.class-name').val(),
                shift_id = $('.shift-name').val(),
                section_id = $('.section-name').val(),
                amount = $('.amount').val(),
                token =$('.token').val();
            $.ajax({
                type:"POST",
                dataType:"JSON",
                url:base_url+ "getStudents",
                data: {
                    _token: token,
                    class_id: class_id,
                    shift_id: shift_id,
                    section_id:section_id,
                },
                success:function(e){
                    $('#student_id').html('');
                    $('#student_id').append('<option value=""> Select Student</option>');
                    $.each( e, function( i, val ) {
                        if(val != ""){
                            $('#student_id').append('<option value='+val.studentinfo.id+'>'+val.studentinfo.name+'</option>');
                        }
                    });
                    $('#loader').hide();
                }
            })
        });
    </script>
@endsection
