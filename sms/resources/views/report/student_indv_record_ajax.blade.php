@if($record_count>0)
    <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/report/student_indv_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button>
    <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
<div id="printDiv">
    <table class="table table-bordered">
    <thead>
      <tr>
          <th>SN</th>
          <th>System ID</th>
          <th>Student</th>
          <th>Topic</th>
          <th>Amount</th>
          <th>Submited On</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $total_paid_amount=0;
    ?>
    @foreach($fee_totals as $index=>$fee)
    <?php
    $total_paid_amount+=$fee->amount;
    ?>
          <tr>
             <td>{{$index+1}}</td>
             <td>{{$fee->bill_id}}</td>

             <td>{{$fee->studentfeename->name}}</td>
            <td>{{$fee->description}}</td>
            <td>{{$fee->amount}}</td>
             <td>{{$fee->created_at}}</td>
          </tr>
    @endforeach
    <tr>
        <td colspan="4">
        Total

        </td>
        <td>
        <strong>Rs. {{$total_paid_amount}}</strong>
        </td>
    </tr>
    </tbody>
  </table>
  </div>

  @else
      <table>
      <div class="alert alert-danger">No result found!!!</div>

      </table>
      @endif