@if($fee_count>0 || $salary_count>0 || $misc_count>0)
    <button type="button" class="btn btn-default btn-sm" id="export" url="{{URL::to('/').'/home/report/finance_record_export'}}">EXPORT RECORD<i class="fa fa-paper-plane-o"></i></button>
    <button type="button" class="btn btn-default btn-sm" onclick="PrintDiv('printDiv')" >PRINT<i class="fa fa-print"></i></button>
<div id="printDiv">
    <table class="table table-bordered">
    <thead>
      <tr>
          <th>SN</th>
          <th>System ID</th>
          <th>Person</th>
          <th>Amount</th>
          <th>Submited On</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $total_paid_amount=0;
    ?>
    <tr>
    <td colspan="2"><strong>Fee Record</strong></td>
    </tr>
    @foreach($fee_totals as $index=>$fee)
    <?php
    $total_paid_amount+=$fee->grand_total;
    ?>
          <tr>
             <td>{{$index+1}}</td>
             <td><a href="{{URL::to('/')}}/home/fee-detail/view/{{$fee->bill_id}}" target="new">{{$fee->bill_id}}</a></td>
             <td>{{$fee->studentfeename->name}}</td>
            {{--<td>{{$fee->description}}</td>--}}
            <td>{{$fee->grand_total}} @if($fee->is_due==1)<span class="label label-warning">Due</span> @endif</td>
             <td>{{$fee->created_at}}</td>
          </tr>
    @endforeach
    <tr>
        <td colspan="3">
        Total

        </td>
        <td>
        <strong>Rs. {{$total_paid_amount}}</strong>
        </td>
    </tr>
    <?php
        $total_paid_salary=0;
        ?>
    <tr>
        <td colspan="2"><strong>Salary Record</strong></td>
    </tr>
     @foreach($salary_totals as $index=>$salary)
        <?php
        $total_paid_salary+=$salary->grand_total;
        ?>
              <tr>
                 <td>{{$index+1}}</td>
                 <td><a href="{{URL::to('/')}}/home/salary-detail/view/{{$salary->bill_id}}" target="new">{{$salary->bill_id}}</a></td>
                 <td>{{$salary->teacherfeename->name}}</td>
                {{--<td>{{$salary->description}}</td>--}}
                <td>{{$salary->grand_total}} </td>
                 <td>{{$salary->created_at}}</td>
              </tr>
        @endforeach
        <tr>
                <td colspan="3">
                Total

                </td>
                <td>
                <strong>Rs. {{$total_paid_salary}}</strong>
                </td>
            </tr>
                <?php
                    $total_miscellaneous=0;
                    ?>
            <tr>
                    <td colspan="2"><strong>Miscellaneous Record</strong></td>
                </tr>
                    <thead>
                      <tr>
                          <th>SN</th>
                          <th>Topic</th>
                          <th>Amount</th>
                          <th>Submited On</th>
                      </tr>
                    </thead>
                 @foreach($misc_totals as $index=>$misc)
                    <?php
                    $total_miscellaneous+=$misc->amount;
                    ?>
                          <tr>
                             <td>{{$index+1}}</td>
                             <td>{{$misc->name}}</td>
                            <td>{{$misc->amount}}</td>
                             <td>{{$misc->created_at}}</td>
                          </tr>
                    @endforeach
                    <tr>
                            <td colspan="2">
                            Total

                            </td>
                            <td>
                            <strong>Rs. {{$total_miscellaneous}}</strong>
                            </td>
                        </tr>
            <tr>
            <td colspan="4">
            <strong>Finance Summary</strong>
            </td>
            </tr>
            <tr>
                            <td colspan="3">
                            Total Income

                            </td>
                            <td>
                            <strong>Rs. {{$total_paid_amount}}</strong>
                            </td>
                        </tr>
                        <tr>
                                        <td colspan="3">
                                        Total Expenditure

                                        </td>
                                        <td>
                                        <strong>Rs. {{$total_paid_salary+$total_miscellaneous}}</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                                    <td colspan="3">
                                                    Available Balance

                                                    </td>
                                                    <td>
                                                    <strong>Rs. {{$total_paid_amount-$total_paid_salary-$total_miscellaneous}}</strong>
                                                    </td>
                                                </tr>
    </tbody>
  </table>
  </div>

  @else
      <table>
      <div class="alert alert-danger">No result found!!!</div>

      </table>
      @endif