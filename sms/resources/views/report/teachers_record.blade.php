@extends('admin.app')
@section('content')
@include('admin.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
	<div class="admin-top">
	<div class="flash-message">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
		    @if(Session::has('alert-' . $msg))
		<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
		  @endif
		@endforeach
	</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
	</div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '4'))
  <div class="heading"><h2>Daily Teacher Record Summary</h2></div>
<form class="general-form" id="category_valid">
  <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
  <div class="row">
  <div class="form-group col-md-3 col-sm-6">
    <label for="category">Teacher</label>
    <div class="">
            <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="teacher_id" name="teacher_id" selected="" required>
                    <option value="0">--Please Select--</option>
                            @foreach($teachers_list as $index=>$teacher)
                            <option value="{{$index}}">{{$teacher}}</option>

                            @endforeach
                        </select>
    </div>
  </div>
  <div class="form-group col-md-3 col-sm-6">
    <label for="category">Class</label>
    <div class="">
            <select data-type="integer" type="text" class="form-control input-sm searchKeys" id="class_id" name="class_id" selected="" required>
                    <option value="0">--Please Select--</option>
                            @foreach($class_list as $index=>$class)
                            <option value="{{$index}}">{{$class}}</option>

                            @endforeach
                        </select>
    </div>
  </div>

  <div class="text-center">
  <button type="button" class="btn btn-default btn-sm" id="search">
    Search<i class="fa fa-paper-plane-o"></i>
  </button>
</div>
</div>
</form>
  <div class="col-md-12">

    <div class="row" id="replaceTable">

    </div>

    @else
    <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
  </div>
</div>

<script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="{{ URL::to('/') }}/js/datepicker.js"></script> 
<script type="text/javascript">
    document.addEventListener('keypress', function(event) {
                 if (event.keyCode == 13) {
                     event.preventDefault();
                     $("#search").click();
                 }
             });
        $('#search').click(function () {
        $('#loader').show();

    var searchParams = {};
    var base_url = '<?php echo url();?>/home/report/getTeachers';
    searchParams['teacher_id']=$('#teacher_id').val();
    searchParams['class_id']=$('#class_id').val();
    var token = $('.token').val();
    $.ajax({
        type: 'get',
        data: 'parameters= ' + JSON.stringify(searchParams) + '&_token=' + token,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
                    $('#loader').hide('slow');

        }
    });
    return false;
});
</script>   
@endsection
