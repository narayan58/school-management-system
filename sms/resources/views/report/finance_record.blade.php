@extends('admin.app')
@section('content')
@include('admin.sidebar')
<div class="loader " id="loader" style="display: none"><img class="loading-image " src="{{ URL::to('/') }}/images/ajax-loader.gif" alt="Loading..." /></div>
<div class="admin-content">
  <div class="admin-top">
  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
    @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
  </div>
  @foreach($user_levels as $userlevel)
  @if(($userlevel->getUserLevel->user_level == '1')||($userlevel->getUserLevel->user_level == '6'))
  <div class="heading"><h2>Finance Record</h2></div>

<div class="form-holder">
  <form class="general-form" id="exam_valid">
    <input type="hidden" name="_token" class="token" value="{{ csrf_token() }}">
    <div class="row">
        {{--<div class="col-md-2">--}}
            {{--<div class="form-group">--}}
                {{--<div class="required">--}}
                    {{--<select class="form-control input-sm class-name searchKeys" id="month_id" name="month_id">--}}
                            {{--<option value="">Select Month</option>--}}
                        {{--@foreach($month_list as $key=>$value)--}}
                        {{--<option value="{{$key}}">{{$value}}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

       <div class="col-md-3">
        <div class="form-group">
            <div class="required">
            <div class="input-group date" data-provide="datepicker" data-date-end-date="0d">
                <input data-type='to_date' type="text" id="from_date" name="from_date" class="form-control input-sm class-name  datepicker searchKeys " value="<?= (isset($from_date))?$from_date:''?>"  placeholder="Date">
                <div class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </div>
            </div>
            </div>
            </div>
        </div>
        <div class="col-md-3">
        <div class="form-group">
            <div class="required">
            <div class="input-group date" data-provide="datepicker">
                <input data-type='to_date' type="text" id="to_date" name="to_date" class="form-control input-sm class-name  datepicker searchKeys " value="<?= (isset($from_date))?$from_date:''?>"  placeholder="Date">
                <div class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </div>
            </div>
            </div>
            </div>
        </div>



    <div class="text-center">
    <button type="button" class="btn btn-default btn-sm" id="search">
      Search<i class="fa fa-paper-plane-o"></i>
    </button>
  </div>
  </div>
  </form>
</div>
<div class="col-md-12">
<div class="row" id="replaceTable">


</div>
    @else
  <div class="heading"><h2>You have no right access</h2></div>
    @endif
    @endforeach
</div>
    <script src="{{ URL::to('/') }}/js/jquery-2.1.3.min.js" type="text/javascript"></script>
    <script src="{{ URL::to('/') }}/js/datepicker.js"></script>
<script type="text/javascript">
    document.addEventListener('keypress', function(event) {
                 if (event.keyCode == 13) {
                     event.preventDefault();
                     $("#search").click();
                 }
             });
        $('#search').click(function () {
        $('#loader').show();

    var searchParams = {};
    var base_url = '<?php echo url();?>/home/report/getFinance';
//    searchParams['month_id']=$('#month_id').val();
    searchParams['from_date']=$('#from_date').val();
    searchParams['to_date']=$('#to_date').val();

    var token = $('.token').val();
    $.ajax({
        type: 'get',
        data: 'parameters= ' + JSON.stringify(searchParams) + '&_token=' + token,
        url: base_url,
        success: function (data) {
            $('#replaceTable').html(data);
                    $('#loader').hide('slow');
        }
    });
    return false;
});
</script>


@endsection
