$( document ).ready(function() {
	$("#class_valid").validate({
    rules:{
        name:{
            required:true,
            minlength:2
        }
    },
    messages:{
        name:{
            required:"Please enter a class name",
            minlength:"Your class name must consists of at least 2 characters"
        }
    }
 });
$("#section_valid").validate({
    rules: {
    	name: {
                required: true
                }
            },
    messages: {
        name: {
            required: "Please enter a section name"
        	}
        }
    });
$("#shift_valid").validate({
        rules:{
            name:{
                required:true,
                minlength:2
            }
        },
        messages:{
            name:{
                required:"Please enter a shift name",
                minlength:"Your topic name must consists of at least 2 characters"
            }
        }
    });
$("#topic_valid").validate({
        rules:{
            name:{
                required:true,
                minlength:2
            },
            topic_hour:{
               required:true,
               digits: true
            }
        },
        messages:{
            name:{
                required:"Please enter a topic name",
                minlength:"Your topic name must consists of at least 2 characters"
            },
            topic_hour:{
                required:"Please enter a topic hours",
            }
        }
    });
$("#period_valid").validate({
        rules:{
            name:{
                required:true,
                minlength:2
            }
        },
        messages:{
            name:{
                required:"Please enter a period name",
                minlength:"Your topic name must consists of at least 2 characters"
            }
        }
    });
$("#subtopic_valid").validate({
        rules:{
            name:{
                required:true,
                minlength:2
            },
            subtopic_hour:{
               required:true,
               digits: true
            }
        },
        messages:{
            name:{
                required:"Please enter a sub topic name",
                minlength:"Your topic name must consists of at least 2 characters"
            },
            subtopic_hour:{
                required:"Please enter a sub topic hours",
            }
        }
    });
$("#subject_valid").validate({
        rules:{
            name:{
                required:true,
                minlength:2
            },
            subject_hour:{
               required:true,
               digits: true
            }
        },
        messages:{
            name:{
                required:"Please enter a subject name",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            subject_hour:{
                required:"Please enter a subject hours",
            }
        }
    });
$("#student_valid").validate({
        rules:{
            name:{
                required:true,
                minlength:2
            },
            address:{
               required:true,
               minlength:2
            },
            roll_no:{
               required:true,
               minlength:2
            },
            date_of_birth:{
               required:true,
               minlength:2
            },
            gender:{
               required:true
            },
            telephone_no:{
               required:true,
               minlength:2
            },
            mobile_no:{
               required:true,
               minlength:2
            },
            image:{
               required:true
            },
            class_id:{
               required:true,
               digits: true
            },
            section_id:{
               required:true,
               digits: true
            },
            shift_id:{
               required:true,
               digits: true
            }
        },
        messages:{
            name:{
                required:"Please enter a full name",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            address:{
                required:"Please enter address",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            roll_no:{
                required:"Please enter roll number",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            date_of_birth:{
                required:"Please enter date of birth",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            telephone_no:{
                required:"Please enter telephone number",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            mobile_no:{
                required:"Please enter mobile number",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            image:{
                required:"Please select a image"
            },
            class_id:{
                required:"Please select a class"
            },
            section_id:{
                required:"Please select a section"
            },
            shift_id:{
                required:"Please select shift"
            }
        }
    });
$("#teacher_valid").validate({
        rules:{
            name:{
                required:true,
                minlength:2
            },
            address:{
               required:true,
               minlength:2
            },
            email:{
               required:true,
               minlength:2
            },
            date_of_birth:{
               required:true,
               minlength:2
            },
            gender:{
               required:true
            },
            telephone_no:{
               required:true,
               minlength:2
            },
            mobile_no:{
               required:true,
               minlength:2
            },
            image:{
               required:true
            }
            // ,
            // class_id:{
            //    required:true
            // },
            // section_id:{
            //    required:true
            // },
            // subject_id:{
            //    required:true
            // },
            // shift_id:{
            //    required:true
            // }
        },
        messages:{
            name:{
                required:"Please enter a full name",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            address:{
                required:"Please enter address",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            email:{
                required:"Please enter email",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            date_of_birth:{
                required:"Please enter date of birth",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            telephone_no:{
                required:"Please enter telephone number",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            mobile_no:{
                required:"Please enter mobile number",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            image:{
                required:"Please select a image"
            }
            // ,
            // class_id:{
            //     required:"Please choose at least a subject"
            // },
            // section_id:{
            //     required:"Please choose at least a section"
            // },
            // shift_id:{
            //     required:"Please choose at least a shift"
            // },
            // subject_id:{
            //     required:"Please choose at leaset a subject"
            // }
        }
    });
$(".exam_class_valid").validate({
        rules:{
            fullmarks:{
                required:true,
                minlength:2
            },
            passmarks:{
               required:true,
            }
        },
        messages:{
            fullmarks:{
                required:"Please enter a subject name",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            passmarks:{
                required:"Please enter a subject hours",
            }
        }
    });
$("#studentsub").validate({
        rules:{
            marks_obt:{
                required:true,
                minlength:2
            }
        },
        messages:{
            marks_obt:{
                required:"Please enter a subject name",
                minlength:"Your subject name must consists of at least 2 characters"
            }
        }
    });
$(".add-event").validate({
        rules:{
            title:{
                required:true
            },
            start:{
                required:true
            }
        },
        messages:{
            title:{
                required:"Please enter a title name"
            },
            start:{
                required:"Please enter a start date"
            }
        }
    });
$("#member_form").validate({
        rules:{
            name:{
                required:true,
                minlength:2
            },
            address:{
               required:true,
               minlength:2
            },
            email:{
               required:true,
               email: true,
               minlength:2
            },
            password:{
               required:true,
               minlength:6
            },
            confirm_password: {
                required: true,
                minlength: 6,
                equalTo: "#password"
                },
            date_of_birth:{
               required:true,
               minlength:2
            },
            gender:{
               required:true
            },
            telephone_no:{
               required:true,
               minlength:2
            },
            mobile_no:{
               required:true,
               minlength:2
            },
            image:{
               required:true
            },
            user_role:{
                required:true
            }
        },
        messages:{
            name:{
                required:"Please enter a full name",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            address:{
                required:"Please enter address",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            email:{
                required:"Please enter email",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            password:{
                required:"Please enter password",
                minlength:"Your password must consists of at least 6 characters"
            },
            date_of_birth:{
                required:"Please enter date of birth",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            telephone_no:{
                required:"Please enter telephone number",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            mobile_no:{
                required:"Please enter mobile number",
                minlength:"Your subject name must consists of at least 2 characters"
            },
            image:{
                required:"Please choose a image"
            },
            user_role:{
                required:"Please select a role"
            }
        }
    });
});