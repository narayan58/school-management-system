jQuery(document).ready(function($) {
    $(window).load(function(){
        $('#preloader').fadeOut('slow',function(){$(this).remove();});
    });
    $('.sidemenu-open').on('click', function(){

        $('#AdminMenu').toggleClass('active-menu');
    })
    var $root = $('html, body');
    $('.page-scroll').click(function(){
        var href = $.attr(this, 'href');
        $root.animate({
            scrollTop: $(href).offset().top
        }, 500, function () {
            window.location.hash = href;
        });
        return false;
    });
    $(window).scroll(function(){
        if ($(this).scrollTop() > 200) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });
    $('.dropdown-menu .login-form').click(function(e) {
        e.stopPropagation();
    });

    $('.list-group-item').click(function () {
        localStorage.setItem('collapseItemParentId', $(this).closest('div[class^="collapse"]').attr('id'));
        localStorage.setItem('href', $(this).attr('href'));
    });
    var collapseItem = localStorage.getItem('collapseItemParentId');
    var collapseLink = localStorage.getItem('href');
    if (collapseItem) {
        var idselect = '#' + collapseItem;
        $(idselect).collapse('show');
        $('a[href="' + collapseLink + '"]').addClass('active');
    }

	// js for delete
	// $('.delete').click(function(event){
        $(document).on('click', '.delete', function(){
		var id = $(event.target).attr("id");
		t=$(event.target).parent().attr("id"),
		token=$(".token").val();

		alertify.confirm("Are you sure you want to delete?",function(){
			url= t+"/delete",
			$.ajax({
				type:"POST",
				dataType:"JSON",
				url:url,
				data:{
				_token:token,
					id : id
				},
				success:function(e){
					alertify.alert(e.msg,function(){
						location.reload()
					})
				},
				error: function (e) {
					alertify.alert('Sorry! this data is used some where');
				}
			});
		});
	});

    // js for edit
    $('.edit').click(function(event){
		var id=$(event.target).attr("id"),
		t=$(event.target).parent().attr("id");

		url=t+"/edit/"+id,
		$.ajax({
			type:"GET",
			dataType:"JSON",
			url:url,
			success:function(response){
				$.each( response, function( key, value ) {
					$('.edit-form .'+key).val(value);
				});
			}
		});
	});


});
function PrintDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
}
// $(document).on('click', '#export', function(){
    
//    var url=$(this).attr("url");;
//    $(location).attr('href',url);
// });
$(document).on('click', '#export', function(){
    var searchParams = {};
   var url=$(this).attr("url");
   $(".searchKeys").each(function () {

       searchParams[$(this).attr('name')] = $(this).val();
   });
    var token = $('.token').val();
    var base_url = url+'?parameters='+ JSON.stringify(searchParams) + '&_token=' + token;
   window.location.href = base_url;
    
});