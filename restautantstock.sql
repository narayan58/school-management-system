-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2017 at 01:35 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `restautantstock`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `categories_created_by_foreign` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `slug`, `is_active`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Kitchen', 'kitchen', 1, 1, NULL, '2016-12-24 06:45:00', '0000-00-00 00:00:00'),
(2, 'Cold Drinks', 'cold-drinks', 1, 1, NULL, '2016-12-24 06:45:00', '0000-00-00 00:00:00'),
(3, 'Hard Drinks', 'hard-drinks', 1, 1, NULL, '2016-12-24 06:45:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `category_items`
--

CREATE TABLE IF NOT EXISTS `category_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `category_items_category_id_foreign` (`category_id`),
  KEY `category_items_created_by_foreign` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `category_items`
--

INSERT INTO `category_items` (`id`, `category_id`, `item_name`, `slug`, `unit`, `is_active`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'Veg Steam Mo:Mo', 'veg-steam-mo:mo', 'Plate', 1, 1, NULL, '2016-12-29 00:06:18', '0000-00-00 00:00:00'),
(2, 1, 'Veg Fry Mo:Mo', 'veg-fry-mo:mo', 'Plate', 1, 1, NULL, '2016-12-29 00:06:40', '0000-00-00 00:00:00'),
(3, 1, 'Buff Steam Mo:Mo', 'buff-steam-mo:mo', 'Plate', 1, 1, NULL, '2016-12-29 00:07:01', '0000-00-00 00:00:00'),
(4, 1, 'Buff Fry Mo:Mo', 'buff-fry-mo:mo', 'Plate', 1, 1, NULL, '2016-12-29 00:07:31', '0000-00-00 00:00:00'),
(5, 2, 'Slice', 'slice', 'Piece', 1, 1, NULL, '2016-12-29 00:10:28', '0000-00-00 00:00:00'),
(6, 3, 'Vodka', 'vodka', 'Ml', 1, 1, NULL, '2016-12-29 00:12:06', '0000-00-00 00:00:00'),
(7, 1, 'test1', 'test1', 'Plate', 1, 1, NULL, '2016-12-29 04:05:48', '0000-00-00 00:00:00'),
(8, 1, 'Chaumin', 'chaumin', 'Plate', 1, 1, NULL, '2016-12-30 23:05:11', '0000-00-00 00:00:00'),
(9, 3, 'Rum Khukuri', 'rum-khukuri', 'Piece', 1, 1, NULL, '2016-12-31 10:06:52', '0000-00-00 00:00:00'),
(10, 1, 'Gundruk', 'gundruk', 'Plate', 1, 1, NULL, '2017-01-01 19:43:39', '0000-00-00 00:00:00'),
(11, 1, 'NepaliKahna', 'nepalikahna', 'Plate', 1, 1, NULL, '2017-01-02 20:41:27', '0000-00-00 00:00:00'),
(12, 1, 'Test items', 'test-item', 'Plate', 1, 1, 1, '2017-01-05 08:53:41', '2017-01-08 03:19:21'),
(13, 1, 'Thakali khana', 'thakali-khana', 'Plate', 1, 1, 1, '2017-01-05 09:33:24', '2017-01-08 03:43:04');

-- --------------------------------------------------------

--
-- Table structure for table `item_adds`
--

CREATE TABLE IF NOT EXISTS `item_adds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_detail_id` int(10) unsigned NOT NULL,
  `item_quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `item_adds_item_detail_id_foreign` (`item_detail_id`),
  KEY `item_adds_created_by_foreign` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `item_adds`
--

INSERT INTO `item_adds` (`id`, `item_detail_id`, `item_quantity`, `is_active`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 13, '100', 1, 1, NULL, '2017-01-05 08:54:09', '2017-01-05 08:54:10'),
(2, 13, '50', 1, 1, NULL, '2017-01-05 08:54:28', '2017-01-05 08:54:28'),
(3, 14, '100', 1, 1, NULL, '2017-01-05 09:33:54', '2017-01-05 09:33:54'),
(4, 14, '50', 1, 1, NULL, '2017-01-05 09:34:35', '2017-01-05 09:34:35'),
(5, 15, '100', 1, 1, NULL, '2017-01-05 09:35:09', '2017-01-05 09:35:09'),
(6, 16, '300', 1, 1, NULL, '2017-01-10 04:05:34', '2017-01-10 04:05:34'),
(7, 16, '2', 1, 1, NULL, '2017-01-10 13:35:25', '2017-01-10 13:35:25'),
(8, 14, '20', 1, 1, NULL, '2017-01-10 14:01:59', '2017-01-10 14:01:59');

-- --------------------------------------------------------

--
-- Table structure for table `item_details`
--

CREATE TABLE IF NOT EXISTS `item_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_item_id` int(10) unsigned NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `item_rate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `item_details_category_item_id_foreign` (`category_item_id`),
  KEY `item_details_created_by_foreign` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `item_details`
--

INSERT INTO `item_details` (`id`, `category_item_id`, `item_quantity`, `item_rate`, `is_active`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 4, 100, '65', 1, 1, 0, '2016-12-29 00:08:37', '0000-00-00 00:00:00'),
(2, 3, 100, '70', 1, 1, 0, '2016-12-29 00:09:04', '0000-00-00 00:00:00'),
(3, 2, 130, '65', 1, 1, 1, '2016-12-29 07:05:51', '2016-12-29 07:05:51'),
(4, 1, 120, '75', 1, 1, 1, '2017-01-03 03:18:11', '2017-01-03 03:18:11'),
(5, 5, 100, '45', 1, 1, 0, '2016-12-29 00:10:43', '0000-00-00 00:00:00'),
(6, 6, 1000, '0.5', 1, 1, 0, '2016-12-29 00:12:44', '0000-00-00 00:00:00'),
(7, 7, 33, '333', 1, 1, 0, '2016-12-29 04:06:00', '0000-00-00 00:00:00'),
(8, 1, 100, '70', 1, 1, 0, '2016-12-29 09:32:39', '0000-00-00 00:00:00'),
(10, 9, 30, '50', 1, 1, 0, '2016-12-31 10:07:21', '0000-00-00 00:00:00'),
(12, 11, 150, '200', 1, 1, 1, '2017-01-03 03:15:37', '2017-01-03 03:15:37'),
(13, 12, 150, '50', 1, 1, 1, '2017-01-05 08:54:09', '2017-01-05 08:54:28'),
(14, 13, 35, '5', 1, 1, 1, '2017-01-05 09:33:54', '2017-01-10 14:01:59'),
(15, 13, 120, '60', 1, 1, 1, '2017-01-05 09:35:09', '2017-01-10 04:29:00'),
(16, 13, 303, '83', 1, 1, 1, '2017-01-10 04:05:34', '2017-01-10 13:48:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_10_22_095632_create_categories_table', 1),
('2016_10_22_101333_create_category_items_table', 1),
('2016_10_23_074727_create_orders_table', 1),
('2016_12_11_034849_create_item_details_table', 1),
('2017_01_03_060442_create_item_adds_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_confirmed` int(11) NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `orders_created_by_foreign` (`created_by`),
  KEY `orders_item_id_foreign` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `bill_id`, `item_id`, `quantity`, `rate`, `total`, `is_confirmed`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2', '700', '140', 1, 1, 1, '2017-01-03 10:25:13', '2017-01-15 08:50:39'),
(2, 1, 3, '4', '50', '200', 1, 1, 1, '2017-01-03 10:25:41', '2017-01-15 09:16:39'),
(3, 1, 3, '4', '65', '260', 1, 1, NULL, '2017-01-03 10:26:08', '0000-00-00 00:00:00'),
(4, 1, 2, '5', '70', '350', 1, 1, NULL, '2017-01-03 10:26:24', '0000-00-00 00:00:00'),
(5, 1, 5, '80', '40', '3200', 1, 1, 1, '2017-01-03 10:26:45', '2017-01-15 09:18:47'),
(6, 1, 8, '8', '70', '560', 1, 1, NULL, '2017-01-03 10:27:13', '0000-00-00 00:00:00'),
(7, 1, 7, '1', '700', '70', 1, 1, 1, '2017-01-03 10:27:46', '2017-01-15 08:09:30'),
(8, 1, 1, '17', '70', '1270', 1, 2, 1, '2017-01-04 02:06:35', '2017-01-15 08:12:52'),
(9, 1, 10, '12', '12', '144', 1, 2, 1, '2017-01-04 02:14:24', '2017-01-15 08:52:37'),
(11, 1, 12, '50', '50', '2500', 1, 1, NULL, '2017-01-05 09:03:48', '0000-00-00 00:00:00'),
(12, 1, 13, '4', '50', '200', 1, 1, NULL, '2017-01-05 09:12:26', '0000-00-00 00:00:00'),
(13, 1, 14, '100', '40', '4000', 1, 1, NULL, '2017-01-05 09:38:36', '0000-00-00 00:00:00'),
(14, 1, 14, '10', '50', '500', 1, 2, NULL, '2017-01-05 09:43:38', '0000-00-00 00:00:00'),
(15, 1, 15, '50', '60', '3000', 1, 2, NULL, '2017-01-05 09:43:38', '0000-00-00 00:00:00'),
(16, 1, 14, '2', '20', '40', 1, 1, NULL, '2017-01-05 09:44:34', '0000-00-00 00:00:00'),
(17, 1, 3, '12', '12', '12', 1, 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 1, 3, '1', '65', '65', 1, 1, NULL, '2017-01-29 09:05:03', '0000-00-00 00:00:00'),
(19, 1, 4, '1', '75', '75', 1, 1, NULL, '2017-01-29 09:06:34', '0000-00-00 00:00:00'),
(20, 1485680853, 4, '1', '75', '75', 1, 1, NULL, '2017-01-29 09:10:51', '0000-00-00 00:00:00'),
(21, 1485681051, 3, '1', '65', '65', 1, 1, NULL, '2017-01-29 09:11:24', '0000-00-00 00:00:00'),
(22, 1485681051, 2, '5', '70', '350', 1, 1, NULL, '2017-01-29 09:11:24', '0000-00-00 00:00:00'),
(23, 314, 4, '1', '75', '75', 1, 1, NULL, '2017-02-05 08:50:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `sold_percent`
--
CREATE TABLE IF NOT EXISTS `sold_percent` (
`item_id` int(10) unsigned
,`item_name` varchar(255)
,`item_rate` varchar(255)
,`unit` varchar(255)
,`total_bought` int(11)
,`total_sold` double
,`stock` double
,`sold_per` double(19,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `total_bought`
--
CREATE TABLE IF NOT EXISTS `total_bought` (
`si_id` int(10) unsigned
,`item_rate` varchar(255)
,`category_item_id` int(10) unsigned
,`bought` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `total_sold`
--
CREATE TABLE IF NOT EXISTS `total_sold` (
`item_id` int(10) unsigned
,`sold` double
);
-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dob` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_encrypt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` int(11) NOT NULL,
  `last_login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login_pre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_admin` int(11) NOT NULL DEFAULT '0',
  `is_manager` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `address`, `email`, `password`, `gender`, `dob`, `telephone_no`, `mobile_no`, `image`, `image_encrypt`, `confirmation_code`, `confirmed`, `last_login`, `last_login_pre`, `is_admin`, `is_manager`, `is_active`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Biratnagar', 'admin@gmail.com', '$2a$08$etifmr7CJoUn2XIdbmKa6OdrODkxHrOgnbn17XlWjFYLu0Qm1xcO2', 'M', '2040/04/29', '021-461571', '9851196943', 'kapeel.jpg', 'kapeel.jpg', '', 1, '02/16/2017 18:19:37', '02/16/2017 18:18:58', 1, 0, 1, 'AFNPbkl7p3gWgWsPRwZZleu4eVQZcfqdgQQ1W6LXGOoju1NnQ6YRuN50rQ5O', 0, NULL, '2017-02-09 18:15:00', '2017-02-16 12:34:37'),
(2, 'Manager1', 'Lele', 'manager1@gmail.com', '$2a$08$etifmr7CJoUn2XIdbmKa6OdrODkxHrOgnbn17XlWjFYLu0Qm1xcO2', 'M', '2045/04/29', '9849220335', '9849220335', 'banner.jpg', '6a6894afd9c151b92dbfcfae867ac8fb.jpg', '3x59Pxjk2NXcmLSqiincz5gzhkgYnR', 1, '01/05/2017 15:28:03', '01/05/2017 14:35:14', 0, 1, 1, 'P8DIDfNHdrtiBQYekLgegD6jvJh86s49bjut93AOIhXqdOi5jEqpuYgPndDe', 1, NULL, '2016-10-21 09:12:12', '2017-01-05 03:59:10');

-- --------------------------------------------------------

--
-- Structure for view `sold_percent`
--
DROP TABLE IF EXISTS `sold_percent`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sold_percent` AS select `category_items`.`id` AS `item_id`,`category_items`.`item_name` AS `item_name`,`total_bought`.`item_rate` AS `item_rate`,`category_items`.`unit` AS `unit`,`total_bought`.`bought` AS `total_bought`,`total_sold`.`sold` AS `total_sold`,(`total_bought`.`bought` - `total_sold`.`sold`) AS `stock`,round(((`total_sold`.`sold` / `total_bought`.`bought`) * 100),2) AS `sold_per` from ((`category_items` left join `total_bought` on((`category_items`.`id` = `total_bought`.`category_item_id`))) left join `total_sold` on((`total_bought`.`si_id` = `total_sold`.`item_id`)));

-- --------------------------------------------------------

--
-- Structure for view `total_bought`
--
DROP TABLE IF EXISTS `total_bought`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `total_bought` AS select `item_details`.`id` AS `si_id`,`item_details`.`item_rate` AS `item_rate`,`item_details`.`category_item_id` AS `category_item_id`,`item_details`.`item_quantity` AS `bought` from `item_details` group by `item_details`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `total_sold`
--
DROP TABLE IF EXISTS `total_sold`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `total_sold` AS select `orders`.`item_id` AS `item_id`,sum(`orders`.`quantity`) AS `sold` from `orders` group by `orders`.`item_id`;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `category_items`
--
ALTER TABLE `category_items`
  ADD CONSTRAINT `category_items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `category_items_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `item_adds`
--
ALTER TABLE `item_adds`
  ADD CONSTRAINT `item_adds_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `item_adds_item_detail_id_foreign` FOREIGN KEY (`item_detail_id`) REFERENCES `item_details` (`id`);

--
-- Constraints for table `item_details`
--
ALTER TABLE `item_details`
  ADD CONSTRAINT `item_details_category_item_id_foreign` FOREIGN KEY (`category_item_id`) REFERENCES `category_items` (`id`),
  ADD CONSTRAINT `item_details_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `orders_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `item_details` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
